package com.atlassian.applinks.cors.rest;

import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.HttpMethod;

/**
 * ServletFilter to allow CORS requests to the specified URLs.
 * Used to allow access to those URLs needed for the auto creation and configuration of AppLinks.
 *
 * TODO potentially remove this once atlassian-oauth allows CORS access by default to the /plugins/servlet/oauth/consumer-info resource see https://ecosystem.atlassian.net/browse/OAUTH-279
 *
 * @since v4.0.0
 */
public class CorsFilter implements Filter {
    private static final String ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
    private static final String ACCESS_CONTROL_ALLOW_CREDENTIALS = "Access-Control-Allow-Credentials";
    private static final String ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
    private static final String ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
    private static final String ORIGIN = "Origin";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String TRUE = String.valueOf(true);

    private final org.slf4j.Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing to do
    }

    /**
     * Adds the headers necessary to support CORS calls to applinks resources.
     * NB. If AppLinks is running in an application that uses the atlassian-whitelist these headers should already be included.
     * Headers Access-Control-Allow-Headers and Access-Control-Allow-Credentials should only be included once therefore
     * it will check if they are present before setting them.
     * Other headers can be included multiple times so lets just include them.
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        Enumeration originHeaders = request.getHeaders(ACCESS_CONTROL_ALLOW_ORIGIN);
        Enumeration credentialsHeaders = request.getHeaders(ACCESS_CONTROL_ALLOW_CREDENTIALS);

        // like Highlander there can be only one!
        // http://www.w3.org/TR/cors/#resource-sharing-check-0
        if (!originHeaders.hasMoreElements()) {
            String origin = request.getHeader(ORIGIN);
            response.setHeader(ACCESS_CONTROL_ALLOW_ORIGIN, origin);
            logger.debug("CORS Header [{}] set to [{}]", ACCESS_CONTROL_ALLOW_CREDENTIALS, origin);
        } else {
            if (logger.isDebugEnabled()) {
                while (originHeaders.hasMoreElements()) {
                    logger.debug("CORS Header [{}] NOT   set. It is already set as [{}]", ACCESS_CONTROL_ALLOW_ORIGIN, originHeaders.nextElement());
                }
            }
        }

        // like Highlander there can be only one!
        // http://www.w3.org/TR/cors/#resource-sharing-check-0
        if (!credentialsHeaders.hasMoreElements()) {
            response.setHeader(ACCESS_CONTROL_ALLOW_CREDENTIALS, TRUE);
            logger.debug("CORS Header [{}] set to [{}]", ACCESS_CONTROL_ALLOW_CREDENTIALS, TRUE);
        } else {
            if (logger.isDebugEnabled()) {
                while (originHeaders.hasMoreElements()) {
                    logger.debug("CORS Header [{}] NOT set. It is already set as [{}]", ACCESS_CONTROL_ALLOW_CREDENTIALS, originHeaders.nextElement());
                }
            }
        }

        // for preflight checks check the content-type will be allowed
        if (request.getMethod().equals(HttpMethod.OPTIONS)) {
            // http://www.w3.org/TR/cors/#resource-preflight-requests

            // These are the anti-highlanders so there can be more than one of each!
            // So just add to make sure we have the headers we want.

            // allow the content type to form part of the header.
            response.addHeader(ACCESS_CONTROL_ALLOW_HEADERS, CONTENT_TYPE);
            logger.debug("CORS Preflight Header [{}] set to [{}]", ACCESS_CONTROL_ALLOW_HEADERS, CONTENT_TYPE);
            // allow the OPTIONS method type
            response.addHeader(ACCESS_CONTROL_ALLOW_METHODS, request.getMethod());
            logger.debug("CORS Preflight Header [{}] set to [{}]", ACCESS_CONTROL_ALLOW_METHODS, request.getMethod());
        }

        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
