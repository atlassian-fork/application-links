<!DOCTYPE html>
#* @vtlvariable name="configured" type="java.lang.Boolean" *#
#* @vtlvariable name="conflicted" type="java.lang.Boolean" *#
#* @vtlvariable name="conflicts" type="java.util.List<com.atlassian.applinks.api.ApplicationLink>" *#
#macro (listConflicts $textKey)
    #if ($conflicted)
    <div class="aui-message aui-message-warning">
        <p class="title">
            <strong>${i18n.getText("auth.cors.config.mismatch")}</strong>
        </p>
        <p>${i18n.getText($textKey)}</p>
        <ul>
            #foreach ($conflict in $conflicts)
            <li>$conflict.name ($conflict.type)</li>
            #end
        </ul>
    </div>
    #end
#end
#parse("/com/atlassian/applinks/core/common/_help_link.vm")
#parse("/com/atlassian/applinks/core/common/_configured_status.vm")
#parse("/com/atlassian/applinks/core/common/_xsrf_token_element.vm")
<html>
<head>
    ${webResources.get()}
    ## The pane made visible on page load:
    <meta name="view" content="#if($configured)enabled#{else}disabled#end">
    ## The pane that will be activated by the Cancel button:
    <meta name="cancel" content="#if($configured)enabled#{else}disabled#end">
</head>
<body class="auth-config aui-layout aui-theme-default">
    ## The pane that is rendered when auth is disabled.
    <div class="auth-cors-view">
        <form id="auth-cors-access" class="aui" method="POST">
            #if ($configured)
                #listConflicts('auth.cors.config.blocked')
                <div class="aui-message aui-message-info">
                    <p>
                        ${i18n.getText("auth.cors.config.enabled", $remoteApplicationName, $remoteApplicationType)}
                    </p>
                </div>
                #status($configured)
                <div class="buttons-container">
                    <div class="buttons">
                        <input type="submit" id="auth-cors-disable" value="${i18n.getText("auth.cors.config.button.disable")}" class="button" />
                    </div>
                </div>
                <input type="hidden" name="method" value="DELETE" />## forms don't support DELETE
            #else
                #listConflicts('auth.cors.config.blocking')
                <div class="aui-message aui-message-info">
                    <p>
                        ${i18n.getText("auth.cors.config.disabled", $remoteApplicationName, $remoteApplicationType)}
                    </p>
                </div>
                #status($configured)
                <div class="buttons-container">
                    <div class="buttons">
                        <input type="submit" id="auth-cors-enable" value="${i18n.getText("auth.cors.config.button.enable")}" class="button" />
                    </div>
                </div>
                <input type="hidden" name="method" value="PUT">## PUT doesn't support www-form-encoded content
            #end
            #xsrfTokenElement()
        </form>
    </div>
</body>
</html>
