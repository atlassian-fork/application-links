package com.atlassian.applinks.core.v1.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.rest.AbstractResource;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.ApplicationLinkListEntity;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.link.ReciprocalActionException;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoNotRequired;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.sun.jersey.spi.resource.Singleton;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.Authorization;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.core.rest.util.RestUtil.badRequest;
import static com.atlassian.applinks.core.rest.util.RestUtil.created;
import static com.atlassian.applinks.core.rest.util.RestUtil.credentialsRequired;
import static com.atlassian.applinks.core.rest.util.RestUtil.forbidden;
import static com.atlassian.applinks.core.rest.util.RestUtil.notFound;
import static com.atlassian.applinks.core.rest.util.RestUtil.ok;
import static com.atlassian.applinks.core.rest.util.RestUtil.serverError;
import static com.atlassian.applinks.core.rest.util.RestUtil.typeNotInstalled;
import static com.atlassian.applinks.core.rest.util.RestUtil.updated;

@Api
@Path(ApplicationLinkResource.CONTEXT)
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@WebSudoRequired
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplicationLinkResource extends AbstractResource {
    public static final String CONTEXT = "applicationlink";

    protected final MutatingApplicationLinkService applicationLinkService;
    protected final ManifestRetriever manifestRetriever;
    protected final I18nResolver i18nResolver;
    protected final UserManager userManager;

    public ApplicationLinkResource(final MutatingApplicationLinkService applicationLinkService,
                                   final I18nResolver i18nResolver,
                                   final InternalTypeAccessor typeAccessor,
                                   final ManifestRetriever manifestRetriever,
                                   final RestUrlBuilder restUrlBuilder,
                                   final RequestFactory requestFactory,
                                   final UserManager userManager) {
        super(restUrlBuilder, typeAccessor, requestFactory, applicationLinkService);
        this.i18nResolver = i18nResolver;
        this.applicationLinkService = applicationLinkService;
        this.manifestRetriever = manifestRetriever;
        this.userManager = userManager;
    }

    @GET
    @ApiResponse(code = 200, message = "Successful")
    @ApiOperation(value = "Returns a list of all Application Links on this server",
            response = ApplicationLinkListEntity.class,
            authorizations = @Authorization("Admin"),
            responseContainer = "Object")
    public Response getApplicationLinks() {
        final List<ApplicationLinkEntity> applicationLinks = new ArrayList<ApplicationLinkEntity>();
        for (final ApplicationLink application : applicationLinkService.getApplicationLinks()) {
            applicationLinks.add(toApplicationLinkEntity(application));
        }
        return ok(new ApplicationLinkListEntity(applicationLinks));
    }

    @GET
    @Path("type/{type}")
    public Response getApplicationLinks(@PathParam("type") final TypeId typeId) {
        final ApplicationType type = typeAccessor.loadApplicationType(typeId);
        if (type == null) {
            return typeNotInstalled(typeId);
        }
        final List<ApplicationLinkEntity> applicationLinks = new ArrayList<ApplicationLinkEntity>();
        for (final ApplicationLink application : applicationLinkService.getApplicationLinks(type.getClass())) {
            applicationLinks.add(toApplicationLinkEntity(application));
        }
        return ok(new ApplicationLinkListEntity(applicationLinks));
    }

    @GET
    @Path("{id}")
    public Response getApplicationLink(@PathParam("id") final String id) throws TypeNotInstalledException {
        final ApplicationLink application = applicationLinkService.getApplicationLink(new ApplicationId(id));
        return ok(toApplicationLinkEntity(application));
    }

    @GET
    @Path("primary/{type}")
    public Response getPrimaryApplicationLink(@PathParam("type") final TypeId typeId) {
        final ApplicationType type = typeAccessor.loadApplicationType(typeId);
        if (type == null) {
            return typeNotInstalled(typeId);
        }
        final ApplicationLink application = applicationLinkService.getPrimaryApplicationLink(type.getClass());
        if (application == null) {
            return notFound(i18nResolver.getText("applinks.error.noprimary", type.getClass()));
        }
        return ok(toApplicationLinkEntity(application));
    }

    /**
     * Creates or updates the application link but not for updating RPC URL of the link.
     *
     * @param id              the Application ID.
     * @param applicationLink The new details to set.
     * @return 'badRequest' if the target application is not reachable or if the name already exists; 'forbidden' if
     * the user performing the operation does not have sysadmin privilege; 'created' or 'updated' in case of success.
     * @throws TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     */
    @PUT
    @Path("{id}")
    public Response updateApplicationLink(@PathParam("id") final String id, final ApplicationLinkEntity applicationLink)
            throws TypeNotInstalledException {
        try {
            // We get the manifest, just to check the application is up and running
            final ApplicationType applicationType = typeAccessor.loadApplicationType(applicationLink.getTypeId());
            if (applicationType == null) {
                LOG.warn("Couldn't load type {} for application link id {}, name {}, rpc.url {}. Type is not installed?",
                        applicationLink.getTypeId(), applicationLink.getId(), applicationLink.getName(), applicationLink.getRpcUrl());
                throw new TypeNotInstalledException(applicationLink.getTypeId().get(), applicationLink.getName(), applicationLink.getRpcUrl());
            }
            manifestRetriever.getManifest(applicationLink.getRpcUrl(), applicationType);

            // Fetch the original & mutable ApplicationLink record.
            ApplicationId applicationId = new ApplicationId(id);
            final MutableApplicationLink existing = applicationLinkService.getApplicationLink(applicationId);

            // If it doesn't exist, we create the link
            if (existing == null) {
                final ApplicationType type = typeAccessor.loadApplicationType(applicationLink.getTypeId());
                applicationLinkService.addApplicationLink(applicationLink.getId(), type, applicationLink.getDetails());
                return created(createSelfLinkFor(applicationLink.getId()));
            } else {
                // only a sysadmin can update a system link.
                if (existing.isSystem() && !userManager.isSystemAdmin(userManager.getRemoteUsername())) {
                    return forbidden(i18nResolver.getText("applinks.error.only.sysadmin.operation"));
                }

                ApplicationLinkDetails linkDetails = applicationLink.getDetails();
                // We need to check the name doesn't already exist.
                if (applicationLinkService.isNameInUse(linkDetails.getName(), applicationId)) {
                    return badRequest(i18nResolver.getText("applinks.error.duplicate.name", applicationLink.getName()));
                }

                // the relocation functionality should be used to change rpcurl instead of this sneaky way.
                if (!existing.getRpcUrl().equals(linkDetails.getRpcUrl())) {
                    return badRequest(i18nResolver.getText("applinks.error.cannot.update.rpcurl"));
                }

                existing.update(linkDetails);
                return updated(createSelfLinkFor(applicationLink.getId()));
            }
        } catch (ManifestNotFoundException e) {
            return badRequest(i18nResolver.getText("applinks.error.url.application.not.reachable", applicationLink.getRpcUrl().toString()));
        }
    }

    @DELETE
    @WebSudoNotRequired
    @Path("{id}")
    public Response deleteApplicationLink(@PathParam("id") final String idString,
                                          @QueryParam("reciprocate") final Boolean reciprocate)
            throws TypeNotInstalledException {
        final ApplicationId id = new ApplicationId(idString);
        final MutableApplicationLink link = applicationLinkService.getApplicationLink(id);
        if (link == null) {
            return notFound(i18nResolver.getText("applinks.notfound", id.get()));
        }

        // only a sysadmin can delete a system link.
        if (link.isSystem() && !userManager.isSystemAdmin(userManager.getRemoteUsername())) {
            return forbidden(i18nResolver.getText("applinks.error.only.sysadmin.operation"));
        }

        if (reciprocate != null && reciprocate) {
            try {
                applicationLinkService.deleteReciprocatedApplicationLink(link);
            } catch (final CredentialsRequiredException e) {
                return credentialsRequired(i18nResolver);
            } catch (final ReciprocalActionException e) {
                return serverError(i18nResolver.getText("applinks.remote.delete.failed", e.getMessage()));
            }
        } else {
            applicationLinkService.deleteApplicationLink(link);
        }
        return ok(i18nResolver.getText("applinks.deleted", id.get()));
    }

    @POST
    @Path("primary/{id}")
    public Response makePrimary(@PathParam("id") final String idString) throws TypeNotInstalledException {
        final ApplicationId id = new ApplicationId(idString);
        applicationLinkService.makePrimary(id);
        return updated(Link.self(applicationLinkService.createSelfLinkFor(id)), i18nResolver.getText("applinks.primary", id.get()));
    }

}
