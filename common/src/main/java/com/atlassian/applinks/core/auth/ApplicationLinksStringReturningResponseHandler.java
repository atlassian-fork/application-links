package com.atlassian.applinks.core.auth;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseStatusException;
import com.atlassian.sal.api.net.ReturningResponseHandler;

/**
 * Response handler for application links requests that deal with strings.
 *
 * @since 3.11.0
 */
public class ApplicationLinksStringReturningResponseHandler implements ReturningResponseHandler<Response, java.lang.String> {
    public String handle(final Response response) throws ResponseException {
        if (!response.isSuccessful()) {
            throw new ResponseStatusException("Unexpected response received. Status code: " + response.getStatusCode(), response);
        }

        return response.getResponseBodyAsString();
    }
}