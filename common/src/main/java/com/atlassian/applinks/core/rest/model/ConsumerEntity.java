package com.atlassian.applinks.core.rest.model;

import com.atlassian.applinks.core.rest.model.adapter.OptionalURIAdapter;
import com.atlassian.plugins.rest.common.Link;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import java.net.URI;
import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

/**
 * Represents the configuration of a Consumer of services provided by the Host via an Application Link.
 *
 * @since 3.12
 */
@XmlRootElement(name = "consumer")
@JsonIgnoreProperties(ignoreUnknown = true)
public class ConsumerEntity extends LinkedEntity {
    @XmlElement(name = "key")
    private String key;
    @XmlElement(name = "name")
    private String name;
    @XmlElement(name = "description")
    private String description;
    @XmlElement(name = "signatureMethod")
    private String signatureMethod;
    @XmlElement(name = "publicKey")
    private String publicKey;
    @XmlElement(name = "sharedSecret")
    private String sharedSecret;
    @XmlJavaTypeAdapter(OptionalURIAdapter.class)
    @XmlElement(name = "callback")
    private URI callback;
    @XmlElement(name = "twoLOAllowed")
    private Boolean twoLOAllowed;
    @XmlElement(name = "executingTwoLOUser")
    private String executingTwoLOUser;
    @XmlElement(name = "twoLOImpersonationAllowed")
    private Boolean twoLOImpersonationAllowed;
    @XmlElement(name = "outgoing")
    private Boolean outgoing;

    public ConsumerEntity() {
    }

    public ConsumerEntity(final Link self,
                          final String key, final String name, final String description,
                          final String signatureMethod, final String publicKey,
                          final URI callback,
                          final boolean twoLOAllowed, final String executingTwoLOUser, final boolean twoLOImpersonationAllowed) {
        this.key = key;
        this.name = name;
        this.description = description;
        this.signatureMethod = signatureMethod;
        this.publicKey = publicKey;
        this.callback = callback;
        this.twoLOAllowed = twoLOAllowed;
        this.executingTwoLOUser = executingTwoLOUser;
        this.twoLOImpersonationAllowed = twoLOImpersonationAllowed;
        this.outgoing = false;

        addLink(self);
    }

    public ConsumerEntity(final Link self,
                          final String key, final String name, final String description,
                          final String signatureMethod, final String publicKey,
                          final URI callback,
                          final boolean twoLOAllowed, final String executingTwoLOUser, final boolean twoLOImpersonationAllowed,
                          final boolean outgoing) {
        this(self,
                key, name, description,
                signatureMethod, publicKey,
                callback,
                twoLOAllowed, executingTwoLOUser, twoLOImpersonationAllowed);
        this.outgoing = outgoing;
    }

    @Nullable
    public String getKey() {
        return key;
    }

    @Nullable
    public String getName() {
        return name;
    }

    @Nullable
    public String getDescription() {
        return description;
    }

    @Nullable
    public String getSignatureMethod() {
        return signatureMethod;
    }

    @Nullable
    public String getPublicKey() {
        return publicKey;
    }

    @Nullable
    public URI getCallback() {
        return callback;
    }

    public boolean isTwoLOAllowed() {
        return Boolean.TRUE.equals(twoLOAllowed);
    }

    @Nullable
    public String getExecutingTwoLOUser() {
        return executingTwoLOUser;
    }

    public boolean isTwoLOImpersonationAllowed() {
        return Boolean.TRUE.equals(twoLOImpersonationAllowed);
    }

    @Nullable
    public String getSharedSecret() {
        return sharedSecret;
    }

    public boolean isOutgoing() {
        return Boolean.TRUE.equals(outgoing);
    }
}
