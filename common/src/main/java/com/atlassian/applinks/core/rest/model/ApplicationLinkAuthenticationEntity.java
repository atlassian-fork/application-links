package com.atlassian.applinks.core.rest.model;

import java.util.List;

import javax.annotation.Nullable;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.atlassian.plugins.rest.common.Link;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;

/**
 * Entity containing the currently configured authentication providers for a specified link
 *
 * @since 3.12
 */
@XmlRootElement(name = "applicationLinkAuthentication")
@JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
@ApiModel
public class ApplicationLinkAuthenticationEntity extends LinkedEntity {
    @XmlElement(name = "configuredAuthProviders")
    private List<AuthenticationProviderEntity> configuredAuthenticationProviders;

    @XmlElement(name = "consumers")
    private List<ConsumerEntity> consumers;

    public ApplicationLinkAuthenticationEntity() {
    }

    public ApplicationLinkAuthenticationEntity(Link self, List<ConsumerEntity> consumers,
                                               List<AuthenticationProviderEntity> configuredAuthenticationProviders) {
        this.configuredAuthenticationProviders = configuredAuthenticationProviders;
        this.consumers = consumers;
        addLink(self);
    }

    @Nullable
    public List<AuthenticationProviderEntity> getConfiguredAuthProviders() {
        return this.configuredAuthenticationProviders;
    }

    @Nullable
    public List<ConsumerEntity> getConsumers() {
        return this.consumers;
    }
}
