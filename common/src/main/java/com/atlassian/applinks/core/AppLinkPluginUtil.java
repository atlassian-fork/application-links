package com.atlassian.applinks.core;

import org.osgi.framework.Version;

import javax.annotation.Nonnull;

public interface AppLinkPluginUtil {
    /**
     * @return the AppLinks plugin key
     */
    @Nonnull
    String getPluginKey();

    /**
     * @return complete module key (wih prepended plugin key), given {@code moduleKey}
     */
    @Nonnull
    String completeModuleKey(@Nonnull String moduleKey);

    /**
     * @return the {@link Version} of the AppLinks plugin
     */
    @Nonnull
    Version getVersion();
}
