package com.atlassian.applinks.core.rest.model;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.rest.model.adapter.ApplicationIdAdapter;
import com.atlassian.applinks.core.rest.model.adapter.OptionalURIAdapter;
import com.atlassian.applinks.core.rest.model.adapter.RequiredBaseURIAdapter;
import com.atlassian.applinks.core.rest.model.adapter.TypeIdAdapter;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.plugins.rest.common.Link;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.net.URI;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import static com.atlassian.applinks.internal.application.IconUriResolver.resolveIconUri;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;
import static org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion.NON_NULL;

@XmlRootElement(name = "applicationLink")
@JsonSerialize
@org.codehaus.jackson.map.annotate.JsonSerialize(include = NON_NULL)
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
@org.codehaus.jackson.annotate.JsonAutoDetect(
        fieldVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.ANY,
        getterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        setterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
)
@JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class ApplicationLinkEntity extends LinkedEntity {
    @XmlJavaTypeAdapter(ApplicationIdAdapter.class)
    private ApplicationId id;
    @XmlJavaTypeAdapter(TypeIdAdapter.class)
    private TypeId typeId;
    private String name;
    @XmlJavaTypeAdapter(RequiredBaseURIAdapter.class)
    private URI displayUrl;
    @Deprecated
    @XmlJavaTypeAdapter(OptionalURIAdapter.class)
    private URI iconUrl;
    //iconUri is the higher resolution replacement for iconUrl, it's resolution should not be assumed to stay the same across versions
    @XmlJavaTypeAdapter(OptionalURIAdapter.class)
    private URI iconUri;
    @XmlJavaTypeAdapter(RequiredBaseURIAdapter.class)
    private URI rpcUrl;
    @XmlElement(name = "isPrimary")
    private Boolean isPrimary;
    @XmlElement(name = "isSystem")
    private Boolean isSystem;

    // used for RESTful table PUTs
    @SuppressWarnings("unused")
    protected ApplicationLinkEntity() {
    }

    /**
     * Copy constructor use for decorating.
     */
    protected ApplicationLinkEntity(ApplicationLinkEntity entity) {
        super(entity.getLinks());
        this.id = entity.id;
        this.typeId = entity.typeId;
        this.name = entity.name;
        this.displayUrl = entity.displayUrl;
        this.iconUrl = entity.iconUrl;
        this.iconUri = entity.iconUri;
        this.isPrimary = entity.isPrimary;
        this.isSystem = entity.isSystem;
        this.rpcUrl = entity.rpcUrl;
    }

    public ApplicationLinkEntity(final ApplicationLink applicationLink, final Link self) {
        this(applicationLink.getId(),
                TypeId.getTypeId(applicationLink.getType()),
                applicationLink.getName(),
                applicationLink.getDisplayUrl(),
                applicationLink.getType().getIconUrl(),
                resolveIconUri(applicationLink.getType()),
                applicationLink.getRpcUrl(),
                applicationLink.isPrimary(),
                applicationLink.isSystem(),
                self);
    }

    public ApplicationLinkEntity(final ApplicationId id,
                                 final TypeId typeId,
                                 final String name,
                                 final URI displayUrl,
                                 final URI iconUrl,
                                 final URI iconUri,
                                 final URI rpcUrl,
                                 final Boolean primary,
                                 final Boolean isSystem,
                                 final Link self) {
        this.id = id;
        this.typeId = typeId;
        this.name = name;
        this.displayUrl = displayUrl;
        this.iconUrl = iconUrl;
        this.iconUri = iconUri;
        this.isPrimary = primary;
        this.isSystem = isSystem;
        this.iconUri = iconUri;

        if (!isSystem()) {
            this.rpcUrl = rpcUrl;
        }

        addLink(self);
    }

    /**
     * @since 4.3.0
     */
    public ApplicationLinkEntity(final Link... links) {
        super(links);
    }

    public ApplicationId getId() {
        return id;
    }

    public TypeId getTypeId() {
        return typeId;
    }

    public String getName() {
        return name;
    }

    public URI getDisplayUrl() {
        return displayUrl;
    }

    @Deprecated
    public URI getIconUrl() {
        return iconUrl;
    }

    public URI getIconUri() {
        return iconUri;
    }

    public URI getRpcUrl() {
        return isSystem() ? null : rpcUrl;
    }

    public boolean isPrimary() {
        return isPrimary != null && isPrimary;
    }

    public Boolean isSystem() {
        return isSystem != null && isSystem;
    }

    public ApplicationLinkDetails getDetails() {
        return ApplicationLinkDetails
                .builder()
                .name(getName())
                .displayUrl(getDisplayUrl())
                .rpcUrl(getRpcUrl())
                .isPrimary(isPrimary())
                .build();
    }
}