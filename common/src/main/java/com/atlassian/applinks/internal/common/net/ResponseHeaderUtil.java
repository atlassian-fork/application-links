package com.atlassian.applinks.internal.common.net;

import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import javax.servlet.http.HttpServletResponse;

import static java.util.Objects.requireNonNull;

public final class ResponseHeaderUtil {
    @VisibleForTesting
    static final String HEADER_XFRAME_OPTIONS = "X-Frame-Options";

    @VisibleForTesting
    static final String HEADER_CONTENT_SECURITY_POLICY = "Content-Security-Policy";

    private ResponseHeaderUtil() {
        // do not instantiate
    }

    public static void preventCrossFrameClickJacking(@Nonnull final HttpServletResponse response) {
        requireNonNull(response, "response");
        response.setHeader(HEADER_XFRAME_OPTIONS, "SAMEORIGIN");
        response.setHeader(HEADER_CONTENT_SECURITY_POLICY, "frame-ancestors 'self'");
    }
}
