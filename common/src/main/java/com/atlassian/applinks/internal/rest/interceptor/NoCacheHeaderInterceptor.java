package com.atlassian.applinks.internal.rest.interceptor;

import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;

import java.lang.reflect.InvocationTargetException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

public class NoCacheHeaderInterceptor implements ResourceInterceptor {
    @Override
    public void intercept(MethodInvocation methodInvocation) throws IllegalAccessException, InvocationTargetException {
        methodInvocation.invoke();
        Response response = methodInvocation.getHttpContext().getResponse().getResponse();
        Response noCacheResponse = Response.fromResponse(response).header(HttpHeaders.CACHE_CONTROL, "no-cache").build();
        methodInvocation.getHttpContext().getResponse().setResponse(noCacheResponse);
    }
}
