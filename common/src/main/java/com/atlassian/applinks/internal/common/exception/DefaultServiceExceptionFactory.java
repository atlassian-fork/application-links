package com.atlassian.applinks.internal.common.exception;

import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkState;
import static java.util.Objects.requireNonNull;

/**
 * Default implementation of {@link ServiceExceptionFactory}, uses SAL's {@code I18nResolver} to construct error
 * messages.
 *
 * @since 4.3
 */
public class DefaultServiceExceptionFactory implements ServiceExceptionFactory {
    @VisibleForTesting
    static final String DEFAULT_MESSAGE_FIELD_NAME = "DEFAULT_MESSAGE";

    private final I18nResolver i18nResolver;

    @Autowired
    public DefaultServiceExceptionFactory(@Nonnull I18nResolver i18nResolver) {
        this.i18nResolver = requireNonNull(i18nResolver, "i18nResolver");
    }

    @Nonnull
    @Override
    public <E extends ServiceException> E raise(@Nonnull Class<E> exceptionClass, Serializable... args) throws E {
        E exc = create(exceptionClass, args);
        exc.fillInStackTrace();
        throw exc;
    }

    @Nonnull
    @Override
    public <E extends ServiceException> E raise(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey) throws E {
        E exc = create(exceptionClass, i18nKey);
        exc.fillInStackTrace();
        throw exc;
    }

    @Nonnull
    @Override
    public <E extends ServiceException> E raise(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey,
                                                @Nonnull Throwable cause) throws E {
        E exc = create(exceptionClass, i18nKey, cause);
        exc.fillInStackTrace();
        throw exc;
    }

    @Override
    @Nonnull
    public <E extends ServiceException> E create(@Nonnull Class<E> exceptionClass, Serializable... args) {
        requireNonNull(exceptionClass, "exceptionClass");
        requireNonNull(args, "args");

        try {
            String defaultMessageKey = (String) exceptionClass.getField(DEFAULT_MESSAGE_FIELD_NAME).get(null);
            checkState(StringUtils.isNotEmpty(defaultMessageKey), "Default message key must not be empty");

            return create(exceptionClass, I18nKey.newI18nKey(defaultMessageKey, args));
        } catch (NoSuchFieldException e) {
            return create(exceptionClass, I18nKey.newI18nKey("applinks.service.error.default.message.not.specified",
                    exceptionClass.getName()));
        } catch (Exception e) {
            throw new IllegalStateException("Unable to instantiate " + exceptionClass.getName(), e);
        }
    }

    @Override
    @Nonnull
    public <E extends ServiceException> E create(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey) {
        requireNonNull(exceptionClass, "exceptionClass");
        requireNonNull(i18nKey, "i18nKey");

        try {
            return exceptionClass.getConstructor(String.class).newInstance(i18nResolver.getText(i18nKey));
        } catch (Exception e) {
            throw new IllegalStateException("Unable to instantiate " + exceptionClass.getName(), e);
        }
    }

    @Override
    @Nonnull
    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    public <E extends ServiceException> E create(@Nonnull Class<E> exceptionClass, @Nonnull I18nKey i18nKey,
                                                 @Nonnull Throwable cause) {
        requireNonNull(exceptionClass, "exceptionClass");
        requireNonNull(i18nKey, "i18nKey");
        requireNonNull(cause, "cause");

        try {
            return exceptionClass.getConstructor(String.class, Throwable.class)
                    .newInstance(i18nResolver.getText(i18nKey), cause);
        } catch (Exception e) {
            throw new IllegalStateException("Unable to instantiate " + exceptionClass.getName(), e);
        }
    }
}
