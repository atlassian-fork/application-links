package com.atlassian.applinks.internal.common.permission;

/**
 * Represents permission levels applicable to Application Links.
 *
 * @since 4.3
 */
public enum PermissionLevel {
    // NOTE: the ordering of the enum constants below is important and should not be changed,
    // because the ordinal() value is used internally

    /**
     * Authenticated user. Service APIs enforcing this level should declare {@code NotAuthenticatedException} in their
     * {@code throws} clause
     */
    USER,
    /**
     * Administrator. Service APIs enforcing this level should declare {@code NoAccessException} in their
     * {@code throws} clause.
     */
    ADMIN,
    /**
     * System administrator. Service APIs enforcing this level should declare {@code NoAccessException} in their
     * {@code throws} clause.
     */
    SYSADMIN;
}