package com.atlassian.applinks.internal.rest;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.rest.RestUrl.forPath;

/**
 * REST API versions in Applinks.
 *
 * @since 4.3
 */
public enum RestVersion {
    /**
     * Original REST API introduced in UAL 3.0
     */
    V1("1.0"),
    V2("2.0"),
    /**
     * 3.0 is where the Status API was added, also contains 2.0 resources
     */
    V3("3.0"),
    /**
     * The latest version, currently 3.0.
     */
    LATEST("latest");

    public static RestVersion DEFAULT = LATEST;

    private final RestUrl path;

    RestVersion(String path) {
        this.path = forPath(path);
    }

    @Nonnull
    public RestUrl getPath() {
        return path;
    }
}
