package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

/**
 * Raised if an applinks was requested that does not exist.
 *
 * @since 4.3
 */
public class NoSuchApplinkException extends NoSuchEntityException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.nosuchentity.applink";

    public NoSuchApplinkException(@Nullable String message) {
        super(message);
    }

    public NoSuchApplinkException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }
}
