package com.atlassian.applinks.internal.common.cache;

import com.atlassian.sal.api.web.context.HttpContext;
import com.google.common.collect.Maps;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * {@link ApplinksRequestCache} based on SAL's {@link HttpContext}.
 *
 * @since 5.0
 */
public class SalApplinksRequestCache implements ApplinksRequestCache {
    public static final String CACHE_KEY = "applinks.internal.cache.SalApplinksRequestCache";

    private final HttpContext httpContext;

    @Autowired
    public SalApplinksRequestCache(HttpContext httpContext) {
        this.httpContext = httpContext;
    }

    @Nonnull
    @Override
    public <K, V> Cache<K, V> getCache(@Nonnull String cacheName, @Nonnull Class<K> keyType, @Nonnull Class<V> valueType) {
        requireNonNull(cacheName, "cacheName");
        requireNonNull(keyType, "keyType");
        requireNonNull(valueType, "valueType");

        return new CacheImpl<>(getMapFromRequest(cacheName, keyType, valueType));
    }

    @Nonnull
    @SuppressWarnings("unchecked")
    private <K, V> Map<K, V> getMapFromRequest(@Nonnull String cacheName, @Nonnull Class<K> keyType,
                                               @Nonnull Class<V> valueType) {
        Map<String, Object> allCaches = getCachesMap();
        Map<K, V> cache = (Map<K, V>) allCaches.get(cacheName);
        if (cache == null) {
            cache = Maps.newHashMap();
            allCaches.put(cacheName, cache);
        }
        return cache;
    }

    @SuppressWarnings("unchecked")
    private Map<String, Object> getCachesMap() {
        HttpServletRequest request = httpContext.getRequest();
        if (request == null) {
            return Maps.newHashMap();
        }

        Map<String, Object> caches = (Map<String, Object>) request.getAttribute(CACHE_KEY);
        if (caches == null) {
            caches = Maps.newHashMap();
            request.setAttribute(CACHE_KEY, caches);
        }
        return caches;
    }

    private static final class CacheImpl<K, V> implements Cache<K, V> {
        private final Map<K, V> map;

        private CacheImpl(Map<K, V> map) {
            this.map = map;
        }

        @Override
        public void put(@Nonnull K key, @Nonnull V value) {
            requireNonNull(key, "key");
            requireNonNull(value, "value");

            map.put(key, value);
        }

        @Nullable
        @Override
        public V get(@Nonnull K key) {
            requireNonNull(key, "key");

            return map.get(key);
        }
    }
}
