package com.atlassian.applinks.internal.common.event;

import com.atlassian.event.api.AsynchronousPreferred;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;

import static java.util.Objects.requireNonNull;

@AsynchronousPreferred
public class ManifestDownloadFailedEvent {
    private final URI uri;
    private final Throwable cause;

    public ManifestDownloadFailedEvent(@Nonnull URI uri, @Nullable Throwable exception) {
        this.uri = requireNonNull(uri, "uri");
        this.cause = exception;
    }

    @Nonnull
    public URI getUri() {
        return uri;
    }

    @Nullable
    public Throwable getCause() {
        return cause;
    }
}
