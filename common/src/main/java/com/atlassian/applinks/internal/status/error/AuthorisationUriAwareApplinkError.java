package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.authentication.AuthorisationUriAware;

/**
 * {@link ApplinkError} that is related to {@link ApplinkErrorCategory#ACCESS_ERROR access problems} and contains
 * information about the authorisation URI to use for solving those.
 *
 * @since 4.3
 */
public interface AuthorisationUriAwareApplinkError extends ApplinkError, AuthorisationUriAware {
}
