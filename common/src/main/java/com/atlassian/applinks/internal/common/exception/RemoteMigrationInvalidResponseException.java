package com.atlassian.applinks.internal.common.exception;

import javax.annotation.Nullable;

public class RemoteMigrationInvalidResponseException extends ConsumerInformationUnavailableException {
    public static final String DEFAULT_MESSAGE = "applinks.service.error.remote.migration.response.invalid";

    public RemoteMigrationInvalidResponseException(@Nullable final String message) {
        super(message);
    }
}
