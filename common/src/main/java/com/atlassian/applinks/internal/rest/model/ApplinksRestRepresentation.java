package com.atlassian.applinks.internal.rest.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.ANY;
import static com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility.NONE;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import java.net.URI;

import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion.NON_NULL;

/**
 * Base class for all RestXyz classes that are used in the Applinks Public API.
 * Required in order to consolidate all the annotations required for swagger to be able to scan the classes to create schemas
 */
@JsonSerialize
@org.codehaus.jackson.map.annotate.JsonSerialize(include = NON_NULL)
@JsonAutoDetect(fieldVisibility = ANY, getterVisibility = NONE, setterVisibility = NONE)
@org.codehaus.jackson.annotate.JsonAutoDetect(
        fieldVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.ANY,
        getterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE,
        setterVisibility = org.codehaus.jackson.annotate.JsonAutoDetect.Visibility.NONE
)
@JsonIgnoreProperties(ignoreUnknown = true)
@org.codehaus.jackson.annotate.JsonIgnoreProperties(ignoreUnknown = true)
public class ApplinksRestRepresentation {

    protected final String validateString(@Nonnull String key, @Nullable String value) {
        if (isEmpty(value)) {
            throw new IllegalRestRepresentationStateException(key);
        }
        return value;
    }

    protected final URI validateURI(@Nonnull String key, @Nullable URI value) {

        if (value == null) {
            throw new IllegalRestRepresentationStateException(key);
        }
        //further validation is done if required in DefaultValidationService
        return value;
    }

}
