package com.atlassian.applinks.internal.common.rest.model.oauth;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.Consumer.SignatureMethod;
import com.atlassian.oauth.util.RSAKeys;
import com.google.common.base.Function;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.URI;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * REST representation of an OAuth {@link Consumer}.
 *
 * @since 5.0
 */
@JsonSerialize
public class RestConsumer extends BaseRestEntity {
    public static final Function<Object, RestConsumer> REST_TRANSFORM = new Function<Object, RestConsumer>() {
        @Nullable
        @Override
        @SuppressWarnings("unchecked")
        public RestConsumer apply(@Nullable Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof RestConsumer) {
                return (RestConsumer) object;
            } else if (object instanceof Map) {
                return new RestConsumer((Map<String, Object>) object);
            }
            throw new IllegalArgumentException("Cannot instantiate RestConsumer from " + object);
        }
    };

    public static final String KEY = "key";
    public static final String NAME = "name";
    public static final String DESCRIPTION = "description";
    public static final String SIGNATURE_METHOD = "signatureMethod";
    public static final String PUBLIC_KEY = "publicKey";
    public static final String CALLBACK = "callback";
    public static final String TWO_LO_ALLOWED = "twoLOAllowed";
    public static final String EXECUTING_TWO_LO_USER = "executingTwoLOUser";
    public static final String TWO_LO_IMPERSONATION_ALLOWED = "twoLOImpersonationAllowed";
    public static final String OUTGOING = "outgoing";
    public static final String SHARED_SECRET = "sharedSecret";

    @SuppressWarnings("unused") // for Jackson
    public RestConsumer() {
    }

    @SuppressWarnings("unused") // RestRepresentation
    public RestConsumer(Map<String, Object> original) {
        super(original);
    }

    public RestConsumer(@Nonnull Consumer consumer, boolean outgoing, @Nullable String sharedSecret) {
        this(consumer);
        put(OUTGOING, outgoing);
        putIfNotNull(SHARED_SECRET, sharedSecret);
    }

    public RestConsumer(@Nonnull Consumer consumer) {
        requireNonNull(consumer, "consumer");
        put(KEY, consumer.getKey());
        put(NAME, consumer.getName());
        putIfNotNull(DESCRIPTION, consumer.getDescription());
        put(SIGNATURE_METHOD, consumer.getSignatureMethod().name());
        if (consumer.getPublicKey() != null) {
            put(PUBLIC_KEY, RSAKeys.toPemEncoding(consumer.getPublicKey()));
        }
        putAsString(CALLBACK, consumer.getCallback());
        put(TWO_LO_ALLOWED, consumer.getTwoLOAllowed());
        putIfNotNull(EXECUTING_TWO_LO_USER, consumer.getExecutingTwoLOUser());
        put(TWO_LO_IMPERSONATION_ALLOWED, consumer.getTwoLOImpersonationAllowed());
    }

    @Nonnull
    public String getKey() {
        return requiredValue(KEY, getString(KEY));
    }

    @Nonnull
    public String getName() {
        return requiredValue(NAME, getString(NAME));
    }

    @Nonnull
    public SignatureMethod getSignatureMethod() {
        return requiredValue(SIGNATURE_METHOD, getEnum(SIGNATURE_METHOD, SignatureMethod.class));
    }

    @Nullable
    @SuppressWarnings("ConstantConditions")
    public URI getCallback() {
        if (containsKey(CALLBACK)) {
            return URI.create(getString(CALLBACK));
        } else {
            return null;
        }
    }

    @Nullable
    @SuppressWarnings("ConstantConditions")
    public PublicKey getPublicKey() {
        if (!containsKey(PUBLIC_KEY)) {
            return null;
        }
        try {
            return RSAKeys.fromPemEncodingToPublicKey(getString(PUBLIC_KEY));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException("Invalid key", e);
        }
    }

    public boolean isTwoLoAllowed() {
        return getBooleanValue(TWO_LO_ALLOWED);
    }

    public boolean isTwoLoImpersonationAllowed() {
        return getBooleanValue(TWO_LO_IMPERSONATION_ALLOWED);
    }
}
