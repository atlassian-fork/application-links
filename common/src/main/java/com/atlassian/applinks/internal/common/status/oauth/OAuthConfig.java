package com.atlassian.applinks.internal.common.status.oauth;

import com.google.common.collect.Ordering;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.math.BigInteger;

/**
 * Represents possible OAuth configuration scenarios.
 *
 * @since 4.3
 */
public final class OAuthConfig {
    /**
     * Ordering that sorts {@code OAuthConfig} instances by the auth level. OAuth config instances with more OAuth
     * levels enabled are ranked higher (e.g. 2LOi will be ranked higher than 3LO+2LO, which in turn will be higher
     * than 3LO only).
     */
    public static Ordering<OAuthConfig> ORDER_BY_LEVEL = new Ordering<OAuthConfig>() {
        @Override
        public int compare(@Nullable OAuthConfig left, @Nullable OAuthConfig right) {
            if (left == right) {
                return 0;
            } else if (left == null) {
                return -1;
            } else if (right == null) {
                return 1;
            }

            if (left.equals(right)) {
                return 0;
            }

            return left.levels.compareTo(right.levels);
        }
    };

    // acts as an immutable bit set
    private final BigInteger levels;

    private OAuthConfig(boolean threeLoEnabled, boolean twoLoEnabled, boolean twoLoImpersonationEnabled) {
        BigInteger value = BigInteger.ZERO;
        value = setBit(value, 0, threeLoEnabled);
        value = setBit(value, 1, twoLoEnabled);
        value = setBit(value, 2, twoLoImpersonationEnabled);

        this.levels = value;
    }

    /**
     * @return config representing OAuth fully disabled
     */
    @Nonnull
    public static OAuthConfig createDisabledConfig() {
        return new OAuthConfig(false, false, false);
    }

    /**
     * @return 3LO only config
     */
    @Nonnull
    public static OAuthConfig createThreeLoOnlyConfig() {
        return new OAuthConfig(true, false, false);
    }

    /**
     * @return the default OAuth config (3LO + 2LO)
     */
    @Nonnull
    public static OAuthConfig createDefaultOAuthConfig() {
        return new OAuthConfig(true, true, false);
    }

    /**
     * @return OAuth with impersonation config (3LO + 2LO + 2LOi)
     */
    @Nonnull
    public static OAuthConfig createOAuthWithImpersonationConfig() {
        return new OAuthConfig(true, true, true);
    }

    @Nonnull
    public static OAuthConfig fromConfig(boolean is3LoConfigured, boolean is2LoConfigured, boolean is2LoIConfigured) {
        if (!is3LoConfigured) {
            return createDisabledConfig();
        } else if (is2LoIConfigured) {
            return createOAuthWithImpersonationConfig();
        } else if (is2LoConfigured) {
            return createDefaultOAuthConfig();
        } else {
            return createThreeLoOnlyConfig();
        }
    }

    /**
     * @return {@code true}, if OAuth is enabled at all for given link/direction. This implies that at least 3-legged
     * OAuth is enabled.
     */
    public boolean isEnabled() {
        return levels.testBit(0);
    }

    public boolean isTwoLoEnabled() {
        return levels.testBit(1);
    }

    public boolean isTwoLoImpersonationEnabled() {
        return levels.testBit(2);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final OAuthConfig that = (OAuthConfig) o;
        return this.levels.equals(that.levels);
    }

    @Override
    public int hashCode() {
        return levels.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        return sb.append("OAuthConfig{").append("enabled=").append(isEnabled()).append(", ")
                .append("twoLoEnabled=").append(isTwoLoEnabled()).append(", ")
                .append("twoLoImpersonationEnabled=").append(isTwoLoImpersonationEnabled()).append("}").toString();
    }

    private static BigInteger setBit(BigInteger integer, int position, boolean value) {
        return value ? integer.setBit(position) : integer;
    }
}
