package com.atlassian.applinks.internal.common.auth.oauth;

import com.atlassian.annotations.Internal;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.RequestFactory;

import javax.annotation.Nonnull;

/**
 * Auto-configure OAuth for given applink remotely and locally, given an authenticated request factory.
 *
 * @since 5.1
 */
@Internal
@Restricted(PermissionLevel.ADMIN)
public interface OAuthAutoConfigurator {
    void enable(@Nonnull OAuthConfig authLevel, @Nonnull ApplicationLink applink,
                @Nonnull RequestFactory requestFactory) throws AuthenticationConfigurationException;

    void enable(@Nonnull OAuthConfig incoming, @Nonnull OAuthConfig outgoing,
                @Nonnull ApplicationLink applink, @Nonnull ApplicationLinkRequestFactory requestFactory)
            throws AuthenticationConfigurationException;

    void disable(@Nonnull ApplicationLink applicationLink, @Nonnull RequestFactory requestFactory)
            throws AuthenticationConfigurationException;
}
