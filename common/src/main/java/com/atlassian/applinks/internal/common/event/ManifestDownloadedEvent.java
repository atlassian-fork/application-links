package com.atlassian.applinks.internal.common.event;

import com.atlassian.applinks.spi.Manifest;
import com.atlassian.event.api.AsynchronousPreferred;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Raised when a manifest has been downloaded from a remote applinked application.
 *
 * @since 5.0
 */
@AsynchronousPreferred
public class ManifestDownloadedEvent {
    private final Manifest manifest;

    public ManifestDownloadedEvent(@Nonnull Manifest manifest) {
        this.manifest = requireNonNull(manifest, "manifest");
    }

    @Nonnull
    public Manifest getManifest() {
        return manifest;
    }
}
