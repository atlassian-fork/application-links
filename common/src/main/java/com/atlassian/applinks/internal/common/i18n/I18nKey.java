package com.atlassian.applinks.internal.common.i18n;

import com.atlassian.sal.api.message.Message;

import javax.annotation.Nonnull;
import java.io.Serializable;

import static java.util.Objects.requireNonNull;

/**
 * Encapsulates data required to retrieve an i18n-ed message.
 *
 * @since 4.3
 */
public class I18nKey implements Message {
    private final String key;
    private final Serializable[] args;

    @Nonnull
    public static I18nKey newI18nKey(@Nonnull String key, @Nonnull Serializable... args) {
        return new I18nKey(requireNonNull(key, "key"), requireNonNull(args, "args"));
    }

    private I18nKey(String key, Serializable[] args) {
        this.key = key;
        this.args = args;
    }

    @Nonnull
    @Override
    public String getKey() {
        return key;
    }

    @Nonnull
    @Override
    public Serializable[] getArguments() {
        return args;
    }

    // copied from SAL's DefaultMessage
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(key);
        builder.append(": ");
        for (Serializable argument : args) {
            builder.append(argument);
            builder.append(",");
        }
        return builder.toString();
    }
}
