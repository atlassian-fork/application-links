package com.atlassian.applinks.internal.common.web.data;

import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.applinks.internal.rest.model.BaseRestEntity.createSingleFieldEntity;

/**
 * Injects help paths into the page for client-side usage.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 5.0
 */
public class HelpPathsDataProvider implements WebResourceDataProvider {
    private static final String APPLINKS_DOCS_ROOT_KEY = "applinks.docs.root";
    private static final String ENTRIES = "entries";

    private final DocumentationLinker documentationLinker;

    public HelpPathsDataProvider(DocumentationLinker documentationLinker) {
        this.documentationLinker = documentationLinker;
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(createSingleFieldEntity(ENTRIES, getAllHelpPaths()));
    }

    private Map<String, String> getAllHelpPaths() {
        return ImmutableMap.<String, String>builder()
                .put(APPLINKS_DOCS_ROOT_KEY, documentationLinker.getDocumentationBaseUrl().toASCIIString())
                .putAll(documentationLinker.getAllLinkMappings())
                .build();
    }
}
