package com.atlassian.applinks.internal.rest.client;

import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.sal.api.net.RequestFilePart;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.google.common.annotations.VisibleForTesting;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class DefaultAuthorisationUriAwareRequest implements AuthorisationUriAwareRequest {
    private final ApplicationLinkRequest delegateRequest;
    private final AuthorisationURIGenerator generator;

    public DefaultAuthorisationUriAwareRequest(@Nonnull ApplicationLinkRequest delegateRequest,
                                               @Nonnull AuthorisationURIGenerator generator) {
        this.delegateRequest = requireNonNull(delegateRequest, "request");
        this.generator = requireNonNull(generator, "generator");
    }

    @Nonnull
    @Override
    public AuthorisationURIGenerator getAuthorisationUriGenerator() {
        return generator;
    }

    @Override
    public ApplicationLinkRequest setConnectionTimeout(int connectionTimeout) {
        delegateRequest.setConnectionTimeout(connectionTimeout);

        return this;
    }

    @Override
    public ApplicationLinkRequest setSoTimeout(int soTimeout) {
        delegateRequest.setSoTimeout(soTimeout);

        return this;
    }

    @Override
    public ApplicationLinkRequest setUrl(String url) {
        delegateRequest.setUrl(url);

        return this;
    }

    @Override
    public ApplicationLinkRequest setRequestBody(String requestBody) {
        delegateRequest.setRequestBody(requestBody);

        return this;
    }

    @Override
    public ApplicationLinkRequest setRequestBody(String requestBody, String contentType) {
        delegateRequest.setRequestBody(requestBody, contentType);

        return this;
    }

    @Override
    public ApplicationLinkRequest setFiles(List<RequestFilePart> files) {
        delegateRequest.setFiles(files);

        return this;
    }

    @Override
    public ApplicationLinkRequest setEntity(Object entity) {
        delegateRequest.setEntity(entity);

        return this;
    }

    @Override
    public ApplicationLinkRequest addRequestParameters(String... params) {
        delegateRequest.addRequestParameters(params);

        return this;
    }

    @Override
    public ApplicationLinkRequest addBasicAuthentication(String hostname, String username, String password) {
        delegateRequest.addBasicAuthentication(hostname, username, password);

        return this;
    }

    @Override
    public ApplicationLinkRequest addHeader(String headerName, String headerValue) {
        delegateRequest.addHeader(headerName, headerValue);

        return this;
    }

    @Override
    public ApplicationLinkRequest setHeader(String headerName, String headerValue) {
        delegateRequest.setHeader(headerName, headerValue);

        return this;
    }

    @Override
    public ApplicationLinkRequest setFollowRedirects(boolean follow) {
        delegateRequest.setFollowRedirects(follow);

        return this;
    }

    @Override
    public Map<String, List<String>> getHeaders() {
        return delegateRequest.getHeaders();
    }

    @Override
    public void execute(ResponseHandler<? super Response> responseHandler) throws ResponseException {
        delegateRequest.execute(responseHandler);
    }

    @Override
    public String execute() throws ResponseException {
        return delegateRequest.execute();
    }

    @Override
    public <RET> RET executeAndReturn(ReturningResponseHandler<? super Response, RET> responseHandler)
            throws ResponseException {
        return delegateRequest.executeAndReturn(responseHandler);
    }

    @Override
    public <R> R execute(ApplicationLinkResponseHandler<R> responseHandler) throws ResponseException {
        return delegateRequest.execute(responseHandler);
    }

    @VisibleForTesting
    ApplicationLinkRequest getDelegateRequest() {
        return delegateRequest;
    }
}
