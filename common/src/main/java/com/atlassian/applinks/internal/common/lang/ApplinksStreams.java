package com.atlassian.applinks.internal.common.lang;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides bridge methods from {@code Iterable} to {@code Stream} and vice versa, as well as some useful
 * {@link Collector collectors}.
 *
 * @since 5.2
 */
public final class ApplinksStreams {
    private ApplinksStreams() {
    }

    @Nullable
    public static <T> T firstOrNull(@Nonnull Iterable<T> iterable) {
        return Iterables.getFirst(iterable, null);
    }

    @Nonnull
    public static <T> Stream<T> toStream(@Nonnull Iterable<T> source) {
        return StreamSupport.stream(source.spliterator(), false);
    }

    @Nonnull
    public static <T> Iterable<T> toIterable(@Nonnull Stream<T> source) {
        // use ref to Stream.iterator() as a lambda that converts into Iterable
        // copy into an immutable list to "terminate" the stream
        return ImmutableList.copyOf(source::iterator);
    }

    /**
     * @param listFactory supplier to create the underlying list instance
     * @param <T>         element type
     * @param <A>         list type
     * @return collector to create an immutable list out of a stream
     */
    @Nonnull
    public static <T, A extends List<T>> Collector<T, A, List<T>> toImmutableList(@Nonnull Supplier<A> listFactory) {
        return Collector.of(listFactory,
                List::add,
                (left, right) -> {
                    left.addAll(right);
                    return left;
                },
                Collections::unmodifiableList);
    }

    /**
     * @param <T> element type
     * @return collector to create an immutable list out of a stream, using an underlying array list
     */
    @Nonnull
    public static <T> Collector<T, List<T>, List<T>> toImmutableList() {
        return toImmutableList(ArrayList::new);
    }

    /**
     * @param setFactory supplier to create the underlying set instance
     * @param <T>        element type
     * @param <A>        set type
     * @return collector to create an immutable set out of a stream
     */
    @Nonnull
    public static <T, A extends Set<T>> Collector<T, A, Set<T>> toImmutableSet(@Nonnull Supplier<A> setFactory) {
        return Collector.of(setFactory,
                Set::add,
                (left, right) -> {
                    left.addAll(right);
                    return left;
                },
                Collections::unmodifiableSet);
    }

    /**
     * @param <T> element type
     * @return collector to create an immutable ordered set out of a stream, using an underlying linked hash set
     */
    @Nonnull
    public static <T> Collector<T, Set<T>, Set<T>> toImmutableSet() {
        return toImmutableSet(LinkedHashSet::new);
    }

    /**
     * @param keyMapper   mapper from input element to the map key
     * @param valueMapper mapper from input element to the map value
     * @param mapFactory  supplier to create the underlying map instance
     * @param <T>         element type
     * @param <K>         map key type
     * @param <V>         map value type
     * @param <A>         intermediate map type
     * @return collector to create an immutable map out of a stream, allowing for overriding existing entries in case of
     * duplicate keys
     */
    @Nonnull
    public static <T, K, V, A extends Map<K, V>> Collector<T, A, Map<K, V>> toImmutableMap(
            Function<? super T, ? extends K> keyMapper,
            Function<? super T, ? extends V> valueMapper,
            @Nonnull Supplier<A> mapFactory) {
        BiConsumer<A, T> accumulator = (map, element) ->
                map.put(keyMapper.apply(element), valueMapper.apply(element));
        BinaryOperator<A> combiner = (map1, map2) -> {
            map1.putAll(map2);
            return map1;
        };

        return Collector.of(mapFactory,
                accumulator,
                combiner,
                Collections::unmodifiableMap);
    }

    /**
     * @param keyMapper   mapper from input element to the map key
     * @param valueMapper mapper from input element to the map value
     * @param <T>         element type
     * @param <K>         map key type
     * @param <V>         map value type
     * @return collector to create an immutable ordered map out of a stream, using an underlying {@link LinkedHashMap}
     * @see #toImmutableMap(Function, Function, Supplier)
     */
    @Nonnull
    public static <T, K, V> Collector<T, Map<K, V>, Map<K, V>> toImmutableMap(
            Function<? super T, ? extends K> keyMapper,
            Function<? super T, ? extends V> valueMapper) {

        return toImmutableMap(keyMapper, valueMapper, LinkedHashMap::new);
    }

    /**
     * @param <K> map key type
     * @param <V> map value type
     * @return collector to create an immutable ordered map out of a stream of map entries, allowing to assemble back a
     * map after transforming it into a stream of entries
     * @see #toImmutableMap(Function, Function)
     */
    @Nonnull
    public static <K, V> Collector<Map.Entry<K, V>, Map<K, V>, Map<K, V>> entryToMap() {
        return toImmutableMap(Map.Entry::getKey, Map.Entry::getValue);
    }
}
