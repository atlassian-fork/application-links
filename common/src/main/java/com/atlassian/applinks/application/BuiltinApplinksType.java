package com.atlassian.applinks.application;

/**
 * Marker interface for types that are built in to the applinks plugin (i.e excludes any test applinks types)
 */
public interface BuiltinApplinksType {
}
