package com.atlassian.applinks.internal.common.web;

import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.exception.EntityUpdateException;
import com.atlassian.applinks.test.mock.MockI18nResolver;
import com.atlassian.applinks.test.mock.MockIconizedIdentifiableApplicationType;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.RequiredResources;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import com.google.common.collect.ImmutableMap;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;

import java.io.IOException;
import java.util.Map;
import javax.servlet.ServletException;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.same;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class AbstractApplinksServiceServletTest {
    @Mock
    private AppLinkPluginUtil appLinkPluginUtil;
    @Spy
    private MockI18nResolver i18nResolver = new MockI18nResolver();
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private LoginUriProvider loginUriProvider;
    @Mock
    private SoyTemplateRenderer soyTemplateRenderer;
    @Mock
    private PageBuilderService pageBuilderService;

    @Mock
    private WebResourceAssembler webResourceAssembler;
    @Mock
    private RequiredResources requiredResources;

    @InjectMocks
    private TestApplinksServiceServlet testServlet;

    private MockHttpServletRequest request = new MockHttpServletRequest();
    private MockHttpServletResponse response = new MockHttpServletResponse();

    private MockIconizedIdentifiableApplicationType applicationType = new MockIconizedIdentifiableApplicationType();

    @Before
    public void setUpPageBuilder() {
        when(pageBuilderService.assembler()).thenReturn(webResourceAssembler);
        when(webResourceAssembler.resources()).thenReturn(requiredResources);
    }

    @Before
    public void setUpInternalHostApplication() {
        when(internalHostApplication.getType()).thenReturn(applicationType);
    }

    @Test
    public void doGetNoErrors() throws Exception {
        when(appLinkPluginUtil.completeModuleKey("test-module")).thenReturn("com.atlassian.applinks:test-module");
        applicationType.setTypeId("test-id");
        testServlet.setTemplate("test-module", "applinks.page.test")
                .setTemplateData(ImmutableMap.<String, Object>of("test-key", "test-value", "test-key2", 15))
                .setWebResourceContexts("test-context")
                .setDecorator("atl.admin")
                .setActiveTab("test-tab")
                .setPageTitle("test-title")
                .setPageInitializer("test/initializer");

        testServlet.doGet(request, response);

        ServletAssertions.assertClickjackingPrevention(response);
        verifyContentType();
        verify(requiredResources).requireContext("test-context");
        verify(soyTemplateRenderer).render(same(response.getWriter()),
                eq("com.atlassian.applinks:test-module"),
                eq("applinks.page.test"),
                // template params
                argThat(new HamcrestArgumentMatcher<>(allOf(
                        hasCommonParams("atl.admin", "test-tab", "test-title"),
                        hasDataEntry(AbstractApplinksServiceServlet.PARAM_PAGE_INITIALIZER, "test/initializer"),
                        hasDataEntry("test-key", "test-value"),
                        hasDataEntry("test-key2", 15)))),
                // injected data
                argThat(new HamcrestArgumentMatcher<>(hasDataEntry(
                        AbstractApplinksServiceServlet.IJ_PARAM_APPLICATION_TYPE, "test-id"))));
    }

    @Test(expected = IOException.class)
    public void doGetIOError() throws Exception {
        testServlet.setIoException(new IOException());

        doGetExpectingError();
    }

    @Test(expected = ServletException.class)
    public void doGetServletError() throws Exception {
        testServlet.setServletException(new ServletException("Boom"));

        doGetExpectingError();
    }

    @Test
    public void doGetServiceExceptionResponseUncommitted() throws Exception {
        when(appLinkPluginUtil.completeModuleKey(AbstractApplinksServiceServlet.MODULE_PAGE_COMMON))
                .thenReturn("com.atlassian.applinks:" + AbstractApplinksServiceServlet.MODULE_PAGE_COMMON);
        applicationType.setTypeId("test-id");
        testServlet.setServletException(new ServletException("Boom", new EntityUpdateException("Not found!")))
                .setWebResourceContexts("test-context")
                .setDecorator("atl.admin")
                .setActiveTab("test-tab")
                .setPageInitializer("test/initializer");

        testServlet.doGet(request, response);

        ServletAssertions.assertClickjackingPrevention(response);
        verifyContentType();
        verify(requiredResources).requireContext("test-context");
        verify(soyTemplateRenderer).render(same(response.getWriter()),
                eq("com.atlassian.applinks:" + AbstractApplinksServiceServlet.MODULE_PAGE_COMMON),
                eq("applinks.page.common.error"),
                // template params
                argThat(new HamcrestArgumentMatcher<>(allOf(
                        hasDataEntry(AbstractApplinksServiceServlet.PARAM_TITLE, "applinks.common.error.title"),
                        hasDataEntry(AbstractApplinksServiceServlet.PARAM_ERROR_MESSAGE, "Not found!"),
                        hasNoEntry(AbstractApplinksServiceServlet.PARAM_ACTIVE_TAB),
                        hasNoEntry(AbstractApplinksServiceServlet.PARAM_PAGE_INITIALIZER),
                        hasNoEntry(AbstractApplinksServiceServlet.PARAM_DECORATOR)))),
                // injected data
                argThat(new HamcrestArgumentMatcher<>(hasDataEntry(
                        AbstractApplinksServiceServlet.IJ_PARAM_APPLICATION_TYPE, "test-id")))
        );
    }

    @Test(expected = ServletException.class)
    public void doGetServiceExceptionResponseCommitted() throws Exception {
        testServlet.setServletException(new ServletException("Boom", new EntityUpdateException("Not found!")));
        response.setCommitted(true);

        doGetExpectingError();
    }

    private void doGetExpectingError() throws Exception {
        try {
            testServlet.doGet(request, response);
            fail("Expected exception");
        } catch (Exception e) {
            ServletAssertions.assertClickjackingPrevention(response);
            throw e;
        }
    }

    private void verifyContentType() {
        assertEquals("text/html; charset=UTF-8", response.getContentType());
    }

    private static Matcher<Map<String, Object>> hasCommonParams(String decorator,
                                                                String activeTab, String pageTitle) {
        return allOf(
                hasDataEntry(AbstractApplinksServiceServlet.PARAM_DECORATOR, decorator),
                hasDataEntry(AbstractApplinksServiceServlet.PARAM_ACTIVE_TAB, activeTab),
                hasDataEntry(AbstractApplinksServiceServlet.PARAM_TITLE, pageTitle)
        );
    }

    @SuppressWarnings("unchecked")
    private static Matcher<Map<String, Object>> hasDataEntry(String key, Object value) {
        return (Matcher) hasEntry(key, value);
    }

    private static Matcher<Map<? extends String, ?>> hasNoEntry(String key) {
        return not(hasKey(key));
    }
}
