package com.atlassian.applinks.internal.common.rest.util;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.internal.common.exception.InvalidApplicationIdException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.test.mock.SimpleServiceExceptionFactory;
import com.atlassian.applinks.test.mock.TestApplinkIds;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.Matchers.isA;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RestApplicationIdParserTest {
    @Spy // for @InjectMocks
    private ServiceExceptionFactory serviceExceptionFactory = new SimpleServiceExceptionFactory();

    @InjectMocks
    private RestApplicationIdParser restApplicationIdParser;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void parseGivenNullId() throws ServiceException {
        restApplicationIdParser.parse(null);
    }

    @Test
    public void parseGivenInvalidId() throws ServiceException {
        expectedException.expect(InvalidApplicationIdException.class);
        expectedException.expectMessage("applinks.service.error.invalidvalue.applinkid");
        expectedException.expectCause(isA(IllegalArgumentException.class));

        String invalidId = TestApplinkIds.DEFAULT_ID.id().replace('-', '.');

        restApplicationIdParser.parse(invalidId);
    }

    @Test
    public void parseGivenValidId() throws ServiceException {
        ApplicationId applicationId = restApplicationIdParser.parse(TestApplinkIds.DEFAULT_ID.id());

        assertNotNull(applicationId);
        assertEquals(TestApplinkIds.DEFAULT_ID.applicationId(), applicationId);
    }
}
