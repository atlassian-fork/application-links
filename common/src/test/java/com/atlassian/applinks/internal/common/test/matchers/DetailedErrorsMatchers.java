package com.atlassian.applinks.internal.common.test.matchers;

import com.atlassian.applinks.internal.common.exception.DetailedError;
import com.atlassian.applinks.internal.common.exception.DetailedErrors;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;

/**
 * @since 5.1
 */
public final class DetailedErrorsMatchers {
    private DetailedErrorsMatchers() {
    }

    @Nonnull
    public static Matcher<DetailedError> detailedError(@Nullable String expectedSummary) {
        return detailedError(null, expectedSummary);
    }

    @Nonnull
    public static Matcher<DetailedError> detailedError(@Nullable String expectedContext,
                                                       @Nullable String expectedSummary) {
        return detailedError(expectedContext, expectedSummary, null);
    }

    @Nonnull
    public static Matcher<DetailedError> detailedError(@Nullable String expectedContext,
                                                       @Nullable String expectedSummary,
                                                       @Nullable String expectedDetails) {
        return allOf(withContext(expectedContext), withSummary(expectedSummary), withDetails(expectedDetails));
    }

    @Nonnull
    public static Matcher<DetailedError> withContext(@Nullable String expectedContext) {
        return withContextThat(is(expectedContext));
    }

    @Nonnull
    public static Matcher<DetailedError> withContextThat(@Nonnull Matcher<String> contextMatcher) {
        checkNotNull(contextMatcher, "contextMatcher");
        return new FeatureMatcher<DetailedError, String>(contextMatcher, "context that", "context") {
            @Override
            protected String featureValueOf(DetailedError error) {
                return error.getContext();
            }
        };
    }

    @Nonnull
    public static Matcher<DetailedError> withSummary(@Nullable String expectedSummary) {
        return withSummaryThat(is(expectedSummary));
    }

    @Nonnull
    public static Matcher<DetailedError> withSummaryThat(@Nonnull Matcher<String> summaryMatcher) {
        checkNotNull(summaryMatcher, "summaryMatcher");
        return new FeatureMatcher<DetailedError, String>(summaryMatcher, "summary that", "summary") {
            @Override
            protected String featureValueOf(DetailedError error) {
                return error.getSummary();
            }
        };
    }

    @Nonnull
    public static Matcher<DetailedError> withDetails(@Nullable String expectedSummary) {
        return withDetailsThat(is(expectedSummary));
    }

    @Nonnull
    public static Matcher<DetailedError> withDetailsThat(@Nonnull Matcher<String> detailsMatcher) {
        checkNotNull(detailsMatcher, "detailsMatcher");
        return new FeatureMatcher<DetailedError, String>(detailsMatcher, "details that", "details") {
            @Override
            protected String featureValueOf(DetailedError error) {
                return error.getDetails();
            }
        };
    }

    @Nonnull
    @SafeVarargs
    public static Matcher<DetailedErrors> withErrorsThat(@Nonnull Matcher<DetailedError>... errorsMatchers) {
        return withErrorsThat(contains(errorsMatchers));
    }

    @Nonnull
    public static Matcher<DetailedErrors> withErrorsThat(
            @Nonnull Matcher<Iterable<? extends DetailedError>> errorsMatcher) {
        checkNotNull(errorsMatcher, "errorsMatcher");
        return new FeatureMatcher<DetailedErrors, Iterable<DetailedError>>(errorsMatcher, "errors that", "errors") {
            @Override
            protected Iterable<DetailedError> featureValueOf(DetailedErrors errors) {
                return errors.getErrors();
            }
        };
    }
}

