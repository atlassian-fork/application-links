package com.atlassian.applinks.internal.common.net;

import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.List;

import static com.atlassian.applinks.internal.common.exception.ServiceExceptions.concreteServiceExceptionClasses;
import static org.junit.Assert.assertThat;

public class ServiceExceptionHttpMapperTest {
    @Test
    public void allServiceExceptionsAreMapped() throws Exception {
        List<String> notMapped = Lists.newArrayList();
        for (Class<? extends ServiceException> serviceExceptionClass : concreteServiceExceptionClasses()) {
            Class<?> exceptionMapping = serviceExceptionClass;
            while (exceptionMapping != ServiceException.class) {
                if (ServiceExceptionHttpMapper.ERROR_TO_CODE.containsKey(exceptionMapping)) {
                    break;
                } else {
                    exceptionMapping = exceptionMapping.getSuperclass();
                }
            }

            if (exceptionMapping == ServiceException.class) {
                notMapped.add(serviceExceptionClass.getName());
            }
        }

        assertThat("Mappings for: " + notMapped + " not found", notMapped, Matchers.emptyIterable());
    }
}
