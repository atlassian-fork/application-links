package com.atlassian.applinks.test.matchers.status;

import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;

import javax.annotation.Nonnull;

import static org.hamcrest.Matchers.allOf;

public final class OAuthStatusMatchers {
    private OAuthStatusMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<ApplinkOAuthStatus> oAuthStatusWith(@Nonnull Matcher<OAuthConfig> incomingConfigMatcher,
                                                              @Nonnull Matcher<OAuthConfig> outgoingConfigMatcher) {
        return allOf(
                withIncomingConfigThat(incomingConfigMatcher),
                withOutgoingConfigThat(outgoingConfigMatcher)
        );
    }

    @Nonnull
    public static Matcher<ApplinkOAuthStatus> oAuthStatusImpersonation() {
        return oAuthStatusWith(oAuthWithImpersonationConfig(), oAuthWithImpersonationConfig());
    }

    @Nonnull
    public static Matcher<ApplinkOAuthStatus> oAuthStatusDefault() {
        return oAuthStatusWith(defaultOAuthConfig(), defaultOAuthConfig());
    }

    @Nonnull
    public static Matcher<ApplinkOAuthStatus> oAuthStatusOff() {
        return oAuthStatusWith(disabledConfig(), disabledConfig());
    }

    @Nonnull
    public static Matcher<ApplinkOAuthStatus> withIncomingConfigThat(
            @Nonnull Matcher<OAuthConfig> incomingConfigMatcher) {
        return new FeatureMatcher<ApplinkOAuthStatus, OAuthConfig>(incomingConfigMatcher, "incoming config that",
                "incoming") {
            @Override
            protected OAuthConfig featureValueOf(ApplinkOAuthStatus status) {
                return status.getIncoming();
            }
        };
    }

    @Nonnull
    public static Matcher<ApplinkOAuthStatus> withOutgoingConfigThat(
            @Nonnull Matcher<OAuthConfig> outgoing) {
        return new FeatureMatcher<ApplinkOAuthStatus, OAuthConfig>(outgoing, "outgoing config that",
                "outgoing") {
            @Override
            protected OAuthConfig featureValueOf(ApplinkOAuthStatus status) {
                return status.getOutgoing();
            }
        };
    }

    @Nonnull
    public static Matcher<OAuthConfig> disabledConfig() {
        return Matchers.equalTo(OAuthConfig.createDisabledConfig());
    }

    @Nonnull
    public static Matcher<OAuthConfig> threeLoOnlyConfig() {
        return Matchers.equalTo(OAuthConfig.createThreeLoOnlyConfig());
    }

    @Nonnull
    public static Matcher<OAuthConfig> defaultOAuthConfig() {
        return Matchers.equalTo(OAuthConfig.createDefaultOAuthConfig());
    }

    @Nonnull
    public static Matcher<OAuthConfig> oAuthWithImpersonationConfig() {
        return Matchers.equalTo(OAuthConfig.createOAuthWithImpersonationConfig());
    }
}
