package com.atlassian.applinks.api;

/**
 * Represents a link to a remote application-level entity (JIRA, Confluence, Bamboo, etc).
 *
 * You can store simple data against {@link ApplicationLink}s using the methods provided by the {@link PropertySet}
 * interface. Note that these properties are shared between all plugins in the local application, so be careful to
 * namespace your property keys carefully.
 *
 * @see PropertySet
 * @since 3.0
 */
public interface ApplicationLink extends ReadOnlyApplicationLink, PropertySet {
}