package com.atlassian.applinks.api.auth;

import java.lang.annotation.Documented;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * <p>
 * An annotation that describes dependency of the configuration of an
 * {@link com.atlassian.applinks.api.auth.AuthenticationProvider} on that of others.
 * For example, the configuration of {@link com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider}
 * (i.e., to enable 2LO) depends the configuration of {@link com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider}
 * (i.e., OAuth being enabled).
 * </p>
 * <p>
 *     This dependency will work recursively. If A depends on B, and B depends on C, then A implicitly depends on C.
 *     Such dependency information could be used to perform clean up when removing one provider.
 * </p>
 *
 * @since 4.1.2
 */
@Documented
@Inherited
@Retention(RUNTIME)
@Target(TYPE)
public @interface DependsOn {
    Class<? extends AuthenticationProvider>[] value();
}