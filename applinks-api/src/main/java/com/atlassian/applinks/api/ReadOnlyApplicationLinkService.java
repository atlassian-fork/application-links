package com.atlassian.applinks.api;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nullable;

/**
 * Provides methods for retrieving {@link ReadOnlyApplicationLink}s representing linked applications (e.g. JIRA,
 * Confluence, etc.). The usage of this service is encouraged over {@link ApplicationLinkService}, especially if the
 * {@link ApplicationLink}'s property set is not used at all. Also, there is no need to cache any retrieved application
 * links for performance reasons. Simply query this service again.
 *
 * @since 4.0
 */
@ExperimentalApi
public interface ReadOnlyApplicationLinkService {
    /**
     * Retrieves all {@link ReadOnlyApplicationLink}s.
     *
     * @return an {@link Iterable} of stored {@link ReadOnlyApplicationLink}s, of all
     * {@link com.atlassian.applinks.api.ApplicationType}s.
     */
    Iterable<ReadOnlyApplicationLink> getApplicationLinks();

    /**
     * Retrieves an {@link ReadOnlyApplicationLink} by its {@link ApplicationId}. Use this method only if you know the
     * {@link ApplicationId} of an existing {@link ReadOnlyApplicationLink}. If you storing an {@link ApplicationId} for
     * future look-ups using this method, you should listen for the {@link com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent} to ensure
     * your stored {@link ApplicationId} is kept current.
     *
     * @param applicationId the {@link ApplicationId} of a stored {@link ReadOnlyApplicationLink}.
     * @return the {@link ReadOnlyApplicationLink} specified by the id, or {@code null} if it does not exist
     */
    @Nullable
    ReadOnlyApplicationLink getApplicationLink(ApplicationId applicationId);


    /**
     * Retrieves all {@link ReadOnlyApplicationLink}s of a particular {@link ApplicationType}.
     *
     * @param type the {@link Class} of the {@link ApplicationType}s to return
     * @return an {@link Iterable} containing all stored {@link ReadOnlyApplicationLink}s of the specified type.
     * The primary {@link ReadOnlyApplicationLink} is the first link in the list.
     */
    Iterable<ReadOnlyApplicationLink> getApplicationLinks(Class<? extends ApplicationType> type);

    /**
     * Retrieves the <strong>primary</strong> {@link ReadOnlyApplicationLink} of a particular {@link ApplicationType}. This
     * method should be used when you are implementing an integration feature that requires just <em>one</em> remote
     * entity, for example: determining which linked JIRA project to create an issue in, or which linked Confluence
     * space to create a page in. Features that require <em>all</em> {@link ReadOnlyApplicationLink}s of a particular
     * {@link ApplicationType} (like aggregating activity or searching) should use {@link #getApplicationLinks(Class)}.
     *
     * @param type an application type (e.g. "jira")
     * @return the primary {@link ApplicationLink} of the specified type
     */
    @Nullable
    ReadOnlyApplicationLink getPrimaryApplicationLink(Class<? extends ApplicationType> type);
}
