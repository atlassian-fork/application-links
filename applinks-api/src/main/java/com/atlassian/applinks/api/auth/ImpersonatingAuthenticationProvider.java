package com.atlassian.applinks.api.auth;

import com.atlassian.applinks.api.ApplicationLinkRequestFactory;

/**
 * Impersonating authentication providers allow the caller to specify a user, on his behalf the request
 * to the remote application is made. The remote application will return content / information that is visible
 * and accessible to the specified user. This mode of authentication therefore should be used when information
 * specific to user-context is required.
 *
 * @since 3.0
 */
public interface ImpersonatingAuthenticationProvider extends AuthenticationProvider {
    ApplicationLinkRequestFactory getRequestFactory(String username);
}
