package com.atlassian.applinks.api.auth;

/**
 * Marker interface for all Application Links authentication providers.
 *
 * @see ImpersonatingAuthenticationProvider
 * @see NonImpersonatingAuthenticationProvider
 * @since 3.0
 */
public interface AuthenticationProvider {
}
