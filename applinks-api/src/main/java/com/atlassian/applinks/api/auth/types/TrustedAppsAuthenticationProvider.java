package com.atlassian.applinks.api.auth.types;

import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;

/**
 * Atlassian Trusted App is an authentication mode which allows a user to execute requests as himself on the remote
 * application, assuming his account on the remote machine exists and has the same username as the user's username
 * on local.
 *
 * Pass this type to the {@link com.atlassian.applinks.api.ApplicationLink#createAuthenticatedRequestFactory(Class)}
 * method to obtain a {@link com.atlassian.sal.api.net.RequestFactory} instance
 * that uses the Trusted Apps protocol for transparent authentication.
 *
 * @see com.atlassian.applinks.api.ApplicationLink#createAuthenticatedRequestFactory(Class)
 * @since 3.0
 */
@Deprecated
public interface TrustedAppsAuthenticationProvider extends ImpersonatingAuthenticationProvider {
}
