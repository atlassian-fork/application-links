package com.atlassian.applinks.oauth.auth;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerInformationHelper;
import com.atlassian.oauth.Consumer;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import net.oauth.OAuth;
import net.oauth.OAuthMessage;

import java.util.List;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * A helper class that detects, if the remote application has the Atlassian OAuth plugin installed and can also fetch
 * the consumer information from this application.
 * <br>
 * since 3.0
 */
public class OAuthHelper {
    private OAuthHelper() {
    }

    public static boolean isOAuthPluginInstalled(final ApplicationLink applicationLink) {
        final boolean oAuthPluginInstalled = false;
        try {
            final Consumer consumer = fetchConsumerInformation(applicationLink);
            return consumer.getKey() != null;
        } catch (ResponseException e) {
            //ignored
        }
        return oAuthPluginInstalled;
    }

    /**
     * @param applicationLink link
     * @return consumer information from {@code applicationLink}
     * @throws ResponseException if the fetch fails
     * @deprecated use {@link ConsumerInformationHelper} instead
     */
    @Deprecated
    public static Consumer fetchConsumerInformation(final ApplicationLink applicationLink) throws ResponseException {
        return ConsumerInformationHelper.fetchConsumerInformation(applicationLink);
    }

    /**
     * Converts the {@code Request} to an {@code OAuthMessage}.
     *
     * @param request {@code Request} to be converted to an {@code OAuthMessage}
     * @return {@code OAuthMessage} converted from the {@code Request}
     */
    public static OAuthMessage asOAuthMessage(final com.atlassian.oauth.Request request) {
        requireNonNull(request, "request");
        return new OAuthMessage(
                request.getMethod().name(),
                request.getUri().toString(),
                // We'd rather not do the copy, but since we need a Collection of these things we don't have much choice
                ImmutableList.copyOf(asOAuthParameters(request.getParameters()))
        );
    }

    /**
     * Converts the list of {@code Request.Parameter}s to {@code OAuth.Parameter}s.
     *
     * @param requestParameters {@code Request.Parameter}s to be converted to {@code OAuth.Parameter}s
     * @return {@code OAuth.Parameter}s converted from the {@code Request.Parameter}s
     */
    public static Iterable<OAuth.Parameter> asOAuthParameters(final Iterable<com.atlassian.oauth.Request.Parameter> requestParameters) {
        requireNonNull(requestParameters, "requestParameters");
        return Iterables.transform(requestParameters, toOAuthParameters);
    }

    /**
     * Converts the list of {@code OAuth.Parameter}s to {@code Request.Parameter}s.
     *
     * @param oauthParameters {@code OAuth.Parameter}s to be converted to {@code Request.Parameter}s
     * @return {@code Request.Parameter}s converted from the {@code OAuth.Parameter}s
     */
    public static Iterable<com.atlassian.oauth.Request.Parameter> fromOAuthParameters(final List<? extends Map.Entry<String, String>> oauthParameters) {
        requireNonNull(oauthParameters, "oauthParameters");
        return Iterables.transform(oauthParameters, toRequestParameters);
    }

    private static final Function<Map.Entry<String, String>, com.atlassian.oauth.Request.Parameter> toRequestParameters = new Function<Map.Entry<String, String>, com.atlassian.oauth.Request.Parameter>() {
        public com.atlassian.oauth.Request.Parameter apply(final Map.Entry<String, String> p) {
            requireNonNull(p, "parameter");
            return new com.atlassian.oauth.Request.Parameter(p.getKey(), p.getValue());
        }
    };

    private static final Function<com.atlassian.oauth.Request.Parameter, OAuth.Parameter> toOAuthParameters = new Function<com.atlassian.oauth.Request.Parameter, OAuth.Parameter>() {
        public OAuth.Parameter apply(final com.atlassian.oauth.Request.Parameter p) {
            requireNonNull(p, "parameter");
            return new OAuth.Parameter(p.getName(), p.getValue());
        }
    };
}
