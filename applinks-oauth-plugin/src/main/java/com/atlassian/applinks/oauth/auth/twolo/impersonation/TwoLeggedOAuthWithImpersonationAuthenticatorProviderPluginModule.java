package com.atlassian.applinks.oauth.auth.twolo.impersonation;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.auth.OrphanedTrustAwareAuthenticatorProviderPluginModule;
import com.atlassian.applinks.core.auth.OrphanedTrustCertificate;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthAutoConfigurator;
import com.atlassian.applinks.core.ElevatedPermissionsService;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.oauth.auth.twolo.AbstractTwoLeggedOAuthAuthenticatorProviderPluginModule;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.consumer.ConsumerService;
import com.atlassian.sal.api.net.RequestFactory;

import java.util.concurrent.Callable;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.google.common.base.Throwables.propagateIfInstanceOf;

/**
 * Plugin module implemenation for 2-legged OAuth with Impersonation authentication.
 *
 * @since 3.10
 */
public class TwoLeggedOAuthWithImpersonationAuthenticatorProviderPluginModule
        extends AbstractTwoLeggedOAuthAuthenticatorProviderPluginModule
        implements OrphanedTrustAwareAuthenticatorProviderPluginModule

{
    private final AuthenticationConfigurationManager authenticationConfigurationManager;
    private final ConsumerService consumerService;
    private final OAuthAutoConfigurator oAuthAutoConfigurator;
    private final RequestFactory requestFactory;
    private final ServiceProviderStoreService serviceProviderStoreService;
    private final ElevatedPermissionsService elevatedPermissions;

    public TwoLeggedOAuthWithImpersonationAuthenticatorProviderPluginModule(
            AuthenticationConfigurationManager authenticationConfigurationManager,
            ConsumerService consumerService,
            OAuthAutoConfigurator oAuthAutoConfigurator,
            InternalHostApplication hostApplication,
            RequestFactory requestFactory,
            ServiceProviderStoreService serviceProviderStoreService,
            ElevatedPermissionsService elevatedPermissions) {
        super(hostApplication);
        this.authenticationConfigurationManager = authenticationConfigurationManager;
        this.consumerService = consumerService;
        this.requestFactory = requestFactory;
        this.oAuthAutoConfigurator = oAuthAutoConfigurator;
        this.serviceProviderStoreService = serviceProviderStoreService;
        this.elevatedPermissions = elevatedPermissions;
    }

    public AuthenticationProvider getAuthenticationProvider(final ApplicationLink link) {
        AuthenticationProvider provider = null;

        if (authenticationConfigurationManager.isConfigured(link.getId(), TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)) {
            provider = new TwoLeggedOAuthWithImpersonationAuthenticationProvider() {
                public ApplicationLinkRequestFactory getRequestFactory(String username) {
                    return new TwoLeggedOAuthWithImpersonationRequestFactoryImpl(link,
                            authenticationConfigurationManager,
                            consumerService,
                            requestFactory,
                            username);
                }
            };
        }
        return provider;
    }

    public boolean isApplicable(final AuthenticationScenario authenticationScenario, final ApplicationLink applicationLink) {
        return authenticationScenario.isCommonUserBase() && authenticationScenario.isTrusted();
    }


    @Override
    public boolean isApplicable(String certificateType) {
        return OrphanedTrustCertificate.Type.OAUTH.name().equals(certificateType);
    }

    public boolean incomingEnabled(final ApplicationLink applicationLink) {
        final Consumer consumer = serviceProviderStoreService.getConsumer(applicationLink);
        return consumer != null && consumer.getTwoLOAllowed() && consumer.getTwoLOImpersonationAllowed();
    }

    @Override
    public void enable(final RequestFactory authenticatedRequestFactory, final ApplicationLink applink)
            throws AuthenticationConfigurationException {
        try {
            // Used to elevate permissions while calling {@code OAuthAutoConfigurator} as it requires admin permission level. 
            elevatedPermissions.executeAs(PermissionLevel.SYSADMIN, new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    oAuthAutoConfigurator.enable(createOAuthWithImpersonationConfig(), applink, authenticatedRequestFactory);
                    return null;
                }
            });
        } catch (Exception e) {
            propagateIfInstanceOf(e, AuthenticationConfigurationException.class);
            throw new AuthenticationConfigurationException(e);
        }

    }

    @Override
    public void disable(RequestFactory authenticatedRequestFactory, ApplicationLink applicationLink)
            throws AuthenticationConfigurationException {
        // don't do anything, this will be handled by the OAuth provider plugin module
    }

    @Override
    public Class<? extends AuthenticationProvider> getAuthenticationProviderClass() {
        return TwoLeggedOAuthWithImpersonationAuthenticationProvider.class;
    }
}


