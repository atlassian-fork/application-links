package com.atlassian.applinks.oauth.auth.servlets.serviceprovider;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.core.rest.RestResourceTestUtils;
import com.atlassian.applinks.core.util.MessageFactory;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.applinks.ui.auth.AdminUIAuthenticator;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoManager;
import com.atlassian.sal.api.websudo.WebSudoSessionException;
import com.atlassian.sal.api.xsrf.XsrfTokenAccessor;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.PrintWriter;
import java.net.URI;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.Principal;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(Theories.class)
public class AddConsumerByUrlServletTest extends AbstractConsumerServletTest {
    @DataPoints
    public static String[] methods = new String[]{"GET", "POST"};

    @Mock
    I18nResolver i18nResolver;
    @Mock
    MessageFactory messageFactory;
    @Mock
    TemplateRenderer templateRenderer;
    @Mock
    WebResourceManager webResourceManager;
    @Mock
    ApplicationLinkService applicationLinkService;
    @Mock
    AdminUIAuthenticator adminUIAuthenticator;
    @Mock
    InternalHostApplication internalHostApplication;
    @Mock
    DocumentationLinker documentationLinker;
    @Mock
    LoginUriProvider loginUriProvider;
    @Mock
    WebSudoManager webSudoManager;
    @Mock
    RequestFactory requestFactory;
    @Mock
    ManifestRetriever manifestRetriever;
    @Mock
    ServiceProviderStoreService serviceProviderStoreService;
    @Mock
    XsrfTokenAccessor xsrfTokenAccessor;
    @Mock
    XsrfTokenValidator xsrfTokenValidator;
    @Mock
    UserManager userManager;

    @Mock
    ApplicationLink applicationLink;

    @Mock
    HttpServletRequest request;
    @Mock
    HttpServletResponse response;
    @Mock
    HttpSession session;
    @Mock
    PrintWriter writer;

    String applinkId = "f81d4fae-7dec-11d0-a765-00a0c91e6bf6";
    HttpServlet servlet;

    static KeyPair KEYS;

    static {
        try {
            KEYS = RSAKeys.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        when(request.getPathInfo()).thenReturn(applinkId);
        when(request.getServletPath()).thenReturn("servlet-path");
        when(xsrfTokenValidator.validateFormEncodedToken(request)).thenReturn(true);

        servlet = new AddConsumerByUrlServlet(i18nResolver, messageFactory, templateRenderer, webResourceManager,
                applicationLinkService, adminUIAuthenticator, requestFactory, manifestRetriever,
                internalHostApplication, serviceProviderStoreService, loginUriProvider,
                documentationLinker, webSudoManager, xsrfTokenAccessor, xsrfTokenValidator, userManager);
    }

    @Test
    public void verifyHalfDeletedApplinkIsHandledWithoutException() throws Exception {
        when(request.getMethod()).thenReturn("GET");
        whenUserIsLoggedInAsAdmin();
        when(applicationLinkService.getApplicationLink(new ApplicationId(applinkId))).thenReturn(applicationLink);
        when(applicationLink.getDisplayUrl()).thenReturn(new URI("http://localhost:2990/jira/"));
        when(applicationLink.getProperty(AbstractConsumerServlet.OAUTH_INCOMING_CONSUMER_KEY)).thenReturn(true);
        when(applicationLink.getType()).thenReturn(RestResourceTestUtils.getDummyApplicationType());
        when(internalHostApplication.getType()).thenReturn(RestResourceTestUtils.getDummyApplicationType());

        servlet.service(request, response);
    }

    @Theory
    public void verifyThatAdminAccessIsCheckedForAdminOnlyServlet(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();
        servlet.service(request, response);

        verify(adminUIAuthenticator).checkAdminUIAccessBySessionOrCurrentUser(request);
    }

    @Theory
    public void verifyThatWebSudoRequestIsExecutedForAdminOnlyServlet(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();

        servlet.service(request, response);

        verify(webSudoManager).willExecuteWebSudoRequest(request);
    }

    @Theory
    public void verifyWebSudoGivenControlWhenRequestRequiresSudoAuth(String method) throws Exception {
        when(request.getMethod()).thenReturn(method);
        whenUserIsLoggedInAsAdmin();

        doThrow(new WebSudoSessionException("blah")).when(webSudoManager).willExecuteWebSudoRequest(request);
        servlet.service(request, response);

        verify(webSudoManager).enforceWebSudoProtection(request, response);
    }

    @Test
    public void verifyAdminCanAlter2LOOptions() throws Exception {
        when(request.getMethod()).thenReturn("POST");
        whenUserIsLoggedInAsAdmin();
        when(request.getParameter("update-2lo")).thenReturn("update");

        when(userManager.getRemoteUsername()).thenReturn("betty");
        when(userManager.isSystemAdmin("betty")).thenReturn(false);
        when(applicationLinkService.getApplicationLink(any())).thenReturn(applicationLink);
        Consumer consumer = createDummyConsumer();

        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(consumer);
        when(i18nResolver.getText("auth.oauth.config.2lo.update.success")).thenReturn("success");

        servlet.service(request, response);
        verify(response).sendRedirect(any());
        verify(i18nResolver).getText("auth.oauth.config.2lo.update.success");
    }

    @Test
    public void verifySysadminCannotBe2LOExecutingUser() throws Exception {
        when(request.getMethod()).thenReturn("POST");
        whenUserIsLoggedInAsAdmin();
        when(request.getParameter("update-2lo")).thenReturn("update");
        when(request.getParameter("twoLoEnabled")).thenReturn("true");
        when(request.getParameter("twoLoExecuteAs")).thenReturn("admin");

        Principal admin = createPrincipal("admin");
        when(userManager.getRemoteUsername()).thenReturn("betty");
        when(userManager.resolve("admin")).thenReturn(admin);
        when(userManager.isSystemAdmin("admin")).thenReturn(true);
        when(applicationLinkService.getApplicationLink(any(ApplicationId.class))).thenReturn(applicationLink);
        Consumer consumer = createDummyConsumer();

        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(consumer);
        when(i18nResolver.getText("auth.oauth.config.2lo.username.cannot.be.sysadmin")).thenReturn("no sysadmin");

        servlet.service(request, response);
        verify(response).sendRedirect(any());
        verify(i18nResolver).getText("auth.oauth.config.2lo.username.cannot.be.sysadmin");
    }

    @Test
    public void verifyAdminCannotTurnOn2LOWithImpersonation() throws Exception {
        when(request.getMethod()).thenReturn("POST");
        whenUserIsLoggedInAsAdmin();
        when(request.getParameter("update-2lo")).thenReturn("update");
        when(request.getParameter("twoLoEnabled")).thenReturn("true");
        when(request.getParameter("twoLoExecuteAs")).thenReturn("betty");
        when(request.getParameter("twoLoImpersonationEnabled")).thenReturn("true");

        Principal betty = createPrincipal("betty");
        when(userManager.getRemoteUsername()).thenReturn("betty");
        when(userManager.resolve("betty")).thenReturn(betty);
        when(userManager.isSystemAdmin("betty")).thenReturn(false);
        when(applicationLinkService.getApplicationLink(any(ApplicationId.class))).thenReturn(applicationLink);
        when(messageFactory.newI18nMessage("applinks.error.only.sysadmin.operation")).thenReturn(new ErrorMessage("sysadmin only"));
        Consumer consumer = createDummyConsumer();

        when(serviceProviderStoreService.getConsumer(applicationLink)).thenReturn(consumer);

        doAnswerWithExpectedErrorMessage(templateRenderer, "sysadmin only");
        servlet.service(request, response);
        verify(messageFactory).newI18nMessage("applinks.error.only.sysadmin.operation");
    }

    private Consumer createDummyConsumer() {
        return Consumer.key("host-consumer")
                .name("Host Consumer")
                .description("description")
                .signatureMethod(Consumer.SignatureMethod.RSA_SHA1)
                .publicKey(KEYS.getPublic())
                .callback(URI.create("http://consumer/host-callback")).build();
    }

    private static Principal createPrincipal(String name) {
        Principal principal = mock(Principal.class);
        when(principal.getName()).thenReturn(name);

        return principal;
    }

    private void whenUserIsLoggedInAsAdmin() {
        when(adminUIAuthenticator.checkAdminUIAccessBySessionOrCurrentUser(request)).thenReturn(true);
    }
}