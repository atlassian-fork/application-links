package com.atlassian.applinks.oauth.test.matchers;

import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

public final class OAuthExceptionMatchers {
    private OAuthExceptionMatchers() {
        throw new AssertionError("Do not instantiate " + OAuthExceptionMatchers.class.getName());
    }

    @Nonnull
    public static Matcher<OAuthMessageProblemException> withOAuthProblem(@Nullable String expectedProblem,
                                                                         @Nullable String expectedAdvice) {
        return allOf(withOAuthProblem(expectedProblem), withOAuthAdvice(expectedAdvice));
    }

    @Nonnull
    public static Matcher<OAuthMessageProblemException> withOAuthProblem(@Nullable String expectedProblem) {
        return withOAuthProblemThat(is(expectedProblem));
    }

    @Nonnull
    public static Matcher<OAuthMessageProblemException> withOAuthProblemThat(@Nonnull Matcher<String> problemMatcher) {
        return new FeatureMatcher<OAuthMessageProblemException, String>(problemMatcher, "oAuthProblem", "OAuth problem") {
            @Override
            protected String featureValueOf(OAuthMessageProblemException actual) {
                return actual.getOAuthProblem();
            }
        };
    }

    @Nonnull
    public static Matcher<OAuthMessageProblemException> withOAuthAdvice(@Nullable String expectedAdvice) {
        return withOAuthAdviceThat(is(expectedAdvice));
    }

    @Nonnull
    public static Matcher<OAuthMessageProblemException> withOAuthAdviceThat(
            @Nonnull Matcher<String> problemAdviceMatcher) {
        return new FeatureMatcher<OAuthMessageProblemException, String>(
                problemAdviceMatcher, "oAuthAdvice", "OAuth problem advice") {
            @Override
            protected String featureValueOf(OAuthMessageProblemException actual) {
                return actual.getOAuthAdvice();
            }
        };
    }
}
