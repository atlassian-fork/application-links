package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Page representing the trusted apps redirect url.
 *
 * @since 3.11.0
 */
public class TrustedAppsRedirectPage extends ApplinkAbstractPage {
    @ElementBy(tagName = "body")
    PageElement body;

    public String getUrl() {
        return "/plugins/servlet/applinks/tests/trusted-redirect/test";
    }

    public String getTrustedResponse() {
        return body.getText();
    }
}
