package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import org.openqa.selenium.Keys;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.core.Is.is;

/**
 * Base page object to the incoming/outgoing local authentication drop-downs.
 *
 * @since 5.2
 */
public abstract class AbstractLocalAuthDropdown {
    @Inject
    protected PageElementFinder elementFinder;

    @Inject
    protected PageBinder pageBinder;

    public TimedCondition isDropdownVisible() {
        return Conditions.and(
                getReference().timed().isVisible(),
                getInputText().timed().isVisible()
        );
    }

    public TimedQuery<String> getSelectedText() {
        return getInputText().timed().getValue();
    }

    public TimedQuery<String> getSelectedValue() {
        return getReference().timed().getValue();
    }

    public void select(UiOAuthConfig config) {
        waitUntilTrue(isDropdownVisible());

        while (!getInputText().getValue().equals("")) {
            getInputText().type(Keys.BACK_SPACE);
        }

        getInputText().type(config.typingText());
        getInputText().type(Keys.RETURN);

        waitUntilTrue(isSelected(config));
    }

    public TimedCondition isSelected(UiOAuthConfig config) {
        return Conditions.and(
                Conditions.forMatcher(getSelectedValue(), is(config.getId())),
                Conditions.forMatcher(getSelectedText(), is(config.getText()))
        );
    }

    protected abstract PageElement getReference();

    protected abstract PageElement getInputText();
}
