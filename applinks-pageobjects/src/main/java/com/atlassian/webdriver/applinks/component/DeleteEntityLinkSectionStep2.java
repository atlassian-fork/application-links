package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static com.atlassian.webdriver.applinks.util.AuiBlanketUtil.waitUntilBlanketNotVisible;

public class DeleteEntityLinkSectionStep2 {
    @ElementBy(xpath = "//div[@id='delete-application-link-dialog']/div[2]/div[2]/button")
    private PageElement confirmButton;
    @ElementBy(className = "aui-blanket")
    private PageElement auiBlanket;

    public DeleteEntityLinkSectionStep2 confirm() {
        waitUntilTrue(confirmButton.timed().isVisible());
        confirmButton.click();
        waitUntilBlanketNotVisible(auiBlanket);
        return this;
    }
}