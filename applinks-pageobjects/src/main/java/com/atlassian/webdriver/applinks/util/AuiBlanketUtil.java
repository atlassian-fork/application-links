package com.atlassian.webdriver.applinks.util;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.WebDriverElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static com.atlassian.pageobjects.elements.query.Conditions.forSupplier;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;

public final class AuiBlanketUtil {

    private AuiBlanketUtil() {}

    public static void waitUntilBlanketNotVisible(final PageElement auiBlanket) {
        TimedCondition auiBlanketIsVisible = forSupplier(5000, () ->
                auiBlanket.isPresent() && !"hidden".equals(((WebDriverElement) auiBlanket).asWebElement().getCssValue("visibility")));
        waitUntilFalse(auiBlanketIsVisible);
    }
}
