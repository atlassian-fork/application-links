package com.atlassian.webdriver.applinks;

public enum AuthType {
    OAUTH,
    TRUSTED_APPS
}

