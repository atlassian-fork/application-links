package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import com.atlassian.webdriver.applinks.util.GetTextFunction;
import com.google.common.base.Supplier;
import com.google.common.collect.Sets;
import org.openqa.selenium.By;

import java.util.List;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;

import static com.google.common.collect.Lists.transform;

public class ApplicationDetailsSection extends ConfigureApplicationSection {
    @Inject
    protected Timeouts timeouts;

    @ElementBy(id = "applicationName")
    protected PageElement applicationNameInput;

    @ElementBy(id = "applicationTypeValue")
    protected PageElement applicationTypeText;

    @ElementBy(id = "rpc-url")
    protected PageElement rpcUrlText;

    @ElementBy(id = "display-url")
    protected PageElement displayUrlTextBox;

    @ElementBy(className = "update-validation-errors")
    protected PageElement updateValidationErrorsDiv;

    @Nullable
    public String getApplicationName() {
        return applicationNameInput.getValue();
    }

    @Nonnull
    public TimedQuery<String> getApplicationNameTimed() {
        return applicationNameInput.timed().getValue();
    }

    @Nullable
    public String getApplicationType() {
        return applicationTypeText.getText();
    }

    @Nonnull
    public TimedQuery<String> getApplicationTypeTimed() {
        return applicationTypeText.timed().getText();
    }

    @Nullable
    public String getApplicationUrl() {
        return rpcUrlText.getText();
    }

    @Nonnull
    public TimedQuery<String> getApplicationUrlTimed() {
        return rpcUrlText.timed().getText();
    }

    @Nullable
    public String getDisplayUrl() {
        return displayUrlTextBox.getValue();
    }

    @Nonnull
    public TimedQuery<String> getDisplayUrlTimed() {
        return displayUrlTextBox.timed().getValue();
    }

    @Nonnull
    public TimedQuery<List<String>> getErrorMessagesTimed() {
        return Queries.forSupplier(timeouts, errorMessagesSupplier());
    }

    @Nonnull
    public Set<String> getApplinkErrorMessages() {
        return Sets.newHashSet(errorMessagesSupplier().get());
    }

    @Nonnull
    public ApplicationDetailsSection setApplicationName(String applicationName) {
        applicationNameInput.clear();
        applicationNameInput.type(applicationName);
        return this;
    }

    @Nonnull
    public ApplicationDetailsSection setDisplayUrl(String displayUrl) {
        displayUrlTextBox.clear();
        displayUrlTextBox.type(displayUrl);
        return this;
    }

    @Nonnull
    public ListApplicationLinkPage save() {
        PageElement saveButton = FindUtils.findVisibleBy(By.cssSelector("#edit-application-link-dialog .wizard-submit"), elementFinder);
        saveButton.click();
        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    @Nonnull
    public ApplicationDetailsSection saveExpectingError() {
        PageElement saveButton = FindUtils.findVisibleBy(By.cssSelector("#edit-application-link-dialog .wizard-submit"), elementFinder);
        saveButton.click();

        Poller.waitUntilTrue(updateValidationErrorsDiv.timed().hasClass("fully-populated-errors"));

        return pageBinder.bind(ApplicationDetailsSection.class);
    }

    @Nonnull
    public ListApplicationLinkPage cancel() {
        FindUtils.findVisibleBy(By.className("applinks-cancel-link"), elementFinder).click();
        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    private Supplier<List<String>> errorMessagesSupplier() {
        return new Supplier<List<String>>() {
            public List<String> get() {
                return transform(getErrorElements(), new GetTextFunction());
            }
        };
    }

    private List<PageElement> getErrorElements() {
        return elementFinder.findAll(By.cssSelector("#edit-application-link-dialog .applinks-error"));
    }
}
