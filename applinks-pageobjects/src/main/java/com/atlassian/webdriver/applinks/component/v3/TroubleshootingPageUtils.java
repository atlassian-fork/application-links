package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.elements.timeout.Timeouts;
import org.openqa.selenium.WebDriver;

import java.util.Set;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Queries.forSupplier;
import static com.google.common.collect.Sets.difference;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;

/**
 * Provides utility methods for managing troubleshooting page when Applinks has misconfigured links between products.
 */
class TroubleshootingPageUtils {

    /**
     * This ensures that when calling click on an element, that a new window opens and that the window is closed after.
     * @param clickLambda the element to be clicked, which should open a new window.
     * @param driver that manages the web element being clicked.
     * @param timeouts customized timeout used when waiting for number of windows.
     */
    static void clickAndCloseNewWindow(Runnable clickLambda, WebDriver driver, Timeouts timeouts) {
        String oldWindowHandle = driver.getWindowHandle();
        Set<String> windowHandles = driver.getWindowHandles();
        clickLambda.run();
        Set<String> newHandles = waitUntil(forSupplier(timeouts, driver::getWindowHandles), hasSize(greaterThan(windowHandles.size())));
        String newWindowHandle = difference(newHandles, windowHandles)
                                    .stream()
                                    .findAny()
                                    .orElseThrow(() -> new AssertionError("Window was expected to open"));
        driver.switchTo().window(newWindowHandle).close();
        driver.switchTo().window(oldWindowHandle);
    }
}