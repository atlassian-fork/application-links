package com.atlassian.webdriver.applinks.externalpage;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.webdriver.Elements;
import org.openqa.selenium.By;

import javax.annotation.Nonnull;
import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Conditions.and;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static java.util.Objects.requireNonNull;

/**
 * Login page that can be used for all products.
 *
 * @since 4.3
 */
public class GenericLoginPage implements Page {
    public static final String DEFAULT_URL = "/login";
    public static final By DEFAULT_USERNAME_LOCATOR = By.id("j_username");
    public static final By DEFAULT_PASSWORD_LOCATOR = By.id("j_password");
    public static final By DEFAULT_SUBMIT_LOCATOR = By.id("submit");
    public static final By DEFAULT_WAIT_FOR_AFTER_LOGIN = By.tagName(Elements.TAG_BODY);

    @Inject
    protected PageElementFinder elementFinder;

    protected final String url;
    protected final By usernameLocator;
    protected final By passwordLocator;
    protected final By submitLocator;
    protected final By waitForAfterLogin;

    public GenericLoginPage(@Nonnull String url, @Nonnull By usernameLocator, @Nonnull By passwordLocator,
                            @Nonnull By submitLocator, @Nonnull By waitForAfterLogin) {
        this.url = requireNonNull(url, "url");
        this.usernameLocator = requireNonNull(usernameLocator, "usernameLocator");
        this.passwordLocator = requireNonNull(passwordLocator, "passwordLocator");
        this.submitLocator = requireNonNull(submitLocator, "submitLocator");
        this.waitForAfterLogin = requireNonNull(waitForAfterLogin, "waitForAfterLogin");
    }

    public GenericLoginPage() {
        this(DEFAULT_URL, DEFAULT_USERNAME_LOCATOR, DEFAULT_PASSWORD_LOCATOR,
                DEFAULT_SUBMIT_LOCATOR, DEFAULT_WAIT_FOR_AFTER_LOGIN);
    }

    @Override
    public String getUrl() {
        return url;
    }

    @WaitUntil
    private void waitUntilLoaded() {
        waitUntilTrue(isAt());
    }

    @Nonnull
    public TimedCondition isAt() {
        return and(getUsernameField().timed().isPresent(), getPasswordField().timed().isPresent());
    }

    public void login(@Nonnull String username, @Nonnull String password) {
        requireNonNull(username, "username");
        requireNonNull(password, "password");

        getUsernameField().type(username);
        getPasswordField().type(password);
        getSubmitButton().click();

        // wait until we're not here any more
        waitUntilFalse(isAt());
        waitUntilTrue(elementFinder.find(waitForAfterLogin).timed().isPresent());
    }

    @Nonnull
    protected PageElement getUsernameField() {
        return elementFinder.find(usernameLocator);
    }

    @Nonnull
    protected PageElement getPasswordField() {
        return elementFinder.find(passwordLocator);
    }

    @Nonnull
    protected PageElement getSubmitButton() {
        return elementFinder.find(submitLocator);
    }
}
