package com.atlassian.webdriver.applinks.component.v1;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.Options;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.SelectElement;
import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.ApplicationType;
import com.atlassian.webdriver.applinks.component.FindUtils;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import com.google.common.base.Supplier;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static org.apache.commons.lang3.StringUtils.isNotBlank;

public class AddApplicationLinkDialogNonUalStep2 {
    @Inject
    protected PageElementFinder elementFinder;
    @Inject
    protected PageBinder pageBinder;
    @Inject
    protected Timeouts timeouts;

    @ElementBy(name = "application-name")
    PageElement applicationNameTextBox;

    @ElementBy(name = "application-types")
    SelectElement applicationTypeSelect;

    @ElementBy(className = "non-ual-application-url")
    PageElement applicationUrlLabel;

    public AddApplicationLinkDialogNonUalStep2 setApplicationName(String applicationName) {
        applicationNameTextBox.clear();
        applicationNameTextBox.type(applicationName);

        return this;
    }

    public AddApplicationLinkDialogNonUalStep2 setApplicationType(ApplicationType applicationType) {
        applicationTypeSelect.select(Options.full(null, null, applicationType.value()));

        return this;
    }

    public ListApplicationLinkPage create() {
        getCreateButton().click();
        return pageBinder.bind(ListApplicationLinkPage.class);
    }

    public List<String> createExpectingError() {
        getCreateButton().click();
        return getErrors();
    }

    public TimedQuery<Iterable<String>> createExpectingErrorTimed() {
        getCreateButton().click();
        return getErrorsTimed();
    }

    private PageElement getCreateButton() {
        return FindUtils.findVisibleBy(By.className("wizard-submit"), elementFinder);
    }

    public TimedQuery<Iterable<String>> getErrorsTimed() {
        return Queries.forSupplier(timeouts, errorsSupplier());
    }

    public List<String> getErrors() {
        List<String> errors = new ArrayList<String>();
        if (elementFinder.find(By.id("application-name-error")).isPresent()) {
            PageElement warningAppName = FindUtils.findVisibleBy(By.id("application-name-error"), elementFinder);
            if (isNotBlank(warningAppName.getText())) {
                errors.add(warningAppName.getText());
            }
        }
        if (elementFinder.find(By.id("application-types-error")).isPresent()) {
            PageElement warningAppType = FindUtils.findVisibleBy(By.id("application-types-error"), elementFinder);
            if (isNotBlank(warningAppType.getText())) {
                errors.add(warningAppType.getText());
            }
        }
        List<PageElement> warningsDuplicateUrl = elementFinder.findAll(By.cssSelector("#add-application-link-dialog .create-non-ual-errors.applinks-error"));
        for (PageElement warning : warningsDuplicateUrl) {
            if (isNotBlank(warning.getText())) {
                errors.add(warning.getText());
            }
        }
        //Clear out duplicate messages, since "#add-application-link-dialog .applinks-error" may return the same error
        return errors;
    }

    private Supplier<Iterable<String>> errorsSupplier() {
        return new Supplier<Iterable<String>>() {
            public Iterable<String> get() {
                return getErrors();
            }
        };
    }

    public String getApplicationUrl() {
        return applicationUrlLabel.getText();
    }
}
