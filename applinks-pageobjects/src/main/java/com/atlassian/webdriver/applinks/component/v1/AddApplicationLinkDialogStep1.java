package com.atlassian.webdriver.applinks.component.v1;

import java.util.LinkedList;
import java.util.List;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.applinks.component.FindUtils;

import org.openqa.selenium.By;

public class AddApplicationLinkDialogStep1 extends AbstractAddApplicationLinkDialogStep {
    @ElementBy(id = "application-url")
    PageElement applicationUrlTextBox;

    @ElementBy(cssSelector = "#add-application-link-dialog .applinks-next-button")
    PageElement nextButton;

    @ElementBy(cssSelector = "#add-application-link-dialog .applinks-error")
    PageElement errors;

    public AddApplicationLinkDialogStep1 setApplicationUrl(String applicationUrl) {
        Poller.waitUntilTrue(applicationUrlTextBox.timed().isVisible());

        applicationUrlTextBox.clear();
        applicationUrlTextBox.type(applicationUrl);

        return this;
    }

    public AddApplicationLinkDialogStep2 nextExpectsUalStep2() {
        nextButton.click();
        Poller.waitUntilTrue(FindUtils.isAtLeastOneElementVisibleCondition(By.className("step-2-ual-header"), elementFinder, TimeoutType.PAGE_LOAD));

        return pageBinder.bind(AddApplicationLinkDialogStep2.class);
    }

    public AddApplicationLinkDialogNonUalStep2 nextExpectsNonUalStep2() {
        nextButton.click();
        Poller.waitUntilTrue(FindUtils.isAtLeastOneElementVisibleCondition(By.className("step-2-non-ual-header"), elementFinder, TimeoutType.PAGE_LOAD));

        return pageBinder.bind(AddApplicationLinkDialogNonUalStep2.class);
    }

    /**
     * If the url is not reachable, a confirmation is requested to the user.
     * Calling nextExpectsNonUalStep2(false) is equivalent to calling nextExpectsNonUalStep2()
     *
     * @param requiresConfirmation true if the host is not reachable
     */
    public AddApplicationLinkDialogNonUalStep2 nextExpectsNonUalStep2(boolean requiresConfirmation) {
        AddApplicationLinkDialogStep1 step1 = this;
        if (requiresConfirmation) {
            step1 = step1.nextExpectingError();
        }
        return step1.nextExpectsNonUalStep2();
    }

    public AddApplicationLinkDialogStep1 nextExpectingError() {
        nextButton.click();
        // Wait for single error element
        Poller.waitUntilTrue(errors.withTimeout(TimeoutType.PAGE_LOAD).timed().isVisible());

        return this;
    }

    public List<String> getErrors() {
        List<String> errors = new LinkedList<String>();
        for (PageElement element : elementFinder.findAll(By.cssSelector("#add-application-link-dialog .applinks-error"))) {
            errors.add(element.getText());
        }
        return errors;
    }
}