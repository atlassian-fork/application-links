package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;

import javax.inject.Inject;

/***
 * Represents the Applink's websudo equivalent.
 */
public class AppLinkAdminLogin<T> extends AbstractApplinkChainedComponent<T> {
    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "al_username")
    private PageElement usernameField;

    @ElementBy(id = "al_password")
    private PageElement passwordField;

    @ElementBy(id = "login-btn1")
    private PageElement submitButton;

    @ElementBy(id = "sysadmin-required-text")
    private PageElement sysAdminRequiredText;

    @ElementBy(id = "admin-required-text")
    private PageElement adminRequiredText;

    public AppLinkAdminLogin(T nextPage) {
        super(nextPage);
    }

    public T handleWebLoginIfRequired(String username, String password) {
        // enter username and password only if they are present.
        if (usernameField.isPresent()) {
            usernameField.type(username);
            passwordField.type(password);
            submitButton.click();
        }
        return nextPage;
    }

    public boolean isAskingForSysadmin() {
        return sysAdminRequiredText.isPresent();
    }

    public boolean isAskingForAdmin() {
        return adminRequiredText.isPresent();
    }
}