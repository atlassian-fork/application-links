package com.atlassian.webdriver.applinks.rest;

import com.atlassian.pageobjects.elements.query.Queries;
import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import com.google.common.base.Supplier;

import javax.inject.Inject;

/**
 * * This class can be used to make a rest set calls
 *
 * @since v5.0
 */
public class SetEntityLinkEndpoint extends ApplinkAbstractPage {
    // WRONG WRONG WRONG. let's write Backdoors for REST calls and not use WebDriver please.

    @Inject
    protected Timeouts timeouts;

    private final String entityKey;
    private final String propertyKey;
    private final String propertyValue;

    public SetEntityLinkEndpoint(String entityKey, String propertyKey, String propertyValue) {
        this.entityKey = entityKey;
        this.propertyKey = propertyKey;
        this.propertyValue = propertyValue;
    }

    public String getUrl() {
        return "/rest/applinks-tests/1/properties/setEntityLink/?entityKey=" + entityKey + "&propertyKey=" + propertyKey + "&propertyValue=" + propertyValue;
    }

    public TimedQuery<String> getResponse() {
        return Queries.forSupplier(timeouts, new Supplier<String>() {
            public String get() {
                return driver.getPageSource();
            }
        });
    }
}
