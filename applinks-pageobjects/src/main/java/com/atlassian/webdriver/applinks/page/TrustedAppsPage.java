package com.atlassian.webdriver.applinks.page;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

/**
 * Page object representing a normal request using TA.
 *
 * @since 3.11.0
 */
public class TrustedAppsPage extends ApplinkAbstractPage {
    @ElementBy(tagName = "body")
    PageElement body;

    public String getUrl() {
        return "/plugins/servlet/applinks/tests/trusted/get";
    }

    public String getTrustedResponse() {
        Poller.waitUntilTrue(body.timed().isPresent());
        return body.getText();
    }
}
