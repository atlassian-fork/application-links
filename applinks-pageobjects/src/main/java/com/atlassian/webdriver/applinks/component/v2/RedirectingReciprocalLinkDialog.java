package com.atlassian.webdriver.applinks.component.v2;

/**
 * Represents the Reciprocal Redirection dialog in the v2 workflow.
 *
 * @since v4.0.0
 */
public class RedirectingReciprocalLinkDialog extends PauseDialog {
}
