package com.atlassian.webdriver.applinks.page.v3;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class IncomingConnectivityDialog extends ConnectivityDialog {
    @ElementBy(id = "oauth-mismatch-left-dialog")
    private PageElement dialog;

    @Override
    protected PageElement getDialog() {
        return dialog;
    }
}
