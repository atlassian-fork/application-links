package com.atlassian.webdriver.applinks.component.v3;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

/**
 * Maps to the outgoing local authentication drop-down in the applinks edit page.
 *
 * @since 5.2
 */
public class OutgoingLocalAuthDropdown extends AbstractLocalAuthDropdown {
    public static final String DROPDOWN_SELECTOR = "applink-outgoing-local-auth-dropdown";

    @ElementBy(className = DROPDOWN_SELECTOR)
    private PageElement localAuthDropdown;

    @ElementBy(cssSelector = "." + DROPDOWN_SELECTOR + " input")
    private PageElement localAuthInput;

    @Override
    protected PageElement getReference() {
        return localAuthDropdown;
    }

    @Override
    protected PageElement getInputText() {
        return localAuthInput;
    }
}
