package com.atlassian.webdriver.applinks.rest;

import com.atlassian.pageobjects.elements.query.TimedQuery;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import com.google.common.base.Supplier;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Queries.forSupplier;

/**
 * This class can be used to make a rest get calls
 *
 * @since v5.0
 */
public class GetEntityLinkEndpoint extends ApplinkAbstractPage {
    // WRONG WRONG WRONG. let's write Backdoors for REST calls and not use WebDriver please.

    @Inject
    protected Timeouts timeouts;

    private final String entityKey;
    private final String propertyKey;

    public GetEntityLinkEndpoint(String entityKey, String propertyKey) {
        this.entityKey = entityKey;
        this.propertyKey = propertyKey;
    }

    public String getUrl() {
        return "/rest/applinks-tests/1/properties/getEntityLink/?entityKey=" + entityKey + "&propertyKey=" + propertyKey;
    }

    public TimedQuery<String> getResponse() {
        return forSupplier(timeouts, new Supplier<String>() {
            public String get() {
                return driver.getPageSource();
            }
        });
    }
}
