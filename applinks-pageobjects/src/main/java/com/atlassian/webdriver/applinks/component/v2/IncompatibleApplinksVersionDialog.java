package com.atlassian.webdriver.applinks.component.v2;

import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;

import org.openqa.selenium.By;

/**
 * Represents the dialog shown to a user when they attempt to connect to a pre v4.0.0 applinks.
 *
 * @since v4.0.0
 */
public class IncompatibleApplinksVersionDialog extends InformationDialog {

    public void clickRedirectToRemoteApp() {
        Poller.waitUntilTrue(this.informationMessage.timed().isPresent());
        PageElement redirectLink = this.informationMessage.find(By.tagName("a"));
        Poller.waitUntilTrue(redirectLink.timed().isPresent());
        redirectLink.click();
    }
}
