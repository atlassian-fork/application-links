package com.atlassian.webdriver.applinks.page.v2;

/**
 * Represents the V2 Applinks config screen accessed via the legacy, now removed Confluence action.
 *
 * @since 4.3
 */
public class LegacyConfluenceApplicationLinkPage extends ListApplicationLinkPage {
    @Override
    public String getUrl() {
        return "/admin/listapplicationlinks.action";
    }
}
