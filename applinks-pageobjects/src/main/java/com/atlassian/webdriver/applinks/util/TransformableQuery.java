package com.atlassian.webdriver.applinks.util;

import com.atlassian.pageobjects.elements.query.AbstractTimedQuery;
import com.atlassian.pageobjects.elements.query.ExpirationHandler;
import com.atlassian.pageobjects.elements.query.PollingQuery;
import com.atlassian.pageobjects.elements.query.TimedQuery;

import java.util.function.Function;
import java.util.function.Supplier;
import javax.annotation.Nonnull;

/**
 * {@link TimedQuery} that can be mapped to another result type, inheriting the original query's polling
 * characteristics.
 *
 * @since 5.2
 */
public class TransformableQuery<T> extends AbstractTimedQuery<T> {
    private final Supplier<T> value;

    public TransformableQuery(@Nonnull TimedQuery<T> original) {
        this(original, original::now);
    }

    public TransformableQuery(@Nonnull PollingQuery original, @Nonnull Supplier<T> value) {
        super(original, ExpirationHandler.RETURN_CURRENT);
        this.value = value;
    }

    public static <U> TransformableQuery<U> asTransformable(@Nonnull TimedQuery<U> original) {
        return original instanceof TransformableQuery ? (TransformableQuery<U>) original :
                new TransformableQuery<>(original);
    }

    @Nonnull
    public <R> TransformableQuery<R> map(@Nonnull Function<? super T, ? extends R> mapper) {
        return new TransformableQuery<>(this, () -> mapper.apply(currentValue()));
    }

    @Override
    protected T currentValue() {
        return value.get();
    }

    @Override
    protected boolean shouldReturn(T currentEval) {
        return true;
    }
}
