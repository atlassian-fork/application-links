package com.atlassian.webdriver.applinks.component;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.AtlassianWebDriver;
import com.atlassian.webdriver.applinks.page.ConfigureEntityLinksPage;

import javax.inject.Inject;

public class AddEntityLinkSectionNonUal extends AbstractApplinkChainedComponent<ConfigureEntityLinksPage> {
    @Inject
    private AtlassianWebDriver webDriver;

    @Inject
    private PageBinder pageBinder;

    @Inject
    private PageElementFinder elementFinder;

    @ElementBy(id = "add-non-ual-entity-link-entity")
    private PageElement keyField;

    @ElementBy(id = "add-non-ual-entity-link-alias")
    private PageElement nameField;

    @ElementBy(xpath = "(//div[@id='add-entity-link-wizard']//button[text()='Create'])[2]")
    private PageElement submitButton;

    public AddEntityLinkSectionNonUal(ConfigureEntityLinksPage nextPage) {
        super(nextPage);
    }

    public ConfigureEntityLinksPage create() {
        Poller.waitUntilTrue(submitButton.timed().isVisible());
        submitButton.click();
        return nextPage;
    }

    public AddEntityLinkSectionNonUal setKey(String key) {
        Poller.waitUntilTrue(keyField.timed().isVisible());
        keyField.clear();
        keyField.type(key);
        return this;
    }

    public AddEntityLinkSectionNonUal setName(String name) {
        Poller.waitUntilTrue(nameField.timed().isVisible());
        nameField.clear();
        nameField.type(name);
        return this;
    }
}