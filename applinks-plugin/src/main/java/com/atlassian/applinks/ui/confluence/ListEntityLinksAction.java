package com.atlassian.applinks.ui.confluence;

import com.atlassian.applinks.analytics.EntityLinksAdminViewEvent;
import com.atlassian.applinks.ui.velocity.ListEntityLinksContext;
import com.atlassian.applinks.ui.velocity.VelocityContextFactory;
import com.atlassian.confluence.event.Evented;
import com.atlassian.confluence.spaces.actions.AbstractSpaceAdminAction;
import com.atlassian.xwork.HttpMethod;
import com.atlassian.xwork.PermittedMethods;
import com.opensymphony.webwork.ServletActionContext;

import javax.servlet.http.HttpServletRequest;

/**
 * @since 3.0
 */
public class ListEntityLinksAction extends AbstractSpaceAdminAction implements Evented<EntityLinksAdminViewEvent> {
    private final VelocityContextFactory velocityContextFactory;

    private ListEntityLinksContext context;
    private String typeId;

    public ListEntityLinksAction(final VelocityContextFactory velocityContextFactory) {
        this.velocityContextFactory = velocityContextFactory;
    }

    @PermittedMethods(HttpMethod.GET)
    @Override
    public String execute() {
        return SUCCESS;
    }

    public ListEntityLinksContext getApplinksContext() {
        if (context == null) {
            final HttpServletRequest request = ServletActionContext.getRequest();
            context = velocityContextFactory.buildListEntityLinksContext(request, typeId, key);
        }
        return context;
    }

    @Override
    public EntityLinksAdminViewEvent getEventToPublish(String status) {
        return new EntityLinksAdminViewEvent(typeId, key);
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
}
