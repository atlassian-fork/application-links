package com.atlassian.applinks.internal.rest.applink.data;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.core.webfragment.WebFragmentContext;
import com.atlassian.applinks.core.webfragment.WebFragmentHelper;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Provides web items associated with an applink.
 *
 * @since 5.2
 */
public class WebItemsDataProvider extends AbstractSingleKeyRestApplinkDataProvider {
    public static final String WEB_ITEMS = "webItems";

    private final WebFragmentHelper webFragmentHelper;

    @Autowired
    public WebItemsDataProvider(WebFragmentHelper webFragmentHelper) {
        super(WEB_ITEMS);
        this.webFragmentHelper = webFragmentHelper;
    }

    @Nullable
    @Override
    public Object doProvide(@Nonnull ApplicationLink applink) throws ServiceException {
        // call getItems() to remove the unnecessary wrapper entity
        return webFragmentHelper.getWebItemsForLocation(WebFragmentHelper.APPLICATION_LINK_LIST_OPERATION,
                new WebFragmentContext.Builder().applicationLink(applink).build()).getItems();
    }
}
