package com.atlassian.applinks.internal.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Servlet filter that supports old Confluence action URL for application links list, for backwards compatibility.
 *
 * @since 4.3
 */
public class ConfluenceLegacyApplicationLinksUrlFilter implements Filter {
    private static final String LIST_APPLINKS_URL = "/plugins/servlet/applinks/listApplicationLinks";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        // dispatch to the standard URL
        request.getRequestDispatcher(LIST_APPLINKS_URL).forward(request, response);
    }

    @Override
    public void destroy() {
    }
}
