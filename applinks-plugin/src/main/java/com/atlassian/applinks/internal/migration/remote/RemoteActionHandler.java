package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

class RemoteActionHandler implements ResponseHandler<Response> {
    private boolean successful;
    private Optional<String> responseBody = Optional.empty();
    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteActionHandler.class);

    @Override
    public void handle(final Response response) throws ResponseException {
        successful = response.getStatusCode() >= 200 && response.getStatusCode() < 300;
        responseBody = Optional.of(response.getResponseBodyAsString());
        LOGGER.debug("Status: " + response.getStatusCode());
        LOGGER.debug(responseBody.get());
    }

    protected boolean isSuccessful() {
        return successful;
    }

    public Optional<String> getResponse() {
        return responseBody;
    }
}
