package com.atlassian.applinks.internal.status.oauth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.auth.oauth.ConsumerTokenStoreService;
import com.atlassian.applinks.internal.common.auth.oauth.OAuthConfigurator;
import com.atlassian.applinks.internal.common.auth.oauth.ServiceProviderStoreService;
import com.atlassian.applinks.internal.common.exception.ConsumerInformationUnavailableException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceExceptionFactory;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.google.common.annotations.VisibleForTesting;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.common.i18n.I18nKey.newI18nKey;

public class DefaultOAuthStatusService implements OAuthStatusService {
    private static final I18nKey TWOLOI_OPERATION_KEY = newI18nKey("applinks.service.permission.operation.twoloi");
    private static final I18nKey CHANGE_OAUTH_OPERATION_KEY = newI18nKey("applinks.service.permission.operation.changeoauth");

    private final ApplinkHelper applinkHelper;
    private final PermissionValidationService permissionValidationService;
    private final OAuthConfigurator oAuthConfigurator;

    @Autowired
    public DefaultOAuthStatusService(ApplinkHelper applinkHelper,
                                     AuthenticationConfigurationManager authenticationConfigurationManager,
                                     ConsumerTokenStoreService consumerTokenStoreService,
                                     PermissionValidationService permissionValidationService,
                                     ServiceProviderStoreService serviceProviderStoreService,
                                     ServiceExceptionFactory serviceExceptionFactory) {
        this.applinkHelper = applinkHelper;
        this.permissionValidationService = permissionValidationService;

        this.oAuthConfigurator = new OAuthConfigurator(
                authenticationConfigurationManager,
                consumerTokenStoreService,
                serviceProviderStoreService,
                serviceExceptionFactory
        );
    }

    @VisibleForTesting
    public DefaultOAuthStatusService(ApplinkHelper applinkHelper,
                                     PermissionValidationService permissionValidationService,
                                     OAuthConfigurator oAuthConfigurator) {
        this.applinkHelper = applinkHelper;
        this.permissionValidationService = permissionValidationService;
        this.oAuthConfigurator = oAuthConfigurator;
    }

    @Nonnull
    @Override
    public ApplinkOAuthStatus getOAuthStatus(@Nonnull ApplicationId id) throws NoSuchApplinkException {
        return getOAuthStatus(applinkHelper.getApplicationLink(id));
    }

    @Nonnull
    @Override
    public ApplinkOAuthStatus getOAuthStatus(@Nonnull ApplicationLink link) {
        return new ApplinkOAuthStatus(
                oAuthConfigurator.getIncomingConfig(link),
                oAuthConfigurator.getOutgoingConfig(link)
        );
    }

    @Override
    public void updateOAuthStatus(@Nonnull ApplicationId id, @Nonnull ApplinkOAuthStatus status)
            throws NoSuchApplinkException, NoAccessException, ConsumerInformationUnavailableException {
        updateOAuthStatus(applinkHelper.getApplicationLink(id), status);
    }

    @Override
    public void updateOAuthStatus(@Nonnull ApplicationLink applink, @Nonnull ApplinkOAuthStatus status)
            throws NoAccessException, ConsumerInformationUnavailableException {
        validatePermissions(status);

        oAuthConfigurator.updateIncomingConfig(applink, status.getIncoming());
        oAuthConfigurator.updateOutgoingConfig(applink, status.getOutgoing());
    }

    private void validatePermissions(ApplinkOAuthStatus status) throws NoAccessException {
        if (status.getIncoming().isTwoLoImpersonationEnabled() || status.getOutgoing().isTwoLoImpersonationEnabled()) {
            // 2LOi requires sysadmin
            permissionValidationService.validateSysadmin(TWOLOI_OPERATION_KEY);
        } else {
            permissionValidationService.validateAdmin(CHANGE_OAUTH_OPERATION_KEY);
        }
    }
}
