package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.sal.api.net.ResponseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.IOException;

class TryWithCredentials extends TryWithAuthentication {
    private static final Logger LOGGER = LoggerFactory.getLogger(TryWithCredentials.class);
    private final TryWithAuthentication tryWithAuthentication;

    public TryWithCredentials(final TryWithAuthentication tryWithAuthentication) {
        this.tryWithAuthentication = tryWithAuthentication;
    }

    @Override
    protected boolean execute(@Nonnull final ApplicationLink applicationLink,
                              @Nonnull final ApplicationId applicationId,
                              @Nonnull final ApplicationLinkRequestFactory factory)
            throws IOException, ResponseException, AuthenticationConfigurationException {
        return false;
    }

    boolean execute(@Nonnull final ApplicationLink applicationLink,
                    @Nonnull final ApplicationId applicationId,
                    @Nonnull final Class<? extends AuthenticationProvider> providerClass)
            throws IOException, ResponseException, AuthenticationConfigurationException {
        try {
            return tryWithAuthentication.execute(applicationLink, applicationId, providerClass);
        } catch (CredentialsRequiredException ex) {
            LOGGER.warn(ex.getMessage());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.error("Failed to execute remote action {}", tryWithAuthentication.getClass().getSimpleName(), ex);
            }
            return false;
        }
    }

}
