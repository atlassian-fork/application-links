package com.atlassian.applinks.internal.rest.interceptor;

import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.plugins.rest.common.interceptor.MethodInvocation;
import com.atlassian.plugins.rest.common.interceptor.ResourceInterceptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.net.ServiceExceptionHttpMapper.getStatus;
import static com.atlassian.applinks.internal.common.rest.util.RestErrorsFactory.fromException;
import static com.atlassian.applinks.internal.status.error.ApplinkErrors.findCauseOfType;

/**
 * Transforms service exceptions into corresponding REST errors.
 *
 * @since 4.3
 */
public class ServiceExceptionInterceptor implements ResourceInterceptor {
    private static final Logger log = LoggerFactory.getLogger(ServiceExceptionInterceptor.class);

    @Override
    public void intercept(MethodInvocation invocation) throws IllegalAccessException, InvocationTargetException {
        try {
            invocation.invoke();
        } catch (InvocationTargetException e) {
            ServiceException serviceException = findServiceCause(e);
            if (serviceException != null) {
                log.debug("ServiceException intercepted from a REST call to {}",
                        invocation.getHttpContext().getRequest().getRequestUri(), e);
                invocation.getHttpContext().getResponse().setResponse(createResponse(serviceException));
            } else {
                // propagate the exception
                throw e;
            }
        }
    }

    private static ServiceException findServiceCause(InvocationTargetException e) {
        return findCauseOfType(e, ServiceException.class);
    }

    private static Response createResponse(ServiceException serviceException) {
        Status status = getStatus(serviceException);
        return Response
                .status(status)
                .entity(fromException(status, serviceException))
                .build();
    }
}
