package com.atlassian.applinks.internal.rest.capabilities;

import com.atlassian.applinks.api.ApplicationId;

import com.atlassian.applinks.internal.capabilities.ApplinksCapabilitiesService;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.rest.util.RestEnumParser;
import com.atlassian.applinks.internal.common.rest.util.RestResponses;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.interceptor.ServiceExceptionInterceptor;
import com.atlassian.applinks.internal.rest.model.capabilities.RestRemoteApplicationCapabilities;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.message.I18nResolver;
import com.sun.jersey.spi.resource.Singleton;

import java.util.EnumSet;
import java.util.concurrent.TimeUnit;
import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser.parseApplicationId;
import static com.google.common.base.Strings.nullToEmpty;
import static javax.ws.rs.core.Response.ok;

@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path(ApplinksCapabilitiesResource.CONTEXT)
@Singleton
@InterceptorChain({ServiceExceptionInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplinksCapabilitiesResource {
    public static final String CONTEXT = "capabilities";

    private static final String PARAM_APPLINK_ID = "applinkId";
    private static final String PARAM_CAPABILITY = "capability";
    private static final String PARAM_MAX_AGE = "maxAge";
    private static final String PARAM_TIME_UNIT = "timeUnit";
    private static final String NOT_FOUND_MESSAGE_KEY = "applinks.rest.capabilities.notfound";
    private final ApplinksCapabilitiesService capabilitiesService;
    private final RemoteCapabilitiesService remoteCapabilitiesService;
    private final I18nResolver i18nResolver;
    private final RestEnumParser<ApplinksCapabilities> restEnumParser;
    private final RestEnumParser<TimeUnit> timeUnitParser;

    public ApplinksCapabilitiesResource(ApplinksCapabilitiesService capabilitiesService,
                                        RemoteCapabilitiesService remoteCapabilitiesService,
                                        I18nResolver i18nResolver) {
        this.capabilitiesService = capabilitiesService;
        this.remoteCapabilitiesService = remoteCapabilitiesService;
        this.i18nResolver = i18nResolver;
        this.restEnumParser = new RestEnumParser<>(ApplinksCapabilities.class, i18nResolver, NOT_FOUND_MESSAGE_KEY,
                Status.NOT_FOUND);
        this.timeUnitParser = new RestEnumParser<>(TimeUnit.class, i18nResolver);
    }

    /**
     * @return REST URL builder for the "all capabilities" resource
     */
    @Nonnull
    public static RestUrlBuilder capabilitiesUrl() {
        return new RestUrlBuilder().addPath(CONTEXT);
    }

    /**
     * @param capability capability to query for
     * @return REST URL builder for a specific capability
     */
    @Nonnull
    public static RestUrlBuilder capabilityUrl(@Nonnull ApplinksCapabilities capability) {
        return capabilitiesUrl().addPath(capability.name());
    }

    @GET
    public Response getAllCapabilities() {
        return ok(capabilitiesService.getCapabilities()).build();
    }

    @GET
    @Path("/{capability}")
    public Response hasCapability(@PathParam(PARAM_CAPABILITY) String capabilityName) {
        // 404 or 200 depending on whether the capability exists
        capabilityName = nullToEmpty(capabilityName);
        ApplinksCapabilities capability = restEnumParser.parseEnumParameter(capabilityName, PARAM_CAPABILITY);

        return (capabilitiesService.getCapabilities().contains(capability)) ?
                ok(EnumSet.of(capability)).build() :
                RestResponses.error(Status.NOT_FOUND, i18nResolver.getText(NOT_FOUND_MESSAGE_KEY, capabilityName));
    }

    @GET
    @Path("/remote/{applinkId}")
    public Response getRemoteCapabilities(@PathParam(PARAM_APPLINK_ID) String id, @QueryParam(PARAM_MAX_AGE) Long maxAge,
                                          @QueryParam(PARAM_TIME_UNIT) String timeUnit)
            throws ServiceException {
        ApplicationId applicationId = parseApplicationId(id);

        RemoteApplicationCapabilities capabilities = maxAge != null ?
                remoteCapabilitiesService.getCapabilities(applicationId, maxAge,
                        timeUnitParser.parseEnumParameter(timeUnit, TimeUnit.MINUTES, PARAM_TIME_UNIT)) :
                remoteCapabilitiesService.getCapabilities(applicationId);

        return ok(new RestRemoteApplicationCapabilities(capabilities)).build();
    }
}
