package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.v1.rest.ApplicationLinkResource;
import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.net.ResponseContentException;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.rest.RestUrlBuilder;
import com.atlassian.applinks.internal.rest.RestVersion;
import com.atlassian.applinks.internal.rest.client.AuthorisationUriAwareRequest;
import com.atlassian.applinks.internal.rest.client.RestRequestBuilder;
import com.atlassian.applinks.internal.rest.model.auth.compatibility.RestApplicationLinkAuthentication;
import com.atlassian.applinks.internal.rest.model.auth.compatibility.RestAuthenticationProvider;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.OAuthConfigs;
import com.atlassian.applinks.internal.status.remote.ApplinkStatusAccessException;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableSet;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Set;

import static com.atlassian.applinks.internal.common.net.ResponsePreconditions.checkStatusOk;
import static com.google.common.collect.Iterables.isEmpty;
import static com.google.common.collect.Iterables.transform;
import static java.util.Objects.requireNonNull;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;

/**
 * Fetch OAuth status using the Applink Authentication REST resource, which is located in different places in 4.x vs
 * 5.x.
 *
 * @since 5.0
 */
abstract class ApplinkAuthenticationOAuthFetchStrategy implements OAuthStatusFetchStrategy {
    private static final String INSUFFICIENT_PERMISSION_MESSAGE =
            "Admin permission required to access remote application link authentication";

    private final Class<? extends AuthenticationProvider> authenticationProvider;
    private final OAuthConnectionVerifier oAuthConnectionVerifier;

    protected ApplinkAuthenticationOAuthFetchStrategy(
            @Nonnull Class<? extends AuthenticationProvider> authenticationProvider,
            @Nonnull OAuthConnectionVerifier oAuthConnectionVerifier) {
        this.authenticationProvider = requireNonNull(authenticationProvider, "authenticationProvider");
        this.oAuthConnectionVerifier = requireNonNull(oAuthConnectionVerifier, "oAuthConnectionVerifier");
    }

    @Nullable
    @Override
    public ApplinkOAuthStatus fetch(@Nonnull ApplicationId localId, @Nonnull ApplicationLink applink)
            throws ApplinkStatusException, ResponseException {
        try {
            AuthorisationUriAwareRequest request = new RestRequestBuilder(applink)
                    .authentication(authenticationProvider)
                    .url(getAuthenticationUrl(localId))
                    .build();
            return request.execute(createRequestHandler(request, applink));
        } catch (CredentialsRequiredException e) {
            throw new ApplinkStatusAccessException(ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED, e,
                    "Local OAuth token not established", e);
        }
    }

    private ApplinkAuthenticationOAuthStatusHandler createRequestHandler(AuthorisationUriAwareRequest request,
                                                                         ApplicationLink applink) {
        if (TwoLeggedOAuthWithImpersonationAuthenticationProvider.class.equals(authenticationProvider)) {
            return new ApplinkAuthentication2LoiOAuthStatusHandler(request.getAuthorisationUriGenerator(), applink,
                    oAuthConnectionVerifier);
        } else {
            return new ApplinkAuthenticationOAuthStatusHandler(request.getAuthorisationUriGenerator());
        }
    }

    protected abstract RestUrlBuilder getAuthenticationUrl(ApplicationId localId);

    /**
     * In 5.x the Applinks authentication REST resides under "applinks-oauth", in {@code applinks-plugin-oauth}.
     */
    static final class For5x extends ApplinkAuthenticationOAuthFetchStrategy {
        static final String APPLINKS_OAUTH_REST_MODULE = "applinks-oauth";

        For5x(@Nonnull Class<? extends AuthenticationProvider> authenticationProvider,
              @Nonnull OAuthConnectionVerifier oAuthConnectionVerifier) {
            super(authenticationProvider, oAuthConnectionVerifier);
        }

        @Override
        protected RestUrlBuilder getAuthenticationUrl(ApplicationId localId) {
            return new RestUrlBuilder()
                    .module(APPLINKS_OAUTH_REST_MODULE)
                    .addPath("applicationlink") // OAuthApplicationLinkResource.CONTEXT
                    .addApplicationId(localId)
                    .addPath("authentication");
        }
    }

    /**
     * In 4.x the Applinks authentication REST resides under "oauth", in core {@code applinks-plugin}.
     */
    static final class For4x extends ApplinkAuthenticationOAuthFetchStrategy {

        For4x(@Nonnull Class<? extends AuthenticationProvider> authenticationProvider,
              @Nonnull OAuthConnectionVerifier oAuthConnectionVerifier) {
            super(authenticationProvider, oAuthConnectionVerifier);
        }

        @Override
        protected RestUrlBuilder getAuthenticationUrl(ApplicationId localId) {
            return new RestUrlBuilder()
                    .version(RestVersion.V2)
                    .addPath(ApplicationLinkResource.CONTEXT)
                    .addApplicationId(localId)
                    .addPath("authentication");
        }
    }

    private static class ApplinkAuthenticationOAuthStatusHandler
            implements ApplicationLinkResponseHandler<ApplinkOAuthStatus> {
        private static final Function<RestAuthenticationProvider, String> TO_PROVIDER_NAME =
                new Function<RestAuthenticationProvider, String>() {
                    @Override
                    public String apply(RestAuthenticationProvider provider) {
                        return provider.getProvider();
                    }
                };

        private final AuthorisationURIGenerator uriGenerator;

        ApplinkAuthenticationOAuthStatusHandler(AuthorisationURIGenerator uriGenerator) {
            this.uriGenerator = uriGenerator;
        }

        @Override
        public ApplinkOAuthStatus handle(Response response) throws ResponseException {
            if (response.getStatusCode() == NOT_FOUND.getStatusCode()) {
                return null;
            } else if (response.getStatusCode() == FORBIDDEN.getStatusCode() ||
                    response.getStatusCode() == UNAUTHORIZED.getStatusCode()) {
                throw new ApplinkStatusAccessException(ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION, uriGenerator,
                        INSUFFICIENT_PERMISSION_MESSAGE);
            } else {
                checkStatusOk(response);
                try {
                    RestApplicationLinkAuthentication authenticationEntity = response.getEntity(
                            RestApplicationLinkAuthentication.class);

                    return new ApplinkOAuthStatus(
                            getIncomingConfig(authenticationEntity),
                            getOutgoingConfig(authenticationEntity)
                    );
                } catch (Exception e) {
                    throw new ResponseContentException(response, e);
                }
            }
        }

        @Override
        public ApplinkOAuthStatus credentialsRequired(Response response) throws ResponseException {
            throw new ApplinkStatusAccessException(ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED, uriGenerator,
                    "OAuth trust not established");
        }

        private OAuthConfig getIncomingConfig(RestApplicationLinkAuthentication restAuthentication) {
            if (isEmpty(restAuthentication.getConsumers())) {
                return OAuthConfig.createDisabledConfig();
            }

            // get "highest" available config, in the unlikely case that the remote app has multiple consumers stored
            // against our application ID
            return OAuthConfig.ORDER_BY_LEVEL.max(
                    transform(restAuthentication.getConsumers(), OAuthConfigs.FROM_REST_CONSUMER));
        }

        private OAuthConfig getOutgoingConfig(RestApplicationLinkAuthentication restAuthentication) {
            if (isEmpty(restAuthentication.getConfiguredAuthenticationProviders())) {
                return OAuthConfig.createDisabledConfig();
            }

            ImmutableSet<String> providers = ImmutableSet.copyOf(
                    transform(restAuthentication.getConfiguredAuthenticationProviders(), TO_PROVIDER_NAME));


            final boolean is3LoConfigured = isConfigured(providers, OAuthAuthenticationProvider.class);
            final boolean is2LoConfigured = isConfigured(providers, TwoLeggedOAuthAuthenticationProvider.class);
            final boolean is2LoIConfigured = isConfigured(providers,
                    TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

            return OAuthConfig.fromConfig(is3LoConfigured, is2LoConfigured, is2LoIConfigured);
        }

        private boolean isConfigured(Set<String> providers, Class<? extends AuthenticationProvider> provider) {
            return providers.contains(provider.getCanonicalName());
        }
    }

    static final class ApplinkAuthentication2LoiOAuthStatusHandler extends ApplinkAuthenticationOAuthStatusHandler {
        private final ApplicationLink applink;
        private final OAuthConnectionVerifier oAuthConnectionVerifier;

        ApplinkAuthentication2LoiOAuthStatusHandler(AuthorisationURIGenerator uriGenerator,
                                                    ApplicationLink applink,
                                                    OAuthConnectionVerifier oAuthConnectionVerifier) {
            super(uriGenerator);
            this.applink = applink;
            this.oAuthConnectionVerifier = oAuthConnectionVerifier;
        }

        @Override
        public ApplinkOAuthStatus handle(Response response) throws ResponseException {
            if (ApplinksOAuth.isAuthLevelDisabled(response)) {
                // at this stage we know the remote app knows about our consumer (incoming 3LO enabled remotely),
                // but has incoming 2LOi disabled; we need to check for 2LO to get the complete remote incoming status
                // we use OAuthConnectionVerifier to surface any other potential OAuth problem now, in particular if
                // remote incoming 2LO is not enabled
                oAuthConnectionVerifier.verifyOAuthConnection(applink);

                // if oAuthConnectionVerifier did not raise any error, we know that remote end has incoming OAuth set to
                // "default OAuth", so we can construct a corresponding remote status, with a bit of a leap of faith in
                // that we assume the remote outgoing matches the incoming setting.
                return ApplinkOAuthStatus.DEFAULT;
            } else {
                // otherwise fall back to the "standard" OAuth status detection
                return super.handle(response);
            }

        }
    }
}
