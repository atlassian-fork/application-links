package com.atlassian.applinks.internal.migration;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * Data object that encapsulate the legacy authentication state as well as OAuth authentication state.
 */
public final class AuthenticationStatus {
    private final AuthenticationConfig outgoing;
    private final AuthenticationConfig incoming;

    public AuthenticationStatus(@Nonnull final AuthenticationConfig incoming, @Nonnull final AuthenticationConfig outgoing) {
        this.outgoing = requireNonNull(outgoing, "outgoing");
        this.incoming = requireNonNull(incoming, "incoming");
    }

    public AuthenticationConfig outgoing() {
        return outgoing;
    }

    public AuthenticationConfig incoming() {
        return incoming;
    }

    public AuthenticationStatus outgoing(@Nonnull final AuthenticationConfig outgoing) {
        return new AuthenticationStatus(incoming, outgoing);
    }

    public AuthenticationStatus incoming(@Nonnull final AuthenticationConfig incoming) {
        return new AuthenticationStatus(incoming, outgoing);
    }
}
