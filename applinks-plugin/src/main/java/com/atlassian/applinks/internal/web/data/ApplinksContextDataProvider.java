package com.atlassian.applinks.internal.web.data;

import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.json.JacksonJsonableMarshaller;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.applinks.internal.common.application.ApplicationTypes.resolveApplicationTypeId;

/**
 * Injects information about Applinks context client-side such as current user, host application, etc.
 * For more information how context providers work check the linked documentation.
 *
 * @see <a href="https://developer.atlassian.com/docs/advanced-topics/adding-data-providers-to-your-plugin">
 * Data provider documentation</a>
 * @since 4.3
 */
public class ApplinksContextDataProvider implements WebResourceDataProvider {
    private static final String CURRENT_USER_KEY = "currentUser";

    private static final String HOST_APPLICATION_KEY = "hostApplication";
    private static final String HOST_APPLICATION_ID_KEY = "id";
    private static final String HOST_APPLICATION_TYPE_KEY = "type";

    private final InternalHostApplication hostApplication;
    private final UserManager userManager;

    public ApplinksContextDataProvider(InternalHostApplication hostApplication, UserManager userManager) {
        this.hostApplication = hostApplication;
        this.userManager = userManager;
    }

    @Override
    public Jsonable get() {
        return JacksonJsonableMarshaller.INSTANCE.marshal(getContextMap());
    }

    private Map<String, Object> getContextMap() {
        ImmutableMap.Builder<String, Object> contextMapBuilder = ImmutableMap.builder();
        addCurrentUser(contextMapBuilder);
        addHostApplication(contextMapBuilder);

        return contextMapBuilder.build();
    }

    private void addCurrentUser(ImmutableMap.Builder<String, Object> contextMapBuilder) {
        UserKey userKey = userManager.getRemoteUserKey();
        if (userKey != null) {
            contextMapBuilder.put(CURRENT_USER_KEY, userKey.getStringValue());
        }
    }

    private void addHostApplication(ImmutableMap.Builder<String, Object> contextMapBuilder) {
        contextMapBuilder.put(HOST_APPLICATION_KEY, ImmutableMap.of(
                HOST_APPLICATION_ID_KEY, hostApplication.getId().toString(),
                HOST_APPLICATION_TYPE_KEY, resolveApplicationTypeId(hostApplication.getType())
        ));
    }
}
