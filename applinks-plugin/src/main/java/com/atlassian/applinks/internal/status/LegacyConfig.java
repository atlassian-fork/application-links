package com.atlassian.applinks.internal.status;

public interface LegacyConfig {
    boolean isTrustedConfigured();

    boolean isBasicConfigured();

    default boolean hasLegacy() {
        return isBasicConfigured() || isTrustedConfigured();
    }
}
