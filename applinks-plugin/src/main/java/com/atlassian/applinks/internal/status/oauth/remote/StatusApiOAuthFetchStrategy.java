package com.atlassian.applinks.internal.status.oauth.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.internal.common.net.ResponseContentException;
import com.atlassian.applinks.internal.rest.client.RestRequestBuilder;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.util.remote.AnonymousApplinksResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.net.ResponsePreconditions.checkStatus;
import static com.atlassian.applinks.internal.rest.status.ApplinkStatusResource.oAuthStatusUrl;

/**
 * Fetch OAuth status using the Status API.
 *
 * @since 5.0
 */
final class StatusApiOAuthFetchStrategy implements OAuthStatusFetchStrategy {
    private final Class<? extends AuthenticationProvider> authentication;

    StatusApiOAuthFetchStrategy(Class<? extends AuthenticationProvider> authentication) {
        this.authentication = authentication;
    }

    @Nullable
    @Override
    public ApplinkOAuthStatus fetch(@Nonnull ApplicationId localId, @Nonnull ApplicationLink applink)
            throws ApplinkStatusException, ResponseException {
        ApplicationLinkRequest request = new RestRequestBuilder(applink)
                .authentication(authentication)
                .url(oAuthStatusUrl(localId))
                .buildAnonymous();
        return request.execute(OAuthStatusHandler.INSTANCE);
    }

    private static final class OAuthStatusHandler extends AnonymousApplinksResponseHandler<ApplinkOAuthStatus> {
        static final OAuthStatusHandler INSTANCE = new OAuthStatusHandler();

        @Override
        public ApplinkOAuthStatus handle(Response response) throws ResponseException {
            checkStatus(response, Status.OK, Status.NOT_FOUND);
            return response.getStatusCode() == Status.OK.getStatusCode() ? getStatusFrom(response) : null;
        }

        private ApplinkOAuthStatus getStatusFrom(Response response) throws ResponseException {
            try {
                return response.getEntity(RestApplinkOAuthStatus.class).asDomain();
            } catch (Exception e) {
                throw new ResponseContentException(response, e);
            }
        }
    }
}
