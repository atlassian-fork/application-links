package com.atlassian.applinks.internal.status.error;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Base class for all exceptions that can be mapped to an {@link ApplinkError}.
 *
 * @since 4.3
 */
public abstract class ApplinkStatusException extends RuntimeException implements ApplinkError {
    public ApplinkStatusException() {
    }

    public ApplinkStatusException(@Nullable String message) {
        super(message);
    }

    public ApplinkStatusException(@Nullable String message, @Nullable Throwable cause) {
        super(message, cause);
    }

    // override in classes that implement more specific applink errors
    @Nullable
    @Override
    public <T> T accept(@Nonnull ApplinkErrorVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
