package com.atlassian.applinks.internal.feature;

import com.atlassian.applinks.internal.common.exception.InvalidFeatureKeyException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;

import java.util.Set;
import javax.annotation.Nonnull;

/**
 * Provides API to query for and discover features. Features are identified by string keys (case-insensitive) and are
 * discovered by specific users, therefore this service requires an authenticated user for all operations.
 * <p>
 * Feature keys should be non-empty strings containing maximum 100 characters. Allowed characters include: alphanumeric
 * characters, '.', '-' and '_'. While using this API take care to uniquely namespace feature keys so that they don't
 * clash. E.g. prefer {@code v3.ui.status-lozenge} over {@code status-lozenge}.
 * </p>
 * <p>
 * Note: feature discovery API is not in any way related to {@link ApplinksFeatures}, other than string information
 * about features. In particular discovering any feature key has no bearing on the status of any of
 * {@link ApplinksFeatures}.
 * </p>
 *
 * @since 4.3
 */
@Restricted(PermissionLevel.USER)
public interface FeatureDiscoveryService {
    /**
     * @param featureKey feature key to examine
     * @return {@code true}, if the feature was already discovered by the current authenticated user, {@code false}
     * otherwise
     * @throws NotAuthenticatedException  if there's no authenticated user context
     * @throws InvalidFeatureKeyException if the {@code featureKey} is not a valid feature key. See class javadocs for
     *                                    requirements of a valid feature key
     */
    boolean isDiscovered(@Nonnull String featureKey) throws NotAuthenticatedException, InvalidFeatureKeyException;

    /**
     * Get all discovered feature keys for the current authenticated user. No specific ordering of the keys is
     * guaranteed.
     *
     * @return all discovered feature keys.
     * @throws NotAuthenticatedException if there's no authenticated user context
     */
    Set<String> getAllDiscoveredFeatureKeys() throws NotAuthenticatedException;

    /**
     * Discover feature identified by {@code featureKey} as the current authenticated user.
     *
     * @param featureKey unique feature key
     * @throws NotAuthenticatedException  if there's no authenticated user context
     * @throws InvalidFeatureKeyException if the {@code featureKey} is not a valid feature key. See class javadocs for
     *                                    requirements of a valid feature key
     */
    void discover(@Nonnull String featureKey) throws NotAuthenticatedException, InvalidFeatureKeyException;
}
