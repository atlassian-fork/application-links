package com.atlassian.applinks.internal.rest.model.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.internal.rest.model.ReadOnlyRestRepresentation;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class RestApplicationVersion extends BaseRestEntity implements ReadOnlyRestRepresentation<ApplicationVersion> {
    public static final String VERSION_STRING = "versionString";
    public static final String MAJOR = "major";
    public static final String MINOR = "minor";
    public static final String BUGFIX = "bugfix";
    public static final String SUFFIX = "suffix";

    @SuppressWarnings("unused") // for Jackson
    public RestApplicationVersion() {
    }

    public RestApplicationVersion(@Nonnull ApplicationVersion applicationVersion) {
        requireNonNull(applicationVersion, "applicationVersion");
        put(VERSION_STRING, applicationVersion.getVersionString());
        put(MAJOR, applicationVersion.getMajor());
        put(MINOR, applicationVersion.getMinor());
        put(BUGFIX, applicationVersion.getBugfix());
        put(SUFFIX, applicationVersion.getSuffix());
    }
}
