package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.applinks.internal.status.remote.RemoteOAuthException;
import com.google.common.base.Predicate;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.status.error.ApplinkErrors.findCauseMatching;

/**
 * @since 5.0
 */
final class OAuthErrorMatchers {
    private OAuthErrorMatchers() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    private static Predicate<Throwable> withOAuthProblem(@Nonnull final String oAuthProblem) {
        return new Predicate<Throwable>() {
            @Override
            @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
            public boolean apply(Throwable error) {
                return error instanceof OAuthMessageProblemException &&
                        oAuthProblem.equals(OAuthMessageProblemException.class.cast(error).getOAuthProblem());
            }
        };
    }

    @Nonnull
    static OAuthProblemMatcher matchOAuthProblem(@Nonnull ApplinkErrorType applinkErrorType,
                                                 @Nonnull String oauthProblem) {
        return new SpecificOAuthProblemMatcher(applinkErrorType, oauthProblem);
    }

    @Nonnull
    static OAuthProblemMatcher fallback() {
        return new OAuthProblemMatcher(ApplinkErrorType.OAUTH_PROBLEM);
    }

    static class OAuthProblemMatcher extends NetworkErrorTranslator.ByTypeMatcher {
        OAuthProblemMatcher(ApplinkErrorType applinkErrorType) {
            super(applinkErrorType, OAuthMessageProblemException.class);
        }

        @Override
        public ApplinkStatusException createMatchingError(String message, Throwable original) {
            return new RemoteOAuthException(applinkErrorType, message, original);
        }
    }

    static class SpecificOAuthProblemMatcher extends OAuthProblemMatcher {
        private final Predicate<Throwable> errorMatcher;

        SpecificOAuthProblemMatcher(@Nonnull ApplinkErrorType applinkErrorType, @Nonnull String oauthProblem) {
            super(applinkErrorType);
            this.errorMatcher = withOAuthProblem(oauthProblem);
        }

        @Override
        @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
        public boolean matches(Throwable original) {
            return findCauseMatching(original, errorMatcher) != null;
        }
    }
}
