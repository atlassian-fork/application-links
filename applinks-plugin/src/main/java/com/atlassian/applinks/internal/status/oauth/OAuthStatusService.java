package com.atlassian.applinks.internal.status.oauth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.common.permission.PermissionLevel;
import com.atlassian.applinks.internal.common.permission.Restricted;
import com.atlassian.applinks.internal.common.permission.Unrestricted;
import com.atlassian.applinks.internal.common.exception.ConsumerInformationUnavailableException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;

import javax.annotation.Nonnull;

/**
 * Retrieves status of the local OAuth configuration for Application Links.
 *
 * @since 4.3
 */
public interface OAuthStatusService {
    /**
     * Retrieve OAuth configuration status for given Application {@code id}.
     *
     * @param id application ID to examine
     * @return OAuth config status of the applink with {@code id}
     * @throws NoSuchApplinkException if the corresponding applink could not be retrieved
     */
    @Nonnull
    @Unrestricted("Clients that know application link ID should be free to examine its status")
    ApplinkOAuthStatus getOAuthStatus(@Nonnull ApplicationId id) throws NoSuchApplinkException;

    /**
     * Retrieve OAuth configuration status of {@code link}.
     *
     * @param link application link to examine
     * @return OAuth config status of the applink
     */
    @Nonnull
    @Unrestricted("Clients that know application link ID should be free to examine its status")
    ApplinkOAuthStatus getOAuthStatus(@Nonnull ApplicationLink link);

    /**
     * Update OAuth configuration status for given Application {@code id}. This normally requires admin permission,
     * however enabling {@link com.atlassian.applinks.internal.common.status.oauth.OAuthConfig#isTwoLoImpersonationEnabled() impersonation} for either incoming or outgoing
     * OAuth config necessitates System Administrator permissions.
     *
     * @param id     application ID to apply the update to
     * @param status new status value
     * @throws NoSuchApplinkException                  if the corresponding applink could not be retrieved
     * @throws NoAccessException                       as per the access restriction
     * @throws ConsumerInformationUnavailableException if the client requests to enable OAuth that was previously
     *                                                 disabled and the remote consumer information is currently not available from the linked application
     */
    @Restricted(value = {PermissionLevel.ADMIN, PermissionLevel.SYSADMIN}, reason = "2LOi can only be set by sysadmins")
    void updateOAuthStatus(@Nonnull ApplicationId id, @Nonnull ApplinkOAuthStatus status)
            throws NoSuchApplinkException, NoAccessException, ConsumerInformationUnavailableException;

    /**
     * Update OAuth configuration status for given {@code link}. This normally requires admin permission,
     * however enabling {@link com.atlassian.applinks.internal.common.status.oauth.OAuthConfig#isTwoLoImpersonationEnabled() impersonation} for either incoming or outgoing
     * OAuth config necessitates System Administrator permissions.
     *
     * @param link   application link to apply the update to
     * @param status new status value
     * @throws NoAccessException                       as per the access restriction
     * @throws ConsumerInformationUnavailableException if the client requests to enable OAuth that was previously
     *                                                 disabled and the remote consumer information is currently not available from the linked application
     */
    @Restricted(value = {PermissionLevel.ADMIN, PermissionLevel.SYSADMIN}, reason = "2LOi can only be set by sysadmins")
    void updateOAuthStatus(@Nonnull ApplicationLink link, @Nonnull ApplinkOAuthStatus status)
            throws NoAccessException, ConsumerInformationUnavailableException;
}
