package com.atlassian.applinks.internal.util.remote;

import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;

/**
 * Anonymous response handler, for which {@link #credentialsRequired(Response)} should never be triggered.
 *
 * @since 4.3
 */
public abstract class AnonymousApplinksResponseHandler<R> implements ApplicationLinkResponseHandler<R> {
    @Override
    public R credentialsRequired(Response response) throws ResponseException {
        throw new IllegalStateException("Credentials required invoked for anonymous request");
    }
}
