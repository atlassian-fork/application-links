package com.atlassian.applinks.internal.rest.model.migration;

import com.atlassian.applinks.internal.migration.AuthenticationConfig;
import com.atlassian.applinks.internal.rest.model.ApplinksRestRepresentation;
import io.swagger.annotations.ApiModel;

import javax.annotation.Nonnull;
import java.util.Objects;

@ApiModel
public class RestAuthenticationConfig extends ApplinksRestRepresentation {

    public static final String OAUTH = "oauth";
    public static final String BASIC = "basic";
    public static final String TRUSTED = "trusted";

    private boolean oauth;
    private boolean basic;
    private boolean trusted;

    //for Jackson
    public RestAuthenticationConfig(){

    }

    public RestAuthenticationConfig(@Nonnull final AuthenticationConfig config) {
        Objects.requireNonNull(config, "authenticationConfig");
        this.oauth = config.isOAuthConfigured();
        this.basic = config.isBasicConfigured();
        this.trusted = config.isTrustedConfigured();
    }
}
