package com.atlassian.applinks.internal.rest.applink;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.applink.ApplinkValidationService;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.rest.interceptor.RestRepresentationInterceptor;
import com.atlassian.applinks.internal.common.rest.model.applink.RestApplicationLink;
import com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.rest.applink.data.RestApplinkDataProvider;
import com.atlassian.applinks.internal.rest.applink.data.RestApplinkDataProviders;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.internal.rest.interceptor.ServiceExceptionInterceptor;
import com.atlassian.applinks.internal.rest.model.applink.RestExtendedApplicationLink;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.google.common.collect.ImmutableList;
import com.sun.jersey.spi.resource.Singleton;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

/**
 * Application link resource V3. Provides access to selected, or all application links with additional ability to inject
 * custom data and selected properties into the response.
 *
 * @since 5.0
 */
@AnonymousAllowed
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
@Path(ApplicationLinkV3Resource.CONTEXT)
@Singleton
@InterceptorChain({ServiceExceptionInterceptor.class, RestRepresentationInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplicationLinkV3Resource {
    private static final Logger log = LoggerFactory.getLogger(ApplicationLinkV3Resource.class);

    public static final String CONTEXT = "applinks";

    private final MutatingApplicationLinkService applicationLinkService;
    private final ApplinkHelper applinkHelper;
    private final ApplinkValidationService applinkValidationService;
    private final PermissionValidationService permissionValidationService;
    private final RestApplinkDataProviders dataProviders;
    private final RestApplicationIdParser restApplicationIdParser;

    public ApplicationLinkV3Resource(MutatingApplicationLinkService applicationLinkService,
                                     ApplinkHelper applinkHelper,
                                     ApplinkValidationService applinkValidationService,
                                     PermissionValidationService permissionValidationService,
                                     RestApplinkDataProviders dataProviders,
                                     RestApplicationIdParser restApplicationIdParser) {
        this.applicationLinkService = applicationLinkService;
        this.applinkHelper = applinkHelper;
        this.applinkValidationService = applinkValidationService;
        this.permissionValidationService = permissionValidationService;
        this.dataProviders = dataProviders;
        this.restApplicationIdParser = restApplicationIdParser;
    }

    @GET
    public Response getAll(@QueryParam("property") final Set<String> propertyKeys,
                           @QueryParam("data") final Set<String> dataKeys) throws ServiceException {
        permissionValidationService.validateAdmin();

        Iterable<ApplicationLink> links = applicationLinkService.getApplicationLinks();
        ImmutableList.Builder<RestExtendedApplicationLink> restApplinks = ImmutableList.builder();
        for (ApplicationLink applink : links) {
            restApplinks.add(new RestExtendedApplicationLink(applink, propertyKeys, getData(applink, dataKeys)));
        }

        return Response.ok(restApplinks.build()).build();
    }

    @GET
    @Path("{applinkid}")
    public Response get(@PathParam("applinkid") String applinkId,
                        @QueryParam("property") final Set<String> propertyKeys,
                        @QueryParam("data") final Set<String> dataKeys) throws ServiceException {
        permissionValidationService.validateAdmin();

        ApplicationLink applink = applinkHelper.getApplicationLink(restApplicationIdParser.parse(applinkId));

        return Response.ok(new RestExtendedApplicationLink(applink, propertyKeys, getData(applink, dataKeys))).build();
    }

    @PUT
    @Path("{applinkid}")
    public Response update(@PathParam("applinkid") String applinkId, RestApplicationLink restApplink)
            throws ServiceException {
        permissionValidationService.validateAdmin();

        ApplicationId id = restApplicationIdParser.parse(applinkId);
        ApplicationLinkDetails details = restApplink.toDetails();
        applinkValidationService.validateUpdate(id, details);

        if (details.isPrimary()) {
            applinkHelper.makePrimary(id);
        }

        MutableApplicationLink applink = applinkHelper.getMutableApplicationLink(id);
        applink.update(details);

        return Response.ok(new RestApplicationLink(applink)).build();
    }

    @DELETE
    @Path("{applinkid}")
    public Response delete(@PathParam("applinkid") String applinkId) throws ServiceException {
        permissionValidationService.validateAdmin();

        ApplicationLink applink = applinkHelper.getApplicationLink(restApplicationIdParser.parse(applinkId));
        applicationLinkService.deleteApplicationLink(applink);

        return Response.noContent().build();
    }

    @Nonnull
    private Map<String, Object> getData(ApplicationLink applink, Set<String> dataKeys) throws ServiceException {
        Map<String, Object> data = new LinkedHashMap<>();
        for (String key : dataKeys) {
            RestApplinkDataProvider provider = dataProviders.getProvider(key);
            if (provider != null) {
                data.put(key, provider.provide(key, applink));
            } else {
                log.debug("RestApplinkDataProvider for requested key {} not found", key);
                data.put(key, null);
            }
        }
        return data;
    }
}
