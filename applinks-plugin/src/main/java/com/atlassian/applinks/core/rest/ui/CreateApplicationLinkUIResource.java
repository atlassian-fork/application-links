package com.atlassian.applinks.core.rest.ui;

import com.atlassian.applinks.analytics.ApplinksCreatedEventFactory;
import com.atlassian.applinks.analytics.ApplinksCreatedEventFactory.FAILURE_REASON;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.auth.OrphanedTrustAwareAuthenticatorProviderPluginModule;
import com.atlassian.applinks.core.auth.OrphanedTrustCertificate;
import com.atlassian.applinks.core.auth.OrphanedTrustDetector;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.core.plugin.AuthenticationProviderModuleDescriptor;
import com.atlassian.applinks.core.rest.AbstractResource;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.CreateApplicationLinkRequestEntity;
import com.atlassian.applinks.core.rest.model.CreatedApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.ManifestEntity;
import com.atlassian.applinks.core.rest.model.OrphanedTrust;
import com.atlassian.applinks.core.rest.model.ResponseInfoEntity;
import com.atlassian.applinks.core.rest.model.VerifyTwoWayLinkDetailsRequestEntity;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.core.util.Holder;
import com.atlassian.applinks.core.util.URIUtil;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.common.net.BasicHttpAuthRequestFactory;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.StaticUrlApplicationType;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationException;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.spi.auth.AutoConfiguringAuthenticatorProviderPluginModule;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.AuthenticationResponseException;
import com.atlassian.applinks.spi.link.LinkCreationResponseException;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.link.NotAdministratorException;
import com.atlassian.applinks.spi.link.ReciprocalActionException;
import com.atlassian.applinks.spi.link.RemoteErrorListException;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.security.RequiresXsrfCheck;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.user.UserManager;
import com.google.common.collect.Lists;
import com.sun.jersey.spi.resource.Singleton;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.applinks.core.rest.util.RestUtil.badFormRequest;
import static com.atlassian.applinks.core.rest.util.RestUtil.badRequest;
import static com.atlassian.applinks.core.rest.util.RestUtil.conflict;
import static com.atlassian.applinks.core.rest.util.RestUtil.ok;
import static com.atlassian.applinks.core.rest.util.RestUtil.serverError;
import static java.util.Collections.singletonMap;
import static java.util.stream.StreamSupport.stream;

/**
 * This rest end point handles requests from the add and edit application link form.
 *
 * @since 3.0
 */
@Path("applicationlinkForm")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class CreateApplicationLinkUIResource extends AbstractResource {
    protected final MutatingApplicationLinkService applicationLinkService;
    protected final ManifestRetriever manifestRetriever;
    protected final InternalHostApplication internalHostApplication;
    protected final I18nResolver i18nResolver;
    protected static final Logger LOG = LoggerFactory.getLogger(CreateApplicationLinkUIResource.class);
    protected final OrphanedTrustDetector orphanedTrustDetector;
    protected final PluginAccessor pluginAccessor;
    protected final UserManager userManager;
    private final ApplinksCreatedEventFactory applinksCreatedEventFactory;
    private final EventPublisher eventPublisher;

    public CreateApplicationLinkUIResource(
            final MutatingApplicationLinkService applicationLinkService,
            final RequestFactory requestFactory,
            final InternalHostApplication internalHostApplication,
            final I18nResolver i18nResolver,
            final InternalTypeAccessor typeAccessor,
            final ManifestRetriever manifestRetriever,
            final RestUrlBuilder restUrlBuilder,
            @Qualifier("delegatingOrphanedTrustDetector") final OrphanedTrustDetector orphanedTrustDetector,
            final PluginAccessor pluginAccessor,
            final UserManager userManager,
            final ApplinksCreatedEventFactory applinksCreatedEventFactory,
            final EventPublisher eventPublisher) {
        super(restUrlBuilder, typeAccessor, requestFactory, applicationLinkService);
        this.i18nResolver = i18nResolver;
        this.internalHostApplication = internalHostApplication;
        this.applicationLinkService = applicationLinkService;
        this.manifestRetriever = manifestRetriever;
        this.orphanedTrustDetector = orphanedTrustDetector;
        this.pluginAccessor = pluginAccessor;
        this.userManager = userManager;
        this.applinksCreatedEventFactory = applinksCreatedEventFactory;
        this.eventPublisher = eventPublisher;
    }

    private void sendFailureEvent(FAILURE_REASON reason) {
        eventPublisher.publish(
                applinksCreatedEventFactory.createFailEvent(reason)
        );
    }

    private void sendWarningEvent(FAILURE_REASON reason) {
        eventPublisher.publish(
                applinksCreatedEventFactory.createWarningEvent(reason)
        );
    }

    private void sendSuccessEvent() {
        eventPublisher.publish(
                applinksCreatedEventFactory.createSuccessEvent()
        );
    }

    @GET
    @Path("manifest")
    @RequiresXsrfCheck
    public javax.ws.rs.core.Response tryToFetchManifest(@QueryParam("url") String url) {
        if (StringUtils.isBlank(url)) {
            return badRequest(i18nResolver.getText("applinks.error.rpcurl"));
        }

        final URI manifestUrl;
        Manifest manifest;
        try {
            LOG.debug("URL received '" + url + "'");
            manifestUrl = new URL(url).toURI();
            manifest = manifestRetriever.getManifest(manifestUrl);
        } catch (ManifestNotFoundException e) {
            LOG.error("ManifestNotFoundException thrown while retrieving manifest", e);
            manifest = null;
            final Throwable responseException = e.getCause();
            if (responseException != null) {
                if (responseException instanceof AppLinksManifestDownloader.ManifestGotRedirectedException) {
                    AppLinksManifestDownloader.ManifestGotRedirectedException mgre = (AppLinksManifestDownloader.ManifestGotRedirectedException) responseException;
                    Map<String, String> redirectedUrl = singletonMap("redirectedUrl", mgre.getNewLocation());
                    sendWarningEvent(FAILURE_REASON.REDIRECT);
                    return ok(new ResponseInfoEntity("applinks.warning.redirected.host", getRedirectionWarning(mgre), redirectedUrl));
                } else if (responseException instanceof IOException) {
                    sendWarningEvent(FAILURE_REASON.NO_RESPONSE);
                    return ok(new ResponseInfoEntity("applinks.warning.unknown.host", getNonResponsiveHostWarning()));
                }
            }
        } catch (Exception e) {
            LOG.error("Exception thrown while retrieving manifest", e);
            Pattern p = Pattern.compile("http(s)?:/[^/].*");
            Matcher m = p.matcher(url);
            if (m.matches()) {
                LOG.warn("The url '" + url + "' is missing the double slashes after the protocol. Is there a proxy server in the middle that has replaced the '//' with a single '/'?");
                sendFailureEvent(FAILURE_REASON.NO_DOUBLE_SLASHES);
            } else {
                sendFailureEvent(FAILURE_REASON.INVALID_URL);
            }

            LOG.debug("Invalid URL url='" + url + "'", e);
            return badRequest(i18nResolver.getText("applinks.error.url.invalid", url));
        }

        if (manifest != null) {
            LOG.debug("Manifest retrieved successfully");
            try {
                if (typeAccessor.loadApplicationType(manifest.getTypeId()) == null) {
                    throw new TypeNotInstalledException(manifest.getTypeId().get(), manifest.getName(), manifest.getUrl());
                }
                // Check if a matching app link already exists
                final ApplicationLink existingAppLink = applicationLinkService.getApplicationLink(manifest.getId());
                if (existingAppLink != null) {
                    if (existingAppLink.getDisplayUrl().equals(manifest.getUrl())) {
                        sendFailureEvent(FAILURE_REASON.ALREADY_CONFIGURED);
                        return conflict(i18nResolver.getText("applinks.error.applink.exists", manifest.getUrl()));
                    }
                    sendFailureEvent(FAILURE_REASON.ALREADY_CONFIGURED_UNDER_DIFFERENT_URL);
                    return conflict(i18nResolver.getText("applinks.error.applink.exists.with.different.url"));
                }
            } catch (TypeNotInstalledException e) {
                LOG.error("TypeNotInstalledException thrown", e);
                sendFailureEvent(FAILURE_REASON.TYPE_NOT_INSTALLED);
                return badRequest(String.format(i18nResolver.getText("applinks.error.remote.type.not.installed", e.getType())));
            }
        } else {
            LOG.error("Null manifest retrieved");
            sendWarningEvent(FAILURE_REASON.NULL_MANIFEST);
            return ok(new ResponseInfoEntity());
        }

        if (manifest.getId().equals(internalHostApplication.getId())) {
            sendFailureEvent(FAILURE_REASON.LINK_TO_SELF);
            return conflict(i18nResolver.getText("applinks.error.applink.itsme"));
        }

        sendSuccessEvent();
        return ok(new ManifestEntity(manifest));
    }

    private String getRedirectionWarning(final AppLinksManifestDownloader.ManifestGotRedirectedException mgre) {
        return i18nResolver.getText("applinks.warning.redirected.host.new", StringEscapeUtils.escapeHtml4(mgre.newLocationBaseUrl()));
    }

    private String getNonResponsiveHostWarning() {
        return i18nResolver.getText("applinks.warning.unknown.host.new");
    }

    @POST
    @Path("createStaticUrlAppLink")
    public javax.ws.rs.core.Response createStaticUrlAppLink(@QueryParam("typeId") final String typeId) throws Exception {
        final StaticUrlApplicationType type = (StaticUrlApplicationType) typeAccessor.loadApplicationType(typeId);
        Manifest manifest = manifestRetriever.getManifest(type.getStaticUrl(), type);
        ApplicationLinkDetails details = ApplicationLinkDetails.builder().name(type.getI18nKey())
                .displayUrl(type.getStaticUrl()).rpcUrl(type.getStaticUrl()).isPrimary(true).build();
        final ApplicationLink createdApplicationLink = applicationLinkService.addApplicationLink(manifest.getId(), type, details);
        return ok(new CreatedApplicationLinkEntity(toApplicationLinkEntity(createdApplicationLink), true));
    }

    @POST
    @Path("createAppLink")
    public javax.ws.rs.core.Response createApplicationLink(final CreateApplicationLinkRequestEntity applicationLinkRequest) {
        final ApplicationLinkEntity applicationLink = applicationLinkRequest.getApplicationLink();
        final URI remoteRpcUrl = applicationLink.getRpcUrl();

        if (StringUtils.isEmpty(applicationLink.getName().trim())) {
            return badFormRequest(Lists.newArrayList(i18nResolver.getText("applinks.error.appname")), Lists.newArrayList("application-name"));
        }

        if (StringUtils.isEmpty(applicationLink.getTypeId().get()) || typeAccessor.loadApplicationType(applicationLink.getTypeId()) == null) {
            return badFormRequest(Lists.newArrayList(i18nResolver.getText("applinks.error.apptype")), Lists.newArrayList("application-types"));
        }

        final boolean shareUserbase = applicationLinkRequest.getConfigFormValues().shareUserbase();
        final boolean trustEachOther = applicationLinkRequest.getConfigFormValues().trustEachOther();

        // prevent non sysadmin user to create a trusted app from link creation wizard.
        if (!userManager.isSystemAdmin(userManager.getRemoteUsername()) && shareUserbase) {
            return badFormRequest(Lists.newArrayList(i18nResolver.getText("applinks.error.only.sysadmin.operation")), Lists.newArrayList("same-userbase"));
        }
        if (applicationLinkWithRpcUrlAlreadyExists(applicationLink.getRpcUrl())) {
            return badRequest(i18nResolver.getText("applinks.error.rpcurl.exists"));
        }

        if (applicationLinkRequest.createTwoWayLink()) {
            try {
                applicationLinkService.createReciprocalLink(remoteRpcUrl, applicationLinkRequest.isCustomRpcURL() ? applicationLinkRequest.getRpcUrl() : null, applicationLinkRequest.getUsername(), applicationLinkRequest.getPassword());
            } catch (final NotAdministratorException exception) {
                return badFormRequest(Lists.newArrayList(i18nResolver.getText("applinks.error.unauthorized")), Lists.newArrayList("authorization"));
            } catch (final LinkCreationResponseException exception) {
                return serverError(i18nResolver.getText("applinks.error.response"));
            } catch (final AuthenticationResponseException exception) {
                return serverError(i18nResolver.getText("applinks.error.authorization.response"));
            } catch (final RemoteErrorListException exception) {
                ArrayList<String> errors = Lists.newArrayList(i18nResolver.getText("applinks.error.general"));
                errors.addAll(exception.getErrors());
                return RestUtil.badRequest(errors.toArray(new String[0]));
            } catch (final ReciprocalActionException exception) {
                return serverError(i18nResolver.getText("applinks.error.general"));
            }
        }

        final ApplicationType type = typeAccessor.loadApplicationType(applicationLink.getTypeId().get());
        final ApplicationLink createdApplicationLink;
        try {
            createdApplicationLink = applicationLinkService.createApplicationLink(type, applicationLink.getDetails());
        } catch (ManifestNotFoundException e) {
            return serverError(i18nResolver.getText("applinks.error.incorrect.application.type"));
        }

        boolean autoConfigurationSuccessful = true;
        // TODO in this scenario this really means configure incoming link as we never configure 2 way from here
        // check stay in place for the moment to differentiate between atlassian <- atlassian and atlassian -> 3rdParty.
        if (applicationLinkRequest.createTwoWayLink()) {
            try {
                applicationLinkService.configureAuthenticationForApplicationLink(
                        createdApplicationLink,
                        new AuthenticationScenario() {
                            public boolean isCommonUserBase() {
                                return shareUserbase;
                            }

                            public boolean isTrusted() {
                                return trustEachOther;
                            }
                        },
                        applicationLinkRequest.getUsername(),
                        applicationLinkRequest.getPassword());

            } catch (AuthenticationConfigurationException e) {
                LOG.warn("Error during auto-configuration of authentication providers for application link '" + createdApplicationLink + "'", e);
                autoConfigurationSuccessful = false;
            }
        }

        if (applicationLinkRequest.getOrphanedTrust() != null) {
            OrphanedTrust orphanedTrust = applicationLinkRequest.getOrphanedTrust();
            final OrphanedTrustCertificate.Type certificateType;
            try {
                certificateType = OrphanedTrustCertificate.Type.valueOf(orphanedTrust.getType());
                orphanedTrustDetector.addOrphanedTrustToApplicationLink(orphanedTrust.getId(), certificateType, createdApplicationLink.getId());

                if (applicationLinkRequest.createTwoWayLink()) {
                    AutoConfiguringAuthenticatorProviderPluginModule providerPluginModule = getAutoConfigurationPluginModule(certificateType);
                    if (providerPluginModule != null) {
                        providerPluginModule.enable(getAuthenticatedRequestFactory(applicationLinkRequest), createdApplicationLink);
                    } else {
                        LOG.warn("Failed to find an authentication type for the orphaned trust certificate type='" + orphanedTrust.getType() + "' and id='" + orphanedTrust.getId() + "' that supports auto-configuration");
                    }
                }
            } catch (Exception e) {
                LOG.error("Failed to add orphaned trust certificate with type='" + orphanedTrust.getType() + "' and id='" + orphanedTrust.getId() + "'", e);
            }
        }
        return ok(new CreatedApplicationLinkEntity(toApplicationLinkEntity(createdApplicationLink), autoConfigurationSuccessful));
    }

    private boolean applicationLinkWithRpcUrlAlreadyExists(final URI rpcURl) {
        return stream(applicationLinkService.getApplicationLinks().spliterator(), false)
                .anyMatch(input -> (input.getRpcUrl().equals(rpcURl)));
    }

    private AutoConfiguringAuthenticatorProviderPluginModule getAutoConfigurationPluginModule(final OrphanedTrustCertificate.Type certificateType) {
        return findAutoConfiguringAuthenticationProviderModule(certificateType);
    }

    private AutoConfiguringAuthenticatorProviderPluginModule findAutoConfiguringAuthenticationProviderModule(final OrphanedTrustCertificate.Type certificateType) {
        List<AuthenticationProviderModuleDescriptor> authenticationProviderModuleDescriptors = pluginAccessor.getEnabledModuleDescriptorsByClass(AuthenticationProviderModuleDescriptor.class);
        for (AuthenticationProviderModuleDescriptor authenticationProviderModuleDescriptor : authenticationProviderModuleDescriptors) {
            if (authenticationProviderModuleDescriptor instanceof OrphanedTrustAwareAuthenticatorProviderPluginModule) {
                if (((OrphanedTrustAwareAuthenticatorProviderPluginModule) authenticationProviderModuleDescriptor).isApplicable(certificateType.name()))
                    ;
                {
                    return (AutoConfiguringAuthenticatorProviderPluginModule) authenticationProviderModuleDescriptor.getModule();
                }
            }
        }
        return null;
    }

    private BasicHttpAuthRequestFactory<Request<Request<?, Response>, Response>> getAuthenticatedRequestFactory(final CreateApplicationLinkRequestEntity applicationLinkRequest) {
        return new BasicHttpAuthRequestFactory<Request<Request<?, Response>, Response>>(
                requestFactory,
                applicationLinkRequest.getUsername(),
                applicationLinkRequest.getPassword());
    }

    @POST
    @Path("details")
    public javax.ws.rs.core.Response verifyTwoWayLinkDetails(VerifyTwoWayLinkDetailsRequestEntity linkDetails)
            throws TypeNotInstalledException {
        boolean isAdminUser;
        try {
            isAdminUser = applicationLinkService.isAdminUserInRemoteApplication(linkDetails.getRemoteUrl(), linkDetails.getUsername(), linkDetails.getPassword());
        } catch (ResponseException e) {
            // The first time we connect to the remote server, so we assume any errors are
            // caused by errors connecting TO the remote server
            LOG.error("Error occurred while checking credentials.", e);
            return serverError(i18nResolver.getText("applinks.error.authorization.response"));
        }

        if (isAdminUser) {
            final String applicationType = i18nResolver.getText(internalHostApplication.getType().getI18nKey());
            try {
                if (isRpcUrlValid(linkDetails.getRemoteUrl(), linkDetails.getRpcUrl(), linkDetails.getUsername(), linkDetails.getPassword())) {
                    return ok();
                } else {
                    return badRequest(i18nResolver.getText("applinks.error.url.reciprocal.rpc.url.invalid", internalHostApplication.getName(), applicationType, linkDetails.getRpcUrl()));
                }
            } catch (ResponseException e) {
                // Since we have already connected to the server before, we assume any errors are
                // caused by errors connecting FROM the remote server back to this instance.
                LOG.error("Error occurred while checking reciprocal link.", e);
                return badRequest(i18nResolver.getText("applinks.error.url.reciprocal.rpc.url.invalid", internalHostApplication.getName(), applicationType, linkDetails.getRpcUrl()));
            }
        }
        {
            return badFormRequest(Lists.newArrayList(i18nResolver.getText("applinks.error.unauthorized")), Lists.newArrayList("reciprocal-link-password"));
        }
    }

    private boolean isRpcUrlValid(final URI url, final URI rpcUrl, final String username, final String password)
            throws ResponseException {
        // We send the rpcUrl parameter in a query parameter. For pre-3.4 versions of the REST resource, we also
        //  send it in the path.
        // TODO If we know the server is using applinks 3.4, the rpcUrl path parameter can be empty
        String pathUrl = getUrlFor(URIUtil.uncheckedConcatenate(url, RestUtil.REST_APPLINKS_URL), AuthenticationResource.class).rpcUrlIsReachable(internalHostApplication.getId().get(), rpcUrl, null).toString();

        String urlWithQuery = pathUrl + "?url=" + URIUtil.utf8Encode(rpcUrl);

        final Request request = requestFactory.createRequest(Request.MethodType.GET, urlWithQuery);
        request.addBasicAuthentication(url.getHost(), username, password);

        final Holder<Boolean> rpcUrlValid = new Holder<Boolean>(false);

        request.execute(new ResponseHandler<Response>() {
            public void handle(final Response restResponse) throws ResponseException {
                if (restResponse.isSuccessful()) {
                    rpcUrlValid.set(true);
                }
            }
        });
        return rpcUrlValid.get();
    }
}
