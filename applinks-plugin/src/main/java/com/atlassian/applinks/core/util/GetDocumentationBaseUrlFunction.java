package com.atlassian.applinks.core.util;

import com.atlassian.applinks.internal.common.docs.DocumentationLinker;
import com.atlassian.soy.renderer.JsExpression;
import com.atlassian.soy.renderer.SoyClientFunction;
import com.atlassian.soy.renderer.SoyServerFunction;
import com.google.common.collect.ImmutableSet;

import java.util.Set;

/**
 * Get base url of documentation. For use in soy templates.
 *
 * @since 4.0.0
 */
public class GetDocumentationBaseUrlFunction implements SoyServerFunction<String>, SoyClientFunction {

    private final DocumentationLinker documentationLinker;

    public GetDocumentationBaseUrlFunction(final DocumentationLinker documentationLinker) {
        this.documentationLinker = documentationLinker;
    }

    public String getName() {
        return "getDocumentationBaseUrl";
    }

    @Override
    public JsExpression generate(JsExpression... jsExpressions) {
        return new JsExpression("'" + documentationLinker.getDocumentationBaseUrl().toString() + "'");
    }

    public String apply(Object... objects) {
        return documentationLinker.getDocumentationBaseUrl().toString();
    }

    public Set<Integer> validArgSizes() {
        return ImmutableSet.of(0);
    }
}
