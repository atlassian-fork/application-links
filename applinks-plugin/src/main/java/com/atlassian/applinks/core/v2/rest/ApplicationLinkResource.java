package com.atlassian.applinks.core.v2.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.application.generic.GenericApplicationTypeImpl;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.rest.auth.AdminApplicationLinksInterceptor;
import com.atlassian.applinks.core.rest.context.ContextInterceptor;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntityListEntity;
import com.atlassian.applinks.core.rest.util.RestUtil;
import com.atlassian.applinks.internal.rest.interceptor.NoCacheHeaderInterceptor;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.spi.auth.AuthenticationProviderPluginModule;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugins.rest.common.Link;
import com.atlassian.plugins.rest.common.interceptor.InterceptorChain;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.websudo.WebSudoRequired;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.sun.jersey.spi.resource.Singleton;
import io.swagger.annotations.Api;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * V2.0 of applicationLink REST resource, add resources for managing authentication.
 *
 * @since 4.0
 */
@Api
@Path(ApplicationLinkResource.CONTEXT)
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Singleton
@WebSudoRequired
@InterceptorChain({ContextInterceptor.class, AdminApplicationLinksInterceptor.class, NoCacheHeaderInterceptor.class})
public class ApplicationLinkResource extends com.atlassian.applinks.core.v1.rest.ApplicationLinkResource {
    private final PluginAccessor pluginAccessor;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    public ApplicationLinkResource(final MutatingApplicationLinkService applicationLinkService,
                                   final I18nResolver i18nResolver,
                                   final InternalTypeAccessor typeAccessor,
                                   final ManifestRetriever manifestRetriever,
                                   final RestUrlBuilder restUrlBuilder,
                                   final RequestFactory requestFactory,
                                   final UserManager userManager,
                                   final PluginAccessor pluginAccessor,
                                   final AuthenticationConfigurationManager authenticationConfigurationManager) {
        super(applicationLinkService,
                i18nResolver,
                typeAccessor,
                manifestRetriever,
                restUrlBuilder,
                requestFactory,
                userManager);
        this.pluginAccessor = pluginAccessor;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    /**
     * Creates an application link.
     *
     * @param applicationLink The new details to set.
     * @return 'badRequest' if the target application is not reachable or if the name already exists; 'forbidden' if
     * the user performing the operation does not have sysadmin privilege; 'created' in case of success.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     */
    @PUT
    public Response addApplicationLink(final ApplicationLinkEntity applicationLink)
            throws TypeNotInstalledException {
        final ApplicationType applicationType = typeAccessor.loadApplicationType(applicationLink.getTypeId());
        if (applicationType == null) {
            LOG.warn(String.format("Couldn't load type %s for application link. Type is not installed?", applicationLink.getTypeId()));
            throw new TypeNotInstalledException(applicationLink.getTypeId().get(), applicationLink.getName(), applicationLink.getRpcUrl());
        }

        ApplicationId applicationId = applicationLink.getId();

        if (applicationType instanceof GenericApplicationTypeImpl || applicationId == null) {
            // historically for generic application links the id is created internally from the url.
            // so follow precedent and ignore anything passed in and create on from the rpcUrl
            applicationId = ApplicationIdUtil.generate(applicationLink.getRpcUrl());
        }

        // If the id already exists we can't create the a duplicate link.
        if (applicationLinkService.getApplicationLink(applicationId) != null) {
            // report the error based on the RPC URL because either
            // a) its a generic link in which case the id is effectively a hash of the RPC URL
            // b) its an Atlassian link in which case the id comes form the manifest, which comes vis the RPC URL.
            // and c) the ID is meaningless to the user.
            return RestUtil
                    .badRequest(i18nResolver.getText("applinks.error.duplicate.url", applicationLink.getRpcUrl()));
        }

        applicationLinkService.addApplicationLink(applicationId, applicationType, applicationLink.getDetails());

        return RestUtil.created(createSelfLinkFor(applicationId));
    }

    /**
     * Updates the application link but not for updating RPC URL of the link.
     *
     * @param id              the Application ID.
     * @param applicationLink The new details to set.
     * @return 'badRequest' if the target application is not reachable or if the name already exists; 'forbidden' if
     * the user performing the operation does not have sysadmin privilege; 'updated' in case of success.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     */
    @POST
    @Path("{id}")
    public Response updateApplicationLink(@PathParam("id") final String id, final ApplicationLinkEntity applicationLink)
            throws TypeNotInstalledException {
        final ApplicationType applicationType = typeAccessor.loadApplicationType(applicationLink.getTypeId());
        if (applicationType == null) {
            LOG.warn(String.format("Couldn't load type %s for application link. Type is not installed?", applicationLink.getTypeId()));
            throw new TypeNotInstalledException(applicationLink.getTypeId().get(), applicationLink.getName(), applicationLink.getRpcUrl());
        }

        // Fetch the original & mutable ApplicationLink record.
        ApplicationId applicationId = new ApplicationId(id);
        final MutableApplicationLink existing = applicationLinkService.getApplicationLink(applicationId);

        // If it doesn't exist, we can't update the link
        if (existing == null) {
            return RestUtil.badRequest(i18nResolver.getText("applinks.notfound", applicationLink.getName()));
        }

        // only a sysadmin can update a system link.
        if (existing.isSystem() && !userManager.isSystemAdmin(userManager.getRemoteUsername())) {
            return RestUtil.forbidden(i18nResolver.getText("applinks.error.only.sysadmin.operation"));
        }

        ApplicationLinkDetails linkDetails = applicationLink.getDetails();
        // We need to check the name doesn't already exist.
        if (applicationLinkService.isNameInUse(linkDetails.getName(), applicationId)) {
            return RestUtil.badRequest(i18nResolver.getText("applinks.error.duplicate.name", applicationLink.getName()));
        }

        // the relocation functionality should be used to change rpcurl instead of this sneaky way.
        if (!existing.getRpcUrl().equals(linkDetails.getRpcUrl())) {
            return RestUtil.badRequest(i18nResolver.getText("applinks.error.cannot.update.rpcurl"));
        }

        existing.update(linkDetails);
        return RestUtil.updated(createSelfLinkFor(applicationLink.getId()));
    }

    /**
     * Gets a list of the providers configured for the ApplicationLink specified by the unique id.
     *
     * @param id the id of the ApplicationLink
     * @return 'notfound' if the specified ApplicationLink cannot be found. 'servererror' if there is a problem generating the AuthenticationProviderEntities, 'ok' for success.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     */
    @GET
    @Path("{id}/authentication/provider")
    public Response getAuthenticationProvider(@PathParam("id") final String id)
            throws TypeNotInstalledException {
        ApplicationLink applicationLink = this.findApplicationLink(id);

        if (applicationLink == null) {
            return RestUtil.notFound(i18nResolver.getText("applinks.notfound", id));
        }

        final List<AuthenticationProviderEntity> configuredAuthProviders;
        try {
            configuredAuthProviders = getConfiguredProviders(applicationLink);
        } catch (URISyntaxException e) {
            return RestUtil.serverError(i18nResolver.getText("applinks.authenticationproviders.notfound", id));
        }

        return RestUtil.ok(new AuthenticationProviderEntityListEntity(configuredAuthProviders));
    }

    /**
     * Gets the configuration of the specified Authentication Provider for the the specified ApplicationLink.
     *
     * @param id       the id of the ApplicationLink
     * @param provider the fully qualified type of the provider type, e.g. 'com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider'
     * @return 'notfound' if the specified ApplicationLink cannot be found. 'servererror' if there is a problem generating the AuthenticationProviderEntities, 'ok' for success.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     */
    @GET
    @Path("{id}/authentication/provider/{provider}")
    public Response getAuthenticationProvider(@PathParam("id") final String id, @PathParam("provider") final String provider)
            throws TypeNotInstalledException {
        ApplicationLink applicationLink = this.findApplicationLink(id);

        if (applicationLink == null) {
            return RestUtil.notFound(i18nResolver.getText("applinks.notfound", id));
        }

        final List<AuthenticationProviderEntity> configuredAuthProviders;
        try {
            configuredAuthProviders = getConfiguredProviders(applicationLink, getProviderPredicate(provider));
        } catch (URISyntaxException e) {
            return RestUtil.serverError(i18nResolver.getText("applinks.authenticationproviders.notfound", id));
        }

        return RestUtil.ok(new AuthenticationProviderEntityListEntity(configuredAuthProviders));
    }

    /**
     * Registers the specified AuthenticationProvider against the specified ApplicationLink
     *
     * @param id                           the unique id of the ApplicationId.
     * @param authenticationProviderEntity the AuthenticationProvider definition.
     * @return 'notfound' if the ApplicationLink cannot be found, 'badrequest' if the specified AuthenticationProvider is not recognized, 'created' otherwise.
     * @throws com.atlassian.applinks.api.TypeNotInstalledException if applicationLink.getTypeId() refers to a non-installed type
     * @throws java.net.URISyntaxException                          if an invalid url is used when creating the "self" link in AuthenticationProviderEntities.
     */
    @PUT
    @Path("{id}/authentication/provider")
    public Response putAuthenticationProvider(@PathParam("id") final String id, final AuthenticationProviderEntity authenticationProviderEntity)
            throws TypeNotInstalledException, URISyntaxException {
        ApplicationLink applicationLink = this.findApplicationLink(id);

        if (applicationLink == null) {
            return RestUtil.notFound(i18nResolver.getText("applinks.notfound", id));
        }

        try {
            final Class providerClass = Class.forName(authenticationProviderEntity.getProvider());

            // only Sys Admin can set 2LOi
            if (TwoLeggedOAuthWithImpersonationAuthenticationProvider.class.equals(providerClass)
                    && !userManager.isSystemAdmin(userManager.getRemoteUsername())) {
                return RestUtil.badRequest(i18nResolver
                        .getText("applinks.authentication.provider.2loi.only.available.to.sysadmin", id, authenticationProviderEntity
                                .getProvider()));
            }

            authenticationConfigurationManager.registerProvider(applicationLink.getId(), providerClass, authenticationProviderEntity.getConfig());
            return RestUtil.created(Link
                    .self(new URI(ApplicationLinkResource.CONTEXT + "/" + id + "/authentication/" + authenticationProviderEntity
                            .getProvider())));
        } catch (ClassNotFoundException e) {
            return RestUtil.badRequest(i18nResolver
                    .getText("applinks.authentication.provider.type.not.recognized", id, authenticationProviderEntity
                            .getProvider()));
        }
    }

    /**
     * Get a Predicate to use to find AuthenticationProviders
     */
    private Predicate<AuthenticationProviderPluginModule> getProviderPredicate(final String provider) {
        return new Predicate<AuthenticationProviderPluginModule>() {
            public boolean apply(final AuthenticationProviderPluginModule input) {
                return input.getAuthenticationProviderClass().getName().equals(provider);
            }
        };
    }

    /**
     * Get filtered Configured providers for the current ApplicationLink.
     */
    private List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Predicate<AuthenticationProviderPluginModule> predicate)
            throws URISyntaxException {
        return getConfiguredProviders(applicationLink, Iterables.filter(pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class), predicate));
    }

    /**
     * Get unfiltered Configured providers for the current ApplicationLink.
     */
    private List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink)
            throws URISyntaxException {
        return getConfiguredProviders(applicationLink, pluginAccessor.getEnabledModulesByClass(AuthenticationProviderPluginModule.class));
    }

    /**
     * Get Configured providers for the current ApplicationLink.
     */
    private List<AuthenticationProviderEntity> getConfiguredProviders(ApplicationLink applicationLink, Iterable<AuthenticationProviderPluginModule> pluginModules)
            throws URISyntaxException {
        // get the providers configured for this link
        final List<AuthenticationProviderEntity> configuredAuthProviders = new ArrayList<AuthenticationProviderEntity>();
        for (AuthenticationProviderPluginModule authenticationProviderPluginModule : pluginModules) {
            final AuthenticationProvider authenticationProvider = authenticationProviderPluginModule.getAuthenticationProvider(applicationLink);
            if (authenticationProvider != null) {
                Map<String, String> config = authenticationConfigurationManager.getConfiguration(applicationLink.getId(), authenticationProviderPluginModule.getAuthenticationProviderClass());
                configuredAuthProviders.add(new AuthenticationProviderEntity(
                        Link.self(new URI(ApplicationLinkResource.CONTEXT + "/" + applicationLink.getId().toString() + "/authentication/provider")),
                        authenticationProviderPluginModule.getClass().getName(),
                        authenticationProviderPluginModule.getAuthenticationProviderClass().getName(),
                        config));
            }
        }
        return configuredAuthProviders;
    }

    /**
     * Find the ApplicationLink by its id.
     */
    private ApplicationLink findApplicationLink(final String id) throws TypeNotInstalledException {
        ApplicationId applicationId;
        try {
            applicationId = new ApplicationId(id);
        } catch (IllegalArgumentException e) {
            return null;
        }

        return applicationLinkService.getApplicationLink(applicationId);
    }
}
