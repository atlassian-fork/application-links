package com.atlassian.applinks.core.rest.model;

import com.atlassian.applinks.core.rest.model.adapter.ApplicationLinkStateAdapter;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

@XmlRootElement(name = "applicationLinkState")
public class ApplicationLinkStateEntity {
    @XmlJavaTypeAdapter(ApplicationLinkStateAdapter.class)
    private ApplicationLinkState appLinkState;

    @SuppressWarnings("unused")
    private ApplicationLinkStateEntity() {
    }

    public ApplicationLinkStateEntity(final ApplicationLinkState appLinkState) {
        this.appLinkState = appLinkState;
    }

    public ApplicationLinkState getAppLinkState() {
        return appLinkState;
    }
}
