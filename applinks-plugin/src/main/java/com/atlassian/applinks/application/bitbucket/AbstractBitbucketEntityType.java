// in applinks-plugin for now so we don't have to move ApplinksFeatureService to the common module
package com.atlassian.applinks.application.bitbucket;

import com.atlassian.applinks.application.IconizedIdentifiableType;
import com.atlassian.applinks.core.AppLinkPluginUtil;
import com.atlassian.applinks.internal.feature.ApplinksFeatureService;
import com.atlassian.applinks.internal.feature.ApplinksFeatures;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;

import javax.annotation.Nonnull;

import static java.lang.String.format;

/**
 * Controls the i18n string and icons for the entity type depending on whether the Bitbucket re-brand is enabled.
 *
 * @since 4.3
 */
public abstract class AbstractBitbucketEntityType extends IconizedIdentifiableType {
    private static final String I18N_KEY_TEMPLATE = "applinks.%s.project";
    private static final String I18N_PLURALIZED_KEY_TEMPLATE = "applinks.%s.project.plural";
    private static final String I18N__SHORT_KEY_TEMPLATE = "applinks.%s.project.short";

    private static final String BITBUCKET = "bitbucket";
    private static final String STASH = "stash";

    private final ApplinksFeatureService applinksFeatureService;

    public AbstractBitbucketEntityType(AppLinkPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider,
                                       ApplinksFeatureService applinksFeatureService) {
        super(pluginUtil, webResourceUrlProvider);
        this.applinksFeatureService = applinksFeatureService;
    }

    public final String getI18nKey() {

        return format(I18N_KEY_TEMPLATE, getKey());
    }

    public final String getPluralizedI18nKey() {
        return format(I18N_PLURALIZED_KEY_TEMPLATE, getKey());
    }

    public final String getShortenedI18nKey() {
        return format(I18N__SHORT_KEY_TEMPLATE, getKey());
    }

    @Nonnull
    @Override
    protected String getIconKey() {
        return super.getIconKey();
    }

    private String getKey() {
        return isRebrandEnabled() ? BITBUCKET : STASH;
    }

    private boolean isRebrandEnabled() {
        return applinksFeatureService.isEnabled(ApplinksFeatures.BITBUCKET_REBRAND);
    }
}
