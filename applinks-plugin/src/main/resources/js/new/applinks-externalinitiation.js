(function($, applinksUtil){
    /**
     * Functionality covering initiation of the Application links creation workflow from outside the AppLinks Admin page.
     */
    AppLinks.ExternalInitiation = {
        // parameter key names
        INITIATOR_REDIRECT_KEY : "initiatorRedirect",
        INITIATOR_REDIRECTNAME_KEY : "initiatorRedirectName",
        INITIATOR_TARGETURL_KEY : "initiatorTargetUrl",
        INITIATOR_SHAREDUSERBASE_KEY : "initiatorSharedUserbase",
        INITIATOR_ISREMOTEADMIN_KEY : "initiatorIsRemoteAdmin",
        APPLINKS_CREATION_STATUS_KEY: "applinksCreationStatus",
        APPLINKS_CREATION_STATUS_REASON_KEY: "applinksCreationStatusReason",
        APPLINKS_CREATION_ID_KEY: "applinksCreatedId",
        APPLINKS_CREATION_STATUS_SUCCESS: "created",
        APPLINKS_CREATION_STATUS_FAIL: "failed",
        /**
         * Get the redirect parameter
         * @returns {*}
         */
        getRedirectParam: function () {
            return applinksUtil.getParameterValue(AppLinks.ExternalInitiation.INITIATOR_REDIRECT_KEY);
        },
        /**
         * Get the Application target name parameter.
         * @returns {*}
         */
        getRedirectNameParam: function () {
            return applinksUtil.getParameterValue(AppLinks.ExternalInitiation.INITIATOR_REDIRECTNAME_KEY);
        },
        /**
         * Get the Application target url parameter.
         * @returns {*}
         */
        getTargetUrlParam: function () {
            return applinksUtil.getParameterValue(AppLinks.ExternalInitiation.INITIATOR_TARGETURL_KEY);
        },
        /**
         * Get the SharedUserbase parameter.
         * @returns {*}
         */
        getSharedUserbaseParam: function () {
            return applinksUtil.getParameterValue(AppLinks.ExternalInitiation.INITIATOR_SHAREDUSERBASE_KEY);
        },
        /**
         * Get the isRemoteAdmin parameter.
         * @returns {*}
         */
        getIsRemoteAdminParam: function () {
            return applinksUtil.getParameterValue(AppLinks.ExternalInitiation.INITIATOR_ISREMOTEADMIN_KEY);
        },
        /**
         * Initialize the ExternalInitiation fields.
         */
        init: function(creationState) {
            creationState.setInitiatorRedirect(AppLinks.ExternalInitiation.getRedirectParam());
            creationState.setInitiatorRedirectName(AppLinks.ExternalInitiation.getRedirectNameParam());
            creationState.setInitiatorTargetUrl(AppLinks.ExternalInitiation.getTargetUrlParam());
            creationState.setInitiatorSharedUserbase(AppLinks.ExternalInitiation.getSharedUserbaseParam());
            creationState.setInitiatorIsRemoteAdmin(AppLinks.ExternalInitiation.getIsRemoteAdminParam());

            // if passed a target url, set the applicaiton link url entry field.
            if(creationState.getInitiatorTargetUrl() !== null
                && creationState.getInitiatorTargetUrl() !== "") {
                AppLinks.Creation.userDefinedUrlField.val(creationState.getInitiatorTargetUrl());
            }
        },
        /**
         * Redirect the user to the InitiatorRedirect informing the user what is going on.
         */
        redirectToInitiatorRedirect: function (creationState) {
            applinksUtil.waitUntilDone(function() {
                return {
                    'wait': function() {
                        var target = creationState.getInitiatorRedirectName() !== null ? creationState.getInitiatorRedirectName() : creationState.getInitiatorRedirect();
                        AppLinks.Creation.Dialogs.showRedirectPauseDialog(false, target);
                    },
                    'done': function(){
                        window.location = AppLinks.ExternalInitiation.generateInitiatorRedirectionUrl(creationState);
                    }
                };
            })();
        },
        /**
         * Show a dialog informing the user they are being redirected.
         * @returns {{wait: Function, done: Function}}
         */
        showRedirectToRedirect: function(creationState) {
            return {
                'wait': function() {
                    var target = creationState.getInitiatorRedirectName() !== null ? creationState.getInitiatorRedirectName() : creationState.getInitiatorRedirect();
                    AppLinks.Creation.Dialogs.showRedirectPauseDialog(false, target);
                },
                'done': function(){
                    window.location = AppLinks.ExternalInitiation.generateInitiatorRedirectionUrl(creationState);
                }
            };
        },
        /**
         * Apply the initiator provided values to the creation dialog.
         */
        setDialogInitiatorDefaults: function(creationState) {
            if(!creationState.doInitiatorRedirect()) {
                return;
            }
            if (creationState.getInitiatorSharedUserbase() !== null)
            {
                $('#userbase').prop('checked', creationState.getInitiatorSharedUserbase() == 'true');
            }

            if (creationState.getInitiatorIsRemoteAdmin() !== null)
            {
                $('#isAdmin').prop('checked', creationState.getInitiatorIsRemoteAdmin() == 'true');
            }
        },
        /**
         * Generate the url to use to redirect the user back to the initiator
         * @return {*}
         */
        generateInitiatorRedirectionUrl: function (creationState) {
            var parametersMap = {};

            if(creationState.creationHasCompletedSuccessfully()) {
                parametersMap[AppLinks.ExternalInitiation.APPLINKS_CREATION_STATUS_KEY] = AppLinks.ExternalInitiation.APPLINKS_CREATION_STATUS_SUCCESS;
                parametersMap[AppLinks.ExternalInitiation.APPLINKS_CREATION_ID_KEY] = creationState.getOriginalCreatedApplicationLinkId();
            } else {
                parametersMap[AppLinks.ExternalInitiation.APPLINKS_CREATION_STATUS_KEY] = AppLinks.ExternalInitiation.APPLINKS_CREATION_STATUS_FAIL;
                parametersMap[AppLinks.ExternalInitiation.APPLINKS_CREATION_STATUS_REASON_KEY] = creationState.getFirstFailureStatusDetail();
                if(creationState.getOriginalCreatedApplicationLinkId() !== null) {
                    parametersMap[AppLinks.ExternalInitiation.APPLINKS_CREATION_ID_KEY] = creationState.getOriginalCreatedApplicationLinkId();
                }
            }

            // make sure the redirection URL has a protocol, add http if there isn't one
            return AppLinks.Creation.generateUrl(AppLinks.UI.addProtocolToURL(creationState.getInitiatorRedirect()), parametersMap);
        },
        /**
         * Check the redirect status and if required force the redirect.
         */
        processInitiatorRedirect: function(creationState)
        {
            if (creationState.doInitiatorRedirect() && creationState.creationHasCompleted())
            {
                // redirect to the redirect
                AppLinks.ExternalInitiation.redirectToInitiatorRedirect(creationState);
            }
        }
}
})(AJS.$, require('applinks/js/util'));
