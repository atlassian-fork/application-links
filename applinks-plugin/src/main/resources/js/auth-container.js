/**
 * Refresh the config iframe if you switch click on a tab.
 */

/** AJS.$(document).ready() won't be called on iframes unfortunately. At least not in Firefox.
 * Had to transform it into a 'ready' function which is called at the end of the page.
 */

var atlassianAuthContainerReadyHandler = function() {
    // force disable TZ update flag
    window.__tzTesting = true;

    AJS.tabs.setup();
    (function ($) {
    var $iframes = $('.auth-container').find('iframe');
        $('.menu-item a').each(function (index) {
            var $tab = $(this),
                $iframe = $iframes.eq(index),
                $loadIcon = $('<div class="loading-tabs"><aui-spinner size="small"></aui-spinner></div>').insertAfter($iframe);
            $iframe.load(function() {
                    $loadIcon.hide();
                });
            $tab.bind('tabSelect', function (a, tab) {
                $loadIcon.show();
                $iframe.attr('src', $iframe.attr('src'));
            });
        });
    })(AJS.$);
};
