define('applinks/page/v3', [
    'applinks/lib/jquery',
    'applinks/lib/backbone',
    'applinks/common/events',
    'applinks/common/i18n',
    'applinks/common/initializer',
    'applinks/feature/bitbucket-rebrand',
    'applinks/feature/help-link/analytics',
    'applinks/feature/v3/list',
    'applinks/feature/v3/onboarding',
    'applinks/feature/v3/feedback-collector'
], function(
    $,
    Backbone,
    Events,
    I18n,
    Initializer,
    BitbucketRebrand,
    HelpLinkAnalytics,
    V3List,
    Onboarding,
    FeedbackCollector
){
    var V3PageView = Backbone.View.extend({
       initialize: function () {
           this.applinksList = new V3List({
               el: this.$el.find('.content')
           });
       }
    });

    return {
        init: function() {
            new V3PageView({ el: $('#ual') });
            Initializer.init(BitbucketRebrand, HelpLinkAnalytics, FeedbackCollector, Onboarding);
        }
    }
});

