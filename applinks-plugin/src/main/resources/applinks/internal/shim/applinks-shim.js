define('applinks/shim/applinks', [
    'applinks/lib/window',
    'applinks/common/preconditions'
], function (Window,
             Preconditions) {
    var applinks = Window.AppLinks;
    Preconditions.hasValue(applinks, 'applinks');
    return applinks;
});