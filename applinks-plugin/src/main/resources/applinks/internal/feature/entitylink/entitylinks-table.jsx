import React, { Component } from 'react';
import Lozenge from '@atlaskit/lozenge';
import DynamicTable from '@atlaskit/dynamic-table';
import { I18n } from '@atlassian/wrm-react-i18n';

import { getApplicationTypeName } from 'applinks/common/i18n';
import './entity-links.jsx';
import EntityLinksMoreActions from './entitylinks-table-actions.jsx';
import Tooltip from '@atlaskit/tooltip';

export default class EntityLinkTable extends Component {
    constructor(props) {
        super(props);
    }

    static projectLinksApplicationName(entityLink, serverName, outgoingAppName) {
        return (
            <div className="application-name-alignment">
                <img src={entityLink.iconUri} width={25} height={25} />
                <span className="application-name-margin">
                    {serverName}
                    <span className="application-name-secondary">{`(${outgoingAppName})`}</span>
                </span>
            </div>
        );
    }

    static projectLinksName(projectName, isProjectPrimary) {
        return (
            <div className="name-container">
                <span className={'entity-link-name'}>{projectName}</span>
                {isProjectPrimary && (
                    <Tooltip content={I18n.getText('entitylinks.application.primary.tooltip')}>
                        <Lozenge>{I18n.getText('entitylinks.application.primary')}</Lozenge>
                    </Tooltip>
                )}
            </div>
        );
    }

    static calculateWidth(value, multiplier) {
        return I18n.getText(value).length * multiplier;
    }

    static createHead() {
        return {
            cells: [
                {
                    key: 'application',
                    content: I18n.getText('entitylinks.application.name.header'),
                    width: this.calculateWidth('entitylinks.application.name.header', 0.75),
                    isSortable: true,
                },
                {
                    key: 'name',
                    content: I18n.getText('entitylinks.project.name.header'),
                    width: this.calculateWidth('entitylinks.project.name.header', 1.5),
                    isSortable: true,
                },
                {
                    key: 'key',
                    content: I18n.getText('entitylinks.project.key.header'),
                    width: this.calculateWidth('entitylinks.project.key.header', 0.75),
                    isSortable: true,
                },
                {
                    key: 'actions',
                },
            ],
        };
    }

    createRows(entityLinks, type, applinks) {
        return entityLinks.map((entityLink, index) => {
            const applink = applinks.find(applink => applink.id === entityLink.applicationId);
            const outgoingApplicationName = getApplicationTypeName(applink.type);
            return {
                key: `row-${index}-${entityLink.applicationId}-${entityLink.key}`,
                cells: [
                    {
                        key: `${entityLink.applicationId}-${entityLink.key}`,
                        content: EntityLinkTable.projectLinksApplicationName(
                            entityLink,
                            applink.name,
                            outgoingApplicationName
                        ),
                    },
                    {
                        key: `${entityLink.applicationId}-${entityLink.key}`,
                        content: EntityLinkTable.projectLinksName(
                            entityLink.name,
                            entityLink.isPrimary
                        ),
                    },
                    {
                        key: `${entityLink.applicationId}-${entityLink.key}`,
                        content: entityLink.key,
                    },
                    {
                        key: `${entityLink.applicationId}-${entityLink.key}`,
                        content: (
                            <EntityLinksMoreActions
                                currentApp={this.props.currentApp}
                                entityLink={entityLink}
                                type={type}
                                onDelete={entityLink => this.props.onDelete(entityLink)}
                                onMakePrimary={entityLink => this.props.onMakePrimary(entityLink)}
                                outgoingApplicationName={outgoingApplicationName}
                                projectKey={this.props.projectKey}
                            />
                        ),
                    },
                ],
            };
        });
    }

    render() {
        const { type, entityLinks, applicationLinks } = this.props;

        return (
            <DynamicTable
                head={EntityLinkTable.createHead()}
                rows={this.createRows(entityLinks, type, applicationLinks)}
            />
        );
    }
}
