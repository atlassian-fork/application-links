import React, { Component, Fragment } from 'react';
import Select from '@atlaskit/select';
import { Checkbox } from '@atlaskit/checkbox';
import Form, { CheckboxField, ErrorMessage, Field } from '@atlaskit/form';
import ModalDialog, { ModalFooter } from '@atlaskit/modal-dialog';
import { I18n } from '@atlassian/wrm-react-i18n';
import Button, { ButtonGroup } from '@atlaskit/button';
import SectionMessage from '@atlaskit/section-message';
import {
    createReciproticalLink,
    getAnonymousProjectsResponse,
    getPermissionsResponse,
    getProjectsResponse,
    PermissionCode,
} from './EntitylinksRest';
import AuthButton from './auth-button';
import Spinner from '@atlaskit/spinner';

const ValidationState = {
    ERROR: 'error',
    DEFAULT: 'default',
};

const ApplinkStatus = {
    NO_MORE_PROJECTS: 'no_more_projects',
    REQUEST_FAILED: 'request_failed',
    AUTHENTICATION_REQUIRED: 'authentication_required',
};

const Option = ({ iconUri, primary, secondary }) => (
    <div className="option">
        <img src={iconUri} width={20} height={20} />
        {primary}
        <span className="secondary-container">{secondary}</span>
    </div>
);

const getApplicationOption = option => {
    return (
        <Option iconUri={option.data.iconUri} primary={option.name} secondary={option.displayUrl} />
    );
};

const getProjectOption = option => {
    return <Option iconUri={option.iconUri} primary={option.name} secondary={option.key} />;
};

export default class CreateEntityLinkDialogForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projects: [],
            selectedProject: null,
            appFieldValue: '',
            projectFieldValue: '',
            twoWayAllowed: false,
            appLinkHealth: null,
            authUrl: null,
        };
    }

    checkTwoWayAllowed(selectedApplink) {
        getPermissionsResponse(selectedApplink)
            .then(response => response.json())
            .then(data => {
                const permissionCode = data.code;
                switch (permissionCode) {
                    case PermissionCode.NO_PERMISSION:
                    case PermissionCode.ALLOWED:
                        this.setState({ twoWayAllowed: permissionCode === 'ALLOWED' });
                        this.loadProjectsForSelect(
                            selectedApplink,
                            getProjectsResponse(selectedApplink)
                        );
                        break;
                    case PermissionCode.CREDENTIALS_REQUIRED:
                    case PermissionCode.AUTHENTICATION_FAILED:
                        this.setState({
                            applinkHealth: ApplinkStatus.AUTHENTICATION_REQUIRED,
                            authUrl: data.url,
                        });
                        break;
                    default:
                        console.error(
                            'Something is wrong with the applink the status was: ' + permissionCode
                        );
                        this.setState({ applinkHealth: ApplinkStatus.REQUEST_FAILED });
                }
            })
            .catch(e => {
                console.error('Something went wrong ' + e);
                this.setState({ applinkHealth: ApplinkStatus.REQUEST_FAILED });
            });
    }

    onProjectSelected(selectedOption) {
        const selectedApplink = selectedOption.value;
        this.setState({
            appFieldValue: selectedOption,
            projects: [],
            applinkHealth: null,
        });
        this.checkTwoWayAllowed(selectedApplink);
    }

    skipAuthentication() {
        const selectedApplink = this.state.appFieldValue.value;
        this.loadProjectsForSelect(selectedApplink, getAnonymousProjectsResponse(selectedApplink));
    }

    loadProjectsForSelect(selectedApplink, fetchResponse) {
        fetchResponse
            .then(response => response.json())
            .then(data => {
                const projects = data.entity
                    .map(entity => ({
                        ...entity,
                        label: entity.name,
                        value: entity.key,
                    }))
                    .filter(link => {
                        //filter out existing links
                        return !this.doesLinkExists(link, selectedApplink);
                    });
                this.setState({
                    projects: projects,
                    applinkHealth: projects.length === 0 ? ApplinkStatus.NO_MORE_PROJECTS : null,
                });
            });
    }

    doesLinkExists(link, selectedApplink) {
        return this.props.entityLinks.some(other => {
            return link.key === other.key && selectedApplink === other.applicationId;
        });
    }

    createEntityLink(data) {
        if (this.state.applinkHealth === ApplinkStatus.NO_MORE_PROJECTS) {
            //not really being able to validate this in the AK form because, well AK so we need to check it manually.
            return;
        }
        const { projectKey, type } = this.props;
        const entityLink = {
            applicationId: this.state.appFieldValue.value,
            typeId: data['project-select'].typeId,
            key: data['project-select'].value,
            name: data['project-select'].name,
        };
        createReciproticalLink(entityLink, projectKey, type, data.twowayselect)
            .then(response => response.json())
            .then(data => {
                this.props.onEntityAdded(data);
                this.props.onClose();
            });
    }

    validateEmpty(value) {
        if (!value) {
            return 'EMPTY';
        }

        return;
    }

    getButtons() {
        if (this.state.applinkHealth === ApplinkStatus.AUTHENTICATION_REQUIRED) {
            return () => (
                <ModalFooter>
                    <Button appearance="link" onClick={e => this.skipAuthentication()}>
                        {I18n.getText('entitylinks.create.dailog.anonymous.skip',
                            this.props.currentApp,
                        )}
                    </Button>
                    <ButtonGroup>
                        <AuthButton
                            successCallback={() => this.onProjectSelected(this.state.appFieldValue)}
                            url={this.state.authUrl}
                            currentApp={this.props.currentApp}
                        />
                        <Button appearance="subtle-link" onClick={this.props.onClose}>
                            {I18n.getText('entitylinks.dialog.cancel')}
                        </Button>
                    </ButtonGroup>
                </ModalFooter>
            );
        } else {
            return () => (
                <ModalFooter>
                    <span />
                    <ButtonGroup>
                        <Button appearance="primary" type="submit" className="create-link">
                            {I18n.getText('entitylinks.dialog.create.link')}
                        </Button>
                        <Button appearance="subtle-link" onClick={this.props.onClose}>
                            {I18n.getText('entitylinks.dialog.cancel')}
                        </Button>
                    </ButtonGroup>
                </ModalFooter>
            );
        }
    }

    render() {
        const applinks = this.props.applicationLinks.map(link => ({
            ...link,
            label: link.name,
            value: link.id,
        }));

        const applicationSelected = this.state.projects.length > 0;
        const isLoadingProjects =
            this.state.projects.length === 0 &&
            this.state.applinkHealth === null &&
            this.state.appFieldValue;
        const authenticationRequired =
            this.state.applinkHealth === ApplinkStatus.AUTHENTICATION_REQUIRED;

        const getValidationState = (error, valid) => {
            if (this.state.applinkHealth === ApplinkStatus.NO_MORE_PROJECTS || error) {
                return ValidationState.ERROR;
            }

            return ValidationState.DEFAULT;
        };

        return (
            <ModalDialog
                heading={I18n.getText('entitylinks.create.dialog.title')}
                onClose={this.props.onClose}
                autoFocus={true}
                components={{
                    Container: ({ children, className }) => (
                        <Form onSubmit={data => this.createEntityLink(data)}>
                            {({ formProps }) => (
                                <form {...formProps} className={className}>
                                    {children}
                                </form>
                            )}
                        </Form>
                    ),
                    Footer: this.getButtons(),
                }}
            >
                <div>
                    <p>{I18n.getText('entitylinks.create.dialog.description')}</p>
                    <Field
                        label={I18n.getText('entitylinks.create.dialog.select.application.label')}
                        helperText={I18n.getText(
                            'entitylinks.create.dialog.select.application.helperText'
                        )}
                        id="applselect"
                        name="application-select"
                        validate={this.validateEmpty}
                        defaultValue={this.state.appFieldValue}
                    >
                        {({ fieldProps, error }) => (
                            <Fragment>
                                <Select
                                    {...fieldProps}
                                    menuPortalTarget={document.body}
                                    styles={{ menuPortal: base => ({ ...base, zIndex: 9999 }) }}
                                    formatOptionLabel={getApplicationOption}
                                    className="single-select"
                                    classNamePrefix="react-select"
                                    options={applinks}
                                    placeholder={I18n.getText(
                                        'entitylinks.create.dialog.select.application.placeholder'
                                    )}
                                    onChange={value => {
                                        fieldProps.onChange(value);
                                        this.onProjectSelected(value);
                                    }}
                                    validationState={getValidationState(error)}
                                />
                                {error === 'EMPTY' && (
                                    <ErrorMessage>
                                        {I18n.getText(
                                            'entitylinks.create.dialog.select.application.error.empty'
                                        )}
                                    </ErrorMessage>
                                )}
                                {this.state.applinkHealth === ApplinkStatus.NO_MORE_PROJECTS && (
                                    <ErrorMessage>
                                        {I18n.getText(
                                            'entitylinks.create.dialog.select.application.error.no.options'
                                        )}
                                    </ErrorMessage>
                                )}
                                {this.state.applinkHealth === ApplinkStatus.REQUEST_FAILED && (
                                    <ErrorMessage>
                                        {I18n.getText(
                                            'entitylinks.create.dialog.select.application.error.applink'
                                        )}
                                    </ErrorMessage>
                                )}
                            </Fragment>
                        )}
                    </Field>
                    {isLoadingProjects && (
                        <div className="spinner-container">
                            <Spinner size="large" />
                        </div>
                    )}
                    {applicationSelected && (
                        <div>
                            <Field
                                label={I18n.getText(
                                    'entitylinks.create.dialog.select.project.label'
                                )}
                                helperText={I18n.getText(
                                    'entitylinks.create.dialog.select.project.helperText'
                                )}
                                id="projectselect"
                                validate={this.validateEmpty}
                                name="project-select"
                            >
                                {({ fieldProps, error }) => (
                                    <Fragment>
                                        <Select
                                            autoFocus
                                            {...fieldProps}
                                            formatOptionLabel={getProjectOption}
                                            menuPortalTarget={document.body}
                                            styles={{
                                                menuPortal: base => ({ ...base, zIndex: 9998 }),
                                            }}
                                            className="single-select"
                                            classNamePrefix="react-select"
                                            options={this.state.projects}
                                            placeholder={I18n.getText(
                                                'entitylinks.create.dialog.select.project.placeholder'
                                            )}
                                            validationState={getValidationState(error)}
                                        />
                                        {error === 'EMPTY' && (
                                            <ErrorMessage>
                                                {I18n.getText(
                                                    'entitylinks.create.dialog.select.project.error.empty'
                                                )}
                                            </ErrorMessage>
                                        )}
                                    </Fragment>
                                )}
                            </Field>
                            <CheckboxField
                                name="twowayselect"
                                defaultIsChecked={this.state.twoWayAllowed}
                            >
                                {({ fieldProps }) => (
                                    <Checkbox
                                        {...fieldProps}
                                        isDisabled={!this.state.twoWayAllowed}
                                        label={I18n.getText(
                                            'entitylinks.create.dialog.checkbox.twoway.label'
                                        )}
                                    />
                                )}
                            </CheckboxField>
                        </div>
                    )}
                    {authenticationRequired && (
                        <div className="info-auth-container">
                            <SectionMessage
                                appearance="info"
                                title={I18n.getText('entitylinks.create.dialog.auth.title',
                                    this.props.currentApp,
                                    this.state.appFieldValue.type
                                )}
                            >
                                <p>{I18n.getText('entitylinks.create.dialog.auth.message')}</p>
                            </SectionMessage>
                        </div>
                    )}
                </div>
            </ModalDialog>
        );
    }
}
