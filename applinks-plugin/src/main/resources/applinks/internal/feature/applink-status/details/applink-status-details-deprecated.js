define('applinks/feature/status/details/deprecated', [
    'applinks/feature/migration',
    'applinks/feature/status/errors'
], function(
    Migration,
    ApplinkStatusErrors
) {
   return {
       onLoad: function(statusDialog, applink) {
           statusDialog._dialog.$popup.find('.applinks-status-update').on('click', function() {
               statusDialog._dialog.hide();
                if (ApplinkStatusErrors.MANUAL_LEGACY_REMOVAL.matches(applink.get('status'))) {
                    Migration.removeLocal(applink);
                } else {
                    Migration.migrate(applink);
                }
           })
       }
   };
});