define('applinks/feature/v3/ui', [
    'applinks/lib/aui',
    'applinks/lib/jquery'
], function(
    AJS,
    $
) {
    return {
        isOn: function() {
            return $('#ual.applinks-admin-v3').length > 0;
        },

        isFinishedOnboarding : function() {
            if ($("body").attr("data-v3-onboarding-dialog-finished")) {
                var featureDiscoveryDialog = $('#v3-ui-feature-discovery-dialog');
                if (featureDiscoveryDialog.length === 0 || featureDiscoveryDialog.is(':hidden')) {
                    return true;
                }
            }
        }
    }
});