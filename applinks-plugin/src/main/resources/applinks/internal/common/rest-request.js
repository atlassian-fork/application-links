define('applinks/common/rest-request', [
    'applinks/lib/console',
    'applinks/lib/window',
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/common/preconditions',
    'applinks/common/response-status',
    'applinks/common/response-handlers'
], function(
    console,
    window,
    $,
    _,
    Preconditions,
    ResponseStatus,
    ResponseHandlers
) {
    var defaultRequestSettings = {
        dataType: 'json',
        contentType: 'application/json'
    };

    // construct the target URL by adding query params
    function _toUrl(url, queryParams) {
        if (_.isEmpty(queryParams)) {
            return url;
        }
        var separator = url.indexOf('?') > -1 ? '&' : '?';
        return url + separator + $.param(queryParams);
    }

    function _toQueryParams(paramsObject) {
        if (!paramsObject) {
            return [];
        } else if (_.isArray(paramsObject)) {
            return paramsObject;
        } else {
            var result = [];
            _.each(paramsObject, function(value, key) {
                result.push({name: key, value: value});
            });
            return result;
        }
    }

    /**
     * Creates a new instance of `ApplinksRestRequest`.
     *
     * @param url URL to request to. NOTE: URLs with fragments (`#`) are not supported
     * @param {Object} params query parameters to add to the request URL. Can be an object, or an array of objects
     *                 containing `name` and `value` property
     * @constructor
     */
    function ApplinksRestRequest(url, params) {
        Preconditions.nonEmptyString(url, 'url');
        Preconditions.checkArgument(url.indexOf('#') == -1, 'url: "#" fragments not supported');
        this._url = url;
        this._queryParams = _toQueryParams(params);
        this._expectedStatus = [ResponseStatus.Family.SUCCESSFUL];
    }

    ApplinksRestRequest.GET = 'GET';
    ApplinksRestRequest.PUT = 'PUT';
    ApplinksRestRequest.POST = 'POST';
    ApplinksRestRequest.DELETE = 'DELETE';

    ApplinksRestRequest.prototype.get = function() {
        return this._execute(this._requestSettings(ApplinksRestRequest.GET));
    };

    ApplinksRestRequest.prototype.del = function() {
        return this._execute(this._requestSettings(ApplinksRestRequest.DELETE));
    };

    ApplinksRestRequest.prototype.put = function(data) {
        var settings = this._requestSettings(ApplinksRestRequest.PUT);
        if (data) {
            settings.data = data;
        }
        return this._execute(settings);
    };

    ApplinksRestRequest.prototype.post = function(data) {
        var settings = this._requestSettings(ApplinksRestRequest.POST);
        if (data) {
            settings.data = data;
        }
        return this._execute(settings);
    };

    ApplinksRestRequest.prototype.getUrl = function() {
        return _toUrl(this._url, this._queryParams);
    };

    /**
     * Add query param to this request.
     *
     * @param {string} name param name
     * @param {string} value param value
     * @return {ApplinksRestRequest} this request
     */
    ApplinksRestRequest.prototype.queryParam = function(name, value) {
        this._queryParams.push({name: name, value: value});
        return this;
    };

    /**
     * Add query params to this request.
     *
     * @param {Object} params query parameters to add to the request URL. Can be an object, or an array of objects
     *                 containing `name` and `value` property
     * @returns {ApplinksRestRequest} this request
     */
    ApplinksRestRequest.prototype.queryParams = function(params) {
        this._queryParams = this._queryParams.concat(_toQueryParams(params));
        return this;
    };

    /**
     * Add `authorisationCallback` query parameter to this request, this is useful for resources that are expected to
     * respond with `authorisationUri`
     *
     * @param {string} callback URI to call back once authorisation is done
     * @return {ApplinksRestRequest} this request
     */
    ApplinksRestRequest.prototype.authorisationCallback = function(callback) {
        this.queryParam('authorisationCallback', callback);
        return this;
    };

    /**
     * Expect a specific status (or status family). Use a specific number or one of the members defined in
     * `applinks/common/response-status`. By default a successful 20x response is expected. If the response does not
     * meet this expectation, a default error handler will be invoked.
     *
     * Invoke this method with any number of of statuses expressed as an object that contains `matches` method (using
     * the ones in `applinks/lib/response-status` is highly recommended).
     * @returns {ApplinksRestRequest} this request
     */
    ApplinksRestRequest.prototype.expectStatus = function() {
        this._expectedStatus = _.isArray(arguments[0]) ? arguments[0] : _.toArray(arguments);
        return this;
    };

    ApplinksRestRequest.prototype._requestSettings = function(requestType) {
        var that = this;
        return _.defaults({
            url: _toUrl(that._url, that._queryParams),
            type: requestType
        }, defaultRequestSettings);
    };

    ApplinksRestRequest.prototype._execute = function(settings) {
        return $.ajax(settings)
            .done(ResponseHandlers.done(this._expectedStatus))
            .fail(ResponseHandlers.fail(this._expectedStatus));
    };

    return ApplinksRestRequest;
});
