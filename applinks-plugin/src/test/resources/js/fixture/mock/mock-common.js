define('applinks/test/mock-common', [
    'applinks/test/mock-declaration'
], function(
    MockDeclaration
) {

    // see products.js
    var ProductConstants = {
        BAMBOO: 'bamboo',
        BITBUCKET: 'stash',
        CONFLUENCE: 'confluence',
        FECRU: 'fecru',
        JIRA: 'jira',
        REFAPP: 'refapp',
        STASH: 'stash',

        getTypeName: function(typeId) {
            return MockProductI18n[typeId];
        }
    };

    var MockProductI18n = {
        bamboo: "Bamboo",
        stash: 'Bitbucket Server',
        confluence: 'Confluence',
        fecru: 'Fisheye / Crucible',
        jira: 'JIRA',
        refapp: 'Reference Application'
    };

    // see events.js
    var EventsConstants = {
        PREREADY: 'applinks.event.preready',
        READY: 'applinks.event.ready',
        APPLINKS_LOADED: 'applinks.event.loaded',
        APPLINKS_UPDATED: 'applinks.event.updated',
        ORPHANED_UPGRADE: 'applinks.event.orphaned.upgrade'
    };

    // use factory function because the nested properties can't be deep-copied and are not refreshed between tests
    // see rest.js
    var restConstantsFactory = function() {
        return {
            V2: {},
            V3: {
                ApplinkData: {
                    APPLICATION_VERSION: 'applicationVersion',
                    ATLASSIAN: 'atlassian',
                    CAPABILITIES: 'capabilities',
                    CONFIG_URL: 'configUrl',
                    ICON_URI: 'iconUri',
                    EDITABLE: 'editable',
                    V3_EDITABLE: 'v3Editable',
                    WEB_ITEMS: 'webItems'
                }
            }
        };
    };

    /**
     * Set up utility to call in beforeEach, takes applinks common stubs and adds default return values and "answer"
     * field that points to the returned value. Must be instantiated with the current test instance.
     * @param currentTest current test instance
     * @constructor
     */
    function Stubber(currentTest) {
        if (!currentTest) {
            throw new Error('No current test');
        }
        this.currentTest = currentTest;
    }

    Stubber.prototype.setUpPromise = function() {
        _.each(arguments, function(mockPromise) {
            mockPromise.done.returns(mockPromise);
            mockPromise.fail.returns(mockPromise);
            mockPromise.always.returns(mockPromise);
        });
        return this;
    };

    Stubber.prototype.setUpRestRequest = function() {
        var self = this;
        _.each(arguments, function(MockRestRequest) {
            function stubWithPromise(method) {
                var promise = _newPromiseDeclaration().createMock(self.currentTest.sandbox);
                self.setUpPromise(promise);
                method.answer = promise;
                method.returns(method.answer);
            }

            var restRequest = MockRestRequest.answer;

            stubWithPromise(restRequest.get);
            stubWithPromise(restRequest.put);
            stubWithPromise(restRequest.del);
            stubWithPromise(restRequest.post);
            restRequest.queryParam.returns(restRequest);
            restRequest.queryParams.returns(restRequest);
            restRequest.expectStatus.returns(restRequest);
        });
        return this;
    };

    Stubber.prototype.setUpRest = function(MockApplinksRest) {
        var self = this;

        function setUpAllStubs(namespace) {
            // all sinon stubs assumed to be rest.js functions returning RestRequest
            _.chain(namespace).values().each(function(value) {
                if (value.isSinonProxy) {
                    var MockRestRequest = _newRestRequestDeclaration().createMock(self.currentTest.sandbox);
                    self.setUpRestRequest(MockRestRequest);
                    // MockRestRequest is a constructor function mock
                    value.returns(MockRestRequest.answer);
                    value.answer = MockRestRequest.answer;
                }
            });
        }

        setUpAllStubs(MockApplinksRest.V1);
        setUpAllStubs(MockApplinksRest.V2);
        setUpAllStubs(MockApplinksRest.V3);

        return this;
    };

    function _newPromiseDeclaration() {
        return new MockDeclaration(['done', 'fail', 'always']);
    }

    function _newPromise(currentTest) {
        var promise = _newPromiseDeclaration().createMock(currentTest.sandbox);
        new Stubber(currentTest).setUpPromise(promise);
        return promise;
    }

    function _newRestRequestDeclaration() {
        return new MockDeclaration([
            'get', 'put', 'del', 'post',
            'getUrl',
            'queryParam', 'queryParams',
            'expectStatus'
        ]).withConstructor();
    }

    /**
     * Mock factory and stubber for "applinks/common/*" modules
     */
    return {
        // mock module definitions, require applinks/test/qunit instance
        mockEventsModule: function(QUnit) {
            return QUnit.moduleMock('applinks/common/events',
                new MockDeclaration(['on', 'off', 'trigger'], EventsConstants));
        },
        mockProductsModule: function(QUnit) {
            return QUnit.moduleMock('applinks/common/products', new MockDeclaration(['getTypeName'], ProductConstants));
        },
        mockRestModule: function(QUnit) {
            return QUnit.moduleMock('applinks/common/rest',
                new MockDeclaration([
                    'V1.applink',
                    'V3.applink',
                    'V3.applinks',
                    'V3.featureDiscovery',
                    'V3.features',
                    'V3.status',
                    'V3.oAuthStatus'
                ], restConstantsFactory));
        },
        mockRestRequestModule: function(QUnit) {
            return QUnit.moduleMock('applinks/common/rest-request', _newRestRequestDeclaration());
        },
        mockPromiseModule: function(QUnit) {
            return QUnit.mock(_newPromiseDeclaration())
        },

        // mock declarations
        mockPromiseDeclaration: _newPromiseDeclaration,

        // mock instances
        mockPromise: _newPromise,

        Stubber: Stubber
    };
});