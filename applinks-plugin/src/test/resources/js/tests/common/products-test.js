define([
    'applinks/lib/lodash',
    'applinks/test/hamjest',
    'applinks/test/qunit',
    'applinks/test/mock-declaration'
], function(
    _,
    __,
    QUnit,
    MockDeclaration
) {
    function applinksModulesConstants() {
        return {
            COMMON_EXPORTED: 'applinks-common-exported'
        };
    }

    QUnit.module('applinks/common/products', {
        require: {
            main: 'applinks/common/products'
        },
        mocks: {
            WRM: QUnit.moduleMock('applinks/lib/wrm', new MockDeclaration(['data.claim'])),
            AppliksModules: QUnit.moduleMock('applinks/common/modules',
                new MockDeclaration(['dataFqn'], applinksModulesConstants)),
            Preconditions: QUnit.moduleMock('applinks/common/preconditions', new MockDeclaration(['hasValue']))
        }
    });

    QUnit.test('getTypeName uses applinks-products data', function(assert, ApplinksProducts, mocks) {
        var productMap = {
            jira: 'JIRA',
            stash: 'Bitbucket Server'
        };
        mocks.AppliksModules.dataFqn.withArgs(mocks.AppliksModules.COMMON_EXPORTED, 'applinks-types')
            .returns('applinks-types-id');
        mocks.WRM.data.claim.withArgs('applinks-types-id').returns(productMap);
        mocks.Preconditions.hasValue.withArgs(productMap, 'types', 'Application Types data not found')
            .returns(productMap);

        assert.assertThat(ApplinksProducts.getTypeName('jira'), __.equalTo('JIRA'));
        assert.assertThat(ApplinksProducts.getTypeName('stash'), __.equalTo('Bitbucket Server'));
        assert.assertThat(ApplinksProducts.getTypeName('charlie'), __.equalTo('charlie')); // no mapping, returns key

        // all setup calls should happen once on first call to getTypeName
        assert.assertThat(mocks.WRM.data.claim, __.calledOnce());
        assert.assertThat(mocks.AppliksModules.dataFqn, __.calledOnce());
        assert.assertThat(mocks.Preconditions.hasValue, __.calledOnce());
    });
});