define([
    'applinks/test/qunit',
    'applinks/test/hamjest'
], function(
    QUnit,
    __
) {
    QUnit.module('applinks/common/preconditions', {
        require: {
            main: 'applinks/common/preconditions'
        }
    });

    QUnit.test('Preconditions.nonEmptyString for empty string and no variable name', function(assert, Preconditions) {
        var test = function() {
            Preconditions.nonEmptyString('')
        };
        assert.assertThat(test, __.throws(__.error('[unspecified]: expected a non-empty string, was: <>')));
    });

    QUnit.test('Preconditions.nonEmptyString for empty string with default message', function(assert, Preconditions) {
        var test = function() {
            Preconditions.nonEmptyString('', 'some-var')
        };
        assert.assertThat(test, __.throws(__.error('some-var: expected a non-empty string, was: <>')));
    });

    QUnit.test('Preconditions.nonEmptyString for undefined with default message', function(assert, Preconditions) {
        var test = function() {
            Preconditions.nonEmptyString(undefined, 'some-var')
        };
        assert.assertThat(test, __.throws(__.error('some-var: expected a non-empty string, was: <undefined>')));
    });

    QUnit.test('Preconditions.nonEmptyString for empty string with custom message', function(assert, Preconditions) {
        var test = function() {
            Preconditions.nonEmptyString('', 'some-var', 'Custom message')
        };
        assert.assertThat(test, __.throws(__.error('Custom message')));
    });

    QUnit.test('Preconditions.nonEmptyString OK for non-empty string', function(assert, Preconditions) {
        assert.assertThat(Preconditions.nonEmptyString('non-empty', 'some-var'), __.equalTo('non-empty'));
    });


    QUnit.test('Preconditions.checkArgument for falsy result with custom message', function(assert, Preconditions) {
        var test = function() {
            Preconditions.checkArgument('a' == 'b', 'Custom message')
        };
        assert.assertThat(test, __.throws(__.error('Custom message')));
    });

    QUnit.test('Preconditions.checkArgument for falsy result with no message', function(assert, Preconditions) {
        var test = function() {
            Preconditions.checkArgument('a' == 'b')
        };
        assert.assertThat(test, __.throws(__.error('')));
    });

    QUnit.test('Preconditions.checkArgument OK result', function(assert, Preconditions) {
        assert.assertThat(Preconditions.checkArgument(true), __.truthy());
    });

    QUnit.test('Preconditions.checkArgument OK result custom actual value', function(assert, Preconditions) {
        assert.assertThat(Preconditions.checkArgument(true, 'true was not true', 'return me'), __.equalTo('return me'));
    });


    QUnit.test('Preconditions.isFunction for undefined and no variable name', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isFunction(undefined)
        };
        assert.assertThat(test, __.throws(__.error('[unspecified]: expected a function, was: undefined')));
    });

    QUnit.test('Preconditions.isFunction for undefined', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isFunction(undefined, 'some-function')
        };
        assert.assertThat(test, __.throws(__.error('some-function: expected a function, was: undefined')));
    });

    QUnit.test('Preconditions.isFunction for null', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isFunction(null, 'some-function')
        };
        assert.assertThat(test, __.throws(__.error('some-function: expected a function, was: null')));
    });

    QUnit.test('Preconditions.isFunction for integer', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isFunction(5, 'some-function')
        };
        assert.assertThat(test, __.throws(__.error('some-function: expected a function, was: 5')));
    });

    QUnit.test('Preconditions.isFunction for object', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isFunction({foo: 'bar'}, 'some-function')
        };
        assert.assertThat(test, __.throws(__.error('some-function: expected a function, was: [object Object]')));
    });

    QUnit.test('Preconditions.isFunction OK result', function(assert, Preconditions) {
        var aFunction = function () {};
        assert.assertThat(Preconditions.isFunction(aFunction), __.equalTo(aFunction));
    });


    QUnit.test('Preconditions.isArray for undefined and no variable name', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isArray(undefined)
        };
        assert.assertThat(test, __.throws(__.error('[unspecified]: expected an array, was: undefined')));
    });

    QUnit.test('Preconditions.isArray for undefined', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isArray(undefined, 'some-array')
        };
        assert.assertThat(test, __.throws(__.error('some-array: expected an array, was: undefined')));
    });

    QUnit.test('Preconditions.isArray for null', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isArray(null, 'some-array')
        };
        assert.assertThat(test, __.throws(__.error('some-array: expected an array, was: null')));
    });

    QUnit.test('Preconditions.isArray for string', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isArray('a string', 'some-array')
        };
        assert.assertThat(test, __.throws(__.error('some-array: expected an array, was: a string')));
    });

    QUnit.test('Preconditions.isArray for object', function(assert, Preconditions) {
        var test = function() {
            Preconditions.isArray({foo: 'bar'}, 'some-array')
        };
        assert.assertThat(test, __.throws(__.error('some-array: expected an array, was: [object Object]')));
    });

    QUnit.test('Preconditions.isArray OK result', function(assert, Preconditions) {
        assert.assertThat(Preconditions.isArray(['one', 'two']), __.equalTo(['one', 'two']));
    });


    QUnit.test('Preconditions.hasValue for undefined and no variable name', function(assert, Preconditions) {
        var test = function() {
            Preconditions.hasValue(undefined)
        };
        assert.assertThat(test, __.throws(__.error('[unspecified]: expected a value')));
    });

    QUnit.test('Preconditions.hasValue for undefined', function(assert, Preconditions) {
        var test = function() {
            Preconditions.hasValue(undefined, 'some-var')
        };
        assert.assertThat(test, __.throws(__.error('some-var: expected a value')));
    });

    QUnit.test('Preconditions.hasValue for null', function(assert, Preconditions) {
        var test = function() {
            Preconditions.hasValue(null, 'some-var')
        };
        assert.assertThat(test, __.throws(__.error('some-var: expected a value')));
    });

    QUnit.test('Preconditions.hasValue for empty string', function(assert, Preconditions) {
        var test = function() {
            Preconditions.hasValue('', 'some-var')
        };
        assert.assertThat(test, __.throws(__.error('some-var: expected a value')));
    });

    QUnit.test('Preconditions.hasValue OK non-empty string', function(assert, Preconditions) {
        assert.assertThat(Preconditions.hasValue('value'), __.equalTo('value'));
    });

    QUnit.test('Preconditions.hasValue OK defined object', function(assert, Preconditions) {
        assert.assertThat(Preconditions.hasValue({foo: 'bar'}), __.equalTo({foo: 'bar'}));
    });
});