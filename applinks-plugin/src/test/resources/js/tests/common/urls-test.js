define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/common/urls', {
        require: {
            main: 'applinks/common/urls'
        },
        mocks: {
            main: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['contextPath'])),
        },
        beforeEach: function (assert, ApplinksUrls, MockAJS) {
            MockAJS.contextPath.returns('/test-context')
        }
    });

    QUnit.test('Local admin URL with simple params', function(assert, ApplinksUrls) {
        var url = ApplinksUrls.Local.admin({foo: 'bar', baz: 'bang'});

        assert.assertThat(url, __.stringContainsInOrder(
            '/test-context',
            '/plugins/servlet/applinks/listApplicationLinks',
            '?', 'foo=%22bar%22', '&', 'baz=%22bang%22'
        ));
    });

    QUnit.test('Local edit URL with simple params', function(assert, ApplinksUrls) {
        var url = ApplinksUrls.Local.edit('abcdef', {foo: 'bar', baz: 'bang'});

        assert.assertThat(url, __.stringContainsInOrder(
            '/test-context',
            '/plugins/servlet/applinks/edit/abcdef',
            '?', 'foo=%22bar%22', '&', 'baz=%22bang%22'
        ));
    });

    QUnit.test('Remote admin URL non-Confluence with simple params', function(assert, ApplinksUrls) {
        var url = ApplinksUrls.Remote.admin('http://test.com', 'jira', {foo: 'bar', baz: 'bang'});

        assert.assertThat(url, __.stringContainsInOrder(
            'http://test.com',
            '/plugins/servlet/applinks/listApplicationLinks',
            '?', 'foo=%22bar%22', '&', 'baz=%22bang%22'
        ));
    });

    QUnit.test('Remote admin URL Confluence with simple params', function(assert, ApplinksUrls) {
        var url = ApplinksUrls.Remote.admin('http://test.com', 'confluence', {foo: 'bar', baz: 'bang'});

        assert.assertThat(url, __.stringContainsInOrder(
            'http://test.com',
            '/admin/listapplicationlinks.action',
            '?', 'foo=%22bar%22', '&', 'baz=%22bang%22'
        ));
    });
});