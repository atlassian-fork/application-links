define([
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    _,
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/common/docs', {
        require: {
            main: 'applinks/common/docs',
            jquery: 'applinks/lib/jquery'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['I18n.getText'])),
            ApplinksHelpPaths: QUnit.moduleMock('applinks/common/help-paths', new MockDeclaration(['getFullPath', 'getPath'])),
        },
        beforeEach: function(assert, docs, mocks) {
            mocks.AJS.I18n.getText.returns('Test');
            mocks.ApplinksHelpPaths.getFullPath.returns('full-path');
            mocks.ApplinksHelpPaths.getPath.returns('section-path');
        }
    });

    QUnit.test('Docs href generation', function(assert, docs, mocks) {
        var href = docs.getDocHref('page');

        assert.assertThat(mocks.ApplinksHelpPaths.getFullPath.calledWith('page'), __.is(true));
        assert.assertThat(href, __.is('full-path'));
    });

    QUnit.test('Docs href generation 2', function(assert, docs, mocks) {
        var pageKey = 'Oi2li2'; // Randomly generated strings
        var sectionKey = 'Deib7x';

        var href = docs.getDocHref(pageKey, sectionKey);

        assert.assertThat(mocks.ApplinksHelpPaths.getFullPath.calledWith(pageKey), __.is(true));
        assert.assertThat(mocks.ApplinksHelpPaths.getPath.calledWith(sectionKey), __.is(true));
        assert.assertThat(href, __.is('full-path#section-path'));
    });

    QUnit.test('Class names', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings
        var sectionKey = 'lo1aiR';
        var classNames = ['shohB8', 'ieNgi9', 'ceFi0u'];
        var link = docs.createDocLink(pageKey, sectionKey, classNames.join(' '));

        assert.assertThat(link.hasClass('ual-help-link'), __.truthy());
        assert.assertThat(link.hasClass('help-link'), __.truthy());
        assert.assertThat(link.hasClass(classNames[0]), __.truthy());
        assert.assertThat(link.hasClass(classNames[1]), __.truthy());
        assert.assertThat(link.hasClass(classNames[2]), __.truthy());
    });

    QUnit.test('Default class names', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings
        var sectionKey = 'lo1aiR';

        var link = docs.createDocLink(pageKey, sectionKey);

        assert.assertThat(link.hasClass('ual-help-link'), __.truthy());
        assert.assertThat(link.hasClass('help-link'), __.truthy());
    });

    QUnit.test('Page key', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings

        var link = docs.createDocLink(pageKey);
        assert.assertThat(link.attr('href'), __.is('full-path'));
    });

    QUnit.test('Section key', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings
        var sectionKey = 'lo1aiR';

        var link = docs.createDocLink(pageKey, sectionKey);
        assert.assertThat(link.attr('href'), __.is('full-path#section-path'));
    });

    QUnit.test('Help link key', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings
        var sectionKey = 'lo1aiR';

        var link = docs.createDocLink(pageKey, sectionKey);
        assert.assertThat(link.attr('data-help-link-key'), __.is(pageKey));
    });

    QUnit.test('Link text', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings
        var sectionKey = 'lo1aiR';

        var link = docs.createDocLink(pageKey, sectionKey);
        assert.assertThat(link.attr('title'), __.is('Test'));
        assert.assertThat(link.text(), __.is('Test'));
    });

    QUnit.test('Link icon', function(assert, docs, mocks) {
        var pageKey = 'Fi3quu'; // Randomly generated strings
        var sectionKey = 'lo1aiR';

        var link = docs.createDocLinkIcon(pageKey, sectionKey);
        assert.assertThat(link.attr('title'), __.is('Test'));
        assert.assertThat(link.text(), __.is('Test'));
        var linkContentHtml = link.html();
        assert.assertThat(linkContentHtml, __.is('<span class="aui-icon aui-icon-small aui-iconfont-help">Test</span>'));
    });
});
