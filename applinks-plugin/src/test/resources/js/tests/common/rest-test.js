define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/test/mock-common'
], function(
    QUnit,
    __,
    MockDeclaration,
    ApplinksCommonMocks
) {
    QUnit.module('applinks/common/rest', {
        require: {
            main: 'applinks/common/rest'
        },
        mocks: {
            main: ApplinksCommonMocks.mockRestRequestModule(QUnit),
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['contextPath'])),
            OAuthRest: QUnit.moduleMock('applinks/common/rest-oauth', 'OAuth REST mock')
        },
        beforeEach: function (assert, ApplinksRest, MockRequest, mocks) {
            mocks.AJS.contextPath.returns('/test-context');
            new ApplinksCommonMocks.Stubber(this).setUpRestRequest(MockRequest);
        }
    });

    QUnit.test('V1 applink request', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V1.applink('abcdef');

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/1.0/applicationlink/abcdef'));
    });

    QUnit.test('V3 features request', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V3.features('TEST_FEATURE');

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/3.0/features/TEST_FEATURE'));
    });

    QUnit.test('V3 feature discovery request', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V3.featureDiscovery('TEST_FEATURE');

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/3.0/feature-discovery/TEST_FEATURE'));
    });

    QUnit.test('V3 Applink request no data', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V3.applink('abcdef');

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/3.0/applinks/abcdef'));
    });

    QUnit.test('V3 Applink request with valid data', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V3.applink('abcdef', [
            ApplinksRest.V3.ApplinkData.CONFIG_URL,
            ApplinksRest.V3.ApplinkData.CAPABILITIES
        ]);

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/3.0/applinks/abcdef'));
        assert.assertThat(MockRequest.answer.queryParam, __.calledWith('data', 'configUrl'));
        assert.assertThat(MockRequest.answer.queryParam, __.calledWith('data', 'capabilities'));
    });

    QUnit.test('V3 Applink request with invalid data keys', function(assert, ApplinksRest, MockRequest) {
        var invalidDataKeysNonArray = {
                foo: ApplinksRest.V3.ApplinkData.CONFIG_URL,
                bar: ApplinksRest.V3.ApplinkData.CAPABILITIES
        };

        var invalidCall = function() {
            return ApplinksRest.V3.applink('abcdef', invalidDataKeysNonArray);
        };

        assert.assertThat(invalidCall, __.throws(__.error(__.containsString('dataKeys: expected an array'))));
    });

    QUnit.test('V3 Applinks request no data', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V3.applinks();

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/3.0/applinks'));
    });

    QUnit.test('V3 Applinks request with valid data', function(assert, ApplinksRest, MockRequest) {
        var result = ApplinksRest.V3.applinks([
            ApplinksRest.V3.ApplinkData.CONFIG_URL,
            ApplinksRest.V3.ApplinkData.CAPABILITIES
        ]);

        assert.assertThat(result, __.is(MockRequest.answer));
        assert.assertThat(MockRequest, __.calledWithNew());
        assert.assertThat(MockRequest, __.calledOnceWith('/test-context/rest/applinks/3.0/applinks'));
        assert.assertThat(MockRequest.answer.queryParam, __.calledWith('data', 'configUrl'));
        assert.assertThat(MockRequest.answer.queryParam, __.calledWith('data', 'capabilities'));
    });

    QUnit.test('V3 Applinks request with invalid data keys', function(assert, ApplinksRest, MockRequest) {
        var invalidDataKeysNonArray = {
            foo: ApplinksRest.V3.ApplinkData.CONFIG_URL,
            bar: ApplinksRest.V3.ApplinkData.CAPABILITIES
        };

        var invalidCall = function() {
            return ApplinksRest.V3.applinks(invalidDataKeysNonArray);
        };

        assert.assertThat(invalidCall, __.throws(__.error(__.containsString('dataKeys: expected an array'))));
    });

    QUnit.test('OAuth REST', function(assert, ApplinksRest, MockRequest, mocks) {
        assert.assertThat(ApplinksRest.OAuth, __.equalTo(mocks.OAuthRest));
    });
});