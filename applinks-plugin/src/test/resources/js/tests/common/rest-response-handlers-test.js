define([
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/common/response-status'
], function(
    $,
    _,
    QUnit,
    __,
    MockDeclaration,
    ResponseStatus
) {

    QUnit.module('applinks/common/response-handlers', {
        require: {
            main: 'applinks/common/response-handlers'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['dialog2'])),
            window: QUnit.moduleMock('applinks/lib/window', new MockDeclaration(['location.reload'])),
            console: QUnit.moduleMock('applinks/lib/console', new MockDeclaration(['error', 'debug', 'warn'])),
            dialogHelper :  QUnit.moduleMock('applinks/common/dialog-helper', new MockDeclaration(['isAnyDialogOpen', 'getContainer'])),
            dialog2: QUnit.mock(new MockDeclaration(['show', 'hide', 'remove']))
        },
        templates: {
            "applinks.common.resterror.dialogContents": function (params) {
                return '<div class="applink-errors">' +
                    '<div id="template-params">' + JSON.stringify(params) + '</div>' +
                    '<button id="applinks-error-dialog-close-button">Close</button>' +
                    '</div>';
            }
        },
        beforeEach: function(assert, responseHandlers, mocks) {
            this.fixture.append('<div id="dialog-test-container"></div>');
            mocks.AJS.dialog2.returns(mocks.dialog2);
            mocks.dialogHelper.isAnyDialogOpen.returns(false);
            mocks.dialogHelper.getContainer.returns('#dialog-test-container');
        }
    });

    QUnit.test('Page should be reloaded for unauthorized request', function (assert, ResponseHandlers, mocks) {
        var errorHandler = ResponseHandlers.fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
        errorHandler({status: ResponseStatus.UNAUTHORIZED.code}, 'UNAUTHORIZED', 'UNAUTHORIZED');

        assert.assertThat('Error should be logged', mocks.console.error, __.calledOnce());
        assert.assertThat('Page should be reloaded for unauthorized request', mocks.window.location.reload, __.calledOnce());
    });

    QUnit.test('Fail handler given BAD_REQUEST and no response text', function (assert, ResponseHandlers, mocks) {
        var errorHandler = ResponseHandlers.fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
        errorHandler(createRequest(ResponseStatus.BAD_REQUEST), 'BAD_REQUEST', 'BAD_REQUEST');

        assert.assertThat('Error should be logged', mocks.console.error, __.calledOnce());
        assert.assertThat('Page should not be reloaded', mocks.window.location.reload, __.notCalled());
        assert.assertThat('Dialog should be shown', mocks.dialog2.show, __.calledOnce());
        assert.assertThat('Dialog contents should be rendered', this.fixture.find('.applink-errors').length, __.is(1));
        // no respone text -> no specific errors
        assert.assertThat(parseTemplateParams(assert, this.fixture), __.hasProperty('errors', __.falsy()));
    });

    QUnit.test('Fail handler given BAD_REQUEST and valid response text', function (assert, ResponseHandlers, mocks) {
        var errorHandler = ResponseHandlers.fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
        var request = createRequest(ResponseStatus.BAD_REQUEST,
            errorResponseText('foo', 'Foo is broken', 'Foo is really really broken'));
        errorHandler(request, 'BAD_REQUEST', 'BAD_REQUEST');

        assert.assertThat('Error should be logged', mocks.console.error, __.calledOnce());
        assert.assertThat('Page should not be reloaded', mocks.window.location.reload, __.notCalled());
        assert.assertThat('Dialog should be shown', mocks.dialog2.show, __.calledOnce());
        assert.assertThat('Dialog contents should be rendered', this.fixture.find('.applink-errors').length, __.is(1));
        // valid respone text -> should translate to specific errors
        assert.assertThat(parseTemplateParams(assert, this.fixture), __.hasProperty('errors', __.contains(
            __.hasProperties({
                context: 'foo',
                summary: 'Foo is broken',
                details: 'Foo is really really broken'
            }))));
    });

    QUnit.test('Fail handler given BAD_REQUEST and invalid (non-JSON) response text',
        function (assert, ResponseHandlers, mocks) {
            var errorHandler = ResponseHandlers.fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
            var request = createRequest(ResponseStatus.BAD_REQUEST, '<html><body>Not a JSON hey</body></html>');
            errorHandler(request, 'BAD_REQUEST', 'BAD_REQUEST');

            // error response and parse error should be both logged
            assert.assertThat(mocks.console.error, __.calledOnceWith(__.startsWith('Unexpected response status')));
            assert.assertThat(mocks.console.warn, __.calledOnceWith(__.startsWith('Unable to parse REST error response')));

            assert.assertThat('Page should not be reloaded', mocks.window.location.reload, __.notCalled());
            assert.assertThat('Dialog should be shown', mocks.dialog2.show, __.calledOnce());
            assert.assertThat('Dialog contents should be rendered', this.fixture.find('.applink-errors').length,
                __.is(1));
            // non-JSON respone text -> no specific errors
            assert.assertThat(parseTemplateParams(assert, this.fixture), __.hasProperty('errors', __.falsy()));
        });

    QUnit.test('Modal error dialog should be closed when button is clicked', function (assert, ResponseHandlers, mocks) {
        var errorHandler = ResponseHandlers.fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
        errorHandler({status: ResponseStatus.BAD_REQUEST.code}, 'BAD_REQUEST', 'BAD_REQUEST');

        this.fixture.find('#applinks-error-dialog-close-button').click();
        assert.assertThat('Dialog should be hidden', mocks.dialog2.hide, __.calledOnce());
        assert.assertThat('Dialog should be removed', mocks.dialog2.remove, __.calledOnce());
    });

    QUnit.test('Modal error dialog should not be displayed if other dialog is already open',
        function (assert, ResponseHandlers, mocks) {
            mocks.dialogHelper.isAnyDialogOpen.returns(true);

            var errorHandler = ResponseHandlers.fail(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
            errorHandler({status: ResponseStatus.BAD_REQUEST.code}, 'BAD_REQUEST', 'BAD_REQUEST');
            assert.assertThat('Page should not be reloaded for bad request error', mocks.window.location.reload,
                __.notCalled());
            assert.assertThat('Dialog should be shown', mocks.dialog2.show, __.notCalled());
            assert.assertThat('Message should be written to log', mocks.console.debug, __.calledOnce());
        });

    QUnit.test('Nothing should be logged for expected status in success handler',
        function (assert, ResponseHandlers, mocks) {
            var doneHandler = ResponseHandlers.done(ResponseStatus.Family.SUCCESSFUL, ResponseStatus.NOT_FOUND);
            doneHandler({}, 'OK', {status: ResponseStatus.OK.code});

            assert.assertThat('Nothing should be logged', mocks.console.error, __.notCalled());
            assert.assertThat('Page should not be reloaded', mocks.window.location.reload, __.notCalled());
            assert.assertThat('Dialog should be not be shown', mocks.dialog2.show, __.notCalled());
        });

    QUnit.test('Unexpected status should be logged for success handler', function (assert, ResponseHandlers, mocks) {
        var doneHandler = ResponseHandlers.done(ResponseStatus.OK);
        doneHandler({}, 'NO_CONTENT', createRequest(ResponseStatus.NO_CONTENT.code));

        assert.assertThat('NO_CONTENT status should be logged', mocks.console.error, __.calledOnce());
        assert.assertThat('Page should not be reloaded', mocks.window.location.reload, __.notCalled());
        assert.assertThat('Dialog should be not be shown', mocks.dialog2.show, __.notCalled());
    });

    QUnit.test('Nothing should be logged for expected status in success handler given array of statuses',
        function (assert, ResponseHandlers, mocks) {
            var doneHandler = ResponseHandlers.done([ResponseStatus.OK, ResponseStatus.NO_CONTENT]);
            doneHandler({}, 'NO_CONTENT', createRequest(ResponseStatus.NO_CONTENT));
            assert.assertThat('Nothing should be logged', mocks.console.error, __.notCalled());

            doneHandler({}, 'OK', createRequest(ResponseStatus.OK));
            assert.assertThat('Nothing should be logged', mocks.console.error, __.notCalled());
        });

    function createRequest(responseStatus, responseText) {
        return {
            status: responseStatus.code,
            responseText: responseText
        }
    }

    function errorResponseText(context, summary, details) {
        return JSON.stringify({
            errors: [{
                context: context,
                summary: summary,
                details: details
            }]
        });
    }

    function parseTemplateParams(assert, fixture) {
        try {
            return JSON.parse(fixture.find('#template-params').text());
        } catch(error) {
            assert.fail('Failed to parse JSON text "' + dataText + '": ' + error);
        }
    }

});