define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/lib/lodash'
], function(
    QUnit,
    __,
    _
) {
    QUnit.module('applinks/feature/status/oauth-matcher', {
        require: {
            main: 'applinks/feature/status/oauth-matcher'
        }
    });

    QUnit.test('should throw exception if there is no remote or local', function(assert, match) {
        assert.assertThrows(function() { match({}); });
        assert.assertThrows(function() { match({localAuthentication: {}}); });
        assert.assertThrows(function() { match({remoteAuthentication: {}}); });
    });

    var disabled = { enabled: false, twoLoEnabled: false, twoLoImpersonationEnabled: false };

    /** oauth_2lo should be treated the same as oauth_3lo */
    var oauth_2lo = { enabled: true, twoLoEnabled: true, twoLoImpersonationEnabled: false };
    var oauth_3lo = { enabled: true, twoLoEnabled: false, twoLoImpersonationEnabled: false };

    var oauthWithImpersonation = { enabled: true, twoLoEnabled: true, twoLoImpersonationEnabled: true };

    function matchingFixture() {
        return [
            {
                localAuthentication: { left: oauthWithImpersonation },
                remoteAuthentication: { right: oauthWithImpersonation }
            },
            {
                localAuthentication: { left: oauth_2lo },
                remoteAuthentication: { right: oauth_2lo }
            },
            {
                localAuthentication: { left: oauth_3lo },
                remoteAuthentication: { right: oauth_3lo }
            },
            {
                localAuthentication: { left: oauth_2lo },
                remoteAuthentication: { right: oauth_3lo }
            },
            {
                localAuthentication: { left: oauth_3lo },
                remoteAuthentication: { right: oauth_2lo }
            },
            {
                localAuthentication: { left: disabled },
                remoteAuthentication: { right: disabled }
            }
        ];
    }

    function mismatchFixture() {
        return [
            {
                localAuthentication: { left: disabled },
                remoteAuthentication: { right: oauth_2lo}
            },
            {
                localAuthentication: { left: disabled },
                remoteAuthentication: { right: oauth_3lo }
            },
            {
                localAuthentication: { left: disabled },
                remoteAuthentication: { right: oauthWithImpersonation }
            },
            {
                localAuthentication: { left: oauth_2lo },
                remoteAuthentication: { right: disabled}
            },
            {
                localAuthentication: { left: oauth_2lo },
                remoteAuthentication: { right: oauthWithImpersonation }
            },
            {
                localAuthentication: { left: oauth_3lo },
                remoteAuthentication: { right: disabled}
            },
            {
                localAuthentication: { left: oauth_3lo },
                remoteAuthentication: { right: oauthWithImpersonation }
            },
            {
                localAuthentication: { left: oauthWithImpersonation },
                remoteAuthentication: { right: disabled }
            },
            {
                localAuthentication: { left: oauthWithImpersonation },
                remoteAuthentication: { right: oauth_2lo }
            },
            {
                localAuthentication: { left: oauthWithImpersonation },
                remoteAuthentication: { right: oauth_3lo }
            }
        ];
    }

    function StatusMap() {
        var statusMap = {};
        statusMap[JSON.stringify(disabled)] = 'disabled';
        statusMap[JSON.stringify(oauth_2lo)] = 'oauth';
        statusMap[JSON.stringify(oauth_3lo)] = 'oauth';
        statusMap[JSON.stringify(oauthWithImpersonation)] = 'oauthWithImpersonation';
        return statusMap;
    }

    function assertMatches(assert, match, configs) {
        _.each(configs, function(config) {
            assert.assertThat(match(config), __.undefined());
        });
    }

    function assertMisMatches(assert, match, configs, direction) {
        var statusMap = StatusMap();
        _.each(configs, function(config) {
            var result = match(config);
            assert.assertThat(result[direction].local, __.equalTo(statusMap[JSON.stringify(config.localAuthentication.left)]));
            assert.assertThat(result[direction].remote, __.equalTo(statusMap[JSON.stringify(config.remoteAuthentication.right)]));
        });
    }

    QUnit.test('should not mismatch if local incoming matches remote outgoing', function(assert, match){
        var configs = matchingFixture();
        _.each(configs, function(config){
            config.localAuthentication['incoming'] = config.localAuthentication.left;
            config.localAuthentication['outgoing'] = {};
            config.remoteAuthentication['incoming'] = {};
            config.remoteAuthentication['outgoing'] = config.remoteAuthentication.right;
        });
        assertMatches(assert, match, configs);
    });

    QUnit.test('should not mismatch if local outgoing matches remote incoming', function(assert, match){
        var configs = matchingFixture();
        _.each(configs, function(config){
            config.localAuthentication['incoming'] = {};
            config.localAuthentication['outgoing'] = config.localAuthentication.left;
            config.remoteAuthentication['incoming'] = config.remoteAuthentication.right;
            config.remoteAuthentication['outgoing'] = {};
        });
        assertMatches(assert, match, configs);
    });

    QUnit.test('should display mismatch status for differing local incoming and remote outgoing', function(assert, match) {
        var configs = mismatchFixture();
        _.each(configs, function(config){
            config.localAuthentication['incoming'] = config.localAuthentication.left;
            config.localAuthentication['outgoing'] = {};
            config.remoteAuthentication['incoming'] = {};
            config.remoteAuthentication['outgoing'] = config.remoteAuthentication.right;
        });
        assertMisMatches(assert, match, configs, 'incoming');
    });

    QUnit.test('should display mismatch status for differing local outgoing and remote incoming', function(assert, match) {
        var configs = mismatchFixture();
        _.each(configs, function(config){
            config.localAuthentication['incoming'] = {};
            config.localAuthentication['outgoing'] = config.localAuthentication.left;
            config.remoteAuthentication['incoming'] = config.remoteAuthentication.right;
            config.remoteAuthentication['outgoing'] = {};
        });
        assertMisMatches(assert, match, configs, 'outgoing');
    });
});