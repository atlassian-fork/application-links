define([
    'applinks/lib/jquery',
    'applinks/lib/lodash',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    $,
    _,
    QUnit,
    __,
    MockDeclaration
) {
    QUnit.module('applinks/feature/status/details/deprecated', {
        require: {
            main: 'applinks/feature/status/details/deprecated',
            errors: 'applinks/feature/status/errors'
        },
        mocks: {
            main: QUnit.mock(function(){
                return {
                    id: '1234'
                }
            }),
            migration: QUnit.moduleMock('applinks/feature/migration', new MockDeclaration(['removeLocal', 'migrate'])),
            statusDialog: QUnit.mock(new MockDeclaration(['_dialog.hide', '_dialog.$popup.find']))
        },
        beforeEach: function(assert, Deprecated, applink, mocks) {
            this.fixture.append('<div id="dialog" class=".applinks-status-update"></div>');
            mocks.statusDialog._dialog.$popup.find.returns(this.fixture.find('#dialog'));
        }
    });

    QUnit.test('should remove local authentication', function(assert, Deprecated, applink, mocks) {
        applink.get = _.bind(function() {
           return this.errors.MANUAL_LEGACY_REMOVAL;
        }, this);
        Deprecated.onLoad(mocks.statusDialog, applink);
        $('#dialog').trigger('click');
        assert.assertThat(mocks.migration.removeLocal, __.calledOnceWith(applink));
    });

    QUnit.test('should migrate', function(assert, Deprecated, applink, mocks) {
        applink.get = _.bind(function() {
            return this.errors.LEGACY_UPDATE;
        }, this);
        Deprecated.onLoad(mocks.statusDialog, applink);
        $('#dialog').trigger('click');
        assert.assertThat(mocks.migration.migrate, __.calledOnceWith(applink));
    });
});
