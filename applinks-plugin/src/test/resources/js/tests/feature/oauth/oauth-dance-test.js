define([
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration',
    'applinks/lib/jquery'
], function(
    QUnit,
    __,
    MockDeclaration,
    $
) {
    QUnit.module('applinks/feature/oauth-dance', {
        require: {
            main: 'applinks/feature/oauth-dance'
        },
        mocks: {
            main: QUnit.moduleMock('applinks/feature/oauth-callback',
                new MockDeclaration(['onSuccess', 'onFailure', 'source', 'open']).withConstructor()),
            console: QUnit.moduleMock('applinks/lib/console', new MockDeclaration(['warn'])),
            window: QUnit.moduleMock('applinks/lib/window', new MockDeclaration(['location.reload']))
        },
        beforeEach: function(assert, OAuthDance, OAuthCallbackMock, mocks) {
            setUpOAuthCallbackMock(OAuthCallbackMock);
            setUpHtml(this.fixture);
            mocks.window.document = window.document;
        }
    });

    QUnit.test('OAuthDance requires onSuccess to be function', function(assert, OAuthDance) {
        function wrongSuccessCallback() {
            new OAuthDance(this.fixture, '.oauth-target').onSuccess('blah');
        }
        assert.assertThat(wrongSuccessCallback, __.throws(__.error('onSuccess: expected a function, was: blah')));
    });

    QUnit.test('OAuthDance requires onFailure to be function', function(assert, OAuthDance) {
        function wrongFailureCallback() {
            new OAuthDance(this.fixture, '.oauth-target').onFailure('blah');
        }
        assert.assertThat(wrongFailureCallback, __.throws(__.error('onFailure: expected a function, was: blah')));
    });

    QUnit.test('OAuthDance with default success and failure in global scope',
        function(assert, OAuthDance, OAuthCallbackMock, mocks) {

            new OAuthDance(this.fixture, '.oauth-target').initialize();

            // any .oauth-target in the document should trigger the dance
            this.fixture.find('#oauth-target11').trigger('click');
            this.fixture.find('#oauth-target21').trigger('click');

            assert.assertThat(OAuthCallbackMock, __.calledTwice());
            assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledTwiceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledTwiceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.source, __.calledWithAt(0, elementWithId('oauth-target11')));
            assert.assertThat(OAuthCallbackMock.answer.source, __.calledWithAt(1, elementWithId('oauth-target21')));
            assert.assertThat(OAuthCallbackMock.answer.open, __.calledTwice());
            assertDefaultSuccess(assert, OAuthCallbackMock.answer.onSuccess.getCall(0).args[0], mocks.window);
            assertDefaultSuccess(assert, OAuthCallbackMock.answer.onSuccess.getCall(1).args[0], mocks.window);
        });

    QUnit.test('OAuthDance with custom success and default failure',
        function(assert, OAuthDance, OAuthCallbackMock) {

            var successCallback = function() {};
            new OAuthDance(this.fixture, '.oauth-target').onSuccess(successCallback).initialize();

            this.fixture.find('#oauth-target11').trigger('click');

            assert.assertThat(OAuthCallbackMock, __.calledOnce());
            assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledOnceWith(__.equalTo(successCallback)));
            assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledOnceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.open, __.calledOnce());
        });

    QUnit.test('OAuthDance with custom failure and default success',
        function(assert, OAuthDance, OAuthCallbackMock, mocks) {

            var failureCallback = function() {};
            new OAuthDance('.oauth-target').onFailure(failureCallback).initialize();

            this.fixture.find('#oauth-target11').trigger('click');

            assert.assertThat(OAuthCallbackMock, __.calledOnce());
            assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledOnceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledOnceWith(__.equalTo(failureCallback)));
            assert.assertThat(OAuthCallbackMock.answer.open, __.calledOnce());
            assertDefaultSuccess(assert, OAuthCallbackMock.answer.onSuccess.getCall(0).args[0], mocks.window);
        });

    QUnit.test('OAuthDance with custom success and custom failure',
        function(assert, OAuthDance, OAuthCallbackMock) {

            var successCallback = function() {};
            var failureCallback = function() {};
            new OAuthDance(this.fixture, '.oauth-target')
                .onSuccess(successCallback)
                .onFailure(failureCallback)
                .initialize();

            this.fixture.find('#oauth-target11').trigger('click');

            assert.assertThat(OAuthCallbackMock, __.calledOnce());
            assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledOnceWith(__.equalTo(successCallback)));
            assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledOnceWith(__.equalTo(failureCallback)));
            assert.assertThat(OAuthCallbackMock.answer.open, __.calledOnce());
        });

    QUnit.test('OAuthDance with scope',
        function(assert, OAuthDance, OAuthCallbackMock) {

            new OAuthDance('#oauth-dance-test1', '.oauth-target').initialize();

            this.fixture.find('#oauth-target11').trigger('click');
            // this should not trigger the dance because of the limited scope
            this.fixture.find('#oauth-target21').trigger('click');

            assert.assertThat(OAuthCallbackMock, __.calledOnce());
            assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledOnceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledOnceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.open, __.calledOnce());
        });

    QUnit.test('OAuthDance for elements without data-authorisation-uri',
        function(assert, OAuthDance, OAuthCallbackMock, mocks) {

            new OAuthDance(this.fixture, '.oauth-target').initialize();

            this.fixture.find('#oauth-target13').trigger('click');
            this.fixture.find('#oauth-target23').trigger('click');

            assert.assertThat(OAuthCallbackMock, __.notCalled());
            assert.assertThat(mocks.console.warn,
                __.calledTwiceWith(__.containsString('data-authorisation-uri missing for')));
        });

    QUnit.test('OAuthDance start with scope element',
        function(assert, OAuthDance, OAuthCallbackMock) {
            var scope = '<ul id="oauth-target11" class="oauth-target" data-authorisation-uri="http://test.com"/>';

            new OAuthDance(scope).start();

            assert.assertThat(OAuthCallbackMock, __.calledOnce());
            assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledOnceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledOnceWith(__.func()));
            assert.assertThat(OAuthCallbackMock.answer.open, __.calledOnce());
        });

    QUnit.test('OAuthDance start with selector', function(assert, OAuthDance, OAuthCallbackMock) {
        new OAuthDance(this.fixture, '#oauth-target22').start();

        assert.assertThat(OAuthCallbackMock, __.calledOnce());
        assert.assertThat(OAuthCallbackMock.answer.onSuccess, __.calledOnceWith(__.func()));
        assert.assertThat(OAuthCallbackMock.answer.onFailure, __.calledOnceWith(__.func()));
        assert.assertThat(OAuthCallbackMock.answer.open, __.calledOnce());
    });

    QUnit.test('OAuthDance initialize passes source to OAuthCallback', function(assert, OAuthDance, OAuthCallbackMock) {

        new OAuthDance(this.fixture, '.oauth-target').initialize();

        // any .oauth-target in the document should trigger the dance
        this.fixture.find('#oauth-target11').trigger('click');
        this.fixture.find('#oauth-target21').trigger('click');

        assert.assertThat(OAuthCallbackMock, __.calledTwice());
        assert.assertThat(OAuthCallbackMock.answer.source, __.calledWithAt(0, elementWithId('oauth-target11')));
        assert.assertThat(OAuthCallbackMock.answer.source, __.calledWithAt(1, elementWithId('oauth-target21')));
    });

    QUnit.test('OAuthDance start passes source to OAuthCallback', function(assert, OAuthDance, OAuthCallbackMock) {

        new OAuthDance(this.fixture, '#oauth-target22').start();

        assert.assertThat(OAuthCallbackMock, __.calledOnce());
        assert.assertThat(OAuthCallbackMock.answer.source, __.calledOnceWith(elementWithId('oauth-target22')));
    });

    QUnit.test('OAuthDance start does not work for multiple target elements',
        function(assert, OAuthDance, OAuthCallbackMock, mocks) {

        new OAuthDance(this.fixture, 'oauth-target').start();

        assert.assertThat(OAuthCallbackMock, __.notCalled());
        assert.assertThat(mocks.console.warn, __.calledOnceWith(__.stringContainsInOrder(
            'Could not trigger OAuth dance', 'not a single HTML element')));
    });

    function setUpOAuthCallbackMock(OAuthCallbackMock) {
        var mockOAuthCallbackObj = OAuthCallbackMock.answer;
        mockOAuthCallbackObj.source.returns(mockOAuthCallbackObj);
        mockOAuthCallbackObj.onSuccess.returns(mockOAuthCallbackObj);
        mockOAuthCallbackObj.onFailure.returns(mockOAuthCallbackObj);
    }

    function setUpHtml(fixture) {
        var $scopeOne = $('<div id="oauth-dance-test1"></div>"')
            .append('<ul id="oauth-target11" class="oauth-target" data-authorisation-uri="http://test.com"/>')
            .append('<ul id="oauth-target12" class="oauth-target" data-authorisation-uri="http://test.com"/>')
            .append('<ul id="oauth-target13" class="oauth-target" />');

        var $scopeTwo = $('<div id="oauth-dance-test2"></div>"')
            .append('<ul id="oauth-target21" class="oauth-target" data-authorisation-uri="http://test.com"/>')
            .append('<ul id="oauth-target22" class="oauth-target" data-authorisation-uri="http://test.com"/>')
            .append('<ul id="oauth-target23" class="oauth-target"/>');

        fixture.append($scopeOne).append($scopeTwo);
    }

    function assertDefaultSuccess(assert, successCallback, windowMock) {
        windowMock.location.reload.reset();
        successCallback.call(this);
        assert.assertThat(windowMock.location.reload, __.calledOnce());
    }

    function elementWithId(id) {
        // feature matcher to get HTML ID out of a jQuery element
        return __.FeatureMatcher(id, 'HTML ID', 'id', function(element) {
            return element.attr('id');
        });
    }

});