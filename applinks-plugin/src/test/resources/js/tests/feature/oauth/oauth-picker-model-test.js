define([
    'applinks/lib/jquery',
    'applinks/test/qunit',
    'applinks/test/hamjest',
    'applinks/test/mock-declaration'
], function(
    $,
    QUnit,
    __,
    MockDeclaration
) {

    QUnit.module('applinks/feature/oauth-picker-model', {
        require: {
            main: 'applinks/feature/oauth-picker-model'
        },
        mocks: {
            AJS: QUnit.moduleMock('applinks/lib/aui', new MockDeclaration(['I18n.getText', 'contextPath']))
        },
        beforeEach: function(assert, Initializer, mocks) {
            mocks.AJS.I18n.getText.withArgs('applinks.component.oauth-config.oauth').returns('oauth');
            mocks.AJS.I18n.getText.withArgs('applinks.component.oauth-config.oauth.impersonation').returns('oauth.impersonation');
            mocks.AJS.I18n.getText.withArgs('applinks.component.oauth-config.disabled').returns('oauth.disabled');
        }
    });

    QUnit.test('Test OAuth enabled config', function(assert, OAuthPickerModel) {
        assertOAuthEnabledConfig(assert, new OAuthPickerModel(true, true, false), OAuthPickerModel);
    });

    QUnit.test('Test OAuth impersonation enabled config', function(assert, OAuthPickerModel) {
        assertOAuthImpersonationEnabledConfig(assert, new OAuthPickerModel(true, true, true), OAuthPickerModel);
    });

    QUnit.test('OAuth only 3LO should turn into disabled', function(assert, OAuthPickerModel) {
        assertOAuthDisabledConfig(assert, new OAuthPickerModel(true, false, false), OAuthPickerModel);
    });

    QUnit.test('Test OAuth disabled config', function(assert, OAuthPickerModel) {
        assertOAuthDisabledConfig(assert, new OAuthPickerModel(false, false, false), OAuthPickerModel);
    });

    QUnit.test('Invalid levels should be adjusted to valid states', function(assert, OAuthPickerModel) {
        assertOAuthDisabledConfig(assert, new OAuthPickerModel(false, true, false), OAuthPickerModel);
        assertOAuthDisabledConfig(assert, new OAuthPickerModel(false, true, true), OAuthPickerModel);
        assertOAuthDisabledConfig(assert, new OAuthPickerModel(false, false, true), OAuthPickerModel);
        assertOAuthDisabledConfig(assert, new OAuthPickerModel(true, false, true), OAuthPickerModel);
    });

    QUnit.test('Test object to JSON conversion', function(assert, OAuthPickerModel) {
        assert.assertThat(JSON.stringify(new OAuthPickerModel(true, true, true)), __.equalTo(
            JSON.stringify({
                enabled: true,
                twoLoEnabled: true,
                twoLoImpersonationEnabled: true
            })
        ));

        assert.assertThat(JSON.stringify(new OAuthPickerModel(true, true, false)), __.equalTo(
            JSON.stringify({
                enabled: true,
                twoLoEnabled: true,
                twoLoImpersonationEnabled: false
            })
        ));

        assert.assertThat(JSON.stringify(new OAuthPickerModel(false, false, false)), __.equalTo(
            JSON.stringify({
                enabled: false,
                twoLoEnabled: false,
                twoLoImpersonationEnabled: false
            })
        ));
    });

    QUnit.test('Test parsing OAuthPickerModel from id', function(assert, OAuthPickerModel) {
        assertOAuthEnabledConfig(assert, OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_ID), OAuthPickerModel);
        assertOAuthImpersonationEnabledConfig(assert, OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_IMPERSONATION_ID), OAuthPickerModel);
        assertOAuthDisabledConfig(assert, OAuthPickerModel.fromId(OAuthPickerModel.OAUTH_DISABLED_ID), OAuthPickerModel);
    });

    QUnit.test('Test OAuthPickerModel instance is equal to another one', function(assert, OAuthPickerModel) {
        var config1 = new OAuthPickerModel(true, true, false);
        var config2 = new OAuthPickerModel(true, true, false);
        assert.assertThat(config1.equals(config2), __.truthy());
    });

    QUnit.test('Test OAuthPickerModel instance is not equal to another one', function(assert, OAuthPickerModel) {
        var config1 = new OAuthPickerModel(true, true, false);
        var config2 = new OAuthPickerModel(true, true, true);
        assert.assertThat(config1.equals(config2), __.falsy());
    });

    function assertOAuthEnabledConfig(assert, oauthPickerModel, OAuthPickerConstants) {
        assert.assertThat(oauthPickerModel.isOAuthEnabled(), __.truthy());
        assert.assertThat(oauthPickerModel.isOAuthImpersonationEnabled(), __.falsy());
        assert.assertThat(oauthPickerModel.isOAuthDisabled(), __.falsy());
        assert.assertThat(oauthPickerModel.getId(), __.is(OAuthPickerConstants.OAUTH_ID));
        assert.assertThat(oauthPickerModel.getName(), __.is('oauth'));
    }

    function assertOAuthImpersonationEnabledConfig(assert, oauthPickerModel, OAuthPickerConstants) {
        assert.assertThat(oauthPickerModel.isOAuthEnabled(), __.falsy());
        assert.assertThat(oauthPickerModel.isOAuthImpersonationEnabled(), __.truthy());
        assert.assertThat(oauthPickerModel.isOAuthDisabled(), __.falsy());
        assert.assertThat(oauthPickerModel.getId(), __.is(OAuthPickerConstants.OAUTH_IMPERSONATION_ID));
        assert.assertThat(oauthPickerModel.getName(), __.is('oauth.impersonation'));
    }

    function assertOAuthDisabledConfig(assert, oauthPickerModel, OAuthPickerConstants) {
        assert.assertThat(oauthPickerModel.isOAuthEnabled(), __.falsy());
        assert.assertThat(oauthPickerModel.isOAuthImpersonationEnabled(), __.falsy());
        assert.assertThat(oauthPickerModel.isOAuthDisabled(), __.truthy());
        assert.assertThat(oauthPickerModel.getId(), __.is(OAuthPickerConstants.OAUTH_DISABLED_ID));
        assert.assertThat(oauthPickerModel.getName(), __.is('oauth.disabled'));
    }
});