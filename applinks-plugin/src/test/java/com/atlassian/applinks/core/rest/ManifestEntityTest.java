package com.atlassian.applinks.core.rest;

import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.core.rest.model.ManifestEntity;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ManifestEntityTest {
    /**
     * APL-789 - test that a manifest entity can be created with a null build number
     */
    @Test
    public void testCreatingManifestEntityWithNullBuildNumberSucceeds() {
        Manifest m = mock(Manifest.class);
        when(m.getName()).thenReturn("Mock thing");
        when(m.getBuildNumber()).thenReturn(null);

        ManifestEntity me = new ManifestEntity(m);

        assertEquals(0, me.getBuildNumber());
    }
}
