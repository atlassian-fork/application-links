package com.atlassian.applinks.core.rest.ui;

import java.net.URI;
import java.util.UUID;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.core.InternalTypeAccessor;
import com.atlassian.applinks.core.MockEventPublisher;
import com.atlassian.applinks.core.auth.ApplicationLinkRequestFactoryFactory;
import com.atlassian.applinks.core.link.DefaultApplicationLink;
import com.atlassian.applinks.core.manifest.AppLinksManifestDownloader;
import com.atlassian.applinks.core.property.ApplicationLinkProperties;
import com.atlassian.applinks.core.property.PropertyService;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.link.MutableApplicationLink;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.plugins.rest.common.Status;
import com.atlassian.plugins.rest.common.util.RestUrlBuilder;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.user.UserManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class RelocateApplicationLinkUIResourceTest {
    @Mock
    MutatingApplicationLinkService applicationLinkService;
    @Mock
    RequestFactory requestFactory;
    @Mock
    I18nResolver i18nResolver;
    @Mock
    InternalTypeAccessor typeAccessor;
    @Mock
    ManifestRetriever manifestRetriever;
    @Mock
    AppLinksManifestDownloader manifestDownloader;
    @Mock
    RestUrlBuilder restUrlBuilder;
    @Mock
    UserManager userManager;
    @Mock
    Manifest manifest;
    @Mock
    ApplicationType applicationType;
    @Mock
    PropertyService propertyService;
    @Mock
    ApplicationLinkRequestFactoryFactory requestFactoryFactory;
    @Mock
    MockEventPublisher eventPublisher;
    @Mock
    ApplicationLinkProperties applicationLinkProperties;


    private final ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());
    private final String nonAdminUserName = "nonAdmin";
    private final String adminUserName = "admin";
    private final URI oldDisplayUrl = URI.create("http://localhost/url/old/display");
    private final URI oldRpcUrl = URI.create("http://localhost/url/old/rpc");
    private final URI newRpcUrl = URI.create("http://localhost/url/new/rpc");
    private final URI newDisplayUrl = URI.create("http://localhost/url/new/display");


    private MutableApplicationLink mutableApplicationLink;
    private RelocateApplicationLinkUIResource resource;


    @Before
    public void createService() {
        resource = new RelocateApplicationLinkUIResource(restUrlBuilder, applicationLinkService, i18nResolver, manifestRetriever,
                manifestDownloader, typeAccessor, requestFactory, userManager);

        when(userManager.isSystemAdmin(nonAdminUserName)).thenReturn(false);
        when(userManager.isSystemAdmin(adminUserName)).thenReturn(true);

        mutableApplicationLink = new DefaultApplicationLink(applicationId, applicationType, applicationLinkProperties, requestFactoryFactory, eventPublisher);


        when(i18nResolver.getText("applinks.error.only.sysadmin.operation")).thenReturn("error text");
    }

    @Test
    public void verifyNonSysadminCannotRelocateAnAppLink() throws Exception {
        when(userManager.getRemoteUsername()).thenReturn(nonAdminUserName);
        final Response result = resource.relocate("link-id", "http://localhost/newurl", false);
        assertNotNull(result);

        assertEquals(HttpServletResponse.SC_FORBIDDEN, result.getStatus());
        assertEquals("error text", ((Status) result.getEntity()).getMessage());
    }

    @Test
    public void verifyDisplayUrlIsTakenFromManifest() throws Exception {
        configureApplicationLinks();

        when(manifestDownloader.download(any())).thenReturn(manifest);
        resource.relocate(applicationId.get(), newRpcUrl.toString(), false);

        verify(applicationLinkProperties).setRpcUrl(newRpcUrl);
        verify(applicationLinkProperties).setDisplayUrl(newDisplayUrl);
    }

    @Test
    public void verifyDisplayUrlSetToRpcUrlOnInvalidManifest() throws Exception {
        configureApplicationLinks();

        when(manifestDownloader.download(any(URI.class))).thenThrow(new ManifestNotFoundException("404"));
        resource.relocate(applicationId.get(), newRpcUrl.toString(), false);

        verify(applicationLinkProperties).setRpcUrl(newRpcUrl);
        verify(applicationLinkProperties).setDisplayUrl(newRpcUrl);
    }

    private void configureApplicationLinks() throws TypeNotInstalledException {
        when(userManager.getRemoteUsername()).thenReturn(adminUserName);

        when(applicationLinkProperties.getName()).thenReturn("Name");
        when(applicationLinkProperties.getRpcUrl()).thenReturn(oldRpcUrl);
        when(applicationLinkProperties.getDisplayUrl()).thenReturn(oldDisplayUrl);

        when(applicationLinkService.getApplicationLink(applicationId)).thenReturn(mutableApplicationLink);

        when(manifest.getUrl()).thenReturn(newDisplayUrl);
        when(typeAccessor.loadApplicationType(isNull(TypeId.class))).thenReturn(mutableApplicationLink.getType());
    }

}
