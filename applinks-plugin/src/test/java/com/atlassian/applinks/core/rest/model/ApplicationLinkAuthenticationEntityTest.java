package com.atlassian.applinks.core.rest.model;

import com.atlassian.plugins.rest.common.Link;

import org.junit.Test;
import org.springframework.util.Assert;

import static org.mockito.Mockito.mock;

/**
 * Tests for the ApplicationLinkAuthenticationEntity class
 *
 * @since v4.0
 */
public class ApplicationLinkAuthenticationEntityTest {
    @Test
    public void nullAuthenticationProviders() {
        // TODO APLDEV-3 when abstract consumers make an appearance this test should be updated.
        ApplicationLinkAuthenticationEntity entity = new ApplicationLinkAuthenticationEntity(mock(Link.class), null, null);
        Assert.isNull(entity.getConfiguredAuthProviders());
    }
}
