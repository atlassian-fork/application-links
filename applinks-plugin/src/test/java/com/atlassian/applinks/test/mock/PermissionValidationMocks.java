package com.atlassian.applinks.test.mock;

import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NotAuthenticatedException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.i18n.I18nKey;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import org.hamcrest.Matcher;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.mockito.internal.hamcrest.HamcrestArgumentMatcher;

import static com.atlassian.applinks.internal.common.test.matchers.I18nKeyMatchers.withKey;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;

public final class PermissionValidationMocks {
    private final PermissionValidationService permissionValidationService;

    public PermissionValidationMocks(PermissionValidationService permissionValidationService) {
        this.permissionValidationService = permissionValidationService;
    }

    public void failedValidateSysadmin() throws NoAccessException {
        Mockito.doThrow(new PermissionException("test")).when(permissionValidationService).validateSysadmin();
        Mockito.doThrow(new PermissionException("test")).when(permissionValidationService)
                .validateSysadmin(isA(I18nKey.class));
    }

    public void failedValidateAdmin() throws NoAccessException {
        Mockito.doThrow(new PermissionException("test")).when(permissionValidationService).validateAdmin();
        Mockito.doThrow(new PermissionException("test")).when(permissionValidationService)
                .validateAdmin(isA(I18nKey.class));
        failedValidateSysadmin();
    }

    public void failedValidateAuthenticated() throws NoAccessException {
        Mockito.doThrow(new NotAuthenticatedException("test")).when(permissionValidationService).validateAuthenticated();
        Mockito.doThrow(new NotAuthenticatedException("test")).when(permissionValidationService)
                .validateAuthenticated(isA(I18nKey.class));
        Mockito.doThrow(new NotAuthenticatedException("test")).when(permissionValidationService).validateAdmin();
        Mockito.doThrow(new NotAuthenticatedException("test")).when(permissionValidationService)
                .validateAdmin(isA(I18nKey.class));
        Mockito.doThrow(new NotAuthenticatedException("test")).when(permissionValidationService).validateSysadmin();
        Mockito.doThrow(new NotAuthenticatedException("test")).when(permissionValidationService)
                .validateSysadmin(isA(I18nKey.class));

    }

    public void verifyValidateSysadmin() throws NoAccessException {
        verify(permissionValidationService).validateSysadmin();
    }

    public void verifyValidateSysadmin(ArgumentMatcher<I18nKey> i18nKeyMatcher) throws NoAccessException {
        verify(permissionValidationService).validateSysadmin(argThat(i18nKeyMatcher));
    }

    public void verifyValidateSysadmin(String expectedI18nKey) throws NoAccessException {
        verifyValidateSysadmin(new HamcrestArgumentMatcher<>(withKey(expectedI18nKey)));
    }

    public void verifyValidateAdmin() throws NoAccessException {
        verify(permissionValidationService).validateAdmin();
    }

    public void verifyValidateAdmin(ArgumentMatcher<I18nKey> i18nKeyMatcher) throws NoAccessException {
        verify(permissionValidationService).validateAdmin(argThat(i18nKeyMatcher));
    }

    public void verifyValidateAdmin(String expectedI18nKey) throws NoAccessException {
        verifyValidateAdmin(new HamcrestArgumentMatcher<>(withKey(expectedI18nKey)));
    }

    public void verifyValidateAuthenticated() throws NoAccessException {
        verify(permissionValidationService).validateAuthenticated();
    }

    public void verifyValidateAuthenticated(ArgumentMatcher<I18nKey> i18nKeyMatcher) throws NoAccessException {
        verify(permissionValidationService).validateAuthenticated(argThat(i18nKeyMatcher));
    }

    public void verifyValidateAuthenticated(String expectedI18nKey) throws NoAccessException {
        verifyValidateAuthenticated(new HamcrestArgumentMatcher<>(withKey(expectedI18nKey)));
    }

    public void verifyNoPermissionChecks() {
        verifyZeroInteractions(permissionValidationService);
    }
}
