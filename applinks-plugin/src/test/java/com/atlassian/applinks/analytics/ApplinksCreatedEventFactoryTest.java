package com.atlassian.applinks.analytics;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.sal.api.ApplicationProperties;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static com.atlassian.applinks.analytics.ApplinksCreatedEventFactory.EVENT_STATUS.FAILURE;
import static com.atlassian.applinks.analytics.ApplinksCreatedEventFactory.EVENT_STATUS.SUCCESS;
import static com.atlassian.applinks.analytics.ApplinksCreatedEventFactory.EVENT_STATUS.WARNING;
import static com.atlassian.applinks.analytics.ApplinksCreatedEventFactory.FAILURE_REASON.INVALID_URL;
import static com.atlassian.applinks.analytics.ApplinksCreatedEventFactory.FAILURE_REASON.NO_RESPONSE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class ApplinksCreatedEventFactoryTest {

    private static final String PRODUCT_NAME = "name-foo";
    private static final String APPLICATION_ID = "application-id-foo";

    @Mock
    private ApplicationId applicationId;
    @Mock
    private ApplicationProperties applicationProperties;
    @Mock
    private InternalHostApplication internalHostApplication;

    @InjectMocks
    private ApplinksCreatedEventFactory eventFactory;

    @Before
    public void setUp() {
        when(applicationProperties.getPlatformId()).thenReturn(PRODUCT_NAME);
        when(internalHostApplication.getId()).thenReturn(applicationId);
        when(applicationId.get()).thenReturn(APPLICATION_ID);
    }

    @Test
    public void shouldCreateAnEventIndicatingASuccessfulCreation() {
        // invoke
        final ApplinksCreatedEventFactory.ApplinksCreatedEvent event = eventFactory.createSuccessEvent();

        // check
        assertThat(event.getApplicationId(), is(applicationId.get()));
        assertThat(event.getProduct(), is(PRODUCT_NAME));
        assertThat(event.getReason(), is(nullValue()));
        assertThat(event.getStatus(), is(SUCCESS.name()));
    }

    @Test
    public void shouldCreateAnEventIndicatingAWarningWasShown() {
        // invoke
        final ApplinksCreatedEventFactory.ApplinksCreatedEvent event = eventFactory.createWarningEvent(NO_RESPONSE);

        // check
        assertThat(event.getApplicationId(), is(applicationId.get()));
        assertThat(event.getProduct(), is(PRODUCT_NAME));
        assertThat(event.getReason(), is(NO_RESPONSE.name()));
        assertThat(event.getStatus(), is(WARNING.name()));
    }

    @Test
    public void shouldCreateAnEventIndicatingTheCreationHasFailed() {
        // invoke
        final ApplinksCreatedEventFactory.ApplinksCreatedEvent event = eventFactory.createFailEvent(INVALID_URL);

        // check
        assertThat(event.getApplicationId(), is(applicationId.get()));
        assertThat(event.getProduct(), is(PRODUCT_NAME));
        assertThat(event.getReason(), is(INVALID_URL.name()));
        assertThat(event.getStatus(), is(FAILURE.name()));
    }
}