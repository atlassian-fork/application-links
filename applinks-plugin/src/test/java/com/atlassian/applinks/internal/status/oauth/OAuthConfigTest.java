package com.atlassian.applinks.internal.status.oauth;

import com.google.common.collect.Lists;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createThreeLoOnlyConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.defaultOAuthConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.disabledConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.threeLoOnlyConfig;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class OAuthConfigTest {
    @Test
    public void testCreateDisabledConfig() {
        com.atlassian.applinks.internal.common.status.oauth.OAuthConfig config = createDisabledConfig();

        assertThat(config, is(disabledConfig()));
    }

    @Test
    public void test3LoOnlyConfig() {
        com.atlassian.applinks.internal.common.status.oauth.OAuthConfig config = createThreeLoOnlyConfig();

        assertThat(config, is(threeLoOnlyConfig()));
    }

    @Test
    public void testCreateDefaultConfig() {
        com.atlassian.applinks.internal.common.status.oauth.OAuthConfig config = createDefaultOAuthConfig();

        assertThat(config, is(defaultOAuthConfig()));
    }

    @Test
    public void testCreateImpersonationConfig() {
        com.atlassian.applinks.internal.common.status.oauth.OAuthConfig config = createOAuthWithImpersonationConfig();

        assertThat(config, is(oAuthWithImpersonationConfig()));
    }

    @Test
    public void testOAuthConfigOrdering() {
        List<com.atlassian.applinks.internal.common.status.oauth.OAuthConfig> allConfigs = Lists.newArrayList(
                createDisabledConfig(),
                createThreeLoOnlyConfig(),
                createDefaultOAuthConfig(),
                createOAuthWithImpersonationConfig()
        );

        Collections.shuffle(allConfigs);

        assertThat(com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.ORDER_BY_LEVEL.sortedCopy(allConfigs), Matchers.contains(
                createDisabledConfig(),
                createThreeLoOnlyConfig(),
                createDefaultOAuthConfig(),
                createOAuthWithImpersonationConfig()
        ));
    }
}
