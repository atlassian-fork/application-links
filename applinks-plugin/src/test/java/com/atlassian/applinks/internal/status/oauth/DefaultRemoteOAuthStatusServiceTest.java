package com.atlassian.applinks.internal.status.oauth;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.Anonymous;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.core.v1.rest.ApplicationLinkResource;
import com.atlassian.applinks.host.spi.InternalHostApplication;
import com.atlassian.applinks.internal.applink.ApplinkHelper;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.capabilities.DefaultRemoteCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.capabilities.RemoteCapabilitiesError;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.PermissionException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.permission.PermissionValidationService;
import com.atlassian.applinks.internal.rest.model.auth.compatibility.RestApplicationLinkAuthentication;
import com.atlassian.applinks.internal.rest.model.auth.compatibility.RestAuthenticationProvider;
import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.rest.status.ApplinkStatusResource;
import com.atlassian.applinks.internal.status.error.ApplinkError;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.ApplinkStatusException;
import com.atlassian.applinks.internal.status.error.SimpleApplinkError;
import com.atlassian.applinks.internal.status.error.SimpleApplinkStatusException;
import com.atlassian.applinks.internal.status.oauth.remote.DefaultRemoteOAuthStatusService;
import com.atlassian.applinks.internal.status.oauth.remote.OAuthConnectionVerifier;
import com.atlassian.applinks.internal.status.remote.ApplinkStatusAccessException;
import com.atlassian.applinks.internal.status.remote.NoOutgoingAuthenticationException;
import com.atlassian.applinks.internal.status.remote.NoRemoteApplinkException;
import com.atlassian.applinks.internal.status.remote.RemoteNetworkException;
import com.atlassian.applinks.internal.status.remote.RemoteStatusUnknownException;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.test.mock.MockApplicationLinkRequest;
import com.atlassian.applinks.test.mock.MockApplicationLinkResponse;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.mock.MockRequestAnswer;
import com.atlassian.applinks.test.mock.PermissionValidationMocks;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import com.atlassian.sal.api.net.Request.MethodType;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseTransportException;
import com.google.common.collect.ImmutableList;
import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.ConnectException;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.Collections;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import javax.ws.rs.core.Response.Status;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createOAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withConnectionTimeout;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withMethodType;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withSocketTimeout;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withUrlThat;
import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static com.atlassian.applinks.test.matcher.ThrowableMatchers.withMessageThat;
import static com.atlassian.applinks.test.matchers.AuthorisationUriAwareMatchers.withAuthorisationUri;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.withDetailsThat;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.withType;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.defaultOAuthConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.disabledConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusDefault;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusOff;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthStatusWith;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.oAuthWithImpersonationConfig;
import static com.atlassian.applinks.test.matchers.status.OAuthStatusMatchers.threeLoOnlyConfig;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.createConsumer;
import static com.atlassian.applinks.test.mock.TestApplinkIds.DEFAULT_ID;
import static com.atlassian.applinks.test.mock.TestApplinkIds.ID2;
import static com.atlassian.applinks.test.mock.TestApplinkIds.ID3;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.startsWith;
import static org.hamcrest.Matchers.stringContainsInOrder;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.endsWith;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
@SuppressWarnings("unchecked")
public class DefaultRemoteOAuthStatusServiceTest {
    private static final URI HOST_BASE_URL = URI.create("http://test-local.com");

    private static final ApplicationId APPLINK_ID = DEFAULT_ID.applicationId();
    private static final ApplicationId HOST_ID = ID2.applicationId();
    private static final ApplicationId HOST_FALLBACK_ID = ApplicationIdUtil.generate(HOST_BASE_URL);
    private static final int EXPECTED_TIMEOUT = 15000; // 15 seconds

    @MockApplink(url = "http://test-remote.com")
    private ApplicationLink applicationLink;
    @Mock
    private ApplicationLinkRequestFactory applicationLinkRequestFactory;

    @Mock
    private ApplinkHelper applinkHelper;
    @Mock
    private AuthenticationConfigurationManager authenticationConfigurationManager;
    @Mock
    private InternalHostApplication internalHostApplication;
    @Mock
    private OAuthConnectionVerifier oAuthConnectionVerifier;
    @Mock
    private PermissionValidationService permissionValidationService;
    @Mock
    private RemoteCapabilitiesService remoteCapabilitiesService;

    @InjectMocks
    private PermissionValidationMocks permissionValidationMocks;
    @InjectMocks
    private DefaultRemoteOAuthStatusService remoteOAuthStatusService;

    @Captor
    private ArgumentCaptor<Class<? extends AuthenticationProvider>> authenticationCaptor;

    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    private final MockRequestAnswer mockRequestAnswer = MockRequestAnswer.create();

    @Before
    public void setUpHost() {
        when(internalHostApplication.getId()).thenReturn(HOST_ID);
        when(internalHostApplication.getBaseUrl()).thenReturn(HOST_BASE_URL);
    }

    @Before
    public void setUpApplinkHelper() throws ServiceException {
        when(applinkHelper.getApplicationLink(APPLINK_ID)).thenReturn(applicationLink);
    }

    @Before
    public void setUpRequestCreation() throws Exception {
        when(applicationLink.createAuthenticatedRequestFactory(authenticationCaptor.capture()))
                .thenReturn(applicationLinkRequestFactory);
        when(applicationLinkRequestFactory.createRequest(any(), any()))
                .thenAnswer(mockRequestAnswer);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullApplinkNotAccepted() throws ServiceException {
        remoteOAuthStatusService.fetchOAuthStatus((ApplicationLink) null);
    }

    @SuppressWarnings("ConstantConditions")
    @Test(expected = NullPointerException.class)
    public void nullApplinkIdNotAccepted() throws ServiceException {
        remoteOAuthStatusService.fetchOAuthStatus((ApplicationId) null);
    }

    @Test(expected = NoSuchApplinkException.class)
    public void applinkDoesNotExistLocally() throws ServiceException {
        when(applinkHelper.getApplicationLink(ID3.applicationId())).thenThrow(new NoSuchApplinkException("test"));

        remoteOAuthStatusService.fetchOAuthStatus(ID3.applicationId());
    }

    @Test(expected = PermissionException.class)
    public void adminPermissionRequiredForFetchByApplink() throws ServiceException {
        permissionValidationMocks.failedValidateAdmin();

        remoteOAuthStatusService.fetchOAuthStatus(applicationLink);
    }

    @Test(expected = PermissionException.class)
    public void adminPermissionRequiredForFetchByApplinkId() throws ServiceException {
        permissionValidationMocks.failedValidateAdmin();

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    // the tests below simulate various flows depending on the 3 major variables on the remote app:
    // Status API availability - enabled querying for OAuth status anonymously
    // status exists by default ID - remote app knows about local app by the default ID coming from InternalHostApplication SPI
    // status exists by fallback ID - remote app knows about local app by the fallback ID, used in Atlassian Cloud
    // see DefaultRemoteOAuthStatusService impl for details

    @Test
    public void remoteStatusApiAvailableDefaultRemoteApplinkId() throws ServiceException {
        // remote Status API: available
        // OAuth config by default ID: available
        // -> successfully obtaining status by default ID

        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(createOAuthStatusResponse(new ApplinkOAuthStatus(
                createDefaultOAuthConfig(),
                createOAuthWithImpersonationConfig()
        )));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(defaultOAuthConfig(), oAuthWithImpersonationConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(
                applinkOAuthStatusRequest(HOST_ID)
        ));
        assertAuthentications(Anonymous.class);
        assertRequestTimeouts();
        permissionValidationMocks.verifyValidateAdmin();
    }

    @Test
    public void remoteStatusApiAvailableFallbackRemoteApplinkId() throws ServiceException {
        // remote Status API: available
        // OAuth config by default ID: not available
        // OAuth config by fallback ID: available
        // -> successfully obtaining status by fallback ID

        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(createNotFoundResponse()) // not found for the default ID
                .addResponse(createOAuthStatusResponse(ApplinkOAuthStatus.OFF));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusOff());
        assertThat(mockRequestAnswer.executedRequests(), contains(
                applinkOAuthStatusRequest(HOST_ID),
                applinkOAuthStatusRequest(HOST_FALLBACK_ID)
        ));
        assertAuthentications(Anonymous.class, Anonymous.class);
        assertRequestTimeouts();
        permissionValidationMocks.verifyValidateAdmin();
    }

    @Test(expected = NoRemoteApplinkException.class)
    public void remoteStatusApiAvailableNoRemoteApplink() throws ServiceException {
        // remote Status API: available
        // OAuth config by default ID: not available
        // OAuth config by fallback ID: not available
        // -> remote end does not know about local app

        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(createNotFoundResponse()) // not found by default ID
                .addResponse(createNotFoundResponse()); // not found by fallback ID

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdOAuthOff() throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents "off" OAuth status

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                Collections.<RestConsumer>emptyList(),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusOff());
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdOAuthOff() throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents "off" OAuth status

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                Collections.<RestConsumer>emptyList(),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusOff());
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdIncomingOAuth3LoOnly()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents incoming 3LO-only OAuth status

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                singletonList(createRestConsumer(false, false)),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(threeLoOnlyConfig(), disabledConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdIncomingOAuth3LoOnly()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents incoming 3LO-only OAuth status

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                singletonList(createRestConsumer(false, false)),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(threeLoOnlyConfig(), disabledConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdIncomingOAuthDefault()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents incoming "default" OAuth status (3LO+2LO)

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                asList(
                        createRestConsumer(false, false),
                        createRestConsumer(true, false) // "greatest" consumer should win
                ),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(defaultOAuthConfig(), disabledConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdIncomingOAuthDefault()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents incoming "default" OAuth status (3LO+2LO)

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                asList(
                        createRestConsumer(false, false),
                        createRestConsumer(true, false) // "greatest" consumer should win
                ),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(defaultOAuthConfig(), disabledConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdIncomingOAuthWithImpersonation()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents incoming OAuth "with impersonation" status (3LO+2LO+2LOi)

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                asList(
                        createRestConsumer(false, false),
                        createRestConsumer(true, false),
                        createRestConsumer(true, true) // "greatest" consumer should win
                ),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(oAuthWithImpersonationConfig(), disabledConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdIncomingOAuthWithImpersonation()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents incoming OAuth "with impersonation" status (3LO+2LO+2LOi)

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                asList(
                        createRestConsumer(false, false),
                        createRestConsumer(true, false),
                        createRestConsumer(true, true) // "greatest" consumer should win
                ),
                createNonOAuthRestAuthenticationProvider()
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(oAuthWithImpersonationConfig(), disabledConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }


    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdOutgoingOAuth3LoOnly()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents outgoing 3LO-only OAuth status

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                null,
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class)
                )
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(disabledConfig(), threeLoOnlyConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdOutgoingOAuth3LoOnly()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents outgoing 3LO-only OAuth status

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                null,
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class)
                )
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(disabledConfig(), threeLoOnlyConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }


    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdOutgoingOAuthDefault()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents outgoing "default" OAuth status (3LO+2LO)

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                null,
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class)
                )
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(disabledConfig(), defaultOAuthConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdOutgoingOAuthDefault()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents outgoing "default" OAuth status (3LO+2LO)

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                null,
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class)
                )
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(disabledConfig(), defaultOAuthConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion4xDefaultRemoteApplinkIdOutgoingOAuthWithImpersonation()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents outgoing OAuth "with impersonation" status (3LO+2LO+2LOi)

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                null,
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)
                )));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(disabledConfig(), oAuthWithImpersonationConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink4xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion5xDefaultRemoteApplinkIdOutgoingOAuthWithImpersonation()
            throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 5.x
        // legacy OAuth config by default ID: available
        // -> successfully obtaining status by default ID from legacy REST APIs
        // -> legacy response represents outgoing OAuth "with impersonation" status (3LO+2LO+2LOi)

        setUpDefaultRemoteCapabilities(false, "5.0.4");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                null,
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class)
                )));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(disabledConfig(), oAuthWithImpersonationConfig()));
        assertThat(mockRequestAnswer.executedRequests(), contains(applink5xAuthenticationRequest(HOST_ID)));
        assertAuthentications(OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersion4xFallbackRemoteApplinkId() throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: not available
        // legacy OAuth config by fallback ID: available
        // -> successfully obtaining status by fallback ID from legacy REST APIs

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createNotFoundResponse()) // not found by default ID
                .addResponse(createApplinkAuthenticationResponse(
                        createRestConsumer(true, false),
                        asList(
                                createNonOAuthRestAuthenticationProvider(),
                                createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                                createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class)
                        )
                ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusDefault());
        assertThat(mockRequestAnswer.executedRequests(), contains(
                applink4xAuthenticationRequest(HOST_ID),
                applink4xAuthenticationRequest(HOST_FALLBACK_ID)
        ));
        assertAuthentications(OAuthAuthenticationProvider.class, OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test
    public void remoteStatusApiNotAvailableVersionUnknown() throws ServiceException {
        // remote Status API: not available
        // remote Applinks version from capabilities is unknown (error capabilities)
        // legacy OAuth config by default ID via 5.x resource: not available
        // legacy OAuth config by default ID via 4.x resource: not available
        // legacy OAuth config by fallback ID via 5.x resource: not available
        // legacy OAuth config by fallback ID via 5.x resource: not available
        // -> successfully obtaining status by fallback ID from 4.x legacy REST APIs

        // capabilities error, version is unknown
        setUpDefaultRemoteCapabilities(new RemoteCapabilitiesError(new SimpleApplinkError(ApplinkErrorType.CONNECTION_REFUSED)));
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createNotFoundResponse()) // not found by default ID 5.x
                .addResponse(createNotFoundResponse()) // not found by default ID 4.x
                .addResponse(createNotFoundResponse()) // not found by fallback ID 5.x
                .addResponse(createApplinkAuthenticationResponse(
                        createRestConsumer(true, false),
                        ImmutableList.of(
                                createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                                createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class))
                )); // found by fallback ID 5.x

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusDefault());
        // expected 4 attempts to get the OAuth status
        assertThat(mockRequestAnswer.executedRequests(), contains(
                applink5xAuthenticationRequest(HOST_ID),
                applink4xAuthenticationRequest(HOST_ID),
                applink5xAuthenticationRequest(HOST_FALLBACK_ID),
                applink4xAuthenticationRequest(HOST_FALLBACK_ID)
        ));
        assertAuthentications(OAuthAuthenticationProvider.class, OAuthAuthenticationProvider.class,
                OAuthAuthenticationProvider.class, OAuthAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test(expected = NoRemoteApplinkException.class)
    public void remoteStatusApiNotAvailableersion4xNoRemoteApplink() throws ServiceException {
        // remote Status API: not available
        // remote Applinks version: 4.x
        // legacy OAuth config by default ID: not available
        // legacy OAuth config by fallback ID: not available
        // -> remote end does not know about local app

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createNotFoundResponse()) // not found by default ID
                .addResponse(createNotFoundResponse()); // not found by fallback ID

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }


    @Test
    public void oAuthWithImpersonationPreferredForLegacyStatusRequest() throws ServiceException {
        // remote Status API: not available
        // this tests which outgoing provider will be picked up to execute the legacy status request given more than
        // 1 supported providers are defined
        // 2LOi should be picked up over 3LO if both are configured

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);
        setUpAuthenticationProviderForStatusRequests(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createApplinkAuthenticationResponse(
                createRestConsumer(true, false),
                asList(
                        createNonOAuthRestAuthenticationProvider(),
                        createRestAuthenticationProvider(OAuthAuthenticationProvider.class),
                        createRestAuthenticationProvider(TwoLeggedOAuthAuthenticationProvider.class)
                )
        ));

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusWith(defaultOAuthConfig(), defaultOAuthConfig()));
        // prefer 2LOi over 3LO to make the "legacy" status requests
        assertAuthentications(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);
        assertRequestTimeouts();
    }

    @Test(expected = NoOutgoingAuthenticationException.class)
    public void outgoingAuthNotAvailableForLegacyStatusRequests() throws ServiceException {
        // remote Status API: not available
        // no outgoing auth provider available to execute the legacy status request
        // -> the no outgoing auth error should be raised

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        when(authenticationConfigurationManager.isConfigured(eq(APPLINK_ID), any(Class.class))).thenReturn(false);

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void insufficientRemotePermissionForLegacyStatusRequestUnauthorizedResponseNoOAuthHeaderNoBody()
            throws ServiceException {
        expectedException.expect(instanceOf(ApplinkStatusAccessException.class));
        expectedException.expect(withType(ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION));
        expectedException.expect(withAuthorisationUri(URI.create("http://test.com/auth")));

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthorisationUri("http://test.com/auth");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createResponse(Status.UNAUTHORIZED));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void insufficientRemotePermissionForLegacyStatusRequestUnauthorizedResponseWithOAuthHeaderNonEmptyBody()
            throws ServiceException {
        expectedException.expect(instanceOf(ApplinkStatusAccessException.class));
        expectedException.expect(withType(ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION));
        expectedException.expect(withAuthorisationUri(URI.create("http://test.com/auth")));

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthorisationUri("http://test.com/auth");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(new MockApplicationLinkResponse()
                .setStatus(Status.UNAUTHORIZED)
                .setHeader(ApplinksOAuth.WWW_AUTHENTICATE, "OAuth xxx")
                .setResponseBody("No permission!"));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void legacyStatusTwoLoiRequestUnauthorizedResponseWithOAuthHeaderNoBodyOAuthCheckOk()
            throws ServiceException {
        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthorisationUri("http://test.com/auth");
        setUpAuthenticationProviderForStatusRequests(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

        // should be interpreted as "2LOi disabled"
        mockRequestAnswer.addResponse(new MockApplicationLinkResponse()
                .setStatus(Status.UNAUTHORIZED)
                .setHeader(ApplinksOAuth.WWW_AUTHENTICATE, "OAuth xxx")
                .setResponseBody(""));
        // 2LO ok
        doNothing().when(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);

        ApplinkOAuthStatus status = remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);

        assertThat(status, oAuthStatusDefault());
        verify(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);
        assertRequestTimeouts();
    }

    @Test
    public void legacyStatusTwoLoiRequestUnauthorizedResponseWithOAuthHeaderNoBodyOAuthCheckFailed()
            throws ServiceException {
        expectedException.expect(instanceOf(ApplinkError.class));
        expectedException.expect(withType(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED));

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthorisationUri("http://test.com/auth");
        setUpAuthenticationProviderForStatusRequests(TwoLeggedOAuthWithImpersonationAuthenticationProvider.class);

        // should be interpreted as "2LOi disabled"
        mockRequestAnswer.addResponse(new MockApplicationLinkResponse()
                .setStatus(Status.UNAUTHORIZED)
                .setHeader(ApplinksOAuth.WWW_AUTHENTICATE, "OAuth xxx")
                .setResponseBody(""));
        // 2LO failed with "level unsupported"
        doThrow(new SimpleApplinkStatusException(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED))
                .when(oAuthConnectionVerifier).verifyOAuthConnection(applicationLink);

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void insufficientRemotePermissionsForLegacyStatusRequestForbidden() throws ServiceException {
        expectedException.expect(instanceOf(ApplinkStatusAccessException.class));
        expectedException.expect(withType(ApplinkErrorType.INSUFFICIENT_REMOTE_PERMISSION));
        expectedException.expect(withAuthorisationUri(URI.create("http://test.com/auth")));

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthorisationUri("http://test.com/auth");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        mockRequestAnswer.addResponse(createResponse(Status.FORBIDDEN));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void legacyStatusRequestNoAccessTokenLocally() throws Exception {
        // remote Status API: not available
        // no access token locally, creating legacy status request will fail with CredentialsRequired
        // -> local token error should be raised

        expectedException.expect(instanceOf(ApplinkStatusAccessException.class));
        expectedException.expect(withType(ApplinkErrorType.LOCAL_AUTH_TOKEN_REQUIRED));
        expectedException.expect(withAuthorisationUri(URI.create("http://test.com/auth")));

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);
        // throw CredentialsRequired when service attempts to create request to legacy status
        AuthorisationURIGenerator mockUriGenerator = mock(AuthorisationURIGenerator.class);
        when(mockUriGenerator.getAuthorisationURI()).thenReturn(URI.create("http://test.com/auth"));
        when(applicationLinkRequestFactory.createRequest(eq(MethodType.GET), endsWith("/authentication")))
                .thenThrow(new CredentialsRequiredException(mockUriGenerator, "test"));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void legacyStatusRequestNoAccessTokenRemotely() throws Exception {
        // remote Status API: not available
        // access token rejected remotely, executing legacy status request will fail with CredentialsRequired
        // -> remote token error should be raised

        expectedException.expect(instanceOf(ApplinkStatusAccessException.class));
        expectedException.expect(withType(ApplinkErrorType.REMOTE_AUTH_TOKEN_REQUIRED));
        expectedException.expect(withAuthorisationUri(URI.create("http://test.com/auth")));

        setUpDefaultRemoteCapabilities(false, "4.3.5");
        setUpAuthorisationUri("http://test.com/auth");
        setUpAuthenticationProviderForStatusRequests(OAuthAuthenticationProvider.class);

        // "credentials required" response
        mockRequestAnswer.addResponse(new MockApplicationLinkResponse().setCredentialsRequired(true));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test(expected = RemoteStatusUnknownException.class)
    public void statusRequestFailsDueToUnknownConnectionProblem() throws ServiceException, CredentialsRequiredException {
        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(
                new MockApplicationLinkResponse().setResponseException(new ResponseTransportException("test")));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test(expected = RemoteStatusUnknownException.class)
    public void statusRequestFailsDueToUnknownProblem() throws ServiceException, CredentialsRequiredException {
        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(
                new MockApplicationLinkResponse().setResponseException(new ResponseException("test")));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void statusRequestFailsDueToConnectionProblem() throws ServiceException, CredentialsRequiredException {
        expectedException.expect(instanceOf(RemoteNetworkException.class));
        expectedException.expect(withMessageThat(containsInOrder("Failed to fetch OAuth status",
                RemoteNetworkException.class.getSimpleName(),
                ApplinkErrorType.CONNECTION_REFUSED.name(),
                ConnectException.class.getName())));
        expectedException.expect(withType(ApplinkErrorType.CONNECTION_REFUSED));
        expectedException.expect(withDetailsThat(startsWith(ConnectException.class.getName())));

        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(
                new MockApplicationLinkResponse().setResponseException(
                        new ResponseTransportException("test",
                                new ConnectException("Connection refused"))));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void statusRequestFailsDueToUnknownHostProblem() throws ServiceException, CredentialsRequiredException {
        expectedException.expect(instanceOf(RemoteNetworkException.class));
        expectedException.expect(withMessageThat(containsInOrder("Failed to fetch OAuth status",
                RemoteNetworkException.class.getSimpleName(),
                ApplinkErrorType.UNKNOWN_HOST.name(),
                UnknownHostException.class.getName())));
        expectedException.expect(withType(ApplinkErrorType.UNKNOWN_HOST));
        expectedException.expect(withDetailsThat(startsWith(UnknownHostException.class.getName())));

        setUpDefaultRemoteCapabilities(true, "5.0.5");
        mockRequestAnswer.addResponse(
                new MockApplicationLinkResponse().setResponseException(new ResponseException("test", new UnknownHostException())));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void incompatibleLinkNonAtlassian() throws ServiceException, CredentialsRequiredException {
        expectedException.expect(instanceOf(ApplinkStatusException.class));
        expectedException.expect(withType(ApplinkErrorType.NON_ATLASSIAN));

        setUpDefaultRemoteCapabilities(new RemoteCapabilitiesError(new SimpleApplinkError(ApplinkErrorType.NON_ATLASSIAN)));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    @Test
    public void incompatibleLinkGeneric() throws ServiceException, CredentialsRequiredException {
        expectedException.expect(instanceOf(ApplinkStatusException.class));
        expectedException.expect(withType(ApplinkErrorType.GENERIC_LINK));

        setUpDefaultRemoteCapabilities(new RemoteCapabilitiesError(new SimpleApplinkError(ApplinkErrorType.GENERIC_LINK)));

        remoteOAuthStatusService.fetchOAuthStatus(APPLINK_ID);
    }

    private static MockApplicationLinkResponse createOAuthStatusResponse(ApplinkOAuthStatus status) {
        return new MockApplicationLinkResponse().setEntity(new RestApplinkOAuthStatus(status));
    }

    private static MockApplicationLinkResponse createApplinkAuthenticationResponse(
            RestConsumer consumer, List<RestAuthenticationProvider> providers
    ) {
        return createApplinkAuthenticationResponse(createRestApplinkAuthentication(consumer, providers));
    }

    private static MockApplicationLinkResponse createApplinkAuthenticationResponse(
            List<RestConsumer> consumers, RestAuthenticationProvider provider
    ) {
        return createApplinkAuthenticationResponse(createRestApplinkAuthentication(consumers, provider));
    }

    private static MockApplicationLinkResponse createApplinkAuthenticationResponse(
            RestApplicationLinkAuthentication entity) {
        return new MockApplicationLinkResponse().setEntity(entity);
    }

    private static MockApplicationLinkResponse createNotFoundResponse() {
        return createResponse(Status.NOT_FOUND);
    }

    private static RemoteApplicationCapabilities createRemoteCapabilities(boolean statusApiAvailable,
                                                                          String applinksVersion,
                                                                          ApplinkErrorType error) {
        Set<ApplinksCapabilities> capabilities = statusApiAvailable ?
                EnumSet.of(ApplinksCapabilities.STATUS_API) :
                EnumSet.noneOf(ApplinksCapabilities.class);
        ApplinkError applinkError = error != null ? new SimpleApplinkError(error) : null;

        return new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse("1.0.0"))
                .applinksVersion(ApplicationVersion.parse(applinksVersion))
                .capabilities(capabilities)
                .error(applinkError)
                .build();
    }

    private static MockApplicationLinkResponse createResponse(Status status) {
        return new MockApplicationLinkResponse().setStatus(status);
    }

    private static RestApplicationLinkAuthentication createRestApplinkAuthentication(
            List<RestConsumer> consumers, List<RestAuthenticationProvider> providers
    ) {
        RestApplicationLinkAuthentication authentication = new RestApplicationLinkAuthentication();
        authentication.put(RestApplicationLinkAuthentication.CONSUMERS, consumers);
        authentication.put(RestApplicationLinkAuthentication.CONFIGURED_AUTHENTICATION_PROVIDERS, providers);
        return authentication;
    }

    private static RestApplicationLinkAuthentication createRestApplinkAuthentication(
            RestConsumer consumer, List<RestAuthenticationProvider> providers
    ) {
        return createRestApplinkAuthentication(singletonList(consumer), providers);
    }

    private static RestApplicationLinkAuthentication createRestApplinkAuthentication(
            List<RestConsumer> consumers, RestAuthenticationProvider provider
    ) {
        return createRestApplinkAuthentication(consumers, singletonList(provider));
    }

    private static RestConsumer createRestConsumer(boolean twoLo, boolean twoLoi) {
        return new RestConsumer(createConsumer(true, twoLo, twoLoi));
    }

    private static RestAuthenticationProvider createNonOAuthRestAuthenticationProvider() {
        return createRestAuthenticationProvider(Anonymous.class);
    }

    private static RestAuthenticationProvider createRestAuthenticationProvider(
            Class<? extends AuthenticationProvider> provider) {
        RestAuthenticationProvider restProvider = new RestAuthenticationProvider();
        restProvider.put(RestAuthenticationProvider.PROVIDER, provider.getCanonicalName());
        return restProvider;
    }

    private static Matcher<MockApplicationLinkRequest> applinkOAuthStatusRequest(ApplicationId id) {
        // OAuth status resource available in REST 3.0
        return applinksCoreRestRequest(MethodType.GET, "3.0", ApplinkStatusResource.CONTEXT, id,
                ApplinkStatusResource.OAUTH_PATH.toString());
    }

    private static Matcher<MockApplicationLinkRequest> applink4xAuthenticationRequest(ApplicationId id) {
        // /applicationLink/{id}/authentication resource available in REST 2.0 in Applinks pre-5.x
        return applinksCoreRestRequest(MethodType.GET, "2.0", ApplicationLinkResource.CONTEXT, id, "authentication");
    }

    private static Matcher<MockApplicationLinkRequest> applink5xAuthenticationRequest(ApplicationId id) {
        return applinksOAuthRestRequest(MethodType.GET,
                ApplicationLinkResource.CONTEXT, id, "authentication");
    }

    private static Matcher<MockApplicationLinkRequest> applinksCoreRestRequest(MethodType expectedMethod,
                                                                               String version,
                                                                               String context,
                                                                               ApplicationId expectedApplinkId,
                                                                               String path) {
        // request to Applinks Core Plugin REST
        return allOf(
                withMethodType(expectedMethod),
                withUrlThat(stringContainsInOrder(ImmutableList.of("rest", "applinks", version, context,
                        expectedApplinkId.get(), path)))
        );
    }

    private static Matcher<MockApplicationLinkRequest> applinksOAuthRestRequest(MethodType expectedMethod,
                                                                                String context,
                                                                                ApplicationId expectedApplinkId,
                                                                                String path) {
        // request to Applinks OAuth Plugin latest REST
        return allOf(
                withMethodType(expectedMethod),
                withUrlThat(stringContainsInOrder(ImmutableList.of("rest", "applinks-oauth", "latest", context,
                        expectedApplinkId.get(), path)))
        );
    }

    private void assertAuthentications(Class<? extends AuthenticationProvider>... expected) {
        assertThat(getCapturedAuthentications(), contains(expected));
    }

    /**
     * Assert that all executed requests have default timeouts.
     */
    private void assertRequestTimeouts() {
        assertThat(mockRequestAnswer.executedRequests(), everyItem(withConnectionTimeout(EXPECTED_TIMEOUT)));
        assertThat(mockRequestAnswer.executedRequests(), everyItem(withSocketTimeout(EXPECTED_TIMEOUT)));
    }

    private Iterable<? extends Class<? extends AuthenticationProvider>> getCapturedAuthentications() {
        return authenticationCaptor.getAllValues();
    }

    private void setUpAuthenticationProviderForStatusRequests(Class<? extends AuthenticationProvider> providerClass) {
        when(authenticationConfigurationManager.isConfigured(APPLINK_ID, providerClass)).thenReturn(true);
    }

    private void setUpAuthorisationUri(String uri) {
        when(applicationLinkRequestFactory.getAuthorisationURI()).thenReturn(URI.create(uri));
    }

    private void setUpDefaultRemoteCapabilities(boolean statusApiAvailable, String applinksVersion)
            throws ServiceException {
        setUpDefaultRemoteCapabilities(statusApiAvailable, applinksVersion, null);
    }

    private void setUpDefaultRemoteCapabilities(boolean statusApiAvailable, String applinksVersion,
                                                ApplinkErrorType error) throws ServiceException {
        setUpDefaultRemoteCapabilities(createRemoteCapabilities(statusApiAvailable, applinksVersion, error));
    }

    private void setUpDefaultRemoteCapabilities(RemoteApplicationCapabilities capabilities) throws ServiceException {
        setUpDefaultRemoteCapabilities(capabilities, 1, TimeUnit.HOURS);
    }

    private void setUpDefaultRemoteCapabilities(RemoteApplicationCapabilities capabilities, long maxAge, TimeUnit unit)
            throws ServiceException {
        when(remoteCapabilitiesService.getCapabilities(applicationLink, maxAge, unit)).thenReturn(capabilities);
    }
}
