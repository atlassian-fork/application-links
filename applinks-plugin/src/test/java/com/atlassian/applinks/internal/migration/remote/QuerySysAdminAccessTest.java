package com.atlassian.applinks.internal.migration.remote;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.auth.types.TrustedAppsAuthenticationProvider;
import com.atlassian.sal.api.net.Request;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.UUID;

import static org.junit.Assert.assertFalse;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class QuerySysAdminAccessTest {
    @Mock
    private ApplicationLink link;
    @Mock
    private ApplicationLinkRequestFactory factory;
    @Mock
    private ApplicationLinkRequest applinkRequest;
    private final ApplicationId applicationId = new ApplicationId(UUID.randomUUID().toString());

    @Test
    public void execute() throws Exception {
        when(link.createAuthenticatedRequestFactory(TrustedAppsAuthenticationProvider.class)).thenReturn(factory);
        when(factory.createRequest(any(Request.MethodType.class), any(String.class))).thenReturn(applinkRequest);

        QuerySysAdminAccess.INSTANCE.execute(link, applicationId, TrustedAppsAuthenticationProvider.class);

        verify(factory, times(1)).createRequest(Request.MethodType.HEAD, QuerySysAdminAccess.SYS_ADMIN_ACCESS_PATH + applicationId.toString());
        verify(applinkRequest).setHeader("X-Atlassian-Token", "no-check");
        verify(applinkRequest).execute(any(RemoteActionHandler.class));
        verify(applinkRequest).setConnectionTimeout(TryWithAuthentication.TIME_OUT_IN_SECONDS * 1000);
        verify(applinkRequest).setSoTimeout(TryWithAuthentication.TIME_OUT_IN_SECONDS * 1000);
    }

    @Test
    public void executeWithInvalidFactory() throws Exception {
        when(link.createAuthenticatedRequestFactory(TrustedAppsAuthenticationProvider.class)).thenReturn(null);
        assertFalse(QuerySysAdminAccess.INSTANCE.execute(link, applicationId, TrustedAppsAuthenticationProvider.class));
    }

}