package com.atlassian.applinks.internal.capabilities;

import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.osgi.framework.Version;

import static java.lang.String.format;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;

public class ApplicationVersionTest {
    private static void assertVersion(int major, int minor, int bugfix, String suffix, ApplicationVersion actual) {
        assertVersion(major, minor, bugfix, suffix, format("%d.%d.%d%s", major, minor, bugfix, suffix), actual);
    }

    private static void assertVersion(int major, int minor, int bugfix, String suffix, String versionString,
                                      ApplicationVersion actual) {
        assertEquals("major", major, actual.getMajor());
        assertEquals("minor", minor, actual.getMinor());
        assertEquals("bugfix", bugfix, actual.getBugfix());
        assertEquals("suffix", suffix, actual.getSuffix());
        assertEquals("versionString", versionString, actual.getVersionString());
    }

    @Test
    public void simpleValidVersionNoSuffix() {
        assertVersion(5, 0, 4, "", ApplicationVersion.parse("5.0.4"));
    }

    @Test
    public void simpleValidVersionWithSuffix() {
        assertVersion(5, 0, 4, "-m01", ApplicationVersion.parse("5.0.4-m01"));
    }

    @Test
    public void versionWithWhitespace() {
        assertVersion(5, 0, 4, "-m01", ApplicationVersion.parse("  5.0.4-m01\t"));
    }

    @Test
    public void versionWithDoubleDigits() {
        assertVersion(10, 1, 12, "", ApplicationVersion.parse("10.1.12"));
    }

    @Test
    public void versionWithNoBugfixAndSuffix() {
        assertVersion(5, 1, 0, "", "5.1", ApplicationVersion.parse("5.1"));
    }

    @Test
    public void versionWithNoBugfixButWithSuffix() {
        assertVersion(5, 1, 0, "-m01", "5.1-m01", ApplicationVersion.parse("5.1-m01"));
    }

    @Test
    public void versionWithMajorOnlyNoSuffix() {
        assertVersion(5, 0, 0, "", "5", ApplicationVersion.parse("5"));
    }

    @Test
    public void versionWithMajorOnlyAndSuffix() {
        assertVersion(5, 0, 0, "-m01", "5-m01", ApplicationVersion.parse("5-m01"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void invalidVersionWithNonDigits() {
        ApplicationVersion.parse("m4.0.1");
    }

    @Test
    public void unexpectedVersionSeparators() {
        assertVersion(5, 0, 0, "_0_4", "5_0_4", ApplicationVersion.parse("5_0_4"));
    }

    @Test
    public void versionEqualsAndHashCode() {
        ApplicationVersion version1 = ApplicationVersion.parse(" 5.0.4 ");
        ApplicationVersion version2 = ApplicationVersion.parse("\t5.0.4\n");

        assertEquals(version1, version2);
        assertEquals(version1.hashCode(), version2.hashCode());
    }

    @Test
    public void versionCompareByNumbers() {
        ApplicationVersion version1 = ApplicationVersion.parse("5.0.3");
        ApplicationVersion version2 = ApplicationVersion.parse("5.0.4-m01");

        assertThat(version2, Matchers.greaterThan(version1));
    }

    @Test
    public void versionCompareBySuffixVsNoSuffix() {
        ApplicationVersion version1 = ApplicationVersion.parse("5.0.3");
        ApplicationVersion version2 = ApplicationVersion.parse("5.0.3-m01");

        assertThat(version2, Matchers.greaterThan(version1));
    }

    @Test
    public void versionCompareBySuffix() {
        ApplicationVersion version1 = ApplicationVersion.parse("5.0.3-m01");
        ApplicationVersion version2 = ApplicationVersion.parse("5.0.3-m02");

        assertThat(version2, Matchers.greaterThan(version1));
    }

    @Test
    public void supportsOsgiVersion() {
        Version osgiVersion = new Version(5, 0, 5, "SNAPSHOT");

        assertVersion(5, 0, 5, ".SNAPSHOT", ApplicationVersion.parse(osgiVersion.toString()));
    }
}
