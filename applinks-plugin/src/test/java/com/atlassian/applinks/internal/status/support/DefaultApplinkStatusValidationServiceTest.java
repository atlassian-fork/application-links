package com.atlassian.applinks.internal.status.support;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.internal.capabilities.DefaultRemoteCapabilities;
import com.atlassian.applinks.internal.capabilities.RemoteCapabilitiesError;
import com.atlassian.applinks.internal.common.capabilities.ApplicationVersion;
import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteApplicationCapabilities;
import com.atlassian.applinks.internal.common.capabilities.RemoteCapabilitiesService;
import com.atlassian.applinks.internal.common.exception.InvalidArgumentException;
import com.atlassian.applinks.internal.common.exception.NoAccessException;
import com.atlassian.applinks.internal.common.exception.NoSuchApplinkException;
import com.atlassian.applinks.internal.common.exception.ServiceException;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.migration.AuthenticationConfig;
import com.atlassian.applinks.internal.migration.AuthenticationMigrationService;
import com.atlassian.applinks.internal.migration.AuthenticationStatus;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.error.SimpleApplinkError;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.support.DefaultApplinkStatusValidationService.StatusError;
import com.atlassian.applinks.test.mock.MockApplink;
import com.atlassian.applinks.test.rule.MockApplinksRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.EnumSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import static com.atlassian.applinks.internal.status.OauthNamingHelper.config;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.defaultConfig;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.disabled;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.impersonation;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.incoming;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.outgoing;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.threeLo;
import static com.atlassian.applinks.test.matchers.status.ApplinkErrorMatchers.withType;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class DefaultApplinkStatusValidationServiceTest {
    @Rule
    public final MockApplinksRule mockApplinksRule = new MockApplinksRule(this);
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();
    @Mock
    private ApplinkCompatibilityVerifier compatibilityVerifier;
    @Mock
    private RemoteCapabilitiesService remoteCapabilitiesService;
    @Mock
    private AuthenticationMigrationService authenticationMigrationService;
    @MockApplink
    private ApplicationLink applicationLink;
    @InjectMocks
    private DefaultApplinkStatusValidationService validationService;

    private static RemoteApplicationCapabilities createValidCapabilities(String applicationVersion, String applinksVersion) {
        return new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse(applicationVersion))
                .applinksVersion(ApplicationVersion.parse(applinksVersion))
                .build();
    }

    private static RemoteApplicationCapabilities createValidCapabilities(String applicationVersion, String applinksVersion, Set<ApplinksCapabilities> capabilities) {
        return new DefaultRemoteCapabilities.Builder()
                .applicationVersion(ApplicationVersion.parse(applicationVersion))
                .applinksVersion(ApplicationVersion.parse(applinksVersion))
                .capabilities(capabilities)
                .build();
    }

    @Test
    public void shouldNotThrowExceptionIfLocallyCompatible() {
        when(compatibilityVerifier.verifyLocalCompatibility(applicationLink)).thenReturn(null);

        validationService.checkLocalCompatibility(applicationLink);
    }

    @Test
    public void localCompatibilityFails() throws Exception {
        expectStatusError(ApplinkErrorType.GENERIC_LINK);

        when(compatibilityVerifier.verifyLocalCompatibility(applicationLink)).thenReturn(ApplinkErrorType.GENERIC_LINK);

        validationService.checkLocalCompatibility(applicationLink);
    }

    @Test
    public void shouldNotRaiseErrorIfCannotGetVersion() throws ServiceException {
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS)).thenReturn(
                new RemoteCapabilitiesError(new SimpleApplinkError(ApplinkErrorType.CONNECTION_REFUSED)));

        validationService.checkVersionCompatibility(applicationLink);
    }

    @Test
    public void shouldNotRaiseErrorIfMajorVersionIsAtLeast4() throws ServiceException {
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS)).thenReturn(
                createValidCapabilities("6.4", "5.0.0"));

        validationService.checkVersionCompatibility(applicationLink);
    }

    @Test
    public void testGetApplinkStatusWhenTooLowVersion() throws ServiceException {
        expectStatusError(ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE);

        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS))
                .thenReturn(createValidCapabilities("6.0.1", "4.0.12"));

        validationService.checkVersionCompatibility(applicationLink);
    }

    @Test
    public void shouldNotThrowExceptionWhenConfigsAreNot3LoOnly() {
        validationService.checkOAuthSupportedCompatibility(config(incoming(defaultConfig()), outgoing(defaultConfig())));
        validationService.checkOAuthSupportedCompatibility(config(incoming(defaultConfig()), outgoing(disabled())));
        validationService.checkOAuthSupportedCompatibility(config(incoming(defaultConfig()), outgoing(impersonation())));

        validationService.checkOAuthSupportedCompatibility(config(incoming(disabled()), outgoing(defaultConfig())));
        validationService.checkOAuthSupportedCompatibility(config(incoming(disabled()), outgoing(disabled())));
        validationService.checkOAuthSupportedCompatibility(config(incoming(disabled()), outgoing(impersonation())));

        validationService.checkOAuthSupportedCompatibility(config(incoming(impersonation()), outgoing(defaultConfig())));
        validationService.checkOAuthSupportedCompatibility(config(incoming(impersonation()), outgoing(disabled())));
        validationService.checkOAuthSupportedCompatibility(config(incoming(impersonation()), outgoing(impersonation())));
    }

    @Test
    public void shouldThrowExceptionWhenIncomingIs3LoOnly() {
        assertAuthLevelUnsupported(incoming(threeLo()), outgoing(defaultConfig()));
        assertAuthLevelUnsupported(incoming(threeLo()), outgoing(disabled()));
        assertAuthLevelUnsupported(incoming(threeLo()), outgoing(impersonation()));
    }

    @Test
    public void shouldThrowExceptionWhenOutgoingIs3LoOnly() {
        assertAuthLevelUnsupported(incoming(defaultConfig()), outgoing(threeLo()));
        assertAuthLevelUnsupported(incoming(disabled()), outgoing(threeLo()));
        assertAuthLevelUnsupported(incoming(impersonation()), outgoing(threeLo()));
    }

    @Test
    public void shouldThrowExceptionWhenLocalAndRemoteLevelsMismatch() {
        expectStatusError(ApplinkErrorType.AUTH_LEVEL_MISMATCH);

        final ApplinkOAuthStatus local = config(incoming(defaultConfig()), outgoing(impersonation()));
        final ApplinkOAuthStatus remote = config(incoming(defaultConfig()), outgoing(impersonation()));

        validationService.checkOAuthMismatch(local, remote);
    }

    @Test
    public void shouldNotThrowExceptionWhenLocalMatchesRemote() {
        final ApplinkOAuthStatus local = config(incoming(impersonation()), outgoing(defaultConfig()));
        final ApplinkOAuthStatus remote = config(incoming(defaultConfig()), outgoing(impersonation()));
        validationService.checkOAuthMismatch(local, remote);
    }

    @Test
    public void shouldThrowExceptionIfBothLocalAndRemoteAreDisabled() {
        assertDisabled(ApplinkOAuthStatus.OFF, ApplinkOAuthStatus.OFF);
        assertDisabled(ApplinkOAuthStatus.OFF, null);
        assertDisabled(null, ApplinkOAuthStatus.OFF);
        assertDisabled(null, null);
    }

    @Test
    public void shouldNotThrowExceptionIfNoLegacyAuthentication() throws NoSuchApplinkException, NoAccessException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.DEFAULT;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.IMPERSONATION;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()), new AuthenticationConfig().oauth(local.getOutgoing()));
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        validationService.checkLegacyAuthentication(applicationLink, local, remote);
    }

    @Test
    public void shouldThrowLegacyUpdateRequiredWithDisabledLocalAndRemoteOAuthAndSysAdminAccess() throws NoSuchApplinkException, NoAccessException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.OFF;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.OFF;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()).trustedConfigured(true),
                new AuthenticationConfig().oauth(local.getOutgoing()).basicConfigured(true).trustedConfigured(true));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(true);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(remote, remote, ApplinkErrorType.LEGACY_UPDATE);
    }

    @Test
    public void shouldThrowLegacyRemovalWithDefaultLocalAndRemoteOAuthAndSysAdminAccess() throws NoSuchApplinkException, NoAccessException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.DEFAULT;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.DEFAULT;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()).trustedConfigured(true),
                new AuthenticationConfig().oauth(local.getOutgoing()).basicConfigured(true));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(true);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(local, remote, ApplinkErrorType.LEGACY_REMOVAL);
    }

    @Test
    public void shouldThrowLegacyRemovalWithDefaultLocalAndRemoteOAuthAndBasicOnlyAndNoSysAdmin() throws NoSuchApplinkException, NoAccessException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.DEFAULT;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.DEFAULT;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()),
                new AuthenticationConfig().oauth(local.getOutgoing()).basicConfigured(true));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(false);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(local, remote, ApplinkErrorType.LEGACY_REMOVAL);
    }

    @Test
    public void shouldThrowManualLegacyUpdateRequiredWithDisabledLocalAndRemoteOAuthAndNoSysAdminAccess() throws NoSuchApplinkException, NoAccessException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.OFF;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.OFF;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()).trustedConfigured(true),
                new AuthenticationConfig().oauth(local.getOutgoing()).trustedConfigured(true));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(false);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(local, remote, ApplinkErrorType.MANUAL_LEGACY_UPDATE);
    }

    @Test
    public void shouldThrowManualLegacyUpdateRequiredWithDisabledOAuthAndNoSysAdminAccessAndEnabledIncomingBasic() throws NoSuchApplinkException, NoAccessException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.OFF;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.OFF;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()).basicConfigured(true),
                new AuthenticationConfig().oauth(local.getOutgoing()));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(false);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(local, remote, ApplinkErrorType.MANUAL_LEGACY_UPDATE);
    }

    @Test
    public void shouldThrowManualLegacyRemovalWithDefaultLocalAndRemoteOAuthAndNoSysAdminAccess() throws NoSuchApplinkException, NoAccessException, InvalidArgumentException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.DEFAULT;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.DEFAULT;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()).basicConfigured(true),
                new AuthenticationConfig().oauth(local.getOutgoing()).trustedConfigured(true));
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS)).thenReturn(createValidCapabilities("6.4", "5.0.0", EnumSet.allOf(ApplinksCapabilities.class)));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(false);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(local, remote, ApplinkErrorType.MANUAL_LEGACY_REMOVAL);
    }

    @Test
    public void shouldThrowManualLegacyRemovalWithOldEditForRemoteWithoutMigrationAPIAndNoSysAdminAccess() throws NoSuchApplinkException, NoAccessException, InvalidArgumentException {
        final ApplinkOAuthStatus local = ApplinkOAuthStatus.DEFAULT;
        final ApplinkOAuthStatus remote = ApplinkOAuthStatus.DEFAULT;
        final AuthenticationStatus status = new AuthenticationStatus(new AuthenticationConfig().oauth(local.getIncoming()).trustedConfigured(true),
                new AuthenticationConfig().oauth(local.getOutgoing()).trustedConfigured(true));
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS)).thenReturn(createValidCapabilities("6.4", "5.0.0"));
        when(authenticationMigrationService.hasRemoteSysAdminAccess(applicationLink)).thenReturn(false);
        when(authenticationMigrationService.getAuthenticationMigrationStatus(applicationLink, local)).thenReturn(status);
        assertLegacyAuthentication(local, remote, ApplinkErrorType.MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT);
    }

    private void assertLegacyAuthentication(final ApplinkOAuthStatus local, final ApplinkOAuthStatus remote, final ApplinkErrorType errorType) throws NoSuchApplinkException, NoAccessException {
        try {
            validationService.checkLegacyAuthentication(applicationLink, local, remote);
        } catch (StatusError e) {
            assertThat(e.getType(), equalTo(errorType));
        }
    }

    public void editableGivenGenericApplinks() throws ServiceException {
        expectStatusError(ApplinkErrorType.GENERIC_LINK);
        when(compatibilityVerifier.verifyLocalCompatibility(applicationLink))
                .thenReturn(ApplinkErrorType.GENERIC_LINK);

        validationService.checkEditable(applicationLink);
    }

    @Test
    public void editableGivenNonVersionIncompatible() throws ServiceException {
        expectStatusError(ApplinkErrorType.REMOTE_VERSION_INCOMPATIBLE);
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS))
                .thenReturn(createValidCapabilities("6.0.1", "4.0.12"));

        validationService.checkEditable(applicationLink);
    }

    @Test
    public void editableGivenNoIncompatibility() throws ServiceException {
        when(compatibilityVerifier.verifyLocalCompatibility(applicationLink)).thenReturn(null);
        when(remoteCapabilitiesService.getCapabilities(applicationLink, 1, TimeUnit.HOURS))
                .thenReturn(createValidCapabilities("6.0.1", "4.0.13"));

        validationService.checkEditable(applicationLink);
    }

    private void assertAuthLevelUnsupported(OAuthConfig incoming, OAuthConfig outgoing) {
        try {
            validationService.checkOAuthSupportedCompatibility(config(incoming, outgoing));
        } catch (StatusError e) {
            assertThat(e.getType(), equalTo(ApplinkErrorType.AUTH_LEVEL_UNSUPPORTED));
        }
    }

    private void assertDisabled(ApplinkOAuthStatus local, ApplinkOAuthStatus remote) {
        try {
            validationService.checkDisabled(local, remote);
        } catch (StatusError e) {
            assertThat(e.getType(), equalTo(ApplinkErrorType.DISABLED));
        }
    }

    private void expectStatusError(ApplinkErrorType errorType) {
        expectedException.expect(StatusError.class);
        expectedException.expect(withType(errorType));
    }
}

