package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.test.matcher.ThrowableMatchers;
import org.hamcrest.Matchers;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class ApplinkErrorsTest {
    @Test
    public void findCauseOfTypeGivenCausePresent() {
        IllegalArgumentException cause = ApplinkErrors.findCauseOfType(
                new Exception(new RuntimeException(new IllegalArgumentException("test"))),
                IllegalArgumentException.class);

        assertNotNull(cause);
        assertThat(cause, Matchers.instanceOf(IllegalArgumentException.class));
    }

    @Test
    public void findCauseOfTypeGivenMultipleCausesOfTargetType() {
        IllegalArgumentException cause = ApplinkErrors.findCauseOfType(
                new Exception(new RuntimeException(new IllegalArgumentException("test", new Exception(new IllegalArgumentException("test2"))))),
                IllegalArgumentException.class);

        assertNotNull(cause);
        // should find the first matching throwable in the causal chain
        assertThat(cause, ThrowableMatchers.withMessage("test"));
    }

    @Test
    public void findCauseOfTypeGivenNoMatches() {
        IllegalArgumentException cause = ApplinkErrors.findCauseOfType(
                new Exception(new RuntimeException(new IllegalStateException(new Exception()))),
                IllegalArgumentException.class);

        assertNull(cause);
    }
}
