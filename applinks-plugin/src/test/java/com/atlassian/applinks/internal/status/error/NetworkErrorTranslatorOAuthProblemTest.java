package com.atlassian.applinks.internal.status.error;

import com.atlassian.applinks.internal.common.auth.oauth.OAuthMessageProblemException;
import com.atlassian.sal.api.net.ResponseException;
import org.junit.Test;

import static com.atlassian.applinks.internal.status.error.NetworkErrorTranslator.toApplinkErrorException;
import static org.junit.Assert.assertEquals;

@SuppressWarnings("ThrowableResultOfMethodCallIgnored")
public class NetworkErrorTranslatorOAuthProblemTest {
    public static final String MESSAGE = "Message";

    @Test
    public void toApplinkErrorFallbackOAuthProblem() throws Exception {
        final ApplinkStatusException remoteStatusException = toApplinkErrorException(
                new ResponseException(
                        new OAuthMessageProblemException("Something different", "some_custom_problem", "some_custom_advice", null)), MESSAGE);

        assertEquals(ApplinkErrorType.OAUTH_PROBLEM, remoteStatusException.getType());
        assertEquals(remoteStatusException.getDetails(), "some_custom_problem");
    }
}
