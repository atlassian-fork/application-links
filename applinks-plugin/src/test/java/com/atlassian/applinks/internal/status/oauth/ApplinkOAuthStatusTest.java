package com.atlassian.applinks.internal.status.oauth;

import org.junit.Test;

import static com.atlassian.applinks.internal.status.OauthNamingHelper.current;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.defaultConfig;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.disabled;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.impersonation;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.incoming;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.other;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.outgoing;
import static com.atlassian.applinks.internal.status.OauthNamingHelper.threeLo;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ApplinkOAuthStatusTest {
    @Test
    public void shouldNotMatchIfIncomingIsNotEqualsOtherOutgoingOrOutgoingIsNotEqualsOtherIncoming() throws Exception {
        assertFalse(current(incoming(disabled()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(defaultConfig()))));

        assertFalse(current(incoming(disabled()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(threeLo()))));

        assertFalse(current(incoming(disabled()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(impersonation()))));

        assertFalse(current(incoming(defaultConfig()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(disabled()))));

        assertFalse(current(incoming(defaultConfig()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(threeLo()))));

        assertFalse(current(incoming(defaultConfig()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(impersonation()))));

        assertFalse(current(incoming(threeLo()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(disabled()))));

        assertFalse(current(incoming(threeLo()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(defaultConfig()))));

        assertFalse(current(incoming(threeLo()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(impersonation()))));

        assertFalse(current(incoming(impersonation()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(disabled()))));

        assertFalse(current(incoming(impersonation()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(defaultConfig()))));

        assertFalse(current(incoming(impersonation()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(threeLo()))));
    }

    @Test
    public void shouldMatchIfIncomingEqualsOtherOutgoingAndOutgoingEqualsOtherIncoming() throws Exception {
        assertTrue(current(incoming(disabled()), outgoing(defaultConfig()))
                .matches(other(incoming(defaultConfig()), outgoing(disabled()))));

        assertTrue(current(incoming(disabled()), outgoing(threeLo()))
                .matches(other(incoming(threeLo()), outgoing(disabled()))));

        assertTrue(current(incoming(disabled()), outgoing(impersonation()))
                .matches(other(incoming(impersonation()), outgoing(disabled()))));
    }

}
