package com.atlassian.applinks.internal.status.remote;

import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseTransportException;
import org.junit.Test;

import java.net.ConnectException;
import java.net.NoRouteToHostException;
import java.net.SocketTimeoutException;

import static com.atlassian.applinks.test.matcher.StringMatchers.containsInOrder;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

public class RemoteStatusUnknownExceptionTest {
    public static final String MESSAGE = "Test";

    @Test
    public void withUnderlyingSocketTimeoutException() {
        RemoteStatusUnknownException exception = new RemoteStatusUnknownException(MESSAGE,
                new ResponseTransportException(MESSAGE, new SocketTimeoutException(MESSAGE)));

        assertEquals(ApplinkErrorType.UNKNOWN, exception.getType());
        assertThat(exception.getDetails(), containsInOrder("java.net.SocketTimeoutException: ", MESSAGE));
    }

    @Test
    public void withUnderlyingNoRouteToHostException() {
        RemoteStatusUnknownException exception = new RemoteStatusUnknownException(MESSAGE,
                new ResponseTransportException(MESSAGE, new NoRouteToHostException(MESSAGE)));

        assertEquals(ApplinkErrorType.UNKNOWN, exception.getType());
        assertThat(exception.getDetails(), containsInOrder("java.net.NoRouteToHostException: ", MESSAGE));
    }

    @Test
    public void withDirectConnectExceptionCause() {
        RemoteStatusUnknownException exception = new RemoteStatusUnknownException(MESSAGE,
                new ConnectException(MESSAGE));

        assertEquals(ApplinkErrorType.UNKNOWN, exception.getType());
        assertThat(exception.getDetails(), containsInOrder("java.net.ConnectException: ", MESSAGE));
    }

    @Test
    public void withNoUnderlyingJavaNetException() {
        RemoteStatusUnknownException exception = new RemoteStatusUnknownException(MESSAGE,
                new ResponseTransportException(MESSAGE,
                        new ResponseException(MESSAGE)));

        assertEquals(ApplinkErrorType.UNKNOWN, exception.getType());
        assertThat(exception.getDetails(),
                containsInOrder("com.atlassian.sal.api.net.ResponseTransportException: ", MESSAGE));
    }

    @Test
    public void withNoCause() {
        RemoteStatusUnknownException exception = new RemoteStatusUnknownException(MESSAGE);

        assertEquals(ApplinkErrorType.UNKNOWN, exception.getType());
        assertNull(exception.getDetails());
    }
}
