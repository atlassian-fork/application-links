package com.atlassian.applinks.ui;

import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;

import java.net.URI;

/**
 * An example of an application type that is not a built in application type, used for testing
 *
 * @since 4.3
 */
public class SampleApplicationType implements NonAppLinksApplicationType {
    final TypeId TYPE_ID = new TypeId("twitter");

    public String getI18nKey() {
        return "applinks.test.twitter";
    }

    public TypeId getId() {
        return TYPE_ID;
    }

    public URI getIconUrl() {
        return null;
    }

}