const path = require("path");
const { HotModuleReplacementPlugin } = require("webpack");
const merge = require("webpack-merge");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const TerserPlugin = require("terser-webpack-plugin");
const WrmPlugin = require("atlassian-webresource-webpack-plugin");
const {
  ENTRYPOINTS,
  I18N_PROPERTY_FILES,
  PLUGIN_KEY,
  XML_OUTPUT_PATH
} = require("./webpack.constants");
const {
  WEBAPP_FRONTEND_PATH,
  P2_FRONTEND_SRC_PATH,
  PLUGIN_RESOURCES_FOLDER,
  P2_FRONTEND_TARGET_PATH
} = require("./path.utils");

const HOSTNAME = "0.0.0.0";
const DEV_SERVER_PORT = 3335;
const output = path.resolve(path.join(P2_FRONTEND_TARGET_PATH, "classes"));

const getCommonConfig = ({ production = false }) => {
  console.log({ production});

  return {
    mode: "development",

    devtool: "cheap-module-source-map",

    module: {
      rules: [
        {
          test: /\.jsx?$/, // matches .js and .jsx
          exclude: /node_modules/,
          use: [
            {
              loader: "@atlassian/i18n-properties-loader",
              options: {
                i18nFiles: I18N_PROPERTY_FILES
              }
            },

            {
              loader: "babel-loader",
              options: {
                cacheDirectory: true
              }
            }
          ]
        },
        {
          test: /\.less$/,
          use: [
            {
              loader: production ? MiniCssExtractPlugin.loader : "style-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "css-loader",
              options: {
                sourceMap: true
              }
            },
            {
              loader: "less-loader",
              options: {
                sourceMap: true,
                paths: [WEBAPP_FRONTEND_PATH]
              }
            }
          ]
        },
        {
          test: /\.(png|jpg|gif|svg)$/,
          use: [
            {
              loader: "file-loader",
              options: {}
            }
          ]
        }
      ]
    },

    resolve: {
      extensions: [".js", ".jsx"],
      modules: ["node_modules", P2_FRONTEND_SRC_PATH]
    },

    output: {
      filename: "[name].bundle.js",
      path: output
    },

    plugins: [
      new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[name].bundle.css"
      })
    ],

    optimization: {
      minimize: false,
      minimizer: [
        new TerserPlugin({
          cache: true,
          extractComments: "all",
          sourceMap: true,
          terserOptions: {
            mangle: {
              // Don't mangle usage of I18n.getText() from internal react-i18n module
              reserved: ["I18n", "getText"]
            },
            compress: {
              // Removes console.debug() calls from the bundle
              pure_funcs: ["console.debug"]
            }
          }
        })
      ],
      splitChunks: {
        minSize: 0,
        chunks: "all",
        maxInitialRequests: Infinity
      }
    }
  };
};

function getEntryPoints() {
  return ENTRYPOINTS.reduce((entrypoints, entry) => {
    entrypoints[entry.key] = path.join(PLUGIN_RESOURCES_FOLDER, entry.path);

    return entrypoints;
  }, {});
}

function getWatchPrepareConfig() {
  const common = getCommonConfig({ production: false });

  return merge(common, {
    output: {
      publicPath: `http://${HOSTNAME}:${DEV_SERVER_PORT}/`,
      filename: "[name].js",
      chunkFilename: "[name].chunk.js"
    },

    devtool: "cheap-module-source-map",

    optimization: {
      concatenateModules: false,
      minimize: false,
      runtimeChunk: false,
      splitChunks: false
    }
  });
}

function getWatchConfig() {
  const watchPrepare = getWatchPrepareConfig();

  return merge(watchPrepare, {
    devServer: {
      overlay: true,
      port: DEV_SERVER_PORT,
      host: HOSTNAME,
      disableHostCheck: true,
      hot: true,
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Access-Control-Allow-Headers": "x-atlassian-mau-ignore"
      },
      stats: {
        colors: true
      }
    },

    plugins: [new HotModuleReplacementPlugin()]
  });
}

function getProductionConfig() {
  const common = getCommonConfig({ production: true });

  return merge(common, {
    mode: "production",
    devtool: "source-map"
  });
}

function getEnvConfig(env) {
  let config;

  switch (env) {
    case "watch:prepare":
      config = getWatchPrepareConfig();
      break;
    case "watch":
      config = getWatchConfig();
      break;
    default:
      config = getProductionConfig();
      break;
  }

  return config;
}

module.exports = async env => {
  const config = getEnvConfig(env);

  return merge(config, {
    entry: getEntryPoints(),

    plugins: [
      new WrmPlugin({
        pluginKey: PLUGIN_KEY,
        xmlDescriptors: path.join(XML_OUTPUT_PATH, "bundles.xml"),

        providedDependencies: {
          'wrm/format': {
            dependency: 'com.atlassian.plugins.atlassian-plugins-webresource-plugin:format',
            import: {
              var: 'require("wrm/format")',
              amd: 'wrm/format',
            }
          },
          'applinks/common/i18n': {
            dependency: 'com.atlassian.applinks.applinks-plugin:applinks-common',
            import: {
              var: 'require("applinks/common/i18n")',
              amd: 'applinks/common/i18n',
            }
          },
          'applinks/common/help-paths': {
            dependency: 'com.atlassian.applinks.applinks-plugin:applinks-common',
            import: {
              var: 'require("applinks/common/help-paths")',
              amd: 'applinks/common/help-paths',
            }
          },
        },

        contextMap: ENTRYPOINTS.reduce((contextMap, entry) => {
          if (entry.context && entry.context.length) {
            contextMap[entry.key] = entry.context;
          }

          return contextMap;
        }, {}),

        transformationMap: {
          js: ["jsI18n"]
        },
        watch: env === "watch" || env === "watch:prepare",
        watchPrepare: env === "watch:prepare"
      })
    ],

    optimization: {
      runtimeChunk: 'single'
    }
  });
};
