package com.atlassian.applinks.test.rest.specification;

import com.atlassian.applinks.internal.common.rest.model.status.RestOAuthConfig;
import com.atlassian.applinks.internal.common.status.oauth.OAuthConfig;
import com.atlassian.applinks.internal.rest.model.status.RestApplinkOAuthStatus;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.jayway.restassured.RestAssured;
import com.jayway.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.specification.RestExpectations.jsonPath;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Specifications for expectations of results of Applinks OAuth status requests.
 *
 * @since 4.3
 */
public final class ApplinksOAuthStatusExpectations {
    private ApplinksOAuthStatusExpectations() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    /**
     * Expect OAuth status matching {@code expectedStatus}. Assumes OAuth status is the root object.
     *
     * @param expectedStatus expected OAuth status
     * @return expectation to use while making request to the Applinks status resource
     */
    @Nonnull
    public static ResponseSpecification expectOAuthStatus(@Nonnull ApplinkOAuthStatus expectedStatus) {
        return RestExpectations.allOf(
                expectIncomingOAuthStatus(expectedStatus.getIncoming()),
                expectOutgoingOAuthStatus(expectedStatus.getOutgoing())
        );
    }

    /**
     * Expect OAuth status matching {@code expectedStatus}.
     *
     * @param path           JSON path to the OAuth status object
     * @param expectedStatus expected OAuth status
     * @return expectation to use while making request to the Applinks status resource
     */
    @Nonnull
    public static ResponseSpecification expectOAuthStatus(@Nullable String path,
                                                          @Nonnull ApplinkOAuthStatus expectedStatus) {
        return RestExpectations.allOf(
                expectIncomingOAuthStatus(path, expectedStatus.getIncoming()),
                expectOutgoingOAuthStatus(path, expectedStatus.getOutgoing())
        );
    }

    /**
     * Expect incoming OAuth config to be in a state as described by {@code expectedConfig}. Assumes OAuth status is the
     * root object
     *
     * @param expectedConfig expected incoming OAuth config
     * @return expectation to use while making request to the Applinks status resource
     */
    @Nonnull
    public static ResponseSpecification expectIncomingOAuthStatus(@Nonnull OAuthConfig expectedConfig) {
        return expectIncomingOAuthStatus("", expectedConfig);
    }

    /**
     * Expect incoming OAuth config to be in a state as described by {@code expectedConfig}.
     *
     * @param path           JSON path to the OAuth status object
     * @param expectedConfig expected incoming OAuth config
     * @return expectation to use while making request to the Applinks status resource
     */
    @Nonnull
    public static ResponseSpecification expectIncomingOAuthStatus(@Nullable String path,
                                                                  @Nonnull OAuthConfig expectedConfig) {
        return expectConfig(jsonPath(path, RestApplinkOAuthStatus.INCOMING), expectedConfig);
    }

    /**
     * Expect outgoing OAuth config to be in a state as described by {@code expectedConfig}. Assumes OAuth status is the
     * root object.
     *
     * @param expectedConfig expected outgoing OAuth config
     * @return expectation to use while making request to the Applinks status resource
     */
    @Nonnull
    public static ResponseSpecification expectOutgoingOAuthStatus(@Nonnull OAuthConfig expectedConfig) {
        return expectOutgoingOAuthStatus("", expectedConfig);
    }

    /**
     * Expect outgoing OAuth config to be in a state as described by {@code expectedConfig}.
     *
     * @param path           JSON path to the OAuth status object
     * @param expectedConfig expected outgoing OAuth config
     * @return expectation to use while making request to the Applinks status resource
     */
    @Nonnull
    public static ResponseSpecification expectOutgoingOAuthStatus(@Nullable String path, @Nonnull OAuthConfig expectedConfig) {
        return expectConfig(jsonPath(path, RestApplinkOAuthStatus.OUTGOING), expectedConfig);
    }

    private static ResponseSpecification expectConfig(String prefix, OAuthConfig expectedConfig) {
        requireNonNull(expectedConfig, "expectedConfig");
        return RestAssured.expect()
                .body(prefix, notNullValue())
                .body(prefix + "." + RestOAuthConfig.ENABLED, Matchers.is(expectedConfig.isEnabled()))
                .body(prefix + "." + RestOAuthConfig.TWO_LO_ENABLED, Matchers.is(expectedConfig.isTwoLoEnabled()))
                .body(prefix + "." + RestOAuthConfig.TWO_LO_IMPERSONATION_ENABLED, Matchers.is(expectedConfig.isTwoLoImpersonationEnabled()));
    }
}
