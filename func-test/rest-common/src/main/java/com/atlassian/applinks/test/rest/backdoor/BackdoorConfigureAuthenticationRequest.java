package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.spi.auth.AuthenticationScenario;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.model.RestAuthenticationConfiguration;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

public class BackdoorConfigureAuthenticationRequest extends AbstractBackdoorRestRequest {
    private final String applinkId;
    private final TestAuthentication remoteAuthentication;
    private final AuthenticationScenario authenticationScenario;

    private BackdoorConfigureAuthenticationRequest(Builder builder) {
        super(builder);
        this.applinkId = builder.applinkId;
        this.remoteAuthentication = builder.remoteAuthentication;
        this.authenticationScenario = builder.authenticationScenario;
    }

    @Nullable
    @Override
    @SuppressWarnings("ConstantConditions")
    protected Object getBody() {
        return new RestAuthenticationConfiguration(authenticationScenario, remoteAuthentication.getUsername(),
                remoteAuthentication.getPassword());
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(applinkId);
    }

    public static class Builder extends AbstractBackdoorBuilder<Builder, BackdoorConfigureAuthenticationRequest> {
        private final String applinkId;

        private TestAuthentication remoteAuthentication = TestAuthentication.admin();
        private AuthenticationScenario authenticationScenario;

        public Builder(@Nonnull String applinkId) {
            this.applinkId = requireNonNull(applinkId, "applinkId");
        }

        public Builder(TestApplink.Side side) {
            this(side.id());
        }

        @Nonnull
        @SuppressWarnings("ConstantConditions")
        public Builder remoteAuthentication(@Nonnull TestAuthentication remoteAuthentication) {
            requireNonNull(remoteAuthentication, "remoteAuthentication");
            this.remoteAuthentication = remoteAuthentication;

            // for DELETE
            queryParam(RestAuthenticationConfiguration.USERNAME, remoteAuthentication.getUsername());
            queryParam(RestAuthenticationConfiguration.PASSWORD, remoteAuthentication.getPassword());

            return this;
        }

        @Nonnull
        public Builder authenticationScenario(@Nullable AuthenticationScenario authenticationScenario) {
            this.authenticationScenario = authenticationScenario;
            return this;
        }

        @Nonnull
        public Builder authenticationScenario(final boolean trusted, final boolean sharedUsers) {
            return authenticationScenario(new AuthenticationScenario() {
                @Override
                public boolean isCommonUserBase() {
                    return sharedUsers;
                }

                @Override
                public boolean isTrusted() {
                    return trusted;
                }
            });
        }

        @Nonnull
        @Override
        public BackdoorConfigureAuthenticationRequest build() {
            requireNonNull(remoteAuthentication, "remoteAuthentication");
            return new BackdoorConfigureAuthenticationRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
