package com.atlassian.applinks.test.rest.oauth;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.jayway.restassured.response.Response;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrlsFactory.forOAuthRestModule;

public class ConsumerTokenRestTester extends BaseRestTester {
    public ConsumerTokenRestTester(@Nonnull TestedInstance instance) {
        super(forOAuthRestModule(instance).consumerToken());
    }

    public ConsumerTokenRestTester(@Nonnull TestedInstance instance, @Nullable TestAuthentication defaultAuthentication) {
        super(forOAuthRestModule(instance).consumerToken(), defaultAuthentication);
    }

    @Nonnull
    public Response delete(@Nonnull ConsumerTokenRequest request) {
        return super.delete(request);
    }

}
