package com.atlassian.applinks.test.rest.url;

import com.atlassian.applinks.internal.rest.RestUrl;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public final class ApplinksStatusRestUrls {
    public static final String PATH_OAUTH = "oauth";

    private final RestUrl baseUrl;

    ApplinksStatusRestUrls(@Nonnull RestUrl baseUrl) {
        this.baseUrl = requireNonNull(baseUrl, "baseUrl");
    }

    @Nonnull
    public RestUrl root() {
        return baseUrl;
    }

    @Nonnull
    public RestUrl oAuth() {
        return baseUrl.add(PATH_OAUTH);
    }
}
