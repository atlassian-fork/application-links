package com.atlassian.applinks.test.rest.backdoor;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.SimpleRestRequest;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.forRestModule;
import static java.util.Objects.requireNonNull;

/**
 * Backdoor to access the {@code sal} resource in the {@code applinks-tests} backdoor plugin. This can be used
 * to interact with SAL APIs for test purposes.
 * <p>
 * NOTE: this will only work against applications running the {@code applinks-tests} plugin.
 *
 * @since 4.3
 */
public class SalBackdoor {
    public static final String RESOURCE_PATH = "sal";

    private final BaseRestTester salRestTester;

    public SalBackdoor(@Nonnull String baseUrl) {
        // no need for authentication, the resource allows anonymous access
        this.salRestTester = new BaseRestTester(getRestPath(baseUrl));
    }

    public SalBackdoor(final TestedInstance productInstance) {
        this.salRestTester = new BaseRestTester(getRestPath(productInstance.getBaseUrl()));
    }

    public void cleanUpUserSettings(@Nonnull TestAuthentication authentication) {
        requireNonNull(authentication, "authentication");
        salRestTester.delete(new UserSettingsRequest(authentication));
    }

    private static RestUrl getRestPath(@Nonnull String baseUrl) {
        return forRestModule(baseUrl, ApplinksRestUrls.RestModules.APPLINKS_TESTS).path(RESOURCE_PATH);
    }

    private static class UserSettingsRequest extends SimpleRestRequest {
        UserSettingsRequest(TestAuthentication authentication) {
            super(new SimpleRestRequest.Builder().expectStatus(Response.Status.NO_CONTENT)
                    .authentication(authentication));
        }

        @Nonnull
        @Override
        protected RestUrl getPath() {
            return RestUrl.forPath("user").add("settings");
        }
    }
}
