package com.atlassian.applinks.test.rest.version;

import com.atlassian.applinks.test.rest.AbstractRestRequest;
import com.atlassian.applinks.internal.rest.RestUrl;

import javax.annotation.Nonnull;

public class VersionRestRequest extends AbstractRestRequest {
    private final String applicationId;

    protected VersionRestRequest(final Builder builder) {
        super(builder);
        this.applicationId = builder.applicationId;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.forPath(applicationId);
    }

    public static class Builder extends AbstractBuilder<Builder, VersionRestRequest> {
        private String applicationId;

        public Builder(@Nonnull String applicationId) {
            this.applicationId = applicationId;
        }

        @Nonnull
        @Override
        public VersionRestRequest build() {
            return new VersionRestRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
