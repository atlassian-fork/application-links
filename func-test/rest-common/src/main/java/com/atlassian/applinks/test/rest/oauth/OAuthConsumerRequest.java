package com.atlassian.applinks.test.rest.oauth;

import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.rest.AbstractApplinkRequest;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static java.util.Objects.requireNonNull;

public class OAuthConsumerRequest extends AbstractApplinkRequest {
    static final String PARAM_AUTO_CONFIGURE = "autoConfigure";

    static final String PATH_AUTHENTICATION = "authentication";
    static final String PATH_CONSUMER = "consumer";

    private final RestConsumer consumer;

    public OAuthConsumerRequest(@Nonnull Builder builder) {
        super(builder);
        this.consumer = builder.consumer;
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return super.getPath().add(PATH_AUTHENTICATION).add(PATH_CONSUMER);
    }

    @Nullable
    @Override
    protected Object getBody() {
        return consumer;
    }

    public static class Builder extends AbstractApplinkRequestBuilder<Builder, OAuthConsumerRequest> {
        private RestConsumer consumer;

        public Builder(@Nonnull String applinkId) {
            super(applinkId);
        }

        public Builder(@Nonnull TestApplink.Side applink) {
            this(applink.id());
        }

        @Nonnull
        public Builder consumer(@Nonnull RestConsumer consumer) {
            this.consumer = requireNonNull(consumer, "consumer");
            return this;
        }

        @Nonnull
        public Builder autoConfigure(boolean autoConfigure) {
            return queryParam(PARAM_AUTO_CONFIGURE, autoConfigure);
        }

        @Nonnull
        @Override
        public OAuthConsumerRequest build() {
            return new OAuthConsumerRequest(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
