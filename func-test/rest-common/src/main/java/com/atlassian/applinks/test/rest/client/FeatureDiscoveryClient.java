package com.atlassian.applinks.test.rest.client;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.feature.FeatureDiscoveryRequest;
import com.atlassian.applinks.test.rest.feature.FeatureDiscoveryRestTester;
import com.atlassian.applinks.test.rest.url.ApplinksRestUrls;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.test.rest.specification.FeatureDiscoveryExpectations.expectSingleDiscoveredFeature;
import static javax.ws.rs.core.Response.Status.OK;

/**
 * @since 5.0
 */
public class FeatureDiscoveryClient {
    private final FeatureDiscoveryRestTester featureDiscoveryRestTester;

    public FeatureDiscoveryClient(@Nonnull TestedInstance instance) {
        featureDiscoveryRestTester = new FeatureDiscoveryRestTester(ApplinksRestUrls.forDefaultRestModule(instance.getBaseUrl()));
    }

    public void discoverFeature(String featureKey) {
        featureDiscoveryRestTester.put(new FeatureDiscoveryRequest.Builder(featureKey)
                .authentication(TestAuthentication.admin())
                .expectStatus(OK)
                .specification(expectSingleDiscoveredFeature(featureKey))
                .build());
    }

}
