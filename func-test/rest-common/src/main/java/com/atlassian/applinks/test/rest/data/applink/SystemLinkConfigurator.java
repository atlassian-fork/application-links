package com.atlassian.applinks.test.rest.data.applink;

import com.atlassian.applinks.test.data.applink.TestApplink;
import com.atlassian.applinks.test.data.applink.config.AbstractTestApplinkConfigurator;
import com.atlassian.applinks.test.rest.backdoor.ApplinksBackdoor;

import javax.annotation.Nonnull;

/**
 * @since 4.3
 */
public class SystemLinkConfigurator extends AbstractTestApplinkConfigurator {
    @Nonnull
    public static SystemLinkConfigurator setSystemLink() {
        return new SystemLinkConfigurator(true);
    }

    @Nonnull
    public static SystemLinkConfigurator setNonSystemLink() {
        return new SystemLinkConfigurator(false);
    }

    private final boolean system;

    private SystemLinkConfigurator(boolean system) {
        this.system = system;
    }

    public void configureSide(@Nonnull TestApplink.Side side) {
        new ApplinksBackdoor(side.product()).setSystem(side, system);
    }
}
