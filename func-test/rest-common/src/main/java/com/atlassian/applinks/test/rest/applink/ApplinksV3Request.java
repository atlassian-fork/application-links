package com.atlassian.applinks.test.rest.applink;

import com.atlassian.applinks.internal.rest.RestUrl;

import javax.annotation.Nonnull;

/**
 * V3 request that is not related to a particular applink (e.g. to get a list of applinks).
 *
 * @since 5.0
 */
public class ApplinksV3Request extends AbstractApplinksV3Request {
    private ApplinksV3Request(Builder builder) {
        super(builder);
    }

    @Nonnull
    @Override
    protected RestUrl getPath() {
        return RestUrl.EMPTY;
    }

    public static final class Builder extends AbstractApplinksV3Builder<Builder, ApplinksV3Request> {
        @Nonnull
        @Override
        public ApplinksV3Request build() {
            return new ApplinksV3Request(this);
        }

        @Nonnull
        @Override
        protected Builder self() {
            return this;
        }
    }
}
