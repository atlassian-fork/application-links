package com.atlassian.applinks.test.response;

import com.google.common.collect.ImmutableListMultimap;
import com.google.common.collect.Multimap;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.ws.rs.core.Response;
import java.util.Collection;
import java.util.Map;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public class MockResponseDefinition {
    private final int statusCode;
    private final String contentType;
    private final String body;
    private final Multimap<String, String> headers;

    private MockResponseDefinition(Builder builder) {
        this.statusCode = builder.statusCode;
        this.body = builder.body;
        this.contentType = builder.contentType;
        this.headers = builder.headers.build();
    }

    public int getStatusCode() {
        return statusCode;
    }

    @Nullable
    public String getBody() {
        return body;
    }

    @Nullable
    public String getContentType() {
        return contentType;
    }

    @Nonnull
    public Multimap<String, String> getHeaders() {
        return headers;
    }

    public static class Builder {
        private final int statusCode;
        private String body = "";
        private String contentType;
        private final ImmutableListMultimap.Builder<String, String> headers = ImmutableListMultimap.builder();

        public Builder(@Nonnull Response.Status status) {
            this(requireNonNull(status, "status").getStatusCode());
        }

        public Builder(int statusCode) {
            this.statusCode = statusCode;
        }

        @Nonnull
        public Builder body(@Nonnull String body, @Nonnull String contentType) {
            this.body = requireNonNull(body, "body");
            this.contentType = requireNonNull(contentType, "contentType");
            return this;
        }

        @Nonnull
        public Builder header(@Nonnull String name, @Nonnull String value) {
            requireNonNull(name, "name");
            requireNonNull(value, "value");

            this.headers.put(name, value);
            return this;
        }

        @Nonnull
        public Builder headers(@Nonnull Map<String, Collection<String>> headers) {
            requireNonNull(headers, "headers");

            for (Map.Entry<String, Collection<String>> headerEntry : headers.entrySet()) {
                this.headers.putAll(headerEntry.getKey(), headerEntry.getValue());
            }

            return this;
        }

        @Nonnull
        public MockResponseDefinition build() {
            return new MockResponseDefinition(this);
        }
    }
}
