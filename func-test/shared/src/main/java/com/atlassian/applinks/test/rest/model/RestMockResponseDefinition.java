package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;
import com.atlassian.applinks.internal.rest.model.IllegalRestRepresentationStateException;
import com.atlassian.applinks.internal.rest.model.RestRepresentation;
import com.atlassian.applinks.test.response.MockResponseDefinition;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class RestMockResponseDefinition extends BaseRestEntity implements RestRepresentation<MockResponseDefinition> {
    public static final String STATUS = "status";
    public static final String CONTENT_TYPE = "contentType";
    public static final String BODY = "body";
    public static final String HEADERS = "headers";

    @SuppressWarnings("unused") // for deserialization
    public RestMockResponseDefinition() {
    }

    public RestMockResponseDefinition(@Nonnull MockResponseDefinition responseDefinition) {
        requireNonNull(responseDefinition, "responseDefinition");
        put(STATUS, responseDefinition.getStatusCode());
        put(HEADERS, responseDefinition.getHeaders().asMap());
        putIfNotNull(BODY, responseDefinition.getBody());
        putIfNotNull(CONTENT_TYPE, responseDefinition.getContentType());
    }

    @SuppressWarnings("unused") // RestRepresentation
    public RestMockResponseDefinition(@Nonnull Map<String, Object> entity) {
        super(entity);
    }

    @Nonnull
    @Override
    @SuppressWarnings("unchecked")
    public MockResponseDefinition asDomain() throws IllegalRestRepresentationStateException {
        MockResponseDefinition.Builder builder = new MockResponseDefinition.Builder(getRequiredInt(STATUS));
        if (hasBody()) {
            builder.body(getRequiredString(BODY), getRequiredString(CONTENT_TYPE));
        }
        if (containsKey(HEADERS)) {
            Map<String, Collection<String>> headers = (Map) requiredValue(HEADERS, get(HEADERS));
            builder.headers(headers);
        }
        return builder.build();
    }

    public boolean hasBody() {
        return containsKey(BODY) && containsKey(CONTENT_TYPE);
    }
}
