package com.atlassian.applinks.test.rest.model;

import com.atlassian.applinks.internal.rest.model.BaseRestEntity;

/**
 * @since 5.2
 */
public class RestBasicStatus extends BaseRestEntity {
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String ENABLED = "enabled";

    @SuppressWarnings("unused") // for deserialization
    public RestBasicStatus() {
    }

    public RestBasicStatus(String username, String password, boolean enabled) {
        put(USERNAME, username);
        put(PASSWORD, password);
        put(ENABLED, enabled);
    }

    public RestBasicStatus(String username, String password) {
        put(USERNAME, username);
        put(PASSWORD, password);
        put(ENABLED, false);
    }

    public String getUsername() {
        return getString(USERNAME);
    }

    public String getPassword() {
        return getString(PASSWORD);
    }

    public boolean getEnabled() {
        return getBooleanValue(ENABLED);
    }

}
