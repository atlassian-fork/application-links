package com.atlassian.applinks.test.data.util;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

/**
 * @since 5.0
 */
public final class TestUrls {
    private TestUrls() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static URI createRandomUri() {
        return URI.create("http://" + UUID.randomUUID().toString() + ".com");
    }

    @Nonnull
    public static String createRandomUrl() {
        return createRandomUri().toString();
    }

    @Nonnull
    public static String withRandomPort(@Nonnull String originalUrl) {
        requireNonNull(originalUrl, "originalUrl");
        try {
            URI uri = URI.create(originalUrl);
            return new URI(uri.getScheme(), uri.getUserInfo(), uri.getHost(), findFreePort(), uri.getPath(),
                    uri.getQuery(), uri.getFragment()).toString();
        } catch (URISyntaxException e) {
            throw new IllegalArgumentException("Unable to create an URI with a swapped port", e);
        }
    }

    private static int findFreePort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        } catch (IOException e) {
            throw new IllegalStateException("Could not find free port");
        }
    }
}
