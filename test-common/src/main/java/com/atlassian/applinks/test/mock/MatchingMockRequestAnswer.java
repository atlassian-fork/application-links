package com.atlassian.applinks.test.mock;

import com.atlassian.sal.api.net.Request;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.hamcrest.Matcher;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withMethodType;
import static com.atlassian.applinks.test.matcher.MockRequestMatchers.withUrlThat;
import static com.atlassian.applinks.test.mock.MockRequestAnswer.newMockRequest;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

/**
 * Mockito answer that allows to set up response objects matching specific requested URLs and method types. Unmatched URLs will result in
 * Assertion error.
 *
 * @see MockApplicationLinkRequest
 * @see MockApplicationLinkResponse
 * @since 5.0
 */
public class MatchingMockRequestAnswer implements Answer<MockApplicationLinkRequest> {
    private final Map<Matcher<MockApplicationLinkRequest>, MockApplicationLinkResponse> responses = new HashMap<>();
    private final List<MockApplicationLinkRequest> executedRequests = Lists.newArrayList();

    @Override
    public MockApplicationLinkRequest answer(InvocationOnMock invocation) {
        MockApplicationLinkRequest newRequest = newMockRequest(invocation);
        MockApplicationLinkResponse response = findMatchingResponse(newRequest);

        newRequest.setResponse(response);
        executedRequests.add(newRequest);

        return newRequest;
    }

    @Nonnull
    public MatchingMockRequestAnswer addResponse(@Nonnull Request.MethodType expectedMethodType,
                                                 @Nonnull Matcher<String> urlMatcher,
                                                 @Nonnull MockApplicationLinkResponse response) {
        requireNonNull(expectedMethodType, "expectedMethodType");
        requireNonNull(urlMatcher, "urlMatcher");
        requireNonNull(response, "response");

        responses.put(allOf(withMethodType(expectedMethodType), withUrlThat(urlMatcher)), response);
        return this;
    }

    @Nonnull
    public MatchingMockRequestAnswer addResponse(@Nonnull Request.MethodType expectedMethodType,
                                                 @Nullable String expectedUrl,
                                                 @Nonnull MockApplicationLinkResponse response) {
        return addResponse(expectedMethodType, is(expectedUrl), response);
    }

    @Nonnull
    public List<MockApplicationLinkRequest> executedRequests() {
        return ImmutableList.copyOf(executedRequests);
    }

    private MockApplicationLinkResponse findMatchingResponse(MockApplicationLinkRequest request) {
        for (Map.Entry<Matcher<MockApplicationLinkRequest>, MockApplicationLinkResponse> matcherEntry : responses.entrySet()) {
            if (matcherEntry.getKey().matches(request)) {
                return matcherEntry.getValue();
            }
        }
        throw new AssertionError(format("Response for %s:%s not found", request.getMethodType(), request.getUrl()));
    }
}
