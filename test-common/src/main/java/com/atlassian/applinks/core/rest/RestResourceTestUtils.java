package com.atlassian.applinks.core.rest;

import com.atlassian.applinks.api.ApplicationType;
import com.atlassian.applinks.spi.application.IconizedType;
import com.atlassian.applinks.spi.application.IdentifiableType;
import com.atlassian.applinks.spi.application.TypeId;

import java.net.URI;
import javax.annotation.Nonnull;

public final class RestResourceTestUtils {
    private RestResourceTestUtils() {
    }

    private static final ApplicationType DUMMY_TYPE = new DummyApplicationType();

    public static ApplicationType getDummyApplicationType() {
        return DUMMY_TYPE;
    }

    static class DummyApplicationType implements ApplicationType, IdentifiableType, IconizedType {
        @Nonnull
        public String getI18nKey() {
            return "some.dummy.i18n.key";
        }

        public URI getIconUrl() {
            return URI.create("http://localhost");
        }

        @Override
        public URI getIconUri() {
            return URI.create("http://localhost");
        }

        @Nonnull
        public TypeId getId() {
            return new TypeId("dummy");
        }
    }

    ;
}
