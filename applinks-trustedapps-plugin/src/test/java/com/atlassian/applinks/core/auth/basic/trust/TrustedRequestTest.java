package com.atlassian.applinks.core.auth.basic.trust;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkResponseHandler;
import com.atlassian.applinks.trusted.auth.TrustedApplinksResponseHandler;
import com.atlassian.applinks.trusted.auth.TrustedApplinksReturningResponseHandler;
import com.atlassian.applinks.trusted.auth.TrustedRequest;
import com.atlassian.applinks.trusted.auth.TrustedResponseHandler;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ResponseHandler;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import com.atlassian.security.auth.trustedapps.CurrentApplication;
import com.atlassian.security.auth.trustedapps.DefaultEncryptedCertificate;
import com.atlassian.security.auth.trustedapps.EncryptedCertificate;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Tests for the trusted request object.  This will not be exhaustive
 * as the inter-dependencies are complex and exercised in an integration style by the applinks-tests project.
 */
public class TrustedRequestTest {
    private String url = "http://www.foo.com";
    private ApplicationLinkRequest wrappedRequest;
    private ApplicationId applicationId;
    private CurrentApplication currentApplication;
    private String username = "admin";

    @Before
    public void createMocks() {
        wrappedRequest = mock(ApplicationLinkRequest.class);
        applicationId = mock(ApplicationId.class);
        currentApplication = mock(CurrentApplication.class);
    }

    @Test
    public void willSignUnsignedRequest() {
        EncryptedCertificate cert = new DefaultEncryptedCertificate("id", "key", "cert");
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setHeader(any(), any())).thenReturn(wrappedRequest);
        when(currentApplication.encode("admin", url)).thenReturn(cert);
        TrustedRequest request = createRequest();
        request.signRequest();

        verify(wrappedRequest).setFollowRedirects(false);
        verify(wrappedRequest, times(4)).setHeader(any(), any());
        verify(currentApplication).encode("admin", url);
    }

    @Test
    public void willCreateUnsignedRequest() {
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);

        TrustedRequest request = createRequest();
        com.atlassian.security.auth.trustedapps.request.TrustedRequest unsignedRequest = request.unsignedRequest();
        Assert.assertNotNull(unsignedRequest);
    }

    @Test
    public void willExecuteRequest() {
        configureMocksForSigning();
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.execute();
            verify(wrappedRequest).executeAndReturn(any(ReturningResponseHandler.class));
        } catch (ResponseException e) {
            Assert.fail("Response exception should not be thrown.");
        }
    }

    @Test
    public void willExecuteRequestWithApplicationLinkResponseHandler() {
        configureMocksForSigning();
        ApplicationLinkResponseHandler handler = mock(ApplicationLinkResponseHandler.class);
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.execute(handler);
            ArgumentCaptor<ReturningResponseHandler> captor = ArgumentCaptor.forClass(ReturningResponseHandler.class);
            verify(wrappedRequest).executeAndReturn(captor.capture());
            assertTrue(captor.getValue() instanceof TrustedApplinksResponseHandler);
        } catch (ResponseException e) {
            Assert.fail("Response exception should not be thrown.");
        }
    }

    @Test
    public void willExecuteRequestWithTrustedApplinksResponseHandler() {
        configureMocksForSigning();
        ApplicationLinkResponseHandler handler = mock(TrustedApplinksResponseHandler.class);
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.execute(handler);
            verify(wrappedRequest).executeAndReturn(handler);
        } catch (ResponseException e) {
            fail("Response exception should not be thrown.");
        }
    }

    @Test
    public void willExecuteRequestWithReturningResponseHandler() {
        configureMocksForSigning();
        ReturningResponseHandler handler = mock(ReturningResponseHandler.class);
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.executeAndReturn(handler);
            verify(wrappedRequest).executeAndReturn(any(ReturningResponseHandler.class));
        } catch (ResponseException e) {
            fail("Response exception should not be thrown.");
        }
    }

    @Test
    public void willExecuteRequestWithTrustedApplinksReturningResponseHandler() {
        configureMocksForSigning();
        ReturningResponseHandler handler = mock(TrustedApplinksReturningResponseHandler.class);
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.executeAndReturn(handler);
            verify(wrappedRequest).executeAndReturn(handler);
        } catch (ResponseException e) {
            fail("Response exception should not be thrown.");
        }
    }

    @Test
    public void willExecuteRequestWithResponseHandler() {
        configureMocksForSigning();
        ResponseHandler handler = mock(ResponseHandler.class);
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.execute(handler);
            verify(wrappedRequest).execute(any(ResponseHandler.class));
        } catch (ResponseException e) {
            fail("Response exception should not be thrown.");
        }
    }

    @Test
    public void willExecuteRequestWithTrustedResponseHandler() {
        configureMocksForSigning();
        ResponseHandler handler = mock(TrustedResponseHandler.class);
        TrustedRequest request = createRequest();
        try {
            when(wrappedRequest.execute()).thenReturn("response");
            request.execute(handler);
            verify(wrappedRequest).execute(handler);
        } catch (ResponseException e) {
            Assert.fail("Response exception should not be thrown.");
        }
    }

    private void configureMocksForSigning() {
        EncryptedCertificate cert = new DefaultEncryptedCertificate("id", "key", "cert");
        when(wrappedRequest.setFollowRedirects(false)).thenReturn(wrappedRequest);
        when(wrappedRequest.setHeader(any(), any())).thenReturn(wrappedRequest);
        when(currentApplication.encode("admin", url)).thenReturn(cert);
    }


    private TrustedRequest createRequest() {
        return new TrustedRequest(
                url, wrappedRequest, currentApplication, username);
    }
}
