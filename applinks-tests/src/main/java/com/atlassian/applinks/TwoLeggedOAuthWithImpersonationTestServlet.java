package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.ImpersonatingAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;

public class TwoLeggedOAuthWithImpersonationTestServlet extends AbstractTwoLeggedOAuthTestServlet {
    public TwoLeggedOAuthWithImpersonationTestServlet(ApplicationLinkService applicationLinkService,
                                                      AuthenticationConfigurationManager authenticationConfigurationManager) {
        super(applicationLinkService, authenticationConfigurationManager);
    }

    protected ApplicationLinkRequestFactory createRequest(ApplicationLink applicationLink) {
        return applicationLink.createAuthenticatedRequestFactory(ImpersonatingAuthenticationProvider.class);
    }
}