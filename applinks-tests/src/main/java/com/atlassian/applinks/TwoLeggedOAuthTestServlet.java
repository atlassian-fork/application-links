package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.NonImpersonatingAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;

public class TwoLeggedOAuthTestServlet extends AbstractTwoLeggedOAuthTestServlet {
    public TwoLeggedOAuthTestServlet(ApplicationLinkService applicationLinkService,
                                     AuthenticationConfigurationManager authenticationConfigurationManager) {
        super(applicationLinkService, authenticationConfigurationManager);
    }

    protected ApplicationLinkRequestFactory createRequest(ApplicationLink applicationLink) {
        return applicationLink.createAuthenticatedRequestFactory(NonImpersonatingAuthenticationProvider.class);
    }
}