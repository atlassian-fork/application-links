package com.atlassian.applinks;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/**
 * Sets and deletes system properties
 */
@Path("systemProperties")
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SystemPropertiesResource {
    @Path("/{key}")
    @GET
    public Response getSystemProperty(@PathParam("key") String key) {
        String val = System.getProperty(key);
        return val == null ? Response.status(Status.NOT_FOUND).build() : Response.ok(new SystemPropertiesEntity(val)).build();
    }

    @Path("/{key}")
    @PUT
    public void setSystemProperty(@PathParam("key") String key, SystemPropertiesEntity entity) {
        System.setProperty(key, entity.getValue());
    }

    @Path("/{key}")
    @DELETE
    public void clearSystemProperty(@PathParam("key") String key) {
        System.clearProperty(key);
    }

}
