package com.atlassian.applinks;

import com.atlassian.sal.api.user.UserManager;
import com.atlassian.security.auth.trustedapps.TrustedApplicationUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The RedirectsToSomewhereServlet will throw a redirect to this servlet.
 *
 * @since 3.2
 */
public class RedirectedServlet extends HttpServlet {
    private static final Logger log = LoggerFactory.getLogger(RedirectedServlet.class);

    private final UserManager userManager;

    public RedirectedServlet(final UserManager userManager) {
        this.userManager = userManager;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        log.debug("doGet - redirected servlet signature for trusted apps:" + req.getHeader(TrustedApplicationUtils.Header.Request.SIGNATURE));

        resp.setContentType("text/html");
        final PrintWriter writer = resp.getWriter();
        final String username = userManager.getRemoteUsername(req);
        if (username != null) {
            final String requestType = req.getParameter("requestType");
            writer.print("You are user " + username + " and have been redirected and your initial request was a " + requestType);
        } else {
            writer.print("Sorry, you are not logged in!");
        }
    }
}
