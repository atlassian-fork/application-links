package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.net.Request;
import com.atlassian.security.auth.trustedapps.request.TrustedRequest;
import com.google.common.collect.ImmutableMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.ok;

@AnonymousAllowed
@Path("authentication")
public class AuthenticationRegistrationResource {
    private final ApplicationLinkService applicationLinkService;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    public AuthenticationRegistrationResource(ApplicationLinkService applicationLinkService, AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.applicationLinkService = applicationLinkService;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    @GET
    @Path("register")
    public Response register(@QueryParam("rpc-url") String rpcurl, @QueryParam("authentication-class") String authenticationClass) {
        ApplicationLink applicationLink = getApplicationLinkByRpcUrl(rpcurl);
        Class<? extends AuthenticationProvider> clazz = getAuthenticationClass(authenticationClass);
        authenticationConfigurationManager.registerProvider(applicationLink.getId(), clazz, ImmutableMap.<String, String>of());

        return ok("done").build();
    }

    @GET
    @Path("unregister")
    public Response unregister(@QueryParam("rpc-url") String rpcurl, @QueryParam("authentication-class") String authenticationClass) {
        ApplicationLink applicationLink = getApplicationLinkByRpcUrl(rpcurl);
        Class<? extends AuthenticationProvider> clazz = getAuthenticationClass(authenticationClass);
        authenticationConfigurationManager.unregisterProvider(applicationLink.getId(), clazz);

        return ok("done").build();
    }

    @GET
    @Path("getRequestType")
    public Response getRequestType(@QueryParam("rpc-url") String rpcurl) {
        ApplicationLink applicationLink = getApplicationLinkByRpcUrl(rpcurl);
        ApplicationLinkRequestFactory factory = applicationLink.createAuthenticatedRequestFactory();

        return ok(getRequestType(factory)).build();
    }

    @GET
    @Path("getImpersonatingRequestType")
    public Response getImpersonatingRequestType(@QueryParam("rpc-url") String rpcurl) {
        ApplicationLink applicationLink = getApplicationLinkByRpcUrl(rpcurl);
        ApplicationLinkRequestFactory factory = applicationLink.createImpersonatingAuthenticatedRequestFactory();

        return ok(getRequestType(factory)).build();
    }

    @GET
    @Path("getNonImpersonatingRequestType")
    public Response getNonImpersonatingRequestType(@QueryParam("rpc-url") String rpcurl) {
        ApplicationLink applicationLink = getApplicationLinkByRpcUrl(rpcurl);
        ApplicationLinkRequestFactory factory = applicationLink.createNonImpersonatingAuthenticatedRequestFactory();

        return ok(getRequestType(factory)).build();
    }

    private String getRequestType(ApplicationLinkRequestFactory factory) {
        ApplicationLinkRequest request;
        try {
            request = factory.createRequest(Request.MethodType.GET, "/blah");
        } catch (CredentialsRequiredException cre) {
            // Only 3LO currently throws this exception so we know for sure what it is.
            // This is the name of class {@link com.atlassian.applinks.oauth.auth.ThreeLeggedOAuthRequest} but
            // I have to use string literal instead since the pacakge is not exported by AppLink plugin.
            return "com.atlassian.applinks.oauth.auth.ThreeLeggedOAuthRequest";
        }

        // In case of Trusted App, the request is wrapped multiple times. It's easier to just look at headers.
        if (request.getHeaders().containsKey("X-Seraph-Trusted-App-Version")) {
            return TrustedRequest.class.getName();
        }

        return request.getClass().getName();
    }

    private Class<? extends AuthenticationProvider> getAuthenticationClass(String authenticationClass) {
        try {
            return (Class<? extends AuthenticationProvider>) Class.forName(authenticationClass);
        } catch (ClassNotFoundException e) {
            throw new IllegalArgumentException("authenticationClass '" + authenticationClass + "' cannot be resolved to any supported authentication class.");
        }
    }

    private ApplicationLink getApplicationLinkByRpcUrl(String rpcurl) {
        for (ApplicationLink link : applicationLinkService.getApplicationLinks()) {
            if (link.getRpcUrl().toString().equals(rpcurl)) {
                return link;
            }
        }

        throw new IllegalArgumentException("Application Link with rpcurl '" + rpcurl + "' doesn't exist.");
    }
}
