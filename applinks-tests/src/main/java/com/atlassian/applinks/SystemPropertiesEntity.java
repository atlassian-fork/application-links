package com.atlassian.applinks;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * An entity just for holding the value of a system property, necessary for JSON deserialisation
 */
@JsonSerialize
public class SystemPropertiesEntity {
    @JsonProperty
    private String value;

    @SuppressWarnings("unused") // for Jersey
    public SystemPropertiesEntity() {
    }

    public SystemPropertiesEntity(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
