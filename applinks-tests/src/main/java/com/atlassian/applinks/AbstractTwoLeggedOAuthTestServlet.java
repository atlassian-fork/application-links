package com.atlassian.applinks;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequest;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthAuthenticationProvider;
import com.atlassian.applinks.api.auth.types.TwoLeggedOAuthWithImpersonationAuthenticationProvider;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.sal.api.net.Request;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public abstract class AbstractTwoLeggedOAuthTestServlet extends HttpServlet {
    private final ApplicationLinkService applicationLinkService;
    private final AuthenticationConfigurationManager authenticationConfigurationManager;

    public AbstractTwoLeggedOAuthTestServlet(ApplicationLinkService applicationLinkService,
                                             AuthenticationConfigurationManager authenticationConfigurationManager) {
        this.applicationLinkService = applicationLinkService;
        this.authenticationConfigurationManager = authenticationConfigurationManager;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp)
            throws ServletException, IOException {
        ApplicationLink applicationLink;

        try {
            applicationLink = Iterables.get(applicationLinkService.getApplicationLinks(), 0);
        } catch (IndexOutOfBoundsException iobe) {
            throw new RuntimeException("Need one application link to perform the test");
        }

        authenticationConfigurationManager.registerProvider(applicationLink.getId(), TwoLeggedOAuthAuthenticationProvider.class, ImmutableMap.<String, String>of());
        authenticationConfigurationManager.registerProvider(applicationLink.getId(), TwoLeggedOAuthWithImpersonationAuthenticationProvider.class, ImmutableMap.<String, String>of());

        String result;
        ApplicationLinkRequestFactory requestFactory = createRequest(applicationLink);

        try {
            ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.GET, "/plugins/servlet/applinks/whoami");
            result = request.execute();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        resp.setContentType("text/plain");
        PrintWriter out = resp.getWriter();
        out.println(result);
        out.close();
    }

    abstract ApplicationLinkRequestFactory createRequest(ApplicationLink applicationLink);
}