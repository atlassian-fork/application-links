package com.atlassian.applinks.internal.test.user;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static com.atlassian.applinks.internal.rest.model.BaseRestEntity.createSingleFieldEntity;


/**
 * REST resource that retrieves a valid session ID cookie that allows for authenticating a specific user.
 * Also see {@code AuthenticationBackdoor}.
 *
 * @since 4.3
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("auth")
public class AuthenticationBackdoorResource {
    private static final String SESSION_ID = "sessionId";

    @GET
    public Response authenticate(@Context HttpServletRequest request) {
        HttpSession session = request.getSession(true);
        return Response.ok(createSingleFieldEntity(SESSION_ID, session.getId())).build();
    }
}
