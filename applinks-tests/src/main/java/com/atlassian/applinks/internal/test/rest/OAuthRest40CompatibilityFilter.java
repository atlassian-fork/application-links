package com.atlassian.applinks.internal.test.rest;

import com.atlassian.applinks.host.spi.InternalHostApplication;

import java.io.IOException;
import java.util.regex.Pattern;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

/**
 * When activated, makes the application's OAuth REST resources act as in Applinks 4.x, by redirecting to the new
 * location of those resources.
 *
 * @since 5.1
 */
public class OAuthRest40CompatibilityFilter implements Filter {
    public static final String PROP_OAUTH_40_COMPATIBILITY = "applinks.compatibility.rest.oauth.40";
    public static final String PROP_OAUTH_40_COMPATIBILITY_INVOCATIONS = PROP_OAUTH_40_COMPATIBILITY + ".invocations";
    public static final String PROP_OAUTH_40_COMPATIBILITY_RETURN_OK = PROP_OAUTH_40_COMPATIBILITY + ".returnok";

    private static final String REST_PATH = "/rest";
    private static final Pattern PATH_PATTERN = Pattern.compile(
            "/applinks/.+/applicationlink/[\\w-]+/authentication(/consumer)?");

    private final InternalHostApplication hostApplication;

    public OAuthRest40CompatibilityFilter(InternalHostApplication hostApplication) {
        this.hostApplication = hostApplication;
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {
        if (!Boolean.getBoolean(PROP_OAUTH_40_COMPATIBILITY)) {
            filterChain.doFilter(request, response);
            return;
        }

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        if (matchesOAuthRestRequest(httpRequest)) {
            updateInvocationCount();
            String fixedPath = httpRequest.getPathInfo()
                    .replace("/applinks/", "/applinks-oauth/")
                    .replace("/2.0/", "/latest/");


            if (Boolean.getBoolean(PROP_OAUTH_40_COMPATIBILITY_RETURN_OK)) {
                // bypass redirecting as it's not well supported by HttpClient for POST/PUT/DELETE and just return OK
                httpResponse.setStatus(HttpServletResponse.SC_OK);
                httpResponse.getWriter().close();
            } else {
                // manual 303 redirect to support POST/PUT/DELETE; sendRedirect() returns 302 which cannot be
                // automatically redirected for POST/PUT/DELETE according to the HTTP spec
                httpResponse.setStatus(HttpServletResponse.SC_SEE_OTHER);
                httpResponse.setHeader(HttpHeaders.LOCATION, hostApplication.getBaseUrl().toString() + REST_PATH + fixedPath);
                httpResponse.getWriter().close();
            }
        } else {
            filterChain.doFilter(request, response);
        }
    }

    public void init(final FilterConfig filterConfig) throws ServletException {
        // no-op
    }

    public void destroy() {
        // no-op
    }

    private boolean matchesOAuthRestRequest(HttpServletRequest httpRequest) {
        return REST_PATH.equals(httpRequest.getServletPath()) &&
                PATH_PATTERN.matcher(httpRequest.getPathInfo()).matches();
    }

    private synchronized static void updateInvocationCount() {
        int currentCount = Integer.getInteger(PROP_OAUTH_40_COMPATIBILITY_INVOCATIONS, 0);
        System.setProperty(PROP_OAUTH_40_COMPATIBILITY_INVOCATIONS, Integer.toString(currentCount + 1));
    }
}
