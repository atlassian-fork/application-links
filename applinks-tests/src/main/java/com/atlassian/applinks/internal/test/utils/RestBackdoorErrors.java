package com.atlassian.applinks.internal.test.utils;

import com.atlassian.applinks.internal.rest.model.RestError;

import javax.annotation.Nonnull;
import javax.ws.rs.core.Response;

public final class RestBackdoorErrors {
    private RestBackdoorErrors() {
    }

    @Nonnull
    public static Response notFound(@Nonnull String message) {
        return Response.status(Response.Status.NOT_FOUND).entity(new RestError(message)).build();
    }
}
