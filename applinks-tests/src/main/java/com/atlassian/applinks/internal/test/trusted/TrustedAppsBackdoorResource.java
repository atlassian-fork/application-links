package com.atlassian.applinks.internal.test.trusted;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.internal.common.rest.util.RestApplicationIdParser;
import com.atlassian.applinks.internal.test.utils.TrustedUtils;
import com.atlassian.applinks.spi.auth.AuthenticationConfigurationManager;
import com.atlassian.applinks.test.rest.model.RestTrustedAppsStatus;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.security.auth.trustedapps.TrustedApplicationsConfigurationManager;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Use to set up and get trusted apps status per applink
 *
 * @since 5.0
 */
@AnonymousAllowed
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("trustedapps")
public class TrustedAppsBackdoorResource {
    private final ApplicationLinkService applicationLinkService;
    private final TrustedUtils trustedUtils;

    public TrustedAppsBackdoorResource(ApplicationLinkService applicationLinkService,
                                       AuthenticationConfigurationManager authenticationConfigurationManager,
                                       TrustedApplicationsConfigurationManager trustedConfigManager) {
        this.applicationLinkService = applicationLinkService;
        this.trustedUtils = new TrustedUtils(authenticationConfigurationManager, trustedConfigManager);
    }

    @GET
    @Path("status/{applinkid}")
    public Response get(@PathParam("applinkid") String applinkId) throws Exception {
        ApplicationId id = RestApplicationIdParser.parseApplicationId(applinkId);
        ApplicationLink applink = applicationLinkService.getApplicationLink(id);

        return Response.ok(new RestTrustedAppsStatus(
                trustedUtils.inboundTrustEnabled(applink),
                trustedUtils.outboundTrustEnabled(applink)))
                .build();
    }

    @PUT
    @Path("status/{applinkid}")
    public Response put(@PathParam("applinkid") String applinkId, RestTrustedAppsStatus status) throws Exception {
        ApplicationId id = RestApplicationIdParser.parseApplicationId(applinkId);
        ApplicationLink applink = applicationLinkService.getApplicationLink(id);

        if (status.getIncoming()) {
            trustedUtils.issueInboundTrust(applink);
        } else {
            trustedUtils.revokeInboundTrust(applink);
        }

        if (status.getOutgoing()) {
            trustedUtils.issueOutboundTrust(applink);
        } else {
            trustedUtils.revokeOutboundTrust(applink);
        }

        return Response.ok(status).build();
    }
}
