package com.atlassian.applinks.core.v2.rest;

import com.atlassian.applinks.AbstractClient;
import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.oauth.rest.ConsumerEntityBuilder;
import com.atlassian.oauth.Consumer;
import com.atlassian.oauth.util.RSAKeys;
import com.atlassian.pageobjects.TestedProduct;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;

/**
 * Client for v4.x AppLinks admin REST API
 *
 * @since 4.0.14
 */
public class ApplicationLinkResourceClient extends AbstractClient {
    public static ApplicationLinkResourceClient forProduct(TestedProduct<?> product) {
        return new ApplicationLinkResourceClient(product);
    }

    public ApplicationLinkResourceClient(TestedProduct<?> product) {
        super(product);
    }

    /**
     * Create a new applink
     */
    public ApplicationId addApplicationLink(ApplicationLinkEntity applicationLinkEntity) {
        final String url = this.baseUrl + "/rest/applinks/2.0/applicationlink.json";
        try {
            HttpPut httpPut = new HttpPut(url);

            // generate the payload defining the new link.
            JSONObject jsonObject = serializeApplicationLinkEntity(applicationLinkEntity);

            StringEntity entity = new StringEntity(jsonObject.toString());
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
            String response = execute(httpPut);

            // get the id of the new applink
            // a bit fragile but should work
            JSONObject responseObject = new JSONObject(response);
            String href = ((JSONObject) ((JSONArray) responseObject.get("resources-created")).get(0)).get("href").toString();
            String id = href.substring(href.lastIndexOf("/") + 1);

            return new ApplicationId(id);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Request to '%s' failed.", url), e);
        }
    }

    /**
     * Create a new applink
     */
    public void putAuthenticationProvider(final ApplicationId applicationId, final AuthenticationProviderEntity authenticationProviderEntity) {
        final String url = this.baseUrl + "/rest/applinks/2.0/applicationlink/" + applicationId.get() + "/authentication/provider.json";
        try {
            HttpPut httpPut = new HttpPut(url);
            JSONObject jsonObject = new JSONObject(authenticationProviderEntity);
            StringEntity entity = new StringEntity(jsonObject.toString());
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
            execute(httpPut);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Request to '%s' failed.", url), e);
        }
    }

    /**
     * Get the OAuth consumer
     */
    public Consumer getConsumer(final ApplicationId applicationId) {
        final String url = this.baseUrl + "/rest/applinks-oauth/1.0/applicationlink/" + applicationId.get() + "/authentication/consumer.json";
        try {
            HttpGet httpGet = new HttpGet(url);
            String response = execute(httpGet);
            final JSONObject responseObject = new JSONObject(response);
            JSONObject consumerObject = (JSONObject) ((JSONArray) responseObject.get("consumers")).get(0);
            return Consumer
                    .key(consumerObject.getString("key"))
                    .name(consumerObject.getString("name"))
                    .description(consumerObject.getString("description"))
//                    .threeLOAllowed(consumerObject.getBoolean("threeLOAllowed"))
                    .twoLOAllowed(consumerObject.getBoolean("twoLOAllowed"))
                    .twoLOImpersonationAllowed(consumerObject.getBoolean("twoLOImpersonationAllowed"))
                    .signatureMethod(Consumer.SignatureMethod.valueOf(consumerObject.getString("signatureMethod")))
                    .publicKey(RSAKeys.fromPemEncodingToPublicKey(consumerObject.getString("publicKey")))
                    .build();
        } catch (Exception e) {
            throw new RuntimeException(String.format("Request to '%s' failed.", url), e);
        }
    }

    /**
     * Add a default configured OAuth consumer
     */
    public void putConsumer(final ApplicationId applicationId) {
        final String url = this.baseUrl + "/rest/applinks-oauth/1.0/applicationlink/" + applicationId.get() + "/authentication/consumer.json?autoConfigure=true";
        try {
            HttpPut httpPut = new HttpPut(url);

            JSONObject jsonObject = new JSONObject(ConsumerEntityBuilder.consumer(Consumer.key("not used").name("not used").signatureMethod(Consumer.SignatureMethod.HMAC_SHA1).build()).build());
            StringEntity entity = new StringEntity(jsonObject.toString());
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
            execute(httpPut);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Request to '%s' failed.", url), e);
        }
    }

    public void putConsumer(final ApplicationId applicationId, final Consumer consumer) {
        final String url = this.baseUrl + "/rest/applinks-oauth/1.0/applicationlink/" + applicationId.get() + "/authentication/consumer.json";
        try {
            HttpPut httpPut = new HttpPut(url);
            JSONObject jsonObject = new JSONObject(ConsumerEntityBuilder.consumer(consumer).build());
            StringEntity entity = new StringEntity(jsonObject.toString());
            entity.setContentType("application/json");
            httpPut.setEntity(entity);
            execute(httpPut);
        } catch (Exception e) {
            throw new RuntimeException(String.format("Request to '%s' failed.", url), e);
        }
    }

    public static JSONObject serializeApplicationLinkEntity(ApplicationLinkEntity applicationLinkEntity)
            throws IOException, JSONException {
        // TODO JSONObject mapping doesn't handle custom XMLAdapters
        JSONObject jsonObject = new JSONObject(applicationLinkEntity);
        removeClassAttributes(jsonObject); // including super class adds these
        if (applicationLinkEntity.getId() != null) {
            jsonObject.put("id", applicationLinkEntity.getId().get());
        }
        if (applicationLinkEntity.getTypeId() != null) {
            jsonObject.put("typeId", applicationLinkEntity.getTypeId().get());
        }

        return jsonObject;
    }

    private static void removeClassAttributes(JSONObject object) {
        try {
            object.remove("class");
            object.remove("declaringClass");
            Iterator<?> keys = object.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if ((object.get(key)) instanceof JSONObject) {
                    removeClassAttributes((JSONObject) object.get(key));

                }
            }
        } catch (Exception e) {
            // we remove class attributes optimistically.
        }
    }
}
