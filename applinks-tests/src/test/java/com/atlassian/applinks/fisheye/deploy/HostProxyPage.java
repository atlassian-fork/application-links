package com.atlassian.applinks.fisheye.deploy;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

public class HostProxyPage extends ApplinkAbstractPage {

    @ElementBy(tagName = "body")
    private PageElement body;

    public String getUrl() {
        return "/rest/applinks-tests/1/";
    }

    public boolean isSuccessful() {
        return "Success".equals(body.getText());
    }
}
