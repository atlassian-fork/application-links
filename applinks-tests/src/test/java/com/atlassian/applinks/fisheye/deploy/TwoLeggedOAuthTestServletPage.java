package com.atlassian.applinks.fisheye.deploy;

import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

public class TwoLeggedOAuthTestServletPage extends ApplinkAbstractPage {
    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/twolo-test";
    }

    public String getContent() {
        return driver.getPageSource();
    }
}