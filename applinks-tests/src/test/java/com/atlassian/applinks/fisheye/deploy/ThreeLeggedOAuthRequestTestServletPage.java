package com.atlassian.applinks.fisheye.deploy;

import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;

public class ThreeLeggedOAuthRequestTestServletPage extends ApplinkAbstractPage {
    public String getUrl() {
        return "/plugins/servlet/applinks/applinks-tests/three-lo-test";
    }

    public String getContent() {
        return driver.getPageSource();
    }
}
