package com.atlassian.applinks.internal.feature;

import com.atlassian.sal.api.features.DarkFeatureManager;

import javax.annotation.Nonnull;

/**
 * Access the Applinks feature keys in tests without having to expose them to the rest of the world
 *
 * @since 4.3
 */
public class ApplinksSystemFeatures {
    private ApplinksSystemFeatures() {
        throw new AssertionError("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static String featureKeyFor(@Nonnull ApplinksFeatures feature) {
        return DarkFeatureManager.ATLASSIAN_DARKFEATURE_PREFIX + feature.featureKey;
    }
}
