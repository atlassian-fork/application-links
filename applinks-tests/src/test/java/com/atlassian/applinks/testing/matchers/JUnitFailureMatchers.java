package com.atlassian.applinks.testing.matchers;

import org.hamcrest.FeatureMatcher;
import org.hamcrest.Matcher;
import org.junit.runner.notification.Failure;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @since 4.3
 */
public final class JUnitFailureMatchers {
    private JUnitFailureMatchers() {
        throw new UnsupportedOperationException("Do not instantiate " + getClass().getSimpleName());
    }

    @Nonnull
    public static Matcher<Failure> withExceptionThat(Matcher<Throwable> exceptionMatcher) {
        return new FeatureMatcher<Failure, Throwable>(checkNotNull(exceptionMatcher, "exceptionMatcher"), "exception",
                "exception that") {
            @Override
            protected Throwable featureValueOf(Failure actual) {
                return actual.getException();
            }
        };
    }
}
