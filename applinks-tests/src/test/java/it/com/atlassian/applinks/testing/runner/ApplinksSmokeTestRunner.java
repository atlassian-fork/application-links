package it.com.atlassian.applinks.testing.runner;

import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import it.com.atlassian.applinks.testing.product.IgnoredProducts;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.product.Products;
import org.apache.commons.lang3.StringUtils;
import org.junit.internal.AssumptionViolatedException;
import org.junit.runner.notification.RunNotifier;
import org.junit.runners.BlockJUnit4ClassRunner;
import org.junit.runners.model.FrameworkMethod;
import org.junit.runners.model.InitializationError;
import org.junit.runners.model.Statement;

import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.concurrent.NotThreadSafe;

import static it.com.atlassian.applinks.testing.product.ProductInstances.forInstanceId;
import static java.lang.String.format;

/**
 * JUnit runner for Applinks smoke tests. NOTE: this runner is not thread safe and therefore does not support parallel
 * run mode.
 *
 * This runner relies on a number of assumptions (i.e. contracts) that the test
 * classes being run by it must fulfil, as well as runtime system properties that must be provided to find the tested
 * products.
 * <p>
 * <h2>Test class</h2>
 * Test classes must provide constructor that accepts 2 arguments that are instances of one of the following:
 * <ul>
 * <li>{@link ProductInstance}</li>
 * <li>{@link TestedProduct}</li>
 * </ul>
 * The first argument is the "main" ("from") instance in the test, the second argument is the "background" (or "to")
 * instance that may, or may not be used depending on whether the test requires to interact with 2 instances.
 * </p>
 * <p>
 * Each test case in the test class - provided that the test supports both currently running products (see below)
 * will be run <i>twice</i> such that each tested instance becomes the "main" instance.
 * </p>
 * <p>
 * <h2>Supported products</h2>
 * Test classes can use the {@link Products} annotation, on both the entire class, as well as particular test methods,
 * to narrow down the range of {@link SupportedProducts products types} it wants to run against. {@link Products}
 * declarations on test methods take precedence over class declarations. Not specifying {@link Products} is equivalent
 * to supporting all product types. If a given test method declares support for both products that the test is currently
 * running against, each test method is invoked twice, with each product taking turn as the "from" product. Otherwise
 * the test method is invoked once (if only one of the products is supported), or not at all (if none of the current
 * products is supported), in which case the test case is marked as ignored ("assumption failed") in {@code JUnit} test
 * results.
 * </p>
 * <p>
 * <h2>Runtime</h2>
 * In runtime, this runner requires the following 2 system properties to determine the products currently under test:<br>
 * {@code applinks.smoke.instance1}<br>
 * {@code applinks.smoke.instance2}<br>
 * The values of those should point to valid AMPS instance IDs, as defined in {@link ProductInstances}.
 * </p>
 * <p>
 * <h2>IDEA</h2>
 * <h3>Smoke test instances</h3>
 * To make IDEA run your smoke tests using this runner, you need to specify smoke tested instances manually, by adding
 * {@code applinks.smoke.instance1} and {@code applinks.smoke.instance2} system properties to our run configuration,
 * pointing to applications running in your local environment, e.g.
 * {@code
 * -Dapplinks.smoke.instance1=jira -Dapplinks.smoke.instance2=bitbucket
 * }
 * <h3>Single test methods</h3>
 * When running single test cases (methods) IDEA matches the tests by name. This does not work well with this runner,
 * because it modifies original method names by adding {@code [FROM -> TO]} suffix. To make this work, you need to
 * modify your IDEA run configuration to match that pattern. E.g. to run a test method
 * {@code MySmokeTest.mySmokeTestMethod} given {@code applinks.smoke.instance1} being {@code jira} and
 * {@code applinks.smoke.instance2} {@code bitbucket}, the following test method name in your run configuration would
 * run {@code MySmokeTest.mySmokeTestMethod} with JIRA as main instance and Bitbucket as background instance:
 * {@code mySmokeTestMethod [JIRA -> BITBUCKET]}. The reverse test would be run by specifying
 * {@code mySmokeTestMethod [BITBUCKET -> JIRA]} as method name in your run configuration. Note that whitespace is
 * important in this case to get a match.
 * </p>
 *
 * @since 4.3
 */
@NotThreadSafe
public class ApplinksSmokeTestRunner extends BlockJUnit4ClassRunner {
    private static final String APPLINKS_SMOKE_INSTANCE1 = "applinks.smoke.instance1";
    private static final String APPLINKS_SMOKE_INSTANCE2 = "applinks.smoke.instance2";

    private static final Set<Class<?>> SUPPORTED_CONSTRUCTOR_ARGS = ImmutableSet.of(
            ProductInstance.class,
            TestedProduct.class
    );

    private FrameworkMethod currentTest; // see createTest()

    public ApplinksSmokeTestRunner(Class<?> klass) throws InitializationError {
        super(klass);
    }

    @Override
    protected void collectInitializationErrors(List<Throwable> errors) {
        // this method invoked in the super constructor and unfortunately relies on instance properties being
        // already initialized, so we need validate them here
        validateInstanceProperty(APPLINKS_SMOKE_INSTANCE1, errors);
        validateInstanceProperty(APPLINKS_SMOKE_INSTANCE2, errors);

        super.collectInitializationErrors(errors);

        validateProductAnnotations(errors);
    }

    @Override
    protected List<FrameworkMethod> computeTestMethods() {
        ProductInstances instance1 = getInstanceSafe(APPLINKS_SMOKE_INSTANCE1);
        ProductInstances instance2 = getInstanceSafe(APPLINKS_SMOKE_INSTANCE2);

        // degrade to "standard" behaviour if one of the properties is not set. The validation makes sure the test
        // initialization will fail in that case anyway and this method is called from later stages of validation, so
        // we don't want it to throw exceptions
        if (instance1 == null || instance2 == null) {
            return super.computeTestMethods();
        }

        ImmutableList.Builder<FrameworkMethod> builder = ImmutableList.builder();

        for (FrameworkMethod method : super.computeTestMethods()) {
            if (supportsInstance(method, instance1)) {
                builder.add(new ProductFrameworkMethod(method.getMethod(), instance1, instance2));
            }
            if (supportsInstance(method, instance2)) {
                builder.add(new ProductFrameworkMethod(method.getMethod(), instance2, instance1));
            }
            if (!supportsInstance(method, instance1) && !supportsInstance(method, instance2)) {
                Set<SupportedProducts> running = ImmutableSet.of(instance1.getProduct(), instance2.getProduct());
                Products products = getProductAnnotation(method);
                IgnoredProducts ignoredProducts = getAnnotation(method, IgnoredProducts.class);
                IgnoredMethod ignoredMethod = products != null ?
                        new ProductNotSupported(method.getMethod(), running, ImmutableSet.copyOf(products.value())) :
                        new ProductIgnored(method.getMethod(), running, ImmutableSet.copyOf(ignoredProducts.value()));
                builder.add(ignoredMethod);
            }
        }

        return builder.build();
    }

    @Override
    protected void runChild(FrameworkMethod method, RunNotifier notifier) {
        currentTest = method;
        super.runChild(method, notifier);
    }

    @Override
    protected Statement methodBlock(FrameworkMethod method) {
        if (method instanceof IgnoredMethod) {
            final IgnoredMethod ignoredMethod = (IgnoredMethod) method;
            return new Statement() {
                @Override
                public void evaluate() throws Throwable {
                    throw ignoredMethod.raiseIgnoreError();
                }
            };
        } else {
            return super.methodBlock(method);
        }
    }

    @Override
    protected Object createTest() throws Exception {
        // unfortunately createTest() does not have direct access to the framework method (bad design again?)
        // so we need to store it as instance field so we know what instances to use to instantiate the test object
        if (currentTest instanceof ProductFrameworkMethod) {
            ProductFrameworkMethod productMethod = (ProductFrameworkMethod) currentTest;
            Constructor<?> constructor = getTestClass().getOnlyConstructor();
            return instantiateSmokeTest(productMethod, constructor);
        } else {
            return super.createTest();
        }
    }

    @Override
    protected String testName(FrameworkMethod method) {
        if (method instanceof ProductFrameworkMethod) {
            ProductFrameworkMethod productMethod = (ProductFrameworkMethod) method;
            return format("%s [%s -> %s]", productMethod.getName(), productMethod.getMainInstance(),
                    productMethod.getBackgroundInstance());
        } else {
            return super.testName(method);
        }
    }

    @Override
    protected void validateConstructor(List<Throwable> errors) {
        validateOnlyOneConstructor(errors);
        validateSmokeTestConstructor(errors);
    }

    private void validateProductAnnotations(List<Throwable> errors) {
        Class<?> testClass = getTestClass().getJavaClass();
        if (testClass.getAnnotation(Products.class) != null && testClass.getAnnotation(IgnoredProducts.class) != null) {
            errors.add(new IllegalStateException("Test class has both @Products and @IgnoredProducts annotation, " +
                    "which is not allowed"));
        }
        for (FrameworkMethod annotatedMethod : getTestClass().getAnnotatedMethods(Products.class)) {
            if (annotatedMethod.getAnnotation(IgnoredProducts.class) != null) {
                errors.add(new IllegalStateException(format("Test method '%s' has both @Products and @IgnoredProducts " +
                        "annotation, which is not allowed", annotatedMethod.getName())));
            }
        }
    }

    /**
     * Create tested product given its class and a product instance. You probably don't want to override it in your
     * "production" test suite.
     *
     * @param testedProductClass tested product class
     * @param instance           product instance to use
     * @return instance of the product
     */
    @VisibleForTesting
    @Nonnull
    protected TestedProduct createTestedProduct(@Nonnull Class<? extends TestedProduct> testedProductClass,
                                                @Nonnull ProductInstances instance) {
        return TestedProductFactory.create(testedProductClass, instance, null);
    }

    private static ProductInstances getInstanceSafe(String propertyName) {
        String value = System.getProperty(propertyName);
        if (value != null) {
            return forInstanceId(value);
        } else {
            return null;
        }
    }

    private static ProductInstances getInstanceOrFail(String propertyName) {
        String value = System.getProperty(propertyName);
        if (StringUtils.isBlank(value)) {
            throw new IllegalStateException(format("Product system property '%s' is not set, unable to " +
                    "determine the smoke tested product", propertyName));
        }
        ProductInstances instance = forInstanceId(value);
        if (instance == null) {
            throw new IllegalStateException(format("Product system property '%s' has invalid value '%s': unable to " +
                    "determine the smoke tested product. Supported product instances: " +
                    Arrays.toString(ProductInstances.values()), propertyName, value));
        }
        return instance;
    }

    private static void validateInstanceProperty(String propertyName, List<Throwable> errors) {
        try {
            getInstanceOrFail(propertyName);
        } catch (IllegalStateException e) {
            errors.add(e);
        }
    }

    private boolean supportsInstance(FrameworkMethod method, ProductInstances product) {
        Products supportedProducts = getProductAnnotation(method);
        if (supportedProducts != null) {
            return ImmutableSet.copyOf(supportedProducts.value()).contains(product.getProduct());
        }
        IgnoredProducts ignoredProducts = getAnnotation(method, IgnoredProducts.class);
        return ignoredProducts == null || !ImmutableSet.copyOf(ignoredProducts.value()).contains(product.getProduct());
    }

    private Products getProductAnnotation(FrameworkMethod method) {
        return (method.getAnnotation(Products.class) != null) ?
                method.getAnnotation(Products.class) :
                getTestClass().getJavaClass().getAnnotation(Products.class);
    }

    private <A extends Annotation> A getAnnotation(FrameworkMethod method, Class<A> annotationType) {
        return (method.getAnnotation(annotationType) != null) ?
                method.getAnnotation(annotationType) :
                getTestClass().getJavaClass().getAnnotation(annotationType);
    }

    /**
     * Validate that constructor conforms to the smoke test constructor, that is only accepts 2 arguments <i>of a
     * supported type</i>, which is one of:
     * <ul>
     * <li>{@link ProductInstance}</li>
     * <li>{@link TestedProduct}</li>
     * </ul>
     *
     * @param errors validation errors to update
     */
    private void validateSmokeTestConstructor(List<Throwable> errors) {
        Constructor<?> constructor = getTestClass().getOnlyConstructor();
        // only 2-arg constructor
        if (constructor.getParameterTypes().length != 2) {
            errors.add(new IllegalStateException(format("Test constructor has invalid number of arguments. " +
                    "Expected: <2>, was: <%d>", constructor.getParameterTypes().length)));
        }
        // supported types only
        for (Class<?> argType : constructor.getParameterTypes()) {
            if (!isSupportedTestConstructorType(argType)) {
                errors.add(new IllegalStateException(format("Unsupported test constructor argument type: '%s'. " +
                        "The following types are supported: %s", argType.getName(), SUPPORTED_CONSTRUCTOR_ARGS)));
            }
        }
    }

    private static boolean isSupportedTestConstructorType(Class<?> firstArgType) {
        for (Class<?> supported : SUPPORTED_CONSTRUCTOR_ARGS) {
            // tests should be able to demand specific sub-type of e.g. ProductInstance or TestedProduct, although it
            // may result in runtime error whole instantiating the tests
            if (supported.isAssignableFrom(firstArgType)) {
                return true;
            }
        }

        return false;
    }

    private Object instantiateSmokeTest(ProductFrameworkMethod productMethod, Constructor<?> constructor)
            throws Exception {
        Object firstArg = detectArg(constructor, productMethod.getMainInstance(), 0);
        Object secondArg = detectArg(constructor, productMethod.getBackgroundInstance(), 1);
        return constructor.newInstance(firstArg, secondArg);
    }

    @SuppressWarnings("unchecked")
    private Object detectArg(Constructor<?> constructor, ProductInstances instance, int argIndex) {
        Class<?> argClass = constructor.getParameterTypes()[argIndex];
        return ProductInstance.class.isAssignableFrom(argClass) ?
                instance :
                createTestedProduct((Class<? extends TestedProduct>) argClass, instance);
    }

    public static class ProductFrameworkMethod extends FrameworkMethod {
        private final ProductInstances mainInstance;
        private final ProductInstances backgroundInstance;

        public ProductFrameworkMethod(Method method, ProductInstances main, ProductInstances second) {
            super(method);

            this.mainInstance = main;
            this.backgroundInstance = second;
        }

        public ProductInstances getMainInstance() {
            return mainInstance;
        }

        public ProductInstances getBackgroundInstance() {
            return backgroundInstance;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            if (!super.equals(o)) return false;
            ProductFrameworkMethod that = (ProductFrameworkMethod) o;
            return mainInstance == that.mainInstance &&
                    backgroundInstance == that.backgroundInstance;
        }

        @Override
        public int hashCode() {
            return Objects.hash(super.hashCode(), mainInstance, backgroundInstance);
        }
    }

    public abstract static class IgnoredMethod extends FrameworkMethod {
        protected final Set<SupportedProducts> runningProducts;

        private IgnoredMethod(Method method, Set<SupportedProducts> runningProducts) {
            super(method);
            this.runningProducts = runningProducts;
        }

        public abstract AssumptionViolatedException raiseIgnoreError();
    }

    public final static class ProductNotSupported extends IgnoredMethod {
        private final Set<SupportedProducts> supportedProducts;

        private ProductNotSupported(Method method, Set<SupportedProducts> runningProducts,
                                    Set<SupportedProducts> supportedProducts) {
            super(method, runningProducts);
            this.supportedProducts = supportedProducts;
        }

        @Override
        public AssumptionViolatedException raiseIgnoreError() {
            throw new AssumptionViolatedException("Test method does not support products " + runningProducts +
                    ", it only supports the following: " + supportedProducts);
        }
    }

    public final static class ProductIgnored extends IgnoredMethod {
        private final Set<SupportedProducts> ignoredProducts;

        private ProductIgnored(Method method, Set<SupportedProducts> runningProducts,
                               Set<SupportedProducts> ignoredProducts) {
            super(method, runningProducts);
            this.ignoredProducts = ignoredProducts;
        }

        @Override
        public AssumptionViolatedException raiseIgnoreError() {
            throw new AssumptionViolatedException("Test method does not support products " + runningProducts +
                    " as they are ignored: " + ignoredProducts);
        }
    }
}
