package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.SystemPropertiesBackdoor;
import com.google.common.base.Preconditions;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.osgi.framework.Version;

import javax.annotation.Nonnull;

/**
 * Overrides Applinks version advertised by the Applinks Manifest of the tested instance.
 *
 * @since 5.0
 */
public class ApplinksVersionOverrideRule extends TestWatcher {
    private static final String VERSION_OVERRIDE = "applinks.version.override";

    private final SystemPropertiesBackdoor systemPropertiesBackdoor;

    public ApplinksVersionOverrideRule(@Nonnull TestedInstance product, @Nonnull String versionToSet) {
        this(product);
        setApplinksVersion(versionToSet);
    }

    public ApplinksVersionOverrideRule(@Nonnull TestedInstance product) {
        systemPropertiesBackdoor = new SystemPropertiesBackdoor(product);
    }

    /**
     * @param version version to set, due to implementation limitations it must be a valid OSGi version
     */
    public void setApplinksVersion(String version) {
        Preconditions.checkNotNull(version, "version");
        systemPropertiesBackdoor.setProperty(VERSION_OVERRIDE, version);
    }

    public void setApplinksVersion(Version version) {
        setApplinksVersion(version.toString());
    }

    @Override
    protected void finished(Description description) {
        systemPropertiesBackdoor.clearProperty(VERSION_OVERRIDE);
    }
}