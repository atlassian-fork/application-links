package it.com.atlassian.applinks;

import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;

/**
 * Abstract Applinks browser test for V2 tests which require V3_UI feature to be disabled.
 */
public abstract class V2AbstractApplinksTest extends AbstractApplinksTest {
    @Rule
    public final ApplinksFeaturesRule REFAPP1_FEATURES = new ApplinksFeaturesRule(INSTANCE1).withDisabledFeatures(V3_UI);

    @Rule
    public final ApplinksFeaturesRule REFAPP2_FEATURES = new ApplinksFeaturesRule(INSTANCE2).withDisabledFeatures(V3_UI);
}
