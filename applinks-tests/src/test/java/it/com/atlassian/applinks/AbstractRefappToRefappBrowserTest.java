package it.com.atlassian.applinks;

import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.webdriver.refapp.RefappTestedProduct;
import it.com.atlassian.applinks.testing.product.ProductInstances;

import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

/**
 * Base test class that defines 2 refapp products, which is the default configuration for most Applinks browser tests.
 *
 * @see it.com.atlassian.applinks.testing.product.ProductInstances#REFAPP1
 * @see it.com.atlassian.applinks.testing.product.ProductInstances#REFAPP2
 * @since 4.3
 */
public abstract class AbstractRefappToRefappBrowserTest {
    public static final ProductInstances INSTANCE1 = REFAPP1;
    public static final ProductInstances INSTANCE2 = REFAPP2;

    public static final RefappTestedProduct PRODUCT = createRefappProduct(INSTANCE1);
    public static final RefappTestedProduct PRODUCT2 = createRefappProduct(INSTANCE2);

    private static RefappTestedProduct createRefappProduct(ProductInstance instance) {
        return TestedProductFactory.create(RefappTestedProduct.class, instance, null);
    }
}
