package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import static com.atlassian.applinks.internal.status.error.ApplinkErrorType.*;
import static com.atlassian.applinks.test.data.applink.config.BasicAuthenticationApplinkConfigurator.enableBasic;
import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.setUpTrustedApps;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.RemoteCapabilitiesConfigurator.refreshRemoteCapabilities;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusError;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_USER_BARNEY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.SYSADMIN;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;

/**
 * Tests Status resource when running against applications configured with Trusted,
 * Basic Authentications.
 *
 * @since 5.2
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceDeprecatedTest {

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);
    @Rule
    public final ApplinksCapabilitiesRule applinksCapabilitiesRule = new ApplinksCapabilitiesRule(REFAPP2);
    @Rule
    public final ApplinksVersionOverrideRule refapp2Version = new ApplinksVersionOverrideRule(REFAPP2);

    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    @Test
    public void legacyRemovalForWorkingOAuthWithTrusted() {
        applink.configure(enableDefaultOAuth());
        applink.configure(setUpTrustedApps(true, true));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), LEGACY_REMOVAL))
                .build());
    }

    @Test
    public void legacyRemovalForWorkingOAuthWithBasic() {
        applink.configure(enableDefaultOAuth());
        applink.configure(enableBasic(SYSADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), LEGACY_REMOVAL))
                .build());
    }

    @Test
    public void legacyUpdateForWorkingOAuthWithTrusted() {
        applink.configure(setUpTrustedApps(true, true));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), LEGACY_UPDATE))
                .build());
    }

    @Test
    public void legacyUpdateForWorkingOAuthWithBasic() {
        applink.configure(enableBasic(SYSADMIN));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), LEGACY_UPDATE))
                .build());
    }

    @Test
    public void manualLegacyUpdateForIncomingTrusted() {
        applink.configureFrom(setUpTrustedApps(true, false));
        applink.configureTo(setUpTrustedApps(false, true));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), MANUAL_LEGACY_UPDATE))
                .build());
    }

    @Test
    public void manualLegacyUpdateForNonAdminBasic() {
        applink.configure(enableBasic(REFAPP_USER_BARNEY));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), MANUAL_LEGACY_UPDATE))
                .build());
    }

    @Test
    public void manualLegacyRemovalForWorkingOAuthAndIncomingTrusted() {
        applink.configure(enableDefaultOAuth());
        applink.configureFrom(setUpTrustedApps(true, false));
        applink.configureTo(setUpTrustedApps(false, true));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), MANUAL_LEGACY_REMOVAL))
                .build());
    }

    @Test
    public void manualLegacyRemovalWithOldEditForWorkingOAuthAndIncomingTrusted() {
        removeMigrationApiForRefapp2();
        applink.configure(enableDefaultOAuth());
        applink.configureFrom(setUpTrustedApps(true, false));
        applink.configureTo(setUpTrustedApps(false, true));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), MANUAL_LEGACY_REMOVAL_WITH_OLD_EDIT))
                .build());
    }

    @Test
    public void legacyRemovalForWorkingOAuthAndNonAdminBasic() {
        applink.configure(enableDefaultOAuth());
        applink.configure(enableBasic(REFAPP_USER_BARNEY));

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(SYSADMIN)
                .specification(expectStatusError(applink.from().id(), LEGACY_REMOVAL))
                .build());
    }

    private void removeMigrationApiForRefapp2() {
        refapp2Version.setApplinksVersion("5.0.5");
        applinksCapabilitiesRule.disable(ApplinksCapabilities.MIGRATION_API);
        applink.configureFrom(refreshRemoteCapabilities());
    }
}
