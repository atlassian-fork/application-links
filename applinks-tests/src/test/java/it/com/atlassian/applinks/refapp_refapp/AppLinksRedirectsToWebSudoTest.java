package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.fisheye.deploy.DropWebSudoPage;
import com.atlassian.applinks.pageobjects.AdministratorPrivilegesRequiredPage;
import com.atlassian.applinks.pageobjects.LoginRequiredPage;
import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.atlassian.pageobjects.elements.timeout.DefaultTimeouts;
import com.atlassian.pageobjects.elements.timeout.Timeouts;
import com.atlassian.webdriver.applinks.component.v2.CreateUalDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import com.atlassian.webdriver.refapp.page.RefappHomePage;
import com.atlassian.webdriver.refapp.page.RefappLoginPage;
import com.atlassian.webdriver.refapp.page.RefappWebSudoPage;
import com.google.common.base.Supplier;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.WindowSession;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.RefappWebSudoRule;
import it.com.atlassian.applinks.testing.setup.EnableWebSudo;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class AppLinksRedirectsToWebSudoTest extends V2AbstractApplinksTest {
    @Rule
    public final RefappWebSudoRule webSudoRule = new RefappWebSudoRule(INSTANCE1);

    @Test
    @EnableWebSudo
    public void applinksPromptsForPasswordWhenAjaxFails() {
        final ListApplicationLinkPage listApplicationLinkPage = PRODUCT.gotoLoginPage().loginAsSysAdmin(ListApplicationLinkPage.class);

        final RefappWebSudoPage webSudoPage = PRODUCT.visit(DropWebSudoPage.class).navigateBack(RefappWebSudoPage.class);
        waitUntilTrue(isRequestMessagePresent(webSudoPage));

        final Page targetPage = webSudoPage.confirm(SYSADMIN_PASSWORD, ListApplicationLinkPage.class);
        assertThat(targetPage.getUrl(), is(listApplicationLinkPage.getUrl()));
    }

    @Test
    public void applinksRedirectsToLoginInitiallyWhenNotAuthenticated() {
        // both pages wait until they are fully loaded, so they do some checking for the current page
        ListApplicationLinkPage target = PRODUCT.visit(LoginRequiredPage.class)
                .login(ADMIN_USERNAME, ADMIN_PASSWORD, ListApplicationLinkPage.class);
        assertTrue(target.isCurrentPage());
    }

    @Test
    public void applinksExplainsAdminStatusIsRequiredWhenLoggedInAsNonAdmin() {
        final AdministratorPrivilegesRequiredPage page = PRODUCT.visit(RefappLoginPage.class)
                .login(USER_USERNAME, USER_PASSWORD, AdministratorPrivilegesRequiredPage.class);

        // auth.config.applink.no.admin.privileges
        assertTrue(page.isWarningMessageVisible());
        assertThat(page.getWarningMessageTitle(),
                containsString("To access it, please login as a user with administrator privileges."));
    }

    @Test
    @EnableWebSudo
    public void droppingWebSudoDuringWizardWithoutReciprocalLinkRedirectsToPasswordPrompt() {
        PRODUCT.gotoLoginPage().loginAsSysAdmin(RefappHomePage.class);
        final CreateUalDialog createUalDialog = navigateToSecondStepOfCreatingAppLinkToRefApp2();

        dropWebSudoInAnotherWindow();

        createUalDialog.unCheckAdmin().clickContinueToCreateUalLink();

        final RefappWebSudoPage webSudoPage = PRODUCT.getPageBinder().bind(RefappWebSudoPage.class);
        waitUntilTrue(isRequestMessagePresent(webSudoPage));
    }

    @Test
    @EnableWebSudo
    public void droppingWebSudoDuringWizardWithReciprocalLinkRedirectsToPasswordPrompt() {
        PRODUCT.gotoLoginPage().loginAsSysAdmin(RefappHomePage.class);
        final CreateUalDialog createUalDialog = navigateToSecondStepOfCreatingAppLinkToRefApp2();

        dropWebSudoInAnotherWindow();

        createUalDialog.clickContinueToCreateUalLink();
        final RefappWebSudoPage webSudoPage = PRODUCT.getPageBinder().bind(RefappWebSudoPage.class);
        waitUntilTrue(isRequestMessagePresent(webSudoPage));
    }

    private CreateUalDialog navigateToSecondStepOfCreatingAppLinkToRefApp2() {
        PRODUCT.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLink();
        return PRODUCT.getPageBinder().bind(CreateUalDialog.class);
    }

    protected TimedCondition isRequestMessagePresent(RefappWebSudoPage page) {
        return PRODUCT.getPageBinder().bind(RequestMessagePresentCondition.class).isPresent(page);
    }

    public static final class RequestMessagePresentCondition {

        @Inject
        private Timeouts timeouts;

        public TimedCondition isPresent(final RefappWebSudoPage webSudoPage) {
            return Conditions.forSupplier(timeouts, new Supplier<Boolean>() {
                public Boolean get() {
                    return webSudoPage.isRequestAccessMessagePresent();
                }
            });
        }
    }

    protected void dropWebSudoInAnotherWindow() {
        final WindowSession windowSession = new WindowSession(PRODUCT.getTester().getDriver(), new DefaultTimeouts());
        final WindowSession.BrowserWindow dropWebSudoBrowserWindow = windowSession.openNewWindow("dropwebsude");
        dropWebSudoBrowserWindow.switchTo();

        PRODUCT.visit(DropWebSudoPage.class);

        dropWebSudoBrowserWindow.close();
        windowSession.switchToDefault();
    }
}
