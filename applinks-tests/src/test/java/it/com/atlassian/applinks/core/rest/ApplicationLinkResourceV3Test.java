package it.com.atlassian.applinks.core.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.types.OAuthAuthenticationProvider;
import com.atlassian.applinks.core.rest.model.ApplicationLinkEntity;
import com.atlassian.applinks.core.rest.model.AuthenticationProviderEntity;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.core.net.HttpClientResponse;
import com.google.common.collect.ImmutableMap;
import it.com.atlassian.applinks.core.RestTestHelper;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.CleanupAppLinksRule;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import static com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth.AUTH_CONFIG_CONSUMER_KEY_OUTBOUND;
import static com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth.SERVICE_PROVIDER_ACCESS_TOKEN_URL;
import static com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth.SERVICE_PROVIDER_AUTHORIZE_URL;
import static com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth.SERVICE_PROVIDER_REQUEST_TOKEN_URL;
import static com.atlassian.applinks.core.json.JsonParser.toJson;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@Category(RefappTest.class)
public class ApplicationLinkResourceV3Test {
    private static final ProductInstances PRODUCT1 = ProductInstances.REFAPP1;
    private static final ProductInstances PRODUCT2 = ProductInstances.REFAPP2;

    private static final String PRODUCT_BASE_URL = PRODUCT1.getBaseUrl();
    private static final String PRODUCT2_BASE_URL = PRODUCT2.getBaseUrl();

    private static final String REST_URL = "/rest";
    private static final String APPLICATIONLINK_REST_ENDPOINT = REST_URL + "/applinks/3.0/applicationlink";
    private static final String SINGLE_APPLICATIONLINK_REST_ENDPOINT = PRODUCT_BASE_URL + APPLICATIONLINK_REST_ENDPOINT + "/%s";
    private static final String PUT_AUTHENTICATIONPROVIDER_REST_ENDPOINT = SINGLE_APPLICATIONLINK_REST_ENDPOINT + "/authentication/provider";
    private static final String URL_GENERIC_APP = "http://www.google.com";

    @Rule
    public RuleChain ruleChain = CleanupAppLinksRule.forProducts(PRODUCT1, PRODUCT2);

    @Test
    public void addApplicationLinkAtlassian2Atlassian() throws URISyntaxException, ResponseException, JSONException, IOException {
        UUID applicationId = UUID.randomUUID();

        String input = getAtlassianContent(applicationId);

        addApplicationLink(input);
    }

    @Test
    public void updateApplicationLinkAtlassian2Atlassian() throws URISyntaxException, ResponseException, JSONException, IOException {
        UUID applicationId = UUID.randomUUID();

        addApplicationLink(getAtlassianContent(new ApplicationId(applicationId.toString()), "refapp START", PRODUCT2_BASE_URL, PRODUCT2_BASE_URL));

        updateApplicationLink(applicationId, getAtlassianContent(new ApplicationId(applicationId.toString()), "refapp END", PRODUCT2_BASE_URL + "X", PRODUCT2_BASE_URL));
    }

    @Test
    public void verifyUpdateApplicationLinkFailsForRpcUrlChange() throws URISyntaxException, ResponseException, JSONException, IOException {
        UUID applicationId = UUID.randomUUID();

        addApplicationLink(getAtlassianContent(new ApplicationId(applicationId.toString()), "refapp START", PRODUCT2_BASE_URL, PRODUCT2_BASE_URL));

        String url = String.format(SINGLE_APPLICATIONLINK_REST_ENDPOINT, applicationId);

        final HttpClientResponse response = RestTestHelper.postRestResponse(
                RestTestHelper.getDefaultUser(),
                url,
                getAtlassianContent(
                        new ApplicationId(applicationId.toString()), "refapp END", PRODUCT2_BASE_URL, PRODUCT2_BASE_URL + "X"));

        assertThat(response.getStatusCode(), is(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void addApplicationLink3rdParty() throws URISyntaxException, ResponseException, JSONException, IOException {
        addApplicationLink(get3rdPartyContent());
    }

    @Test
    public void verifyAddApplicationLink3rdPartyFailsWithoutRpcUrl()
            throws URISyntaxException, ResponseException, JSONException, IOException {
        addApplicationLink(get3rdPartyContent());
    }

    @Test
    public void putAuthenticationProviderNoConfig()
            throws URISyntaxException, ResponseException, JSONException, IOException {
        UUID applicationId = addApplicationLink(get3rdPartyContent());

        AuthenticationProviderEntity authProvider = new AuthenticationProviderEntity(null, null,
                OAuthAuthenticationProvider.class.getName(), ImmutableMap.<String, String>of());

        registerProvider(applicationId, toJson(authProvider), HttpServletResponse.SC_CREATED);
    }

    @Test
    public void putAuthenticationProvider3rdParty()
            throws URISyntaxException, ResponseException, JSONException, IOException {
        UUID applicationId = addApplicationLink(get3rdPartyContent());

        AuthenticationProviderEntity authProvider = new AuthenticationProviderEntity(null, null,
                OAuthAuthenticationProvider.class.getName(),
                ImmutableMap.of(SERVICE_PROVIDER_ACCESS_TOKEN_URL, "http://6",
                        SERVICE_PROVIDER_REQUEST_TOKEN_URL, "http://55",
                        AUTH_CONFIG_CONSUMER_KEY_OUTBOUND, "2",
                        SERVICE_PROVIDER_AUTHORIZE_URL, "http://77"));

        registerProvider(applicationId, toJson(authProvider), HttpServletResponse.SC_CREATED);
    }

    private String get3rdPartyContent() throws URISyntaxException, IOException {
        URI url = new URI(URL_GENERIC_APP);
        return toJson(createApplinkEntity(null, false, false, "search", url, url, new TypeId("generic")));
    }

    private ApplicationLinkEntity createApplinkEntity(ApplicationId id, boolean primary, boolean system, String name, URI displayUrl, URI rpcUrl, TypeId typeId) {
        return new ApplicationLinkEntity(id, typeId, name, displayUrl, null, null, rpcUrl, primary, system, null);
    }

    private String getAtlassianContent(UUID applicationId) throws URISyntaxException, IOException {

        ApplicationId id = new ApplicationId(applicationId.toString());
        String name = PRODUCT2.getInstanceId();
        URI displayUrl = new URI(PRODUCT2_BASE_URL);
        URI rpcUrl = new URI(PRODUCT2_BASE_URL);
        return getAtlassianContent(id, name, displayUrl.toASCIIString(), rpcUrl.toASCIIString());
    }

    private String getAtlassianContent(ApplicationId id, String name, String displayUrl, String rpcUrl)
            throws URISyntaxException, IOException {
        return toJson(createApplinkEntity(id, true, false, name, new URI(displayUrl), new URI(rpcUrl), new TypeId("refapp")));
    }

    private UUID addApplicationLink(String content) throws URISyntaxException, ResponseException, JSONException {
        String url = String.format(SINGLE_APPLICATIONLINK_REST_ENDPOINT, "");

        final HttpClientResponse response = RestTestHelper.putRestResponse(RestTestHelper.getDefaultUser(), url, content);

        assertThat(response.getStatusCode(), is(HttpServletResponse.SC_CREATED));

        return getApplicationIdFromLink(response);
    }

    private UUID getApplicationIdFromLink(HttpClientResponse response) throws JSONException, ResponseException {
        JSONObject object = new JSONObject(response.getResponseBodyAsString());
        JSONArray resources = (JSONArray) object.get("resources-created");
        assertEquals(1, resources.length());
        String createdResource = ((JSONObject) resources.get(0)).get("href").toString();
        String id = createdResource.substring(createdResource.lastIndexOf('/') + 1);
        return UUID.fromString(id);
    }

    private void updateApplicationLink(UUID applicationId, String content) throws URISyntaxException {
        String url = String.format(SINGLE_APPLICATIONLINK_REST_ENDPOINT, applicationId);

        final HttpClientResponse response = RestTestHelper.postRestResponse(RestTestHelper.getDefaultUser(), url, content);

        assertThat(response.getStatusCode(), is(HttpServletResponse.SC_OK));
    }


    private HttpClientResponse registerProvider(UUID applicationId, String content, int expectedStatus) throws URISyntaxException {
        String url = String.format(PUT_AUTHENTICATIONPROVIDER_REST_ENDPOINT, applicationId);

        final HttpClientResponse response = RestTestHelper.putRestResponse(RestTestHelper.getDefaultUser(), url, content);

        assertThat(response.getStatusCode(), is(expectedStatus));
        return response;
    }
}
