package it.com.atlassian.applinks.smoke.v2;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.applinks.pageobjects.TrustedAppsApplinksClient;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.smoke.AbstractDefaultSmokeTest;
import it.com.atlassian.applinks.testing.categories.SmokeTest;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.rules.ApplinksFeaturesRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.annotation.Nonnull;

import static com.atlassian.applinks.internal.feature.ApplinksFeatures.V3_UI;
import static com.atlassian.applinks.pageobjects.LoginClient.loginAsSysadmin;
import static com.atlassian.applinks.pageobjects.OAuthApplinksClient.createOneWayLink;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static it.com.atlassian.applinks.testing.product.TestedInstances.fromProductInstance;

/**
 * Smoke test for the V2 UI in Atlassian products
 */
@SuppressWarnings("unchecked")
@Category({SmokeTest.class})
public class ApplinksV2UiSmokeTest extends AbstractDefaultSmokeTest {
    @Rule
    public final ApplinksFeaturesRule applinksFeaturesRule;

    public ApplinksV2UiSmokeTest(@Nonnull GenericTestedProduct mainProduct,
                                 @Nonnull GenericTestedProduct backgroundProduct) {
        super(mainProduct, backgroundProduct);
        this.applinksFeaturesRule = new ApplinksFeaturesRule(fromProductInstance(mainInstance)).withDisabledFeatures(V3_UI);
    }

    @Test
    public void shouldDisplayTwoWayLink() {
        OAuthApplinksClient.createReciprocal3LO2LOLinkViaRest(mainProduct, backgroundProduct);
        loginAsSysadmin(mainProduct);
        final ListApplicationLinkPage listApplicationLinkPage = goToApplinksAdmin();
        listApplicationLinkPage.clearDialogs();

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(),
                contains(hasDisplayUrl(backgroundInstance.getBaseUrl())));
    }

    @Test
    public void shouldDisplayOneWayLinkFromMainToBackgroundProduct() {
        loginAsSysadmin(mainProduct);
        ListApplicationLinkPage listApplicationLinkPage = createOneWayLink(mainProduct, backgroundProduct);
        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(),
                contains(hasDisplayUrl(backgroundInstance.getBaseUrl())));
    }

    @Test
    public void shouldDisplayTwoWayTrustedLink() {
        TrustedAppsApplinksClient.createReciprocalTrustedAppsLinkViaBackDoor(mainProduct, backgroundProduct);
        loginAsSysadmin(mainProduct);
        final ListApplicationLinkPage listApplicationLinkPage = goToApplinksAdmin();
        listApplicationLinkPage.clearDialogs();

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(),
                contains(hasDisplayUrl(backgroundInstance.getBaseUrl())));
    }

    private ListApplicationLinkPage goToApplinksAdmin() {
        return mainProduct.visit(ListApplicationLinkPage.class);
    }
}
