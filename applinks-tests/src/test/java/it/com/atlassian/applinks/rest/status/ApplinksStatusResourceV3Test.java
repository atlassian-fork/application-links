package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.common.auth.oauth.ApplinksOAuth;
import com.atlassian.applinks.internal.common.rest.model.oauth.RestConsumer;
import com.atlassian.applinks.internal.status.error.ApplinkErrorType;
import com.atlassian.applinks.internal.status.oauth.ApplinkOAuthStatus;
import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRequest;
import com.atlassian.applinks.test.rest.status.ApplinkStatusRestTester;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.TestRule;

import java.util.UUID;

import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDefaultOAuthConfig;
import static com.atlassian.applinks.internal.common.status.oauth.OAuthConfig.createDisabledConfig;
import static com.atlassian.applinks.test.authentication.TestAuthentication.anonymous;
import static com.atlassian.applinks.test.data.applink.config.OAuthConsumerApplinkConfigurator.addOAuthConsumer;
import static com.atlassian.applinks.test.mock.MockOAuthConsumerFactory.createRsaConsumer;
import static com.atlassian.applinks.test.rest.data.applink.DeleteApplinkConfigurator.deleteApplink;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableDefaultOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuth;
import static com.atlassian.applinks.test.rest.data.applink.config.OAuthApplinkConfigurator.enableOAuthWithImpersonation;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectErrorDetails;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectLocalAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectNoRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectRemoteAuthentication;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusError;
import static com.atlassian.applinks.test.rest.specification.ApplinksStatusExpectations.expectStatusWorking;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_ADMIN_BETTY;
import static it.com.atlassian.applinks.testing.ApplinksAuthentications.REFAPP_USER_BARNEY;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static javax.ws.rs.core.Response.Status.BAD_REQUEST;
import static javax.ws.rs.core.Response.Status.FORBIDDEN;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.UNAUTHORIZED;
import static org.hamcrest.Matchers.is;

/**
 * REST tests for Status resource, where 2 Atlassian Atlassian apps running that Status API (V3) are connected. This
 * includes working status and config error cases. Also tests basic invariants like security and extra parameter
 * validation.
 *
 * @see com.atlassian.applinks.internal.status.error.ApplinkErrorCategory#CONFIG_ERROR
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceV3Test {
    private static final TestAuthentication DEFAULT_AUTH_ADMIN = REFAPP_ADMIN_BETTY;

    @Rule
    public final TestRule restTestRules = ApplinksRuleChain.forRestTests(REFAPP1, REFAPP2);
    @Rule
    public final TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    private final ApplinkStatusRestTester refapp1statusTester = new ApplinkStatusRestTester(REFAPP1);

    @Test
    public void notFoundGivenNotExistingLinkId() {
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(UUID.randomUUID().toString())
                .authentication(DEFAULT_AUTH_ADMIN)
                .expectStatus(NOT_FOUND)
                .build());
    }

    @Test
    public void getStatusNotAvailableToAnonymous() {
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(anonymous())
                .expectStatus(UNAUTHORIZED)
                .build());
    }

    @Test
    public void getStatusNotAvailableToNonAdmins() {
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(REFAPP_USER_BARNEY)
                .expectStatus(FORBIDDEN)
                .build());
    }

    @Test
    public void nonAbsoluteAuthorisationCallbackParameter() {
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .authorisationCallback("/non/absolute")
                .expectStatus(BAD_REQUEST)
                .build());
    }

    @Test
    public void invalidAuthorisationCallbackParameter() {
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .authorisationCallback("???this\\is\\invalid::")
                .expectStatus(BAD_REQUEST)
                .build());
    }

    @Test
    public void statusWorkingDefaultOAuth() {
        applink.configure(enableDefaultOAuth());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusWorking(applink.from().id()))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());
    }

    @Test
    public void statusWorkingOff() {
        // OAuth off on both sides

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.DISABLED))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.OFF))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.OFF))
                .build());
    }

    @Test
    public void statusOAuthConfigMismatch() {
        applink.configureFrom(enableDefaultOAuth())
                .configureTo(enableOAuthWithImpersonation());

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_MISMATCH))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.IMPERSONATION))
                .build());
    }

    @Test
    public void statusOAuthConfigMismatchRemoteOff() {
        applink.configureFrom(enableDefaultOAuth()); // only enable OAuth on one side

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.AUTH_LEVEL_MISMATCH))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.OFF))
                .build());
    }

    @Test
    public void statusNoRemoteApplink() {
        applink.configureFrom(enableDefaultOAuth());
        applink.configureTo(deleteApplink()); // delete applink on remote end

        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.NO_REMOTE_APPLINK))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectNoRemoteAuthentication())
                .build());
    }

    @Test
    public void statusErrorOAuthProblemConsumerKeyUnknown() {
        applink.configureFrom(enableDefaultOAuth());
        // add a fake consumer that doesn't match REFAPP1
        applink.configureTo(
                enableOAuth(createDisabledConfig(), createDefaultOAuthConfig()),
                addOAuthConsumer(new RestConsumer(createRsaConsumer(true, true, false)))
        );

        // expecting OAuth problem with remote authentication available - because it is retrieved anonymously before
        // OAuth is validated
        refapp1statusTester.get(new ApplinkStatusRequest.Builder(applink.from())
                .authentication(DEFAULT_AUTH_ADMIN)
                .specification(expectStatusError(applink.from().id(), ApplinkErrorType.OAUTH_PROBLEM))
                .specification(expectErrorDetails(is(ApplinksOAuth.PROBLEM_CONSUMER_KEY_UNKNOWN)))
                .specification(expectLocalAuthentication(ApplinkOAuthStatus.DEFAULT))
                .specification(expectRemoteAuthentication(ApplinkOAuthStatus.DEFAULT))
                .build());
    }
}
