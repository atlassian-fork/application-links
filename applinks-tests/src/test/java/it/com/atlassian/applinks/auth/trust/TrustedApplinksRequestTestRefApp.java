package it.com.atlassian.applinks.auth.trust;

import com.atlassian.webdriver.applinks.page.TrustedAppsPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.TrustedAppsTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static junit.framework.Assert.assertTrue;

/**
 * Tests for the simple case of requesting a url from a trusted application.
 */
@Category({TrustedAppsTest.class, RefappTest.class})
public class TrustedApplinksRequestTestRefApp extends V2AbstractApplinksTest {

    @Rule
    public TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    @Before
    public void setUp() throws Exception {
        applink.configure(enableTrustedApps());
        loginAsSysadmin(PRODUCT, PRODUCT2);
    }

    @Test
    public void willSuccessfullyRequestPageFromTrustedApplication() throws Exception {
        final String response = PRODUCT.visit(TrustedAppsPage.class).getTrustedResponse();
        assertTrue(response.contains("You are user " + SYSADMIN_USERNAME + " and have been redirected and your initial request was a GET"));
    }
}
