package it.com.atlassian.applinks.testing.product;

import com.atlassian.pageobjects.Page;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.binder.BrowserModule;
import com.atlassian.pageobjects.binder.InjectPageBinder;
import com.atlassian.pageobjects.binder.LoggerModule;
import com.atlassian.pageobjects.binder.StandardModule;
import com.atlassian.pageobjects.elements.ElementModule;
import com.atlassian.pageobjects.elements.timeout.TimeoutsModule;
import com.atlassian.webdriver.AtlassianWebDriverModule;
import com.atlassian.webdriver.pageobjects.DefaultWebDriverTester;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Can be used to drive {@code WebDriver} tests in any product.
 * <p>
 * NOTE: this doesn't have defaults defined so it can <i>only</i> be instantiated with a {@code ProductInstance}.
 * </p>
 *
 * @since 4.3
 */
public class GenericTestedProduct implements TestedProduct<WebDriverTester> {
    private final PageBinder pageBinder;
    private final WebDriverTester webDriverTester;
    private final ProductInstance productInstance;

    public GenericTestedProduct(TestedProductFactory.TesterFactory<WebDriverTester> testerFactory,
                                ProductInstance productInstance) {
        this.productInstance = checkNotNull(productInstance);

        this.webDriverTester = testerFactory == null ? new DefaultWebDriverTester() : testerFactory.create();
        this.pageBinder = new InjectPageBinder(productInstance, webDriverTester,
                new StandardModule(this),
                new AtlassianWebDriverModule(this),
                new BrowserModule(),
                new LoggerModule(LoggerFactory.getLogger("com.atlassian.applinks.WEBDRIVER")),
                new ElementModule(),
                new TimeoutsModule()
        );
    }

    @Override
    public <P extends Page> P visit(Class<P> pageClass, Object... args) {
        return pageBinder.navigateToAndBind(pageClass, args);
    }

    @Override
    public PageBinder getPageBinder() {
        return pageBinder;
    }

    @Override
    public ProductInstance getProductInstance() {
        return productInstance;
    }

    @Override
    public WebDriverTester getTester() {
        return webDriverTester;
    }
}
