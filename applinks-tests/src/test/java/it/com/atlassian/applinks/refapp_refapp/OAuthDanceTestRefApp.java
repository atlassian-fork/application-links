package it.com.atlassian.applinks.refapp_refapp;

import java.net.InetAddress;
import java.util.Locale;

import com.atlassian.webdriver.applinks.externalcomponent.OAuthConfirmPage;
import com.atlassian.webdriver.applinks.page.AuthIFrameTestPage;
import com.atlassian.webdriver.applinks.page.AuthTestPage;

import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.not;

@Category(RefappTest.class)
public class OAuthDanceTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public RuleChain ruleChain = CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2);

    @Before
    public void setUp() throws Exception {
        login(PRODUCT, PRODUCT2);
    }

    @Test
    public void assertThatOAuthInsideIFrameShowsAuthenticateLinkIfUserIsNotAuthenticated() throws Exception {
        assertTrue(PRODUCT.visit(AuthIFrameTestPage.class) //
                .canAuthenticate());
    }

    @Test
    public void assertThatOAuthDanceInsideIFrameIsSuccessful() throws Exception {
        AuthIFrameTestPage authIFrameTestPage = PRODUCT.visit(AuthIFrameTestPage.class) //
                .authenticate() //
                // wait for the oauth popup window
                .confirmHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);

        // verify confirmation text displayed
        assertTrue(authIFrameTestPage.hasConfirmedAuthorization("applinks.refapp", ADMIN_USERNAME));
        // verify that the OAuth confirmation message has been displayed
        assertTrue(authIFrameTestPage.hasOAuthConfirmationMessage());
        assertTrue(authIFrameTestPage.hasOAuthAccessTokens());
    }

    @Test
    public void assertThatOAuthDanceDisplaysUsernameAndDomainBeforeApproval() throws Exception {
        OAuthConfirmPage<AuthIFrameTestPage> oauthConfirmPage = PRODUCT.visit(AuthIFrameTestPage.class)
                .authenticate()
                .handleWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);

        try {
            String text = oauthConfirmPage.getConfirmationText();
            String hostName = InetAddress.getLocalHost().getHostName();
            assertThat("The text should display the hostname", text.toLowerCase(Locale.ENGLISH), containsString(hostName.toLowerCase(Locale.ENGLISH)));
            assertThat("The text should display the user name", text.toLowerCase(Locale.ENGLISH), containsString(ADMIN_USERNAME.toLowerCase(Locale.ENGLISH)));
        } finally {
            // It's necessary to click "deny" so that the window closes and the next test can start with a clean environment.
            oauthConfirmPage.denyHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);
        }
    }

    @Test
    public void testOAuthDanceInsideIFrameRefreshesTheIFrameButNotThePage() throws Exception {
        AuthIFrameTestPage authIFrameTestPage = PRODUCT.visit(AuthIFrameTestPage.class);

        String timeBeforeAuth = authIFrameTestPage.getTime();

        String timeAfterAuth = authIFrameTestPage //
                .authenticate() //
                // wait for the oauth popup window
                .confirmHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD) //
                .getTime();

        // make sure that the timestamp did not change, but the iframe is refreshed
        assertNotNull(timeBeforeAuth);
        assertNotNull(timeAfterAuth);
        assertEquals(timeBeforeAuth, timeAfterAuth);

        assertTrue(authIFrameTestPage.hasConfirmedAuthorization("applinks.refapp", ADMIN_USERNAME));
    }

    @Test
    public void assertThatOAuthShowsAuthenticateLinkIfUserIsNotAuthenticated() throws Exception {
        assertTrue(PRODUCT.visit(AuthTestPage.class) //
                .canAuthenticate());
    }


    @Test
    public void assertThatOAuthShowsJSAuthenticateLinkIfUserIsNotAuthenticated() throws Exception {
        assertTrue(PRODUCT.visit(AuthTestPage.class) //
                .hasJSAuthenticateLink());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatJSAuthenticateMessageHasCorrectBannerText() throws Exception {
        // Note, the equivalent test for messages created from Java is handled by the plugin unit tests
        assertThat(PRODUCT.visit(AuthTestPage.class).getJsAuthMessageText(),
                allOf(containsString("Additional information may be available, please"),
                        containsString("authenticate")));
    }

    @Test
    public void assertThatJSAuthenticateMessageHasCorrectLinkUri() throws Exception {
        AuthTestPage page = PRODUCT.visit(AuthTestPage.class);
        assertThat(page.getJsAuthenticateLinkUri(),
                equalTo(page.getAuthenticateLinkUri()));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatJSInlineAuthenticateMessageHasCorrectText() throws Exception {
        // Note, the equivalent test for messages created from Java is handled by the plugin unit tests
        assertThat(PRODUCT.visit(AuthTestPage.class, "?inline=true&description=MyDescription").getJsAuthMessageText(),
                allOf(containsString("MyDescription -"),
                        containsString("Authenticate"),
                        containsString("to see additional information")));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void assertThatJSInlineAuthenticateMessageOmitsHyphenWhenCustomContentIsAbsent() throws Exception {
        assertThat(PRODUCT.visit(AuthTestPage.class, "?inline=true&description=").getJsAuthMessageText(),
                allOf(not(containsString(" -")),
                        containsString("Authenticate"),
                        containsString("to see additional information")));
    }

    @Test
    public void assertThatJSInlineAuthenticateMessageHasCorrectLinkUri() throws Exception {
        AuthTestPage page = PRODUCT.visit(AuthTestPage.class, "?inline=true");
        assertThat(page.getJsAuthenticateLinkUri(),
                equalTo(page.getAuthenticateLinkUri()));
    }

    @Test
    public void assertThatOAuthDanceWithoutIFrameIsSuccessful() throws Exception {
        AuthTestPage authTestPage = PRODUCT.visit(AuthTestPage.class) //
                .authenticate() //
                // wait for the oauth popup window
                .confirmHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);

        // verify confirmation text displayed
        assertTrue(authTestPage.hasConfirmedAuthorization("applinks.refapp", ADMIN_USERNAME));

        // note that the regular confirmation message from will not be present in this case,
        // because the plugin has no way to display one after a full-page refresh; changing
        // that behavior requires using an event handler as in the next text.
    }

    //
    @Test
    public void assertThatApprovalEventHandlerIsTriggeredAndCanDisableRefresh() throws Exception {
        AuthTestPage authTestPage = PRODUCT.visit(AuthTestPage.class);

        authTestPage
                // select the "disable auto-refresh" checkbox, causing this page to intercept the
                // "applink.auth.approved" event from the applinks code, disabling the default
                // refresh behavior and displaying its own message instead
                .disableRefresh() //
                // Authenticate
                .authenticate() //
                // wait for the oauth popup window
                .confirmHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);

        // verify that the main window has not been refreshed - authenticate link is still there
        assertTrue(authTestPage.canAuthenticate());
        // verify that the OAuth confirmation message has been displayed
        assertTrue(authTestPage.hasOAuthConfirmationMessage());
        // verify that it contains a link to the OAuth Access Tokens page
        assertTrue(authTestPage.hasOAuthAccessTokens());
        // verify that our custom message has been displayed
        String resultText = authTestPage.getResultText();
        assertTrue(resultText.contains("User has authorized"));
        assertTrue(resultText.contains("refresh disabled"));
    }

    @Test
    public void assertThatDenialEventHandlerIsTriggered() throws Exception {
        AuthTestPage authTestPage = PRODUCT.visit(AuthTestPage.class) //
                .authenticate() //
                // wait for the oauth popup window then deny
                .denyHandlingWebLoginIfRequired(ADMIN_USERNAME, ADMIN_PASSWORD);

        assertTrue(authTestPage.canAuthenticate());
        String resultText = authTestPage.getResultText();
        assertTrue(resultText.contains("User has denied access"));
    }
}
