package it.com.atlassian.applinks.testing.categories;

/**
 * JUnit Category to classify tests targeting Configuration functionality.
 *  WARNING: this category is not bound to any integration tests.
 *  if you just add this category to your test it <b>will not be executed</b>
 *
 * @since SHPXXVII-43
 */
public interface ConfigurationTest {
}
