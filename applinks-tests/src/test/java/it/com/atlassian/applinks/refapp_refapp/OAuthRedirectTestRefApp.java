package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.webdriver.applinks.page.OAuthRedirectPage;

import it.com.atlassian.applinks.testing.categories.RefappTest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;

import static org.hamcrest.Matchers.containsString;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @since 3.2
 */
@Category(RefappTest.class)
public class OAuthRedirectTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public RuleChain ruleChain = CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2);

    @Before
    public void setUp() {
        login(PRODUCT, PRODUCT2);
    }

    @Test
    public void testOAuthRedirect() {
        String response = PRODUCT.visit(OAuthRedirectPage.class)
                .approve(ADMIN_USERNAME, ADMIN_PASSWORD)
                .getOAuthResponse();

        assertThat(response, containsString("You are user " + ADMIN_USERNAME +
                " and have been redirected and your initial request was a GET"));
    }

}
