package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.applinks.test.rest.backdoor.SystemPropertiesBackdoor;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Test rule that overrides base URL system property on a remote Refapp instance and resets the override after each test.
 */
public class RefappBaseUrlRule extends TestWatcher {
    private static final String REFAPP_BASEURL_PROPERTY = "refapp.baseurl";

    private final SystemPropertiesBackdoor backdoor;

    public RefappBaseUrlRule(@Nonnull String instanceUrl) {
        this.backdoor = new SystemPropertiesBackdoor(instanceUrl);
    }

    public RefappBaseUrlRule(@Nonnull TestedInstance testedInstance) {
        this.backdoor = new SystemPropertiesBackdoor(testedInstance);
    }

    @Override
    protected void finished(Description description) {
        backdoor.clearProperty(REFAPP_BASEURL_PROPERTY);
    }

    public void setBaseUrl(@Nonnull String baseUrl) {
        checkNotNull(baseUrl, "baseUrl");
        backdoor.setProperty(REFAPP_BASEURL_PROPERTY, baseUrl);
    }
}
