package it.com.atlassian.applinks.testing.product;

import com.atlassian.applinks.test.product.SupportedProducts;
import com.atlassian.applinks.test.product.TestedInstance;
import com.atlassian.pageobjects.ProductInstance;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.net.InetAddress;

import static com.atlassian.applinks.test.product.SupportedProducts.REFAPP;
import static com.google.common.base.Preconditions.checkNotNull;

public enum ProductInstances implements ProductInstance, TestedInstance {
    REFAPP1("refapp1", REFAPP, 5990, "/refapp1"),
    REFAPP2("refapp2", REFAPP, 5992, "/refapp2"),
    REFAPP_APPLINKSV3("refapp-applinks3", REFAPP, 5993, "/refapp3"),

    BAMBOO("bamboo", SupportedProducts.BAMBOO, 6990, "/bamboo"),

    BITBUCKET("bitbucket", SupportedProducts.BITBUCKET, 7990, "/bitbucket"),

    CONFLUENCE("confluence", SupportedProducts.CONFLUENCE, 1990, "/confluence"),

    JIRA("jira", SupportedProducts.JIRA, 2990, "/jira"),
    JIRA_APPLINKSV3("jira-applinksv3", SupportedProducts.JIRA, 2993, "/jira"),

    FECRU("fecru", SupportedProducts.FECRU, 3990, "/fecru");

    @Nullable
    public static ProductInstances forInstanceId(@Nonnull String id) {
        checkNotNull(id, "id");
        for (ProductInstances value : values()) {
            if (value.instanceId.equals(id)) {
                return value;
            }
        }
        return null;
    }

    @Nullable
    public static ProductInstances fromProductInstance(@Nonnull ProductInstance instance) {
        if (instance instanceof ProductInstances) {
            return (ProductInstances) instance;
        } else {
            return forInstanceId(instance.getInstanceId());
        }
    }

    private final String instanceId;
    private final int httpPort;
    private final String contextPath;
    private final String loopbackUrl;

    private final SupportedProducts product;

    private String baseUrl;

    ProductInstances(String instanceId, SupportedProducts product, int httpPortForTests, String contextPathForTests) {
        this.instanceId = checkNotNull(instanceId, "instanceId");
        this.product = product;
        final String baseUrl = System.getProperty("baseurl." + instanceId);

        if (baseUrl != null) {
            // running within an AMPS IntegrationTestMojo invocation - read HTTP address from standard AMPS properties
            this.httpPort = Integer.parseInt(System.getProperty("http." + instanceId + ".port"));
            this.contextPath = System.getProperty("context." + instanceId + ".path");
        } else {
            // running outside of AMPS (e.g. from IDE), need to set default env vars
            this.httpPort = httpPortForTests;
            this.contextPath = contextPathForTests;
        }
        this.loopbackUrl = "http://127.0.0.1:" + httpPort + contextPath;
    }

    public static String getLocalHostName(ProductInstance productInstance) {
        // some products switched to H2 DB and don't support hostname as their base URL any more
        if (JIRA.matches(productInstance) || BITBUCKET.matches(productInstance)) {
            return "localhost";
        } else {
            try {
                return InetAddress.getLocalHost().getHostName();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    public static boolean matches(@Nonnull SupportedProducts product, @Nonnull ProductInstance productInstance) {
        checkNotNull(product, "product");
        checkNotNull(productInstance, "productInstance");

        return productInstance.getInstanceId().startsWith(product.getProductId());
    }

    @Nonnull
    public String getInstanceId() {
        return instanceId;
    }

    @Nonnull
    public SupportedProducts getProduct() {
        return product;
    }

    @Nonnull
    public String getBaseUrl() {
        if (baseUrl == null) {
            baseUrl = calculateBaseUrl();
        }
        return baseUrl;
    }

    public String getLoopbackUrl() {
        return loopbackUrl;
    }

    public int getHttpPort() {
        return httpPort;
    }

    public String getContextPath() {
        return contextPath;
    }

    public boolean matches(@Nonnull ProductInstance productInstance) {
        checkNotNull(productInstance, "productInstance");

        return this == productInstance || matches(getProduct(), productInstance);
    }

    private String calculateBaseUrl() {
        return String.format("http://%s:%d%s", getLocalHostName(this), httpPort, contextPath).toLowerCase();
    }
}
