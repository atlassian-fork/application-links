package it.com.atlassian.applinks.auth.trust;

import com.atlassian.webdriver.applinks.page.TrustedAppsRedirectPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.TrustedAppsTest;
import it.com.atlassian.applinks.testing.rules.TestApplinkRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.applinks.test.data.applink.config.TrustedAppsApplinkConfigurator.enableTrustedApps;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP1;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static junit.framework.Assert.assertTrue;

/**
 * Tests to verify the redirect process when dealing with trusted app links.
 * We need to ensure that the actual request has its authentication re-encoded to ensure
 * it is for the correct url.
 *
 * @since 3.11.0
 */
@Category({TrustedAppsTest.class, RefappTest.class})
public class TrustedApplinksRedirectTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public TestApplinkRule applink = TestApplinkRule.forProducts(REFAPP1, REFAPP2);

    @Before
    public void setUp() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);
        applink.configure(enableTrustedApps());
    }

    @Test
    public void willSuccessfullyRequestRedirectedPageViaATrustedRequest() throws Exception {
        final String response = PRODUCT.visit(TrustedAppsRedirectPage.class).getTrustedResponse();
        assertTrue(response.contains("You are user " + SYSADMIN_USERNAME
                + " and have been redirected and your initial request was a GET"));
    }
}