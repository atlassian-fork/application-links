package it.com.atlassian.applinks.testing.backdoor.analytics;

import com.atlassian.applinks.internal.rest.RestUrl;
import com.atlassian.applinks.test.rest.AbstractRestRequest;
import com.atlassian.applinks.test.rest.BaseRestTester;
import com.atlassian.applinks.test.rest.CustomizableRestRequest;
import com.atlassian.pageobjects.ProductInstance;
import com.google.common.collect.ImmutableMap;
import com.jayway.restassured.specification.ResponseSpecification;
import it.com.atlassian.applinks.testing.ApplinksAuthentications;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.inject.Inject;

import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.REST_PATH;
import static com.atlassian.applinks.test.rest.url.ApplinksRestUrls.VERSION_LATEST;

/**
 * Backdoor to turn analytics logging on and off and collect analytics logs.
 *
 * @since 4.3
 */
public final class AnalyticsBackdoor {
    private final BaseRestTester analyticsConfigTester;
    private final BaseRestTester analyticsReportTester;

    @Inject
    public AnalyticsBackdoor(@Nonnull ProductInstance product) {
        this.analyticsConfigTester = new BaseRestTester(analyticsConfigPath(product), ApplinksAuthentications.SYSADMIN);
        this.analyticsReportTester = new BaseRestTester(analyticsReportPath(product), ApplinksAuthentications.SYSADMIN);
    }

    public void clearLogs() {
        analyticsReportTester.delete(AnalyticsConfigRequest.newRequest());
    }

    /**
     * Get logs using {@link AnalyticsLogMode#DEFAULT default log mode}, with given {@code expectations} about the
     * results
     *
     * @param expectations expectations for the logs
     */
    public void getLogsExpecting(@Nonnull ResponseSpecification... expectations) {
        getLogs(AnalyticsLogRequest.expecting(expectations));
    }

    /**
     * Get logs using the provided {@code logRequest}.
     *
     * @param logRequest request
     */
    public void getLogs(@Nonnull AnalyticsLogRequest logRequest) {
        analyticsReportTester.get(logRequest);
    }

    public void enableAnalytics() {
        analyticsConfigTester.put(new CustomizableRestRequest.Builder()
                .addPath("enable")
                .body(ImmutableMap.of("analyticsEnabled", "true"))
                .build());
    }

    public void turnCapturingOn() {
        analyticsReportTester.put(AnalyticsConfigRequest.turnOn());
    }

    public void turnCapturingOff() {
        analyticsReportTester.put(AnalyticsConfigRequest.turnOff());
    }

    private static RestUrl analyticsConfigPath(@Nonnull ProductInstance product) {
        return analyticsRestPath(product)
                .add("config");
    }

    private static RestUrl analyticsReportPath(@Nonnull ProductInstance product) {
        return analyticsRestPath(product)
                .add("report");
    }

    private static RestUrl analyticsRestPath(@Nonnull ProductInstance product) {
        return RestUrl.forPath(product.getBaseUrl())
                .add(REST_PATH)
                .add("analytics")
                .add(VERSION_LATEST);
    }

    public static final class AnalyticsConfigRequest extends AbstractRestRequest {
        static AnalyticsConfigRequest newRequest() {
            return new AnalyticsConfigRequest.Builder(true).build();
        }

        static AnalyticsConfigRequest turnOn() {
            return newRequest();
        }

        static AnalyticsConfigRequest turnOff() {
            return new AnalyticsConfigRequest.Builder(false).build();
        }

        private final boolean on;

        private AnalyticsConfigRequest(@Nonnull Builder builder) {
            super(builder);
            this.on = builder.on;
        }

        @Nonnull
        @Override
        protected RestUrl getPath() {
            return RestUrl.EMPTY;
        }

        @Nullable
        @Override
        protected Object getBody() {
            return ImmutableMap.of("capturing", on);
        }

        static class Builder extends AbstractBuilder<Builder, AnalyticsConfigRequest> {
            private final boolean on;

            Builder(boolean on) {
                this.on = on;
            }

            @Nonnull
            @Override
            public AnalyticsConfigRequest build() {
                return new AnalyticsConfigRequest(this);
            }

            @Nonnull
            @Override
            protected Builder self() {
                return this;
            }
        }
    }

}
