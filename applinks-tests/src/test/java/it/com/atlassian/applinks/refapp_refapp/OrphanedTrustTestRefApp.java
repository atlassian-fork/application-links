package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.applinks.pageobjects.TrustedAppsApplinksClient;
import com.atlassian.applinks.test.backdoor.OrphanedTrustBackdoor;
import com.atlassian.webdriver.applinks.AuthType;
import com.atlassian.webdriver.applinks.component.OrphanedTrustRelationshipsDialog;
import com.atlassian.webdriver.applinks.page.v1.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.categories.TrustedAppsTest;
import it.com.atlassian.applinks.v1.Creators;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category({TrustedAppsTest.class, RefappTest.class})
public class OrphanedTrustTestRefApp extends V2AbstractApplinksTest {
    private final OrphanedTrustBackdoor orphanedTrustBackdoor1 = new OrphanedTrustBackdoor(INSTANCE1);
    private final OrphanedTrustBackdoor orphanedTrustBackdoor2 = new OrphanedTrustBackdoor(INSTANCE2);

    @Before
    public void setUp() {
        loginAsSysadmin(PRODUCT, PRODUCT2);
        Creators.assertNoApplicationLinksAreConfigured();
    }

    /**
     * Test deletion of orphaned trust relationship
     */
    @Test
    public void testDeleteTrustRelationship() throws Exception {
        testDeleteRelationship(AuthType.TRUSTED_APPS);
    }

    /**
     * Test deletion of orphaned OAuth relationship
     */
    @Test
    public void testDeleteOAuthRelationship() throws Exception {
        testDeleteRelationship(AuthType.OAUTH);
    }

    /**
     * Creates orphaned trust relationship that will be automatically associated with a newly created link to REFAPP2,
     * then tests that the association is created automatically
     */
    @Test
    public void testCreateLinkForTrustRelationship() throws Exception {
        testCreateLinkForRelationship(AuthType.TRUSTED_APPS);
    }

    /**
     * Creates orphaned OAuth relationship that will be automatically associated with a newly created link to REFAPP2,
     * then tests that the association is created automatically
     */
    @Test
    public void testCreateLinkForOAuthRelationship() throws Exception {
        testCreateLinkForRelationship(AuthType.OAUTH);
    }

    private void testDeleteRelationship(AuthType authType) throws Exception {
        prepareLinks(authType);

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        assertTrue(listApplicationLinkPage.isOrphanedTrustWarningVisible());

        OrphanedTrustRelationshipsDialog orphanedTrustRelationshipsDialog = listApplicationLinkPage.showOrphanTrustRelationshipsDialog();
        waitUntilTrue(orphanedTrustRelationshipsDialog.isOpen());

        orphanedTrustRelationshipsDialog.deleteFirstEntry().confirm();

        listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        assertFalse(listApplicationLinkPage.isOrphanedTrustWarningVisible());
    }

    private void testCreateLinkForRelationship(AuthType authType) throws Exception {
        prepareLinks(authType);

        final ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        assertTrue(listApplicationLinkPage.isOrphanedTrustWarningVisible());

        // create application link to RefApp2
        OAuthApplinksClient.createTwoWayLink(PRODUCT, PRODUCT2);
        if (AuthType.TRUSTED_APPS.equals(authType)) {
            TrustedAppsApplinksClient.configureTrustedLink(PRODUCT, PRODUCT2);
        }

        // check that there are no more orphaned trust certs!
        assertFalse(listApplicationLinkPage.isOrphanedTrustWarningVisible());
    }

    private void prepareLinks(AuthType authType) throws Exception {
        if (authType == AuthType.OAUTH) {
            String id = orphanedTrustBackdoor2.getOrphanedOAuthId();
            orphanedTrustBackdoor1.setUpOrphanedOAuth(id);
        } else {
            String id = orphanedTrustBackdoor2.getOrphanedTrustedAppsId();
            orphanedTrustBackdoor1.setUpOrphanedTrustedApps(id);
        }
    }
}
