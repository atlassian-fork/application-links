package it.com.atlassian.applinks.smoke;

import com.atlassian.pageobjects.TestedProduct;
import com.atlassian.webdriver.pageobjects.WebDriverTester;
import it.com.atlassian.applinks.testing.product.GenericTestedProduct;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.ApplinksRuleChain;
import it.com.atlassian.applinks.testing.runner.ApplinksSmokeTestRunner;
import org.junit.Rule;
import org.junit.rules.RuleChain;
import org.junit.runner.RunWith;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkState;
import static it.com.atlassian.applinks.testing.product.ProductInstances.fromProductInstance;
import static java.util.Objects.requireNonNull;

/**
 * Abstract base class for {@code WebDriver} smoke tests. Provides constructor for 2 tested products ("main" and
 * "background"). The main product should be used to run the test, and the background in case an applink is needed
 * to execute the test.
 * <p>
 * This class also declares the {@link ApplinksRuleChain#forProducts(TestedProduct[]) default rule chain} for both
 * products to initialize them and clean up after each test.
 * </p>
 *
 * @since 4.3
 */
@RunWith(ApplinksSmokeTestRunner.class)
public abstract class AbstractApplinksSmokeTest<T extends TestedProduct<WebDriverTester>> {
    protected final T mainProduct;
    protected final GenericTestedProduct backgroundProduct;

    protected final ProductInstances mainInstance;
    protected final ProductInstances backgroundInstance;

    @Rule
    public final RuleChain applinksRuleChain;

    @SuppressWarnings("unchecked")
    protected AbstractApplinksSmokeTest(@Nonnull T mainProduct, @Nonnull GenericTestedProduct backgroundProduct) {
        this.mainProduct = requireNonNull(mainProduct, "mainProduct");
        this.backgroundProduct = requireNonNull(backgroundProduct, "backgroundProduct");
        this.mainInstance = fromProductInstance(mainProduct.getProductInstance());
        this.backgroundInstance = fromProductInstance(backgroundProduct.getProductInstance());
        this.applinksRuleChain = ApplinksRuleChain.forProducts(mainProduct, backgroundProduct);

        checkState(mainInstance != null, "Could not find ProductInstances for " + mainProduct.getProductInstance());
        checkState(backgroundInstance != null, "Could not find ProductInstances for "
                + backgroundProduct.getProductInstance());
    }

    // NOTE: no random helper methods here
}
