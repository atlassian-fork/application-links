package it.com.atlassian.applinks.refapp_refapp;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.AppLinkAdminLogin;
import com.atlassian.webdriver.applinks.externalcomponent.WebSudoPage;
import com.atlassian.webdriver.applinks.page.AbstractApplicationLinkPage;
import com.atlassian.webdriver.applinks.page.ApplinkAbstractPage;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.CreateAppLinkWith3LO2LORule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.rules.RuleChain;

import java.util.List;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(RefappTest.class)
public class AdminServletVisibilityTestRefApp extends V2AbstractApplinksTest {
    @Rule
    public final RuleChain ruleChain = CreateAppLinkWith3LO2LORule.forProducts(PRODUCT, PRODUCT2);

    private String applicationId;

    @Before
    public void setUp() {
        ListApplicationLinkPage listApplicationLink = loginAsSysadminAndGoTo(PRODUCT, ListApplicationLinkPage.class);

        Poller.waitUntilTrue(listApplicationLink.isApplinksListFullyLoaded());
        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        List<AbstractApplicationLinkPage.ApplicationLinkEntryRow> links = listApplicationLink.getApplicationLinks();
        applicationId = links.get(0).getApplicationId();

        logout(PRODUCT);
    }

    @Test
    public void verifyAdminCannotSeeSysadminOnlyServlet() {
        login(PRODUCT);

        IncomingOAuthConfigByDirectUrlPage incomingOAuthPage = PRODUCT.visit(IncomingOAuthConfigByDirectUrlPage.class, applicationId);
        assertTrue(incomingOAuthPage.isShown());

        IncomingTrustedAppConfigByDirectUrlPage incomingTAPage = PRODUCT.visit(IncomingTrustedAppConfigByDirectUrlPage.class, applicationId);
        assertFalse(incomingTAPage.isShown());

        AppLinkAdminLogin loginPage = PRODUCT.getPageBinder().bind(AppLinkAdminLogin.class, new WebSudoPage());
        assertFalse(loginPage.isAskingForAdmin());
        assertTrue(loginPage.isAskingForSysadmin());
    }

    @Test
    public void verifyUserCannotSeeAdminOnlyServlet() {
        loginAsUser(PRODUCT);

        IncomingOAuthConfigByDirectUrlPage incomingOAuthPage = PRODUCT.visit(IncomingOAuthConfigByDirectUrlPage.class, applicationId);
        assertFalse(incomingOAuthPage.isShown());

        AppLinkAdminLogin loginPage = PRODUCT.getPageBinder().bind(AppLinkAdminLogin.class, new WebSudoPage());
        assertTrue(loginPage.isAskingForAdmin());
        assertFalse(loginPage.isAskingForSysadmin());
    }

    abstract static class BaseAuthConfigByDirectUrlPage extends ApplinkAbstractPage {
        String applicationLinkId;

        @ElementBy(className = "auth-config")
        PageElement authConfigSection;

        BaseAuthConfigByDirectUrlPage(String applicationLinkId) {
            this.applicationLinkId = applicationLinkId;
        }

        boolean isShown() {
            return authConfigSection.isPresent();
        }
    }

    public static class IncomingOAuthConfigByDirectUrlPage extends BaseAuthConfigByDirectUrlPage {
        public IncomingOAuthConfigByDirectUrlPage(String applicationLinkId) {
            super(applicationLinkId);
        }

        public String getUrl() {
            return "/plugins/servlet/applinks/auth/conf/oauth/add-consumer-by-url/" + applicationLinkId + "?uiposition=local";
        }
    }

    public static class IncomingTrustedAppConfigByDirectUrlPage extends BaseAuthConfigByDirectUrlPage {
        public IncomingTrustedAppConfigByDirectUrlPage(String applicationLinkId) {
            super(applicationLinkId);
        }

        public String getUrl() {
            return "/plugins/servlet/applinks/auth/conf/trusted/inbound-ual/" + applicationLinkId;
        }
    }
}
