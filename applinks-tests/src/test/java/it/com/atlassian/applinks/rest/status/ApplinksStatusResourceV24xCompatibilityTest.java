package it.com.atlassian.applinks.rest.status;

import com.atlassian.applinks.internal.common.capabilities.ApplinksCapabilities;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.rules.ApplinksCapabilitiesRule;
import it.com.atlassian.applinks.testing.rules.ApplinksVersionOverrideRule;
import it.com.atlassian.applinks.testing.rules.OAuthRest40CompatibilityRule;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.experimental.categories.Category;

import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertThat;

/**
 * Tests Status resource running against an instance without Status API, running Applinks 4.x. This requires making
 * authenticated requests and may result in additional error conditions.
 *
 * @since 5.0
 */
@Category(RefappTest.class)
public class ApplinksStatusResourceV24xCompatibilityTest extends AbstractStatusResourceV2CompatibilityTest {
    // Refapp2 setup rules to act as V2 4.x:
    //  - disable status API by default
    //  - override version to 4.x
    //  - make OAuth REST APIs act as in 4.x
    @ClassRule
    public static final ApplinksCapabilitiesRule REFAPP2_CAPABILITIES = new ApplinksCapabilitiesRule(REFAPP2)
            .withDisabledCapabilities(ApplinksCapabilities.STATUS_API);
    @ClassRule
    public static final ApplinksVersionOverrideRule REFAPP2_VERSION = new ApplinksVersionOverrideRule(REFAPP2, "4.3.10");
    @Rule
    public final OAuthRest40CompatibilityRule refapp2OAuth4xCompatibility = new OAuthRest40CompatibilityRule(REFAPP2);

    protected void verifyRequestToV2Made() {
        // assert that request was made to Refapp2's 4.x applink authentication resource
        assertThat(refapp2OAuth4xCompatibility.getInvocationCount(), greaterThan(0));
    }

    protected void verifyRequestToV2NotMade() {
        // assert that no request was made to Refapp2's 4.x applink authentication resource
        assertThat(refapp2OAuth4xCompatibility.getInvocationCount(), equalTo(0));
    }
}
