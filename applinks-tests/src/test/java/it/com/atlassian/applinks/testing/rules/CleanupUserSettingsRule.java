package it.com.atlassian.applinks.testing.rules;

import com.atlassian.applinks.test.authentication.TestAuthentication;
import com.atlassian.applinks.test.rest.backdoor.SalBackdoor;
import com.atlassian.pageobjects.ProductInstance;
import com.google.common.collect.ImmutableList;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

import javax.annotation.Nonnull;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Removes user settings for specific users after test.
 */
public class CleanupUserSettingsRule extends TestWatcher {
    private final SalBackdoor backdoor;
    private final Iterable<TestAuthentication> authentications;

    public CleanupUserSettingsRule(@Nonnull SalBackdoor backdoor, @Nonnull TestAuthentication... authentications) {
        this.backdoor = checkNotNull(backdoor, "backdoor");
        this.authentications = ImmutableList.copyOf(checkNotNull(authentications, "authentications"));
    }

    public CleanupUserSettingsRule(@Nonnull ProductInstance product, @Nonnull TestAuthentication... authentications) {
        this(new SalBackdoor(product.getBaseUrl()), authentications);
    }

    @Override
    protected void starting(Description description) {
        for (TestAuthentication authentication : authentications) {
            backdoor.cleanUpUserSettings(authentication);
        }
    }

    @Override
    protected void finished(Description description) {
        for (TestAuthentication authentication : authentications) {
            backdoor.cleanUpUserSettings(authentication);
        }
    }
}


