package it.com.atlassian.applinks.refapp_refapp.v2;

import com.atlassian.applinks.pageobjects.OAuthApplinksClient;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.webdriver.applinks.component.v2.ConfigureUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.ConfirmUrlDialog;
import com.atlassian.webdriver.applinks.component.v2.CreateUalDialog;
import com.atlassian.webdriver.applinks.component.v2.InformationDialog;
import com.atlassian.webdriver.applinks.component.v2.PauseDialog;
import com.atlassian.webdriver.applinks.page.v2.ListApplicationLinkPage;
import it.com.atlassian.applinks.V2AbstractApplinksTest;
import it.com.atlassian.applinks.testing.categories.RefappTest;
import it.com.atlassian.applinks.testing.product.ProductInstances;
import it.com.atlassian.applinks.testing.rules.RefappBaseUrlRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import static com.atlassian.pageobjects.elements.query.Poller.waitUntil;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilFalse;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static it.com.atlassian.applinks.ApplicationTestHelper.getProductName;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasApplicationUrl;
import static it.com.atlassian.applinks.testing.matchers.ApplicationLinkRowMatchers.hasDisplayUrl;
import static it.com.atlassian.applinks.testing.matchers.MoreMatchers.contains;
import static it.com.atlassian.applinks.testing.product.ProductInstances.REFAPP2;
import static java.lang.String.format;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertThat;

/**
 * Smoke Tests for the v2 applink creation workflow.
 *
 * @since 4.0.0
 */
@Category({RefappTest.class})
public class CreateApplicationLinkTestRefApp extends V2AbstractApplinksTest

{
    @Rule
    public final RefappBaseUrlRule baseUrlResetRule = new RefappBaseUrlRule(ProductInstances.REFAPP2.getLoopbackUrl());

    @Ignore("flaky")
    @Test
    public void verifyCreate2EndedUalApplicationLinkWorks() throws Exception {
        String expectedFinalStatusMessage = "Application Link '" + getProductName(REFAPP2) +
                "' created successfully";
        boolean expectedFinalThreeLOConfiguration = true;
        boolean expectedFinalTwoLOConfiguration = true;
        boolean expectedFinalTwoLOiConfiguration = false;

        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance().getBaseUrl())
                .clickContinueToCreateUalLink();

        // make sure we are back in product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT2.getProductInstance()
                .getBaseUrl());
        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyAtlassianLinkConfigurationTwoEnded(PRODUCT, PRODUCT2,
                expectedFinalThreeLOConfiguration,
                expectedFinalTwoLOConfiguration,
                expectedFinalTwoLOiConfiguration);
        OAuthApplinksClient.verifyAtlassianLinkConfigurationTwoEnded(PRODUCT2, PRODUCT,
                expectedFinalThreeLOConfiguration,
                expectedFinalTwoLOConfiguration,
                expectedFinalTwoLOiConfiguration);

    }

    @Test
    public void verifyCreate2EndedUalApplicationLinkWithDifferentRpcUrl() {
        baseUrlResetRule.setBaseUrl(ProductInstances.REFAPP2.getBaseUrl());
        String expectedFinalStatusMessage = "Application Link '" + getProductName(REFAPP2) + "' created successfully";
        boolean expectedFinalThreeLOConfiguration = true;
        boolean expectedFinalTwoLOConfiguration = true;
        boolean expectedFinalTwoLOiConfiguration = false;

        // login as as sysadmin on both products, therefore full rights.
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        // enter the target url
        ConfirmUrlDialog confirmUrlDialog =
                listApplicationLinkPage.setApplicationUrl(ProductInstances.REFAPP2.getLoopbackUrl())
                        // click "create link"
                        .clickCreateNewLinkAndOpenConfirmUrlDialog();
        // base and entered should be confirmed
        OAuthApplinksClient.verifyConfirmUrlDialogLayout(confirmUrlDialog, ProductInstances.REFAPP2.getLoopbackUrl(), ProductInstances.REFAPP2.getBaseUrl());

        // confirm differing urls and continue
        confirmUrlDialog.clickContinueAndOpenCreateUalDialog()

                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationCreateUalDialog(ProductInstances.REFAPP2.getBaseUrl())
                .clickContinueToCreateUalLink();

        // make sure we are back in product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(ProductInstances.REFAPP2.getBaseUrl());

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(ProductInstances.REFAPP2.getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(ProductInstances.REFAPP2.getLoopbackUrl())));

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyAtlassianLinkConfigurationTwoEnded(PRODUCT, PRODUCT2, expectedFinalThreeLOConfiguration, expectedFinalTwoLOConfiguration, expectedFinalTwoLOiConfiguration);
        OAuthApplinksClient.verifyAtlassianLinkConfigurationTwoEnded(PRODUCT2, PRODUCT, expectedFinalThreeLOConfiguration, expectedFinalTwoLOConfiguration, expectedFinalTwoLOiConfiguration);
    }

    @Test
    public void create2EndedUalApplicationLinkWithSharedUserbase() throws Exception {
        String expectedFinalStatusMessage = "Application Link '" + getProductName(REFAPP2) + "' created successfully";
        boolean expectedFinalThreeLOConfiguration = true;
        boolean expectedFinalTwoLOConfiguration = true;
        boolean expectedFinalTwoLOiConfiguration = true;

        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .checkSharedUserbase()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance().getBaseUrl())
                .clickContinueToCreateUalLink();

        // make sure we are back in product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())));

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyAtlassianLinkConfigurationTwoEnded(PRODUCT, PRODUCT2, expectedFinalThreeLOConfiguration, expectedFinalTwoLOConfiguration, expectedFinalTwoLOiConfiguration);
        OAuthApplinksClient.verifyAtlassianLinkConfigurationTwoEnded(PRODUCT2, PRODUCT, expectedFinalThreeLOConfiguration, expectedFinalTwoLOConfiguration, expectedFinalTwoLOiConfiguration);
    }

    @Test
    @Ignore("flaky test")
    public void create1EndedUalApplicationLink() throws Exception {
        String expectedFinalStatusMessage = "Application Link '" + getProductName(REFAPP2)
                + "' was created successfully locally but the reciprocal link was not created. " +
                "Please review the configuration using the Edit option.";
        boolean expectedFinalThreeLOConfiguration = false;
        boolean expectedFinalTwoLOConfiguration = false;
        boolean expectedFinalTwoLOiConfiguration = false;

        loginAsSysadmin(PRODUCT, PRODUCT2);

        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);
        // enter the target url
        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .unCheckAdmin()
                .clickContinueToCreateUalLink()
                .clickClose();

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())));

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        OAuthApplinksClient.verifyAtlassianLinkConfigurationOneEnded(PRODUCT, PRODUCT2,
                expectedFinalThreeLOConfiguration, expectedFinalTwoLOConfiguration, expectedFinalTwoLOiConfiguration);
        // TODO confirm the link does not exist on PRODUCT2
    }

    @Test
    public void verifyCreatingLinkToSelfFailsGracefully() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        InformationDialog informationDialog = PRODUCT.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenErrorDialog();

        assertThat(informationDialog.singleDialogIsPresent(), is(true));
        assertThat(informationDialog.getWarning(), containsString("This application has the same ID as myself. Can't create link to myself."));
    }

    @Test
    public void verifyCreatingADuplicateLinkFailsGracefully() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // create the first 1 ended link
        PRODUCT.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .unCheckAdmin()
                .clickContinueToCreateUalLink();

        // create the second duplicate link
        InformationDialog page = PRODUCT.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenErrorDialog();

        assertThat(page.getWarning(), containsString(format("An Application Link to this URL '%s' has already been configured.", PRODUCT2.getProductInstance().getBaseUrl())));
    }

    @Test
    public void verifyCreatingAReciprocalDuplicateLinkFailsGracefully() throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // create the first 1 ended link at the remote end
        PRODUCT2.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .unCheckAdmin()
                .clickContinueToCreateUalLink();


        // create the second duplicate link
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplication(PRODUCT2.getProductInstance()
                        .getBaseUrl());

        InformationDialog informationDialog = listApplicationLinkPage.getInformationDialog();

        assertThat(informationDialog.singleDialogIsPresent(), is(true));
        assertThat(informationDialog.getWarning(), containsString(format("An Application Link to this URL '%s' has already been configured.", PRODUCT.getProductInstance().getBaseUrl())));

        informationDialog.clickContinue();

        // make sure we are back in product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())));

        assertThat(listApplicationLinkPage.getFirstMessage(),
                containsString(format("Application Link '%s' was created successfully locally but the reciprocal " +
                                "link was not created. Please review the configuration using the Edit option.",
                        getProductName(REFAPP2))));

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutFormIsReset(listApplicationLinkPage);
    }

    @Test
    public void verifyCreatingAReciprocalDuplicateLinkFailsGracefullyWhenCancellingOnTheRemoteApplication()
            throws Exception {
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // create the first 1 ended link at the remote end
        PRODUCT2.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .unCheckAdmin()
                .clickContinueToCreateUalLink();


        // create the second duplicate link
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())

                .clickCreateNewLinkAndOpenDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplication(PRODUCT2.getProductInstance().getBaseUrl());

        InformationDialog informationDialog = listApplicationLinkPage.getInformationDialog();

        assertThat(informationDialog.singleDialogIsPresent(), is(true));
        assertThat(informationDialog.getWarning(), containsString(format("An Application Link to this URL '%s' has already been configured.", PRODUCT.getProductInstance().getBaseUrl())));

        informationDialog.clickCancel();

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT.getProductInstance().getBaseUrl())));

        // there should be no message
        Poller.waitUntilFalse(listApplicationLinkPage.getMessageBarFirstMessageTimed()
                .isVisible());

        OAuthApplinksClient.verifyListApplicationLinkPageLayoutFormIsReset(listApplicationLinkPage);
    }

    @Test
    public void verifyOnlySysAdminCanSetSharedUserbaseOption() {
        login(PRODUCT, PRODUCT2);

        // open the dialog
        CreateUalDialog dialog = PRODUCT.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog();

        assertThat(dialog.sharedUserBaseIsPresent(), is(false));

        dialog.clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance().getBaseUrl())
                .clickContinueToCreateUalLink();

        ListApplicationLinkPage listApplicationLink = PRODUCT.visit(ListApplicationLinkPage.class);

        waitUntil(listApplicationLink.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLink.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())));
    }

    @Test
    public void verifyCreating2WayLinkWithNonAdminRemoteUserFailsGracefully() throws Exception {
        String expectedFinalStatusMessage = "Application Link '" + getProductName(REFAPP2) +
                "' was created successfully locally but the reciprocal link was not created. " +
                "Please review the configuration using the Edit option.";
        boolean expectedFinalThreeLOConfiguration = true;
        boolean expectedFinalTwoLOConfiguration = true;
        boolean expectedFinalTwoLOiConfiguration = false;

        // login as sysadmin on product1, therefore full rights.
        loginAsSysadmin(PRODUCT);
        // login as user on product2, therefore no access to the page.
        loginAsUser(PRODUCT2);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog()
                .clickContinueToCreateUalLink()
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance().getBaseUrl())
                .clickContinueToCreateUalLink();

        // make sure we are back in product 1
        PRODUCT.getPageBinder().bind(PauseDialog.class).handleRedirectionToRemoteApplication(PRODUCT.getProductInstance().getBaseUrl());

        waitUntil(listApplicationLinkPage.getApplicationLinksTimed(), contains(hasDisplayUrl(PRODUCT2.getProductInstance().getBaseUrl())));
        assertThat(listApplicationLinkPage.getApplicationLinks(), contains(hasApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())));

        assertThat(listApplicationLinkPage.getFirstMessage(), containsString(expectedFinalStatusMessage));

        // login to Product2 as sysadmin to avoid the websudo requests in the configuration screens.
        logout(PRODUCT2);
        loginAsSysadmin(PRODUCT2);
        OAuthApplinksClient.verifyAtlassianLinkConfigurationOneEnded(PRODUCT, PRODUCT2, expectedFinalThreeLOConfiguration, expectedFinalTwoLOConfiguration, expectedFinalTwoLOiConfiguration);

        // TODO confirm the link does not exist on PRODUCT2
    }

    @Test
    public void verifyCreatingLinkWithEmptyUrlIsNotPossible() throws Exception {
        String targetUrl = "";

        // login as admin on both product1, therefore full rights.
        login(PRODUCT);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class)
                .setApplicationUrl(targetUrl);

        waitUntil(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed(), is(false));
    }

    @Test
    public void verifyCreateNewLinkIsDisabledIfApplicationUrlIsBlank() throws Exception {
        // login as as sysadmin on both products, therefore full rights.
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        // create button is disabled by default
        waitUntilFalse(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());

        listApplicationLinkPage.setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl());

        // create button is enabled when a url is entered
        waitUntilTrue(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());
    }

    @Test
    public void verifyCancellingLocalCreateUalDialogResetsCreateNewLinkButton() throws Exception {
        // login as as sysadmin on both products, therefore full rights.
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        // create button is disabled by default
        waitUntilFalse(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());

        ListApplicationLinkPage listApplicationLinkPage2 = listApplicationLinkPage
                .setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                .clickCreateNewLinkAndOpenDialog().clickCancel();

        // create button is enabled after cancel.
        waitUntilTrue(listApplicationLinkPage2.createNewLinkButtonIsEnabledTimed());

        // verify form is active, button is disabled if textbox is cleared
        listApplicationLinkPage2.clearApplicationUrl();
        waitUntilFalse(listApplicationLinkPage2.createNewLinkButtonIsEnabledTimed());
    }

    @Test
    public void verifyCancellingConfirmUrlsDialogResetsCreateNewLinkButton() throws Exception {
        // login as as sysadmin on both products, therefore full rights.
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        // create button is disabled by default
        waitUntilFalse(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());

        ListApplicationLinkPage listApplicationLinkPage2 = listApplicationLinkPage
                .setApplicationUrl(ProductInstances.REFAPP2.getLoopbackUrl())
                .clickCreateNewLinkAndOpenConfirmUrlDialog()
                .clickCancel();

        // create button is enabled after cancel.
        waitUntilTrue(listApplicationLinkPage2.createNewLinkButtonIsEnabledTimed());

        // verify form is active, button is disabled if textbox is cleared
        listApplicationLinkPage2.clearApplicationUrl();
        waitUntilFalse(listApplicationLinkPage2.createNewLinkButtonIsEnabledTimed());
    }

    @Test
    public void verifyCancellingRemoteUalCreationLeavesUserinRemoteAppWithFocusOnCreateNewLink() throws Exception {
        // login as as sysadmin on both products, therefore full rights.
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to the applinks admin screen in the source application
        CreateUalDialog createUalDialog = PRODUCT.visit(ListApplicationLinkPage.class)
                // enter the target url
                .setApplicationUrl(PRODUCT2.getProductInstance().getBaseUrl())
                // click "create link"
                .clickCreateNewLinkAndOpenDialog()
                // view and confirm the link details
                .clickContinueToCreateUalLink()
                // redirected to the target application
                .handleRedirectionToRemoteApplicationCreateUalDialog(PRODUCT2.getProductInstance()
                        .getBaseUrl());
        // view and confirm the link details

        ListApplicationLinkPage listApplicationLinkPage = createUalDialog.clickCancel();

        // create button is disabled after cancel, because the url is blank.
        waitUntilFalse(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());

        // verify form is active, button is disabled if textbox is cleared
        listApplicationLinkPage.setApplicationUrl("abc");
        waitUntilTrue(listApplicationLinkPage.createNewLinkButtonIsEnabledTimed());
    }

    @Test
    public void verifyCreateNewLinkFormBehaviour() throws Exception {
        // login as admin on both product1, therefore full rights.
        login(PRODUCT);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        OAuthApplinksClient.verifyCreateNewLinkFormBehavior(listApplicationLinkPage);
    }

    @Test
    public void verifyConfirmingDifferentUrlIsValidated() throws Exception {
        baseUrlResetRule.setBaseUrl(ProductInstances.REFAPP2.getBaseUrl());
        String badUrl = "http://badurl.invalid";

        // login as as sysadmin on both products, therefore full rights.
        loginAsSysadmin(PRODUCT, PRODUCT2);

        // go to the applinks admin screen in the source application
        ListApplicationLinkPage listApplicationLinkPage = PRODUCT.visit(ListApplicationLinkPage.class);

        // enter the target url and click create
        ConfirmUrlDialog confirmUrlDialog = listApplicationLinkPage

                .setApplicationUrl(ProductInstances.REFAPP2.getLoopbackUrl())
                .clickCreateNewLinkAndOpenConfirmUrlDialog();

        // base and entered should be confirmed
        OAuthApplinksClient.verifyConfirmUrlDialogLayout(confirmUrlDialog, ProductInstances.REFAPP2.getLoopbackUrl(), ProductInstances.REFAPP2.getBaseUrl());
        // enter a rubbish url and continue
        confirmUrlDialog
                .setApplicationUrlRpc(badUrl)
                .clickContinue();

        // should be directed to the correction dialogue with the bad url
        ConfigureUrlDialog configureUrlDialog = listApplicationLinkPage.getConfigureUrlDialog();
        OAuthApplinksClient.verifyConfigureUrlDialogLayout(configureUrlDialog, badUrl, badUrl);
        assertThat(configureUrlDialog.getWarning(), startsWith("No response"));
    }
}
